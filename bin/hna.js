#!/usr/bin/env node

const loadPath = require('../app/lib/load-path');

/**
 * Module dependencies.
 */

require('dotenv').config();
var program = require('commander');
const Confirm = require('prompt-confirm');

program
.version('1.0.0', '-v, --version')

if (process.env.NODE_ENV === 'production') {
    console.log(`
**************************************
*     Application In Production!     *
**************************************
`);
}

loadPath(__dirname + '/../app/console').forEach(item => {
    /**
     * @type CommandCli
     */
    let CommandCli = new (require(item));

    let Command = program.command(CommandCli.signature());
    Command.description(CommandCli.description())

    CommandCli.options().forEach(option => {
        Command.option(...option);
    });

    Command.action(function(...args) {
        CommandCli.handle.call(CommandCli, ...args);
    });
});
program.on('command:*', function () {
    console.error('Invalid command: %s\nSee --help for a list of available commands.', program.args.join(' '));
    process.exit(1);
});
program.parse(process.argv);

