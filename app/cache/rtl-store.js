import Arr from '../lib/arr';
import lodash from 'lodash';

const md5 = require('crypto-js/md5');
const sha1 = require('crypto-js/sha1');
const serialize = require('../lib/serialize');
const LRU = require('lru-cache');
const cache = new LRU(100000);

const HIERARCHY_SEPARATOR = '|';
const TAG_SEPARATOR = '!';
var keyCache = {};

async function getHierarchyKey(key, object) {
    if (!isHierarchyKey(key)) {
        return key;
    }

    key = explodeKey(key);

    let keyString = '',
        index = '';

    // The comments below is for a $key = ["foo!tagHash", "bar!tagHash"]
    for (let name of key) {
        // 1) $keyString = "foo!tagHash"
        // 2) $keyString = "foo!tagHash![foo_index]!bar!tagHash"
        keyString += name;
        object.pathKey = sha1('path' + TAG_SEPARATOR + keyString).toString();

        if (keyCache[object.pathKey]) {
            index = keyCache[object.pathKey];
        } else {
            index = await redis.get(object.pathKey) || '';
            keyCache[object.pathKey] = index;
        }

        // 1) $keyString = "foo!tagHash![foo_index]!"
        // 2) $keyString = "foo!tagHash![foo_index]!bar!tagHash![bar_index]!"

        keyString += TAG_SEPARATOR + index + TAG_SEPARATOR;
    }

    // Assert: pathKey = "path!foo!tagHash![foo_index]!bar!tagHash"
    // Assert: keyString = "foo!tagHash![foo_index]!bar!tagHash![bar_index]!"

    // Make sure we do not get awfully long (>250 chars) keys
    return sha1(keyString).toString();
}

function isHierarchyKey(key) {
    return typeof key === 'string' && key[0] === HIERARCHY_SEPARATOR;
}

function explodeKey(string) {
    let [key, tag] = `${string}${TAG_SEPARATOR}`.split(TAG_SEPARATOR);

    let parts = ['root'];

    if (key === HIERARCHY_SEPARATOR) {
        parts = ['root'];
    } else {
        parts = key.split(HIERARCHY_SEPARATOR);
        parts[0] = 'root';
    }

    return parts.map(level => {
        return level + TAG_SEPARATOR + tag;
    });
}

const manage = {
    getTagKey: (tag) => {
        // return `tag${TAG_SEPARATOR}${HIERARCHY_SEPARATOR}${process.env.REDIS_PREFIX}_${HIERARCHY_SEPARATOR}${tag}`;
        return 'tag' + TAG_SEPARATOR + tag;
    },
    get: (key) => {
        return cache.get(key);
    },
    set: (a, b, c, d) => {
        return cache.set(a, b, c, d);
    },
    getList: (name) => {
        return redis.lrange(name, 0, -1);
    },
    clearOneObjectFromCache: async (key) => {
        let obj = {pathKey: ''};

        let keyString = await getHierarchyKey(key, obj);

        await manage.preRemoveItem(keyString);

        const {pathKey} = obj;

        keyCache = [];

        const deleted = await redis.del(keyString);

        return !!redis.del(keyString);
    },
    preRemoveItem: async (key) => {
        const tags = await manage.get(key).then(data => {
            if (!data) {
                return [];
            }
            return data;
        });

        for (let tag of tags) {
            await manage.removeListItem(manage.getTagKey(tag), key);
        }

        return this;
    },
    removeListItem: (name, key) => {
        return redis.lrem(name, 0, key);
    },
    deleteItems: async (keys) => {
        let deleted = true;

        for(let key of keys) {
            if (!await manage.clearOneObjectFromCache(key)) {
                deleted = false;
            }
        }

        return deleted;
    },
    clearTags: async (tags) => {
        tags = Arr.array_wrap(tags).map(tag => {
            return `${HIERARCHY_SEPARATOR}${process.env.REDIS_PREFIX}_${HIERARCHY_SEPARATOR}${tag}`;
        });


        let itemIds = [];

        for(let tag of tags) {
            itemIds = [...itemIds, ...await manage.getList(manage.getTagKey(tag))];
        }

        // Remove all items with the tag
        let success = await manage.deleteItems(itemIds);

        if (success) {
            // Remove the tag list
            for(let tag of tags) {
                await redis.del(manage.getTagKey(tag));
            }
        }

        return success;
    },
    remember: async (tags, key, callback, ttl) => {
        return manage.remember2(tags, key, callback, ttl);
    },
    remember2: async (tags, key, callback, ttl) => {
        if (!ttl) {
            return await callback();
        }
        key = md5(JSON.stringify(key)).toString();
        try {
            const value = cache.get( key );
            if ( value !== undefined ){
                return value;
            }

            const docs = await promissCallback(key, callback);

            const success = cache.set(key, docs, ttl ? ttl : 1 * 1000);
            delete dataCallback[key];
            return docs;
        } catch (e) {
            console.error(e);
        }

        return await callback(key);
    },
};

const dataCallback = {};
const promissCallback = (key, callback) => {
    if (dataCallback[key]) {
        return dataCallback[key];
    }
    dataCallback[key] = callback();

    return dataCallback[key];
}

module.exports = manage;