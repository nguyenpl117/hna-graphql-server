const stores = {
    redis: () => {
        return require('./redis-store');
    },
    rtl: () => {
        return require('./rtl-store');
    },
    array: () => {
        return require('./array-store');
    },
}
export class Cache {
    static store(store = 'array') {
        if (!this.stores) {
            this.stores = {};
        }
        if (!this.stores || !this.stores[store]) {
            this.stores[store] = stores[store]();
        }
    
        return this.stores[store];
    }
    
    static driver(store = 'array') {
        return this.store(store);
    }
    
    static get(key) {
        return this.driver().get(key);
    }
    
    static set(key, data, ttl, d) {
        return this.driver().set(key, data, ttl, d);
    }
    
    static clearTags(tags) {
        return this.driver().clearTags(tags);
    }
    
    static remember(tags, key, callback, ttl) {
        return this.driver().remember(tags, key, callback, ttl);
    }
    
    static remember2(tags, key, callback, ttl) {
        return this.driver().remember2(tags, key, callback, ttl);
    }
}