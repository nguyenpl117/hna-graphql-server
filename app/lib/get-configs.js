import { srcDir } from './utils';

var fs = require('fs')
    , p = require('path')
;
const path = require('path');
const loadPath = require('./load-path');

var appDir = srcDir();

function getConfig() {
    let configs = {};

    loadPath(appDir + '/configs').forEach(item => {
        configs[item.replace(/\\/g,'/').split('/').reverse()[0].replace('\.js','')] = require(item);
    });

    return configs;
}

exports.getConfig = getConfig();
