const Confirm = require('prompt-confirm');

class CommandCli {
    constructor() {
        this.dataOptions = {};
    }

    signature() {
        return '';
    }

    description() {
        return '';
    }

    options() {
        return [];
    }

    setOptions(options) {
        this.dataOptions = options;
    }

    option(key) {
        return this.dataOptions[key];
    }

    handle() {

    }

    async confirmToProceed() {
        return new Promise(resolve => {
            new Confirm('Bạn có muốn muốn chạy câu lệnh này?')
            .ask(function(answer) {
                resolve(answer);
            })
        })
    }
}

module.exports = CommandCli;