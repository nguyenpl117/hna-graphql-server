export class Resource {
    static delete(data) {
        if (data) {
            return {
                'status': true,
                'message': 'Xóa thành công',
                'data': data
            };
        } else {
            return {
                'status': false,
                'message': 'Xóa không thành công'
            };
        }
    }
}