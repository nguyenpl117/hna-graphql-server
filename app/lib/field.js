class Field {
    constructor() {
        this.authentication = false;
        this.cache = true;
    }
    
    /**
     * Kiểm tra quyền hạn của người dùng
     *
     * @param args
     * @return {Promise<boolean>}
     */
    async authorize(args) {
        return true;
    }

    /**
     * Kết quả trả ra dạng
     *
     * @return {*}
     */
    type() {
        return Types.ListOf(Types.Post);
    }

    /**
     * Giá trị truyền lên
     *
     * @return {{}}
     */
    args() {
        return {
            filter: Types.String
        };
    }

    /**
     * Validate dữ liệu trước khi xử lý
     *
     * @param args
     */
    rules(args) {

    }

    /**
     * Custom lại tin nhắn validate trước khi response
     *
     * @param args
     * @return {{}}
     */
    validationErrorMessages(args) {
        return {};
    }

    /**
     * Thao tác xử lý dữ liệu
     *
     * @param parent
     * @param args
     * @param context
     * @param info
     */
    resolve(parent, args, context, info) {

    }

    /**
     * Filter dữ liệu ( socket )
     *
     * @param payload
     * @param args
     */
    filter(payload, args) {

    }
}

module.exports = {Field};