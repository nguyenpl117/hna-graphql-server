'use strict';

import Auth from './auth';
import NotificationRepository
    from '../packages/user/repository/elo/notification-repository';

/**
 * Created by Phan Trung Nguyên.
 * User: nguyenpl117
 * Date: 3/30/2019
 * Time: 6:38 AM
 */
export const CREATE_FOLLOW = 'new_follow';
export const CREATE_POST = 'new_post';
export const DELETE_POST = 'delete_post';
export const CREATE_STATUS = 'new_status';
export const CREATE_LIKE = 'new_likes';
export const CREATE_DISLIKE = 'new_dislikes';
export const CREATE_CLIPS = 'new_clips';
export const CREATE_COMMENT = 'new_comment';
export const CREATE_FRIEND = 'new_friend';
export const CREATE_KYC = 'new_kyc';
export const STATUS_TRADE_JOIN = 'status_trade_join';
export const STATUS_TRADE_VERIFIED = 'status_trade_verified';
export const STATUS_TRADE_UNVERIFIED = 'status_trade_unverified';
export const CONFIRM_FRIEND = 'confirm_friend';
export const CONFIRM_KYC = 'confirm_kyc';
export const POST_IS_APPROVAL = 'is_approval';
export const POST_NOT_APPROVAL = 'not_approval';
export const NOT_CONFIRM_KYC = 'not_confirm_kyc';
export const DONATE_POINT = 'donate_point';
export const CREATE_INVITE_MEMBER = 'new_invite_member';

export const TOTAL_ACCOUNT_POINT_PLUS = 'plus_total_account';
export const TOTAL_ACCOUNT_POINT_MINUS = 'minus_total_account';

export class NotificationHelper {
    static getUrl(root) {
        const urls = {
            [CREATE_POST]: '/notification/p/' + root.notificationableId,
            [DELETE_POST]: '/notification/p/' + root.notificationableId,
            [CREATE_FOLLOW]: '/notification/u/' + root.senderId,
            [CREATE_FRIEND]: '/notification/u/' + root.senderId,
            [CREATE_KYC]: '/notification/kyc/' + root.senderId,
            [CONFIRM_KYC]: '/notification/kyc/' + root.userId,
            [NOT_CONFIRM_KYC]: '/notification/kyc/' + root.userId,
            [STATUS_TRADE_JOIN]: '/notification/trade/' + root.senderId,
            [STATUS_TRADE_VERIFIED]: '/notification/trade_verified/' + root.senderId,
            [STATUS_TRADE_UNVERIFIED]: '/notification/trade_verified/' + root.senderId,
            [CONFIRM_FRIEND]: '/notification/u/' + root.senderId,
            [CREATE_STATUS]: '/notification/s/' + root.notificationableId,
            [POST_IS_APPROVAL]: '/notification/post_approval/' + root.userId,
            [POST_NOT_APPROVAL]: '/notification/post_approval/' + root.userId,
            [CREATE_INVITE_MEMBER]: '/notification/i/' + root.notificationableId,
            [TOTAL_ACCOUNT_POINT_PLUS]: '/notification/plus_total_account/' + root.userId,
            [TOTAL_ACCOUNT_POINT_MINUS]: '/notification/minus_total_account/' + root.userId,
        };
        return urls[root.notificationableType];
    }
    
    static deletePost(post, message) {
        if (!Auth.check()) {
            return;
        }
    
        const sender = Auth.user();
    
        return NotificationRepository.send({
            userId: post.authorId,
            senderId: sender.id,
            notificationableType: CREATE_POST,
            notificationableId: post.id,
            title: `Bài viết <b>${post.title}</b> đã bị xóa. Lý do "${message}"`
        });
    }

    static publishNewPost(post) {
        if (!Auth.check()) {
            return;
        }

        const sender = Auth.user();

        return NotificationRepository.sendFollowing({
            senderId: sender.id,
            notificationableType: CREATE_POST,
            notificationableId: post.id,
            title: `<b>${sender.fullname}</b> đã đăng bài viết mới <b>${post.title}</b>`
        });
    }

    static follow(follow) {
        if (!Auth.check()) {
            return;
        }

        const sender = Auth.user();

        return NotificationRepository.send({
            senderId: sender.id,
            userId: follow.following,
            notificationableType: CREATE_FOLLOW,
            notificationableId: follow.following,
            title: `<b>${sender.fullname}</b> đã theo dõi bạn`
        });
    }

    static addFriend($friend) {
        if (!Auth.check()) {
            return;
        }

        const $sender = Auth.user();

        return NotificationRepository.send({
            'senderId': $sender.id,
            'userId': $friend.friendId,
            'notificationableType': CREATE_FRIEND,
            'notificationableId': $friend.id,
            'title': `<b>${$sender.fullname}</b> đã gửi lời mời kết bạn tới bạn`
        });

        // DB.table(User.getTable()).
        //     where({ 'id': $friend.friendId }).
        //     increment('add_friend', 1);
    }

    static confirmFriend($friend) {
        if (!Auth.check()) {
            return;
        }

        const $sender = Auth.user();

        return NotificationRepository.send({
            'senderId': $sender.id,
            'userId': $friend.friendId,
            'notificationableType': CONFIRM_FRIEND,
            'notificationableId': $friend.id,
            'title': `<b>${$sender.fullname}</b> đã đồng ý kết bạn`
        });

        // DB::table(User::getTable()).where(['id': $friend.friendId]).increment('add_friend', 1);
    }

    static publishNewStatus($status) {
        if (!Auth.check()) {
            return;
        }
        const $sender = Auth.user();

        return NotificationRepository.sendFollowing({
            'senderId': $sender.id,
            'notificationableType': CREATE_STATUS,
            'notificationableId': $status.id,
            'title': `<b>${$sender.fullname}</b> đã cập nhật trạng thái mới.`
        });
    }

    static donatePoint($data) {
        if (!Auth.check()) {
            return;
        }

        const $sender = Auth.user();

        return NotificationRepository.send({
            'senderId': $sender.id,
            'userId': $data.reciveId,
            'notificationableType': CONFIRM_FRIEND,
            'notificationableId': $data.notificationableId,
            'title': $data['comment']
        });

        // DB.table(User::getTable())->where(['id' => $data['reciveId']])->increment('not_view_notifi', 1);
    }
    static createUserKYC(userKyc,listUserId) {
        if (!Auth.check()) {
            return;
        }
        const sender = Auth.user();

        return NotificationRepository.sendMultiUser({
            'senderId': sender.id,
            'notificationableType': CREATE_KYC,
            'notificationableId': userKyc.id,
            'title': `<b>${sender.fullname}</b> gửi thông tin KYC`
        },listUserId);

    }
    static userJoinTrade(listUserId) {
        if (!Auth.check()) {
            return;
        }
        const sender = Auth.user();

        return NotificationRepository.sendMultiUser({
            'senderId': sender.id,
            'notificationableType': STATUS_TRADE_JOIN,
            'notificationableId': sender.id,
            'title': `<b>${sender.fullname}</b> gửi yêu cầu tham gia tài khoản thương mại`
        },listUserId);

    }
    static confirmUserKYC(userKyc,status) {
        if (!Auth.check()) {
            return;
        }

        const sender = Auth.user();
        let title = "";
        let notiType = "";
        if (status ===true){
            title ="Hệ thống đã xác thực thông tin";
            notiType = CONFIRM_KYC;
        }
        else {
            title ="Thông tin KYC của bạn không được xác thưc";
            notiType = NOT_CONFIRM_KYC;
        }
        return NotificationRepository.send({
            'senderId': sender.id,
            'userId': userKyc.userId,
            'notificationableType': notiType,
            'notificationableId': userKyc.id,
            'title': title
        });

        // DB::table(User::getTable()).where(['id': $friend.friendId]).increment('add_friend', 1);
    }

    static verifiedUserStatusTrade(userId,status) {
        if (!Auth.check()) {
            return;
        }

        const sender = Auth.user();
        let title = "";
        let notiType = "";
        if (status ===true){
            title ="Tài khoản của bạn đã là tài khoản thương mại";
            notiType = STATUS_TRADE_VERIFIED;
        }
        else {
            title ="Bạn không đủ điều kiện tham gia tài khoản thương mại";
            notiType = STATUS_TRADE_UNVERIFIED;
        }
        return NotificationRepository.send({
            'senderId': sender.id,
            'userId': userId,
            'notificationableType': notiType,
            'notificationableId': userId,
            'title': title
        });

        // DB::table(User::getTable()).where(['id': $friend.friendId]).increment('add_friend', 1);
    }


    static notifiApprovalPost(post,data) {
        if (!Auth.check()) {
            return;
        }
        const sender = Auth.user();
        let title = "";
        let notiType = "";
        if (data.approval ===true){
            title =`Bài viết: <b>${post.title}</b> đã được phê duyệt`
            notiType = POST_IS_APPROVAL;
        }
        else {
            title =`Bài viết: <b>${post.title}</b> không được phê duyệt`
            notiType = POST_NOT_APPROVAL;
        }
        return NotificationRepository.send({
            'senderId': sender.id,
            'userId': post.authorId,
            'notificationableType': notiType,
            'notificationableId': post.id,
            'title': title
        });

        // DB::table(User::getTable()).where(['id': $friend.friendId]).increment('add_friend', 1);
    }

    static chargeTotalAccount(listUserId,money) {
        if (!Auth.check()) {
            return;
        }
        const sender = Auth.user();
        let title,notificationableType;

        title = `<b>${sender.userName}</b> thêm ${money} vào tài khoản tổng`;
        notificationableType =TOTAL_ACCOUNT_POINT_PLUS;

        return NotificationRepository.sendMultiUser({
            'senderId': sender.id,
            'notificationableType': notificationableType,
            'notificationableId': sender.id,
            'title': title
        },listUserId);

    }
}
