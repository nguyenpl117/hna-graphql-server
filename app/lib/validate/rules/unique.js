const {Op} = require('sequelize');
class Unique {
    constructor(model, column) {
        this.unique = {
            model,
            column,
            ignore: {
                id: null,
                idColumn: 'id'
            }
        };
    }
    
    /**
     *
     * @param id
     * @param idColumn
     * @returns {Unique}
     */
    ignore(id, idColumn = 'id') {
        this.unique.ignore = {id, idColumn};
        return this;
    }
    
    /**
     *
     * @param options
     * @returns {Unique}
     */
    where(...args) {
        if (typeof args[0] === 'object') {
            this.unique.where = {...this.unique.where, ...args[0]}
        } else if (typeof args[0] === 'string') {
            let operator = Op.eq;
            if (!args[1]) {
                operator = Op.is;
            }
            this.unique.where = {...this.unique.where, [args[0]]: {[operator]: args[1]}};
        }
        return this;
    }

    whereNot(column, value) {
        if (typeof value !== 'string') {
            return this;
        }
        this.unique.where = {...this.unique.where, [column]: {[Op.ne]: value}};
        return this;
    }

    /**
     *
     * @param column String
     * @param value Array
     * @returns {Unique}
     */
    whereIn(column, value) {
        if (!Array.isArray(value)) {
            return this;
        }
        this.unique.where = {...this.unique.where, [column]: {[Op.in]: value}};
        return this;
    }
}

export default Unique;