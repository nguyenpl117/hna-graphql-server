'use strict';
const {Op} = require('sequelize');
/**
 * Created by Phan Trung Nguyên.
 * User: nguyenpl117
 * Date: 3/29/2019
 * Time: 12:33 AM
 */
class Exists {
    constructor(model, column) {
        this.exists = {
            model,
            column
        };
    }
    
    /**
     *
     * @param args
     * @returns {Exists}
     */
    where(...args) {
        if (typeof args[0] === 'object') {
            this.exists.where = {...this.exists.where, ...args[0]}
        } else if (typeof args[0] === 'string') {
            let operator = Op.eq;
            if (!args[1]) {
                operator = Op.is;
            }
            this.exists.where = {...this.exists.where, [args[0]]: {[operator]: args[1]}};
        }
        return this;
    }
}

export default Exists;