import Rule from './rule';
import { srcDir } from '../utils';

const Validator = require('validatorjs');
const db = require("../../models");
var fs = require('fs');
const path = require('path');
const loadPath = require('../load-path');

var appDir = srcDir();

Validator.useLang('vi');

let validate = loadPath(appDir + '/rules').map(item => {
    return require(item)(Validator, db);
});

Validator.prototype._replaceWildCards = function (path, nums) {
    
    if (!nums) {
        return path;
    }
    
    
    var path2 = path;
    nums.forEach(function (value) {
        if (!Array.isArray(path2) && typeof path2 !== 'string') {
            return path2;
        }
        
        if(Array.isArray(path2)){
            path2 = path2[0];
        }
        
        let pos = path2.indexOf('*');
        if (pos === -1) {
            return path2;
        }
        path2 = path2.substr(0, pos) + value + path2.substr(pos + 1);
    });
    if(Array.isArray(path)){
        path[0] = path2;
        path2 = path;
    }
    return path2;
}

Validator.register('required', function(val, requirement, attribute) { // requirement parameter defaults to null
    var str;
    
    if (val === undefined || val === null) {
        return false;
    }
    
    if (typeof val === 'object' && Object.keys(val).length) {
        return true;
    }
    
    str = String(val).replace(/\s/g, "");
    
    return str.length > 0 ? true : false;
    
}, ":attribute bắt buộc nhập.");

/*
Validator.register('telephone', function(value, requirement, attribute) { // requirement parameter defaults to null
    // requirement = this.getParameters();
    // this.ruleValue()
    // console.log('requirement', requirement, attribute, this.ruleValue);
    return value.match(/^\d{3}-\d{3}-\d{4}$/);
}, 'The :attribute phone number is not in the format XXX-XXX-XXXX.');
*/

/**
 *
 */
Validator.registerImplicit('filled', function(val, requirement, attribute) { // requirement parameter defaults to null
    if (val === undefined) {
        return true;
    }
    
    let str;
    
    if (val === null) {
        return false;
    }
    
    str = String(val).replace(/\s/g, "");
    return str.length > 0 ? true : false;
    
}, 'Trường :attribute không được bỏ trống.');

Validator.registerImplicit('without_spaces', function(val, requirement, attribute) { // requirement parameter defaults to null
    return /^\S+$/g.test(String(val || ''));
}, 'Trường :attribute không hợp lệ.');

/**
 * unique:model,column,except,idColumn
 */
Validator.registerAsync('unique', function(value, req, attribute, passes) { // requirement parameter defaults to null
    let ruleValue = this.ruleValue;
    if (typeof this.ruleValue !== 'object') {
        req = this.getParameters();
        
        ruleValue = Rule.unique(req[0], req[1]).ignore(req[2], req[3]).unique;
    }
    
    const model = db[ruleValue.model];
    
    let idColumn = ruleValue.ignore.idColumn || 'id';
    let except = {};
    
    if (ruleValue.ignore.id) {
        except = {
            [idColumn]: {
                [db.sequelize.Op.ne]: ruleValue.ignore.id
            }
        };
    }

    model.count({
        where: {
            [ruleValue.column]: value,
            ...ruleValue.where,
            ...except
        }
    }).then(total => {
        passes(!total)
    });
}, 'Trường :attribute đã có trong cơ sở dữ liệu.');

/**
 * exists:model,column
 */
Validator.registerAsync('exists', function(value, req, attribute, passes) { // requirement parameter defaults to null
    let ruleValue = this.ruleValue;
    if (typeof this.ruleValue !== 'object') {
        req = this.getParameters();
        
        ruleValue = Rule.exists(req[0], req[1]).exists;
    }
    
    const model = db[ruleValue.model];
    
    model.count({
        where: {
            [ruleValue.column]: value,
            ...ruleValue.where
        }
    }).then(total => {
        passes(!!total)
    });
}, 'Giá trị đã chọn trong trường :attribute không hợp lệ.');

Validator.registerAsync('language_unique', function(value, req, attribute, passes) { // requirement parameter defaults to null
    passes()
}, 'The selected :attribute is invalid.');


module.exports = Validator