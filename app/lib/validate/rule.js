'use strict';

import Unique from './rules/unique';
import Exists from './rules/exists';

/**
 * Created by Phan Trung Nguyên.
 * User: nguyenpl117
 * Date: 3/29/2019
 * Time: 12:15 AM
 */
class Rule {
    
    /**
     *
     * @param model
     * @param column
     * @returns {Unique}
     */
    static unique(model, column) {
        return new Unique(model, column);
    }
    
    /**
     *
     * @param model
     * @param column
     * @returns {Exists}
     */
    static exists(model, column) {
        return new Exists(model, column);
    }

    /**
     *
     * @param args Array
     * @returns {string}
     */
    static notIn(args) {
        return `not_in:${args.join(',')}`;
    }
    
    /**
     *
     * @param args Array
     * @returns {string}
     */
    static in(args) {
        return `in:${args.join(',')}`;
    }
    
}

export default Rule;