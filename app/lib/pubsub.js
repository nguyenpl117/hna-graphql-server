import { RedisPubSub } from 'graphql-redis-subscriptions';
const Redis = require('ioredis');

const options = {
    port: process.env.REDIS_PORT,          // Redis port
    host: process.env.REDIS_HOST,   // Redis host
    family: 4,           // 4 (IPv4) or 6 (IPv6)
    password: process.env.REDIS_PASSWORD,
    db: process.env.REDIS_DB
};

export const pubsub = new RedisPubSub({
    publisher: new Redis(options),
    subscriber: new Redis(options)
});