require('dotenv').config();
import _ from 'lodash';
import * as serialize from './serialize';
import ApiValidationError from '../exceptions/api-validation-error';
const request = require('request');
const marked = require('marked');
const LCrypt = require('lcrypt');
const lcrypt = new LCrypt(process.env.APP_KEY);
const uri = process.env.NODE_ENV === 'production'
    ? process.env.LARAVEL_API_SERVER
    : process.env.LARAVEL_API

var Hashids = require('hashids');

export function srcDir() {
    return process.cwd() + '/' + (process.env.SRC_DIR || (process.env.NODE_ENV === 'production' ? 'dist' : 'app'));
}

export function decrypt(payload, $serialize = true) {
    if ($serialize) {
        return serialize.unserialize(lcrypt.decode(payload));
    }
    return lcrypt.decode(payload);
}

export function encrypt(value, $serialize = true) {
    if ($serialize) {
        return lcrypt.encode(serialize.serialize(value));
    }
    return lcrypt.encode(value);
}

function _phpCastString (value) {
    var type = typeof value

    switch (type) {
        case 'boolean':
            return value ? '1' : ''
        case 'string':
            return value
        case 'number':
            if (isNaN(value)) {
                return 'NAN'
            }

            if (!isFinite(value)) {
                return (value < 0 ? '-' : '') + 'INF'
            }

            return value + ''
        case 'undefined':
            return ''
        case 'object':
            if (Array.isArray(value)) {
                return 'Array'
            }

            if (value !== null) {
                return 'Object'
            }

            return ''
        case 'function':
        // fall through
        default:
            throw new Error('Unsupported value type')
    }
}

function ini_get (varname) { // eslint-disable-line camelcase
    var $global = (typeof window !== 'undefined' ? window : global)
    $global.$locutus = $global.$locutus || {}
    var $locutus = $global.$locutus
    $locutus.php = $locutus.php || {}
    $locutus.php.ini = $locutus.php.ini || {}

    if ($locutus.php.ini[varname] && $locutus.php.ini[varname].local_value !== undefined) {
        if ($locutus.php.ini[varname].local_value === null) {
            return ''
        }
        return $locutus.php.ini[varname].local_value
    }

    return ''
}

export function strip_tags (input, allowed) {
    // making sure the allowed arg is a string containing only tags in lowercase (<a><b><c>)
    allowed = (((allowed || '') + '').toLowerCase().match(/<[a-z][a-z0-9]*>/g) || []).join('')

    var tags = /<\/?([a-z0-9]*)\b[^>]*>?/gi
    var commentsAndPhpTags = /<!--[\s\S]*?-->|<\?(?:php)?[\s\S]*?\?>/gi

    var after = _phpCastString(input)
    // removes tha '<' char at the end of the string to replicate PHP's behaviour
    after = (after.substring(after.length - 1) === '<') ? after.substring(0, after.length - 1) : after

    // recursively remove tags to ensure that the returned string doesn't contain forbidden tags after previous passes (e.g. '<<bait/>switch/>')
    while (true) {
        var before = after
        after = before.replace(commentsAndPhpTags, '').replace(tags, function ($0, $1) {
            return allowed.indexOf('<' + $1.toLowerCase() + '>') > -1 ? $0 : ''
        })

        // return once no more tags are removed
        if (before === after) {
            return after
        }
    }
}

export function substr(str, start, len) {
    str += ''
    var end = str.length

    var iniVal = (typeof require !== 'undefined' ? ini_get('unicode.emantics') : undefined) || 'off'

    if (iniVal === 'off') {
        // assumes there are no non-BMP characters;
        // if there may be such characters, then it is best to turn it on (critical in true XHTML/XML)
        if (start < 0) {
            start += end
        }
        if (typeof len !== 'undefined') {
            if (len < 0) {
                end = len + end
            } else {
                end = len + start
            }
        }

        // PHP returns false if start does not fall within the string.
        // PHP returns false if the calculated end comes before the calculated start.
        // PHP returns an empty string if start and end are the same.
        // Otherwise, PHP returns the portion of the string from start to end.
        if (start >= str.length || start < 0 || start > end) {
            return false
        }

        return str.slice(start, end)
    }

    // Full-blown Unicode including non-Basic-Multilingual-Plane characters
    var i = 0
    var allBMP = true
    var es = 0
    var el = 0
    var se = 0
    var ret = ''

    for (i = 0; i < str.length; i++) {
        if (/[\uD800-\uDBFF]/.test(str.charAt(i)) && /[\uDC00-\uDFFF]/.test(str.charAt(i + 1))) {
            allBMP = false
            break
        }
    }

    if (!allBMP) {
        if (start < 0) {
            for (i = end - 1, es = (start += end); i >= es; i--) {
                if (/[\uDC00-\uDFFF]/.test(str.charAt(i)) && /[\uD800-\uDBFF]/.test(str.charAt(i - 1))) {
                    start--
                    es--
                }
            }
        } else {
            var surrogatePairs = /[\uD800-\uDBFF][\uDC00-\uDFFF]/g
            while ((surrogatePairs.exec(str)) !== null) {
                var li = surrogatePairs.lastIndex
                if (li - 2 < start) {
                    start++
                } else {
                    break
                }
            }
        }

        if (start >= end || start < 0) {
            return false
        }
        if (len < 0) {
            for (i = end - 1, el = (end += len); i >= el; i--) {
                if (/[\uDC00-\uDFFF]/.test(str.charAt(i)) && /[\uD800-\uDBFF]/.test(str.charAt(i - 1))) {
                    end--
                    el--
                }
            }
            if (start > end) {
                return false
            }
            return str.slice(start, end)
        } else {
            se = start + len
            for (i = start; i < se; i++) {
                ret += str.charAt(i)
                if (/[\uD800-\uDBFF]/.test(str.charAt(i)) && /[\uDC00-\uDFFF]/.test(str.charAt(i + 1))) {
                    // Go one further, since one of the "characters" is part of a surrogate pair
                    se++
                }
            }
            return ret
        }
    }
}

export function nl2br (str, isXhtml) {

    // Some latest browsers when str is null return and unexpected null value
    if (typeof str === 'undefined' || str === null) {
        return ''
    }

    // Adjust comment to avoid issue on locutus.io display
    var breakTag = (isXhtml || typeof isXhtml === 'undefined') ? '<br ' + '/>' : '<br>'

    return (str + '')
    .replace(/(\r\n|\n\r|\r|\n)/g, breakTag + '$1')
}

export function htmlField(html, args) {
    html = html || '';

    if (!html) {
        return '';
    }

    const text = strip_tags(marked(html, {
        sanitize: false,
        breaks: false,
        langPrefix: 'hljs '
    })).trim();

    let safeText = text;

    if (args['maxLength'] && args['maxLength'] > 0) {
        safeText = substr(text, 0, args['maxLength']);
    }

    switch (args['format']) {
        case 'HTML':
            if (safeText !== text) {
                // Text was truncated, so just show what's safe:
                return nl2br(safeText);
            } else {
                return html;
            }
        case 'TEXT':
        default:
            return safeText;
    }
}

function customizer(objValue, srcValue) {
    if (_.isArray(objValue)) {
        return _.uniq(objValue.concat(srcValue));
    }
    if (_.isObject(objValue)) {
        return { ...objValue, ...srcValue };
    }
}

export function merge(...args) {
    return _.mergeWith(...args, customizer);
}

export function menu_recursive(data, parentKey = 'parentId', idKey = 'id', childrenKey = 'children') {
    let n = {};
    for (let item of data) {
        if (!n[`${item[parentKey] || 0}-key`]) n[`${item[parentKey] ||
        0}-key`] = [];
        n[`${item[parentKey] || 0}-key`].push(item);
    }
    for (let item of data) {
        if (n[item[idKey] + '-key']) {
            item[childrenKey] = n[item[idKey] + '-key'];
        }
    }
    return n['0-key'];
}

export function withdata(value, callback) {
    return !callback ? value : callback(value);
}

export function generateRandomString(length){
    let characters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    let charsLength = characters.length - 1;
    let string = "";
    for(let i=0; i<length; i++){
        let randNum = Math.floor(Math.random() * charsLength);
        string += characters[randNum];
    }
    return string;
}

export async function requestApi(req, info, key) {
    const query = info.operation.loc.source.body.replace(
        /((\s[a-z]+)([\s]+)?\:([\s]+)?([a-z]+))/ig, '$5');
    const body = {
        operationName: info.operation.name.value,
        query: query,
        variables: info.variableValues
    };

    const data = await new Promise(((resolve, reject) => {
        request(uri,{
            method: 'POST',
            headers: {
                authorization: req.headers.authorization
            },
            json: body,
        }, (error, response, body) => {
            if (!error) {
                if (body.errors) {
                    reject(body);
                } else if (body && body.data && body.data[key]) {
                    resolve(body.data[key]);
                } else {
                    resolve(body);
                }
            } else {
                reject(error);
            }
        });
    })).catch(err => {
        if (err.errors) {
            throw (new ApiValidationError(err.errors[0].message)).setValidator(err.errors[0].validation);
        }
        return err;
    })

    return data;
}

export function round(value, precision) {
    return Number.parseFloat(Number.parseFloat(value).toFixed(precision));
}
export function hash_affiliate_id_encode(id) {

    let hashids = new Hashids('', 6, 'QWERTYUIOPSDFGHJKLZXCVBNM'.toLowerCase());
    return hashids.encodeHex(id)
}
export function hash_affiliate_id_decode(code) {
    let hashids = new Hashids('', 6, 'QWERTYUIOPSDFGHJKLZXCVBNM'.toLowerCase());
    return hashids.decodeHex(code)
}

/**
 * Convert các object, func trả về là Promise
 * @param value
 * @return {Promise<*>}
 */
export async function withAsync(value) {
    if (value instanceof Promise) {
        return await value;
    }
    return value;
}

/**
 * Tính thời gian làm việc
 *
 * Thời gian làm việc được tính bằng s. 60s = 1p
 * @param time {Number}
 *
 * default: h
 * @param hourChar {String}
 *
 * default: p
 * @param minuteChar {String}
 *
 * @return {string}
 */
export function tinhgio(time, hourChar = 'h', minuteChar = 'p') {
    // Giờ âm dương
    let s = time < 0 ? '-' : '',
        t = Math.abs(time),
        // Số ngày.
        d = 0,
        // Số giờ.
        h = 0,
        // Số phút.
        m = Math.floor(time / 60);

    // Nếu quá 60 phút.
    if (m > 60) {
        // Số giờ = số phút / 60 (Làm tròn bỏ đi số dư).
        h = Math.floor(m / 60);
        // Số phút = số dư giờ * 60 (Làm tròn số).
        m = Math.round((m / 60 - h) * 60);
    }

    if (h) {
        // Format nếu chỉ có số h > 0.
        return `${s}${h}${hourChar}${m}${minuteChar}`;
    }
    // Nếu chỉ có phút.
    return `${s}${m}${minuteChar}`;
}