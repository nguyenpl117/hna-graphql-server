const obj = {
    serialize: (mixedValue) => {
        let val, key, okey;
        let ktype = '';
        let vals = '';
        let count = 0;

        let _utf8Size = function(str) {
            return ~-encodeURI(str).split(/%..|./).length;
        };

        let _getType = function(inp) {
            let match;
            let key;
            let cons;
            let types;
            let type = typeof inp;

            if (type === 'object' && !inp) {
                return 'null';
            }

            if (type === 'object') {
                if (!inp.constructor) {
                    return 'object';
                }
                cons = inp.constructor.toString();
                match = cons.match(/(\w+)\(/);
                if (match) {
                    cons = match[1].toLowerCase();
                }
                types = ['boolean', 'number', 'string', 'array'];
                for (key in types) {
                    if (cons === types[key]) {
                        type = types[key];
                        break;
                    }
                }
            }
            return type;
        };

        let type = _getType(mixedValue);

        switch (type) {
            case 'function':
                val = '';
                break;
            case 'boolean':
                val = 'b:' + (mixedValue ? '1' : '0');
                break;
            case 'number':
                val = (Math.round(mixedValue) === mixedValue ? 'i' : 'd') +
                    ':' + mixedValue;
                break;
            case 'string':
                val = 's:' + _utf8Size(mixedValue) + ':"' + mixedValue + '"';
                break;
            case 'array':
            case 'object':
                val = 'a';
                /*
                if (type === 'object') {
                  let objname = mixedValue.constructor.toString().match(/(\w+)\(\)/);
                  if (objname === undefined) {
                    return;
                  }
                  objname[1] = serialize(objname[1]);
                  val = 'O' + objname[1].substring(1, objname[1].length - 1);
                }
                */

                for (key in mixedValue) {
                    if (mixedValue.hasOwnProperty(key)) {
                        ktype = _getType(mixedValue[key]);
                        if (ktype === 'function') {
                            continue;
                        }

                        okey = (key.match(/^[0-9]+$/) ? parseInt(key, 10) : key);
                        vals += obj.serialize(okey) + obj.serialize(mixedValue[key]);
                        count++;
                    }
                }
                val += ':' + count + ':{' + vals + '}';
                break;
            case 'undefined':
            default:
                // Fall-through
                // if the JS object has a property which contains a null value,
                // the string cannot be unserialized by PHP
                val = 'N';
                break;
        }
        if (type !== 'object' && type !== 'array') {
            val += ';';
        }

        return val;
    },
    unserialize: (data) => {
        let utf8Overhead = function(str) {
            let s = str.length;
            for (let i = str.length - 1; i >= 0; i--) {
                let code = str.charCodeAt(i);
                if (code > 0x7f && code <= 0x7ff) {
                    s++;
                } else if (code > 0x7ff && code <= 0xffff) {
                    s += 2;
                }
                // trail surrogate
                if (code >= 0xDC00 && code <= 0xDFFF) {
                    i--;
                }
            }
            return s - 1;
        };
        let error = function(type, msg, filename, line) {
            console.error(msg, filename, line)
            // throw new global[type](msg, filename, line);
        };
        let readUntil = function(data, offset, stopchr) {
            let i = 2;
            let buf = [];
            let chr = data.slice(offset, offset + 1);

            while (chr !== stopchr) {
                if ((i + offset) > data.length) {
                    error('Error', 'Invalid');
                }
                buf.push(chr);
                chr = data.slice(offset + (i - 1), offset + i);
                i += 1;
            }
            return [buf.length, buf.join('')];
        };
        let readChrs = function(data, offset, length) {
            let i, chr, buf;

            buf = [];
            for (i = 0; i < length; i++) {
                chr = data.slice(offset + (i - 1), offset + i);
                buf.push(chr);
                length -= utf8Overhead(chr);
            }
            return [buf.length, buf.join('')];
        };

        function _unserialize(data, offset) {
            let dtype;
            let dataoffset;
            let keyandchrs;
            let keys;
            let contig;
            let length;
            let array;
            let readdata;
            let readData;
            let ccount;
            let stringlength;
            let i;
            let key;
            let kprops;
            let kchrs;
            let vprops;
            let vchrs;
            let value;
            let chrs = 0;
            let typeconvert = function(x) {
                return x;
            };

            if (!offset) {
                offset = 0;
            }
            dtype = (data.slice(offset, offset + 1)).toLowerCase();

            dataoffset = offset + 2;

            switch (dtype) {
                case 'i':
                    typeconvert = function(x) {
                        return parseInt(x, 10);
                    };
                    readData = readUntil(data, dataoffset, ';');
                    chrs = readData[0];
                    readdata = readData[1];
                    dataoffset += chrs + 1;
                    break;
                case 'b':
                    typeconvert = function(x) {
                        return parseInt(x, 10) !== 0;
                    };
                    readData = readUntil(data, dataoffset, ';');
                    chrs = readData[0];
                    readdata = readData[1];
                    dataoffset += chrs + 1;
                    break;
                case 'd':
                    typeconvert = function(x) {
                        return parseFloat(x);
                    };
                    readData = readUntil(data, dataoffset, ';');
                    chrs = readData[0];
                    readdata = readData[1];
                    dataoffset += chrs + 1;
                    break;
                case 'n':
                    readdata = null;
                    break;
                case 's':
                    ccount = readUntil(data, dataoffset, ':');
                    chrs = ccount[0];
                    stringlength = ccount[1];
                    dataoffset += chrs + 2;

                    readData = readChrs(data, dataoffset + 1,
                        parseInt(stringlength, 10));
                    chrs = readData[0];
                    readdata = readData[1];
                    dataoffset += chrs + 2;
                    if (chrs !== parseInt(stringlength, 10) && chrs !==
                        readdata.length) {
                        error('SyntaxError', 'String length mismatch');
                    }
                    break;
                case 'a':
                    readdata = {};

                    keyandchrs = readUntil(data, dataoffset, ':');
                    chrs = keyandchrs[0];
                    keys = keyandchrs[1];
                    dataoffset += chrs + 2;

                    length = parseInt(keys, 10);
                    contig = true;

                    for (i = 0; i < length; i++) {
                        kprops = _unserialize(data, dataoffset);
                        kchrs = kprops[1];
                        key = kprops[2];
                        dataoffset += kchrs;

                        vprops = _unserialize(data, dataoffset);
                        vchrs = vprops[1];
                        value = vprops[2];
                        dataoffset += vchrs;

                        if (key !== i) {
                            contig = false;
                        }

                        readdata[key] = value;
                    }

                    if (contig) {
                        array = new Array(length);
                        for (i = 0; i < length; i++) {
                            array[i] = readdata[i];
                        }
                        readdata = array;
                    }

                    dataoffset += 1;
                    break;
                default:
                    error('SyntaxError',
                        'Unknown / Unhandled data type(s): ' + dtype);
                    break;
            }
            return [dtype, dataoffset - offset, typeconvert(readdata)];
        }

        return _unserialize((data + ''), 0)[2];
    }
}
module.exports = obj;