class Subscription {
    constructor() {
        this.authentication = false
    }

    async authorize(args) {
        return true;
    }

    type() {
        return Types.ListOf(Types.Post);
    }

    subscribe(args) {
        return '';
    }

    args() {
        return {
            filter: Types.String
        };
    }

    rules(args) {

    }

    resolve(parent, args, context, info) {
        return parent;
    }

    filter(payload, args) {
        return true;
    }
}

module.exports = {Subscription};