import Arr from './arr';
import Auth from './auth';
import values from 'lodash/values';
import {InvalidOperationException} from '../exceptions/errors';

class Author {

    /**
     * Checks if the user has a role by its name.
     * @param {string | Array}          roles Role name or array of role names.
     * @param {boolean} requireAll      All roles in the array are required.
     * @return {boolean}
     */
    static hasRole(roles, requireAll = false) {
        roles = Arr.array_wrap(roles, ',');

        for (let role of roles) {
            let check = Auth.getRoles().indexOf(role) !== -1;

            if (check && !requireAll) {
                return true;
            } else if (!check && requireAll) {
                return false;
            }
        }

        return requireAll;
    }

    /**
     *  Check if user has a permission by its name.
     * @param {string|Array} permission         Permission string or array of permissions.
     * @param {boolean} requireAll          All permissions in the array are required.
     * @return {boolean}
     */
    static can(permission, requireAll = false) {
        permission = Arr.array_wrap(permission, ',');

        for (let perName of permission) {
            let check = Auth.getScopes().indexOf(perName) !== -1;

            if (check && !requireAll) {
                return true;
            } else if (!check && requireAll) {
                return false;
            }
        }

        return requireAll;
    }

    /**
     * Checks role(s) and permission(s).
     * @param {string|Array} roles          Array of roles or comma separated string
     * @param {string|Array} permission     Array of permissions or comma separated string.
     * @param {object} options              validateAll (true|false) or returnType (boolean|array|both)
     * @return {array|bool}
     */
    static ability(roles, permission, {validateAll, returnType}) {
        roles = Arr.array_wrap(roles, ',');

        permission = Arr.array_wrap(permission, ',');

        validateAll = validateAll || false;

        returnType = returnType || 'boolean'

        if (typeof validateAll !== 'boolean') {
            throw new InvalidOperationException()
        }
        if (['boolean', 'array', 'both'].indexOf(returnType) !== -1) {
            throw new InvalidArgumentException();
        }

        // Loop through roles and permissions and check each.
        let checkedRoles = {};
        let checkedPermissions = {};

        for (let role of roles) {
            checkedRoles[role] = this.hasRole(role);
        }

        for (let perName of permission) {
            checkedPermissions[perName] = this.can(perName);
        }

        if (
            (validateAll && !((values(checkedRoles).indexOf(false) === -1 || values(checkedPermissions).indexOf(false) === -1 ))) ||
            (!validateAll && ((values(checkedRoles).indexOf(true) === -1 || values(checkedPermissions).indexOf(true) === -1 )))
        ) {
            validateAll = true;
        } else {
            validateAll = false;
        }

        if (returnType === 'boolean') {
            return validateAll;
        } else if (returnType === 'array') {
            return {roles: checkedRoles, permissions: checkedPermissions};
        } else {
            return [
                validateAll,
                {roles: checkedRoles, permissions: checkedPermissions}
            ]
        }
    }
}

export default Author;