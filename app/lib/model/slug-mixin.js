import _ from 'lodash';

const db = require('../../models');

export class SlugMixin {
    static SlugModel(Model, options) {
        var settings = _.extend({
            slug: {
                source: 'title'
            }
        }, options);

        Model.afterCreate(async function(model, options) {
            const models = model.sequelize.models;
            let path = null;

            if (model instanceof models.category && model.parentId > 0) {
                path = await models.slug.findOne({
                    where: {
                        slugType: models.category.options.namespace,
                        slugId: model.parentId
                    },
                    attributes: ['slug', 'path']
                });
            }

            if (path) {
                path = '/' + `${path.path}/${path.slug}`.replace(/^\/|\/$/gm, '');
            }

            models.slug.create({
                name: model[settings.slug.source],
                slugType: this.options.namespace,
                slugId: model.id,
                path
            })
        })
    }
}