
export const ModelNamspace = {
    user: 'App\\User',
    post: 'App\\Models\\Post',
    category: 'App\\Models\\Category',
    comment: 'App\\Models\\Comment',
};

export const NamspaceModel = {
    'App\\User': 'user',
    'App\\Models\\Post': 'post',
    'App\\Models\\Category': 'category',
    'App\\Models\\Comment': 'comment',
    'App\\Models\\Status': 'status',
    'App\\Models\\Setting': 'setting',
};

// mời kết bạn
export const FRIEND_STATUS_ADD = 1;
// bạn bè
export const FRIEND_STATUS_FRIEND = 2;
// được mời kết bạn
export const FRIEND_STATUS_INVITED = 3;
// chưa gửi thông tin KYC
export const USER_STATUS_KYC_NOT_INFO = 1;
// gửi thông tin KYC chưa được xác nhận
export const USER_STATUS_KYC_NOT_COMFIRM = 2;
// đã được xác nhận
export const USER_STATUS_KYC_COMFIRM = 3;
// xác nhận không qua
export const USER_STATUS_KYC_COMFIRM_FALSE = 4;
// xác nhận rồi update lại chưa được duyệt lại
export const USER_STATUS_KYC_UPDATE_NOT_COMFIRM = 5;
// tài khoản vip
export const USER_LEVEL_VIP = "1";
// tài khoản leader
export const USER_LEVEL_LEADER = "2";
// tài khoản thương
export const USER_LEVEL_NORMAL = "3";


// chưa tham gia trạng thái thương mại
export const USER_STATUS_TRADE_NORMAL = 1;
// trạng thái thương mại
export const USER_STATUS_TRADE_TRADE = 2;
/// chờ xác nhận tham gia
export const USER_STATUS_TRADE_WAITING = 3;


/////// chưa duyệt bài
export const NOT_APPROVAL = 1;

////đã duyệt bài
export const IS_APPROVAL = 2;

// export const TRANSACTION_TYPE_POINT_PLUS = '1';// cộng điểm
// export const TRANSACTION_TYPE_POINT_MINUS = '2';//trừ điểm
// export const TRANSACTION_TYPE_POINT_FEES_POST = '3';//nhận tiền bài viết
// export const TRANSACTION_TYPE_POINT_PAID_FEES_POST = '4';//trả tiền để xem bài viết
// export const TRANSACTION_TYPE_DONATE = '5';//donate tien
// export const TRANSACTION_TYPE_RECEIVE_DONATE = '6';//Nhận donate