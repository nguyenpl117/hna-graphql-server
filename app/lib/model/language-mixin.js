export class LanguageMixin {
    static languageModel(Model, options) {
        Model.beforeCreate(async function(model, options) {
            if (model.language) {
                return;
            }
            let lang = await this.sequelize.models.language.findOne({
                where: {
                    default: 2
                }
            });
            model.setDataValue('language', lang.id);
        });

        Model.afterCreate(function(model, options) {
            if (!model.languageMaster) {
                Model.update({languageMaster: model.id}, {
                    where: {
                        id: model.id
                    }
                });
            }
        });

        Model.beforeUpdate(async function(model, options) {
            let seq = await this.count({
                where: {
                    language: model.language,
                    languageMaster: model.languageMaster,
                    id: {
                        [this.sequelize.Op.not]: model.id
                    }
                }
            });
            if (seq > 0) {
                model.setDataValue('languageMaster', model.id);
            }
        });
    }
}