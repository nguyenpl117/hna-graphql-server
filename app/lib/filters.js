'use strict';

import _ from 'lodash';
import Arr from './arr';
const {Op} = require('sequelize');
const sequelize = require('sequelize');

/**
 * Created by Phan Trung Nguyên.
 * User: nguyenpl117
 * Date: 3/31/2019
 * Time: 9:04 AM
 */
export class Filters {
    static async compieFilter(filters) {
        this.include = [];
        const result = {
            where: await this.renderFilter(filters),
            include: _.clone(this.include)
        };
        return result;
    }
    
    static async renderFilter(filters) {
        if (!_.isObject(filters)) {
            return {};
        }
        if (this.is_group(filters) && filters.groups.filter(group => this.is_item(group)).length) {
            return {
                [Op[filters.operator]]: await Promise.all(filters.groups.map(async filter => (this.renderFilter(filter))))
            };
        } else if (this.is_item(filters) && filters.items.filter(item => this.is_item(item) || this.is_group(item) || this.is_where(item)).length) {
            return {
                [Op[filters.operator]]: await Promise.all(filters.items.map(async filter => (this.renderFilter(filter))))
            };
        } else if (this.is_where(filters)) {
            return await this.filter(filters);
        }
        return {};
    }
    
    static async filter(filter) {
        const array = [
            'between',
            'notBetween',
            'in',
            'notIn',
        ];
        let value = array.indexOf(filter.operator) !== -1
            ? filter.value
            : Arr.array_wrap(filter.value).join('');
        
        if (filter.value === null) {
            value = filter.value;
        }
    
        if (typeof filter.field === 'function') {
            const field = await filter.field(value, filter.operator);
    
            if (typeof field === 'string') {
                return {
                    [Op.and]: sequelize.literal(await filter.field(value, filter.operator))
                };
            }
            
            return {
                [field.field]: {
                    [Op[filter.operator]]: field.value
                }
            }
        }
        
        const field = Arr.array_wrap(filter.field, '.');
        
        if (field.length === 2) {
            if (!this.include) {
                this.include = [];
            }
            this.include.push(_.first(field));
            return {
                [Op.col]: sequelize.where(sequelize.col(filter.field), Op[filter.operator], value)
            }
        }
        
        return {
            [filter.field]: {
                [Op[filter.operator]]: value
            }
        };
    }
    
    static getInclude() {
        return this.include;
    }
    
    static is_item(item) {
        return (item.items && item.operator);
    }
    
    static is_group(group) {
        return group.groups && group.operator;
    }
    
    static is_where(where) {
        if (!where) return false;
        
        return where.field && 'value' in where && where.operator;
    }
}