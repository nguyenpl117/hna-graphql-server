import HasOne from './relations/has-one';
import BelongsToMany from './relations/belongs-to-many';
import HasMany from './relations/has-many';

const {Utils, Model, Promise} = require('sequelize');
import sequelizeErrors from 'sequelize/lib/errors';
import BaseRelation from './relations/base';
const _ = require('lodash');

Model.findAll = function(options) {
    if (options !== undefined && !_.isPlainObject(options)) {
        throw new sequelizeErrors.QueryError('The argument passed to findAll must be an options object, use findByPk if you wish to pass a single primary key value');
    }

    if (options !== undefined && options.attributes) {
        if (!Array.isArray(options.attributes) && !_.isPlainObject(options.attributes)) {
            throw new sequelizeErrors.QueryError('The attributes option must be an array of column names or an object');
        }
    }

    this.warnOnInvalidOptions(options, Object.keys(this.rawAttributes));

    const tableNames = {};
    let originalOptions;

    tableNames[this.getTableName(options)] = true;
    options = Utils.cloneDeep(options);

    _.defaults(options, { hooks: true, rejectOnEmpty: this.options.rejectOnEmpty });

    // set rejectOnEmpty option from model config
    options.rejectOnEmpty = options.rejectOnEmpty || this.options.rejectOnEmpty;

    return Promise.try(() => {
        this._injectScope(options);

        if (options.hooks) {
            return this.runHooks('beforeFind', options);
        }
    }).then(() => {
        this._conformOptions(options, this);
        this._expandIncludeAll(options);

        if (options.hooks) {
            return this.runHooks('beforeFindAfterExpandIncludeAll', options);
        }
    }).then(() => {
        if (options.include) {
            options.hasJoin = true;

            this._validateIncludedElements(options, tableNames);

            // If we're not raw, we have to make sure we include the primary key for deduplication
            if (options.attributes && !options.raw && this.primaryKeyAttribute && options.attributes.indexOf(this.primaryKeyAttribute) === -1) {
                options.originalAttributes = options.attributes;
                if (!options.group || !options.hasSingleAssociation || options.hasMultiAssociation) {
                    options.attributes = [this.primaryKeyAttribute].concat(options.attributes);
                }
            }
        }
    
        if (!options.attributes) {
            options.attributes = Object.keys(this.tableAttributes);
        }

        if (options.with && Array.isArray(options.with)) {
            options.with.forEach(data => {
                if (typeof this[data.name] === 'function') {
                    let relation = this[data.name]();
                    options.attributes.push(relation.primaryKey);
                }
            });

        }

        if (this.options && this.options.appendSelect && Array.isArray(this.options.appendSelect)) {
            options.attributes = [
                ...options.attributes,
                ...this.options.appendSelect
            ];
        }

        options.attributes = _.uniq(options.attributes);

        // whereCollection is used for non-primary key updates
        this.options.whereCollection = options.where || null;

        Utils.mapFinderOptions(options, this);

        options = this._paranoidClause(this, options);

        if (options.hooks) {
            return this.runHooks('beforeFindAfterOptions', options);
        }
    }).then(() => {
        originalOptions = Utils.cloneDeep(options);
        options.tableNames = Object.keys(tableNames);
        return this.QueryInterface.select(this, this.getTableName(options), options);
    }).tap(results => {
        if (options.hooks) {
            return this.runHooks('afterFind', results, options);
        }
    }).then(results => {

        //rejectOnEmpty mode
        if (_.isEmpty(results) && options.rejectOnEmpty) {
            if (typeof options.rejectOnEmpty === 'function') {
                throw new options.rejectOnEmpty();
            } else if (typeof options.rejectOnEmpty === 'object') {
                throw options.rejectOnEmpty;
            } else {
                throw new sequelizeErrors.EmptyResultError();
            }
        }

        return Model._findSeparate(results, originalOptions);
    })
    .then(results => {
        return this._eagerLoad(results, originalOptions);
    });
}

Model.selectQuery = function(options) {
    if (options !== undefined && !_.isPlainObject(options)) {
        throw new sequelizeErrors.QueryError('The argument passed to findAll must be an options object, use findByPk if you wish to pass a single primary key value');
    }

    if (options !== undefined && options.attributes) {
        if (!Array.isArray(options.attributes) && !_.isPlainObject(options.attributes)) {
            throw new sequelizeErrors.QueryError('The attributes option must be an array of column names or an object');
        }
    }

    this.warnOnInvalidOptions(options, Object.keys(this.rawAttributes));

    const tableNames = {};
    let originalOptions;

    tableNames[this.getTableName(options)] = true;
    options = Utils.cloneDeep(options);

    _.defaults(options, { hooks: true, rejectOnEmpty: this.options.rejectOnEmpty });

    // set rejectOnEmpty option from model config
    options.rejectOnEmpty = options.rejectOnEmpty || this.options.rejectOnEmpty;

    return Promise.try(() => {
        this._injectScope(options);
    }).then(() => {
        this._conformOptions(options, this);
        this._expandIncludeAll(options);
    }).then(() => {
        if (options.include) {
            options.hasJoin = true;

            this._validateIncludedElements(options, tableNames);

            // If we're not raw, we have to make sure we include the primary key for deduplication
            if (options.attributes && !options.raw && this.primaryKeyAttribute && options.attributes.indexOf(this.primaryKeyAttribute) === -1) {
                options.originalAttributes = options.attributes;
                if (!options.group || !options.hasSingleAssociation || options.hasMultiAssociation) {
                    options.attributes = [this.primaryKeyAttribute].concat(options.attributes);
                }
            }
        }

        if (!options.attributes) {
            options.attributes = Object.keys(this.tableAttributes);
        }

        if (options.with && Array.isArray(options.with)) {
            options.with.forEach(data => {
                if (typeof this[data.name] === 'function') {
                    let relation = this[data.name]();
                    options.attributes.push(relation.primaryKey);
                }
            });

        }

        if (this.options && this.options.appendSelect && Array.isArray(this.options.appendSelect)) {
            options.attributes = [
                ...options.attributes,
                ...this.options.appendSelect
            ];
        }

        options.attributes = _.uniq(options.attributes);

        // whereCollection is used for non-primary key updates
        this.options.whereCollection = options.where || null;

        Utils.mapFinderOptions(options, this);

        options = this._paranoidClause(this, options);
    }).then(() => {
        originalOptions = Utils.cloneDeep(options);
        options.tableNames = Object.keys(tableNames);

        options = Utils.cloneDeep(options);
        options.type = 'SELECT';
        options.model = this;

        return this.sequelize.dialect.QueryGenerator.selectQuery(this.getTableName(options), options, this);
    });
}

/**
 *
 * @param results
 * @param options
 * @return {*}
 * @private
 */
Model._eagerLoad = function(results, options) {
    if (!options.with || options.raw || !results) return results;
    
    const original = results;
    if (options.plain) results = [results];
    
    if (!results.length) return original;
    
    return Promise.map(options.with, async data => {
        if (typeof this[data.name] !== 'function') {
            return;
        }
        let relation = this[data.name]();
    
        if (!(relation instanceof BaseRelation)) {
            return;
        }
    
        let values = await relation.eagerLoad(results, data);
    
        for (let item of results) {
            let value = relation.getItems(values, item);
            // console.log(value);
            if (value) {
                item.set(data.name, value, {raw: true});
            }
        }
    }).return(original);
}

/**
 *
 * @param target
 * @param primaryKey
 * @param foreignKey
 * @param {*} scope
 * @return {HasOne}
 */
Model.customHasOne = function(target, {primaryKey, foreignKey, scope, type}) {
    return new HasOne(this, target, {primaryKey, foreignKey, scope, type});
}

/**
 *
 * @param target
 * @param primaryKey
 * @param foreignKey
 * @param {model: Model, scope: {*}} through
 * @param otherKey
 * @param scope
 * @return {BelongsToMany}
 */
Model.customBelongsToMany = function(target, {primaryKey, foreignKey, through, otherKey, scope, appendField}) {
    if (typeof through === 'function') {
        through = {model: through};
    }
    return new BelongsToMany(this, target, {primaryKey, foreignKey, through, otherKey, scope, appendField});
}

Model.customHasMany = function(target, {primaryKey, foreignKey, scope}) {
    return new HasMany(this, target, {primaryKey, foreignKey, scope});
}

export default Model;