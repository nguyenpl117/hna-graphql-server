import BaseRelation from './relations/base';

const { FragmentSpreadNode, InlineFragmentNode } = require('graphql');
const _ = require('lodash');

class ResolveInfo {
    constructor(info) {
        const {
            fieldName,
            fieldNodes,
            returnType,
            parentType,
            path,
            schema,
            fragments,
            rootValue,
            operation,
            variableValues
        } = info;
        this.fieldName = fieldName;
        this.fieldNodes = fieldNodes;
        this.returnType = returnType;
        this.parentType = parentType;
        this.path = path;
        this.schema = schema;
        this.fragments = fragments;
        this.rootValue = rootValue;
        this.operation = operation;
        this.variableValues = variableValues;
    }

    getFieldSelection(depth = 0) {
        let fields = {};

        /** @var FieldNode fieldNode */
        for (let fieldNode of this.fieldNodes) {
            if (!fieldNode.selectionSet) {
                continue;
            }
            fields = _.mergeWith(fields, this.foldSelectionSet(fieldNode.selectionSet, depth), customizer);
        }

        return fields;
    }

    foldSelectionSet(selectionSet, descend) {
        let fields = {};
        for (let selectionNode of selectionSet.selections) {
            if (selectionNode.kind === 'Field') {
                fields[selectionNode.name.value] = descend > 0 && !empty(selectionNode.selectionSet) ? this.foldSelectionSet(selectionNode.selectionSet, descend - 1) : true;
            } else if (selectionNode.kind === 'FragmentSpread') {
                const spreadName = selectionNode.name.value;
                if (isset(this.fragments[spreadName])) {
                    /** @var FragmentDefinitionNode $fragment */
                    const fragment = this.fragments[spreadName];
                    fields = merge(
                        this.foldSelectionSet(fragment.selectionSet, descend),
                        fields
                    );
                }
            } else if (selectionNode.kind === 'InlineFragment') {
                fields = merge(
                    this.foldSelectionSet(selectionNode.selectionSet, descend),
                    fields
                );
            }
        }
        return fields;
    }
}

function merge(...args) {
    return _.mergeWith(...args, customizer);
}

function customizer(objValue, srcValue) {
    if (_.isObject(objValue)) {
        return _.merge(objValue, srcValue);
    }
    if (_.isArray(objValue)) {
        return _.uniq(objValue.concat(srcValue));
    }
}

function empty (mixedVar) {
    var undef
    var key
    var i
    var len
    var emptyValues = [undef, null, false, 0, '', '0']

    for (i = 0, len = emptyValues.length; i < len; i++) {
        if (mixedVar === emptyValues[i]) {
            return true
        }
    }

    if (typeof mixedVar === 'object') {
        for (key in mixedVar) {
            if (mixedVar.hasOwnProperty(key)) {
                return false
            }
        }
        return true
    }

    return false
}

module.exports = {
    getFieldSelection: (info, depth = 0) => {
        const rinfo = new ResolveInfo(info);
        return rinfo.getFieldSelection(depth);
    },

    getFields: (info, model) => {
        const rinfo = new ResolveInfo(info);
        const fields = rinfo.getFieldSelection(4);
        return getField(fields, model);
    },
    getPaginateFields: function(info, model) {
        const rinfo = new ResolveInfo(info);
        const fields = rinfo.getFieldSelection(5);
        return getField(fields.data, model);
    },

};

function getField(fields, model) {
    const result = [];
    const includes = [];
    let withs = [];
    if (model.options.appendwith) {
        withs = [...withs, ...model.options.appendwith];
    }
    for (let field in fields) {
        if (!_.isObject(fields[field])) {
            if (model.rawAttributes[field]) {
                result.push(field);
            }
            if (typeof model[field] === 'function') {
                let relation = model[field]();
                if (!(relation instanceof BaseRelation)) {
                    continue;
                }
                withs.push({
                    name: field
                });
            }
        } else {
            const association = model.associations[field];
            if (association) {
                includes.push({
                    association: association.associationAccessor,
                    ...getField(fields[field], association.target)
                });
            }
            if (typeof model[field] === 'function') {
                let relation = model[field]();
                if (!(relation instanceof BaseRelation)) {
                    continue;
                }
                // console.log(getField(fields[field], relation.relatedModel));
                withs.push({
                    name: field,
                    ...getField(fields[field], relation.relatedModel)
                });
            }
        }
    }
    return {
        attributes: result,
        include: includes,
        with: withs,
    };
}