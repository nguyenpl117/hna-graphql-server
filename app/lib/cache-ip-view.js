import _ from 'lodash';

export default class CacheIpView {
    static init(options = {}) {
        if (!this.dataCache) {
            this.dataCache = {};
        }
        this.config = _.extend({
            timeout: 1000 * 60 * 5
        }, options)
    }

    static checkIp(ip) {
        const now = (new Date()).getTime();
        if (this.dataCache.hasOwnProperty(ip) && now <= this.dataCache[ip]) {
            return false;
        }
        this.dataCache[ip] = (new Date()).getTime() + this.config.timeout;
        return true;
    }
}