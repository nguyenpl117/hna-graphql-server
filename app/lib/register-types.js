import { merge } from './utils';
import { EnumType } from './support/enum-type';
import { UnionType } from './support/union-type';
import { GraphqlType } from './support/graphql-type';
import { InputType } from './support/input-type';
import { FilterType } from './support/filter-type';
import { ScalarType } from './support/scalar-type';
import { GraphQLScalarType } from 'graphql';
import {SortType} from './support/sort-type';

export default class RegisterType {
    static listType = {};
    
    /**
     *
     * @param objectType
     * @returns {RegisterType}
     */
    static add(objectType) {
        /**
         * @type ObjectType
         */
        let obj = new objectType();

        let name = objectType.iniName + obj.name;
        
        if (!this.listType[name]) {
            this.listType[name] = obj;
        }
        
        return this;
    }
    
    /**
     *
     * @returns {Array}
     */
    static types() {
        
        const types = [];
        
        for (let i in this.listType) {
            /**
             * @type ObjectType
             */
            let item = this.listType[i];
            
            if (item instanceof UnionType) {
                types.push(`"""${item.description}""" union ${item.name} = ${item.types().join('|')}`);
            } else if (item instanceof EnumType) {
                types.push(`"""${item.description}""" enum ${item.name} {${this.getValueEnum(item.values())}}`);
            } else if (item instanceof SortType) {
                types.push(`"""${item.description}""" input ${item.name} {${this.getValueSort(item.fields())}}`);
            } else if (item instanceof FilterType) {
                types.push(`${FilterType.renderFilterType(item.name, this.getValueEnum(item.values()), item.description)}`);
            } else if (item instanceof InputType) {
                types.push(`"""${item.description}""" input ${item.name} {${this.getPropertiesType(item.fields())}}`);
            } else if (item instanceof ScalarType) {
                types.push(`scalar ${item.name}`);
            } else if (item instanceof GraphqlType) {
                
                types.push(`"""${item.description}""" type ${item.name} ${this.getCacheControl(item.cacheControl)} {${this.getPropertiesType({
                    ...item.fields(),
                    noCache: {
                        type: 'Boolean @cacheControl(maxAge: 0)',
                        description: 'Không lấy cache.'
                    },
                })}}`);
            } else {
                types.push(`"""${item.description}""" type ${item.name} {${this.getPropertiesType(item.fields())}}`);
            }
        }
        
        return types;
    }
    
    static getCacheControl(cacheControl) {
        if (!cacheControl) {
            return '';
        }
        
        let {maxAge, scope} = cacheControl;
        
        let m = maxAge ? 'maxAge:' + maxAge : '';
        
        let s = scope ? 'scope:' + scope : '';
        
        if ([m, s].join('').length < 1) {
            return '';
        }
        
        return `@cacheControl(${[m, s].join(',')})`;
    }

    static getValueSort(fields) {
        const properties = [];

        for (let key in fields) {
            if (fields[key].description) {
                properties.push(`"""${fields[key].description}"""`);
            }
            properties.push(`${key}: SortValue`);
        }

        return properties;
    }
    
    static getValueEnum(values) {
        const properties = [];
        
        for (let key in values) {
            if (values[key].description) {
                properties.push(`"""${values[key].description}"""`);
            }
            properties.push(`${key}`);
        }
    
        return properties;
    }
    
    /**
     * @returns {*}
     */
    static resolvers() {
        
        const resolvers = {};
        
        for (let i in this.listType) {
            /**
             * @type GraphqlType|EnumType|FilterType|InputType|ScalarType
             */
            let item = this.listType[i];
    
            if (item instanceof UnionType) {
                resolvers[item.name] = {
                    __resolveType(obj, context, info) {
                        return item.resolveType(obj, context, info);
                    }
                };
            } else if (item instanceof EnumType) {
                resolvers[item.name] = {
                    ...this.getResolveEnum(item.values())
                };
            } else if (item instanceof FilterType) {
                resolvers['FilterField' + item.name] = {
                    ...this.getResolveEnum(item.values())
                };
            } else if (item instanceof InputType) {
                resolvers[item.name] = {
                    ...this.getResolve(item.fields())
                };
            } else if (item instanceof ScalarType) {
                resolvers[item.name] = new GraphQLScalarType(item);
            }  else if (item instanceof SortType) {
                resolvers[item.name] = {
                    ...this.getResolve(item.fields())
                };
            } else {
                resolvers[item.name] = {
                    ...this.getResolve(item.fields())
                };
            }
        }
        
        return resolvers;
    }
    
    static getResolveEnum(values) {
        let r = {};
        for(let i in values) {
            r[i] = i;

            if (values[i].value) {
                r[i] = values[i].value;
            }
        }
        return r;
    }
    
    /**
     *
     * @param obj ObjectType
     */
    static getResolve(obj) {
        if (!obj || typeof obj !== 'object') {
            return {};
        }

        const res = {};
        
        for (let key in obj) {
            let item = obj[key];
            if (item.hasOwnProperty('value')) {
                res[key] = item.value;
            }
            if (item.hasOwnProperty('resolve')) {
                res[key] = item.resolve;
            }
            if (item.hasOwnProperty('privacy')) {
                res[key] = (root, args, cnt, info) => {
                    if (item.privacy(root, args, cnt, info)) {
                        if (item.hasOwnProperty('resolve')) {
                            return item.resolve(root, args, cnt, info);
                        }
                        return root[key];
                    }
                    return null;
                };
            }
        }
        
        return res;
    }
    
    /**
     *
     * @param obj ObjectType
     */
    static getPropertiesType(obj) {
        const properties = [];
        
        for (let key in obj) {
            if (obj[key].description) {
                properties.push(`"""${obj[key].description}"""`);
            }
            if (obj[key].type) {
                properties.push(`${key}${this.compieArgs(obj[key].args)}:${obj[key].type}${this.compiedDirectives(obj[key].directives)}`);
            } else {
                properties.push(`${key}:${obj[key]}`);
            }
        }
        
        return properties;
    }
    
    static compiedDirectives(obj) {
        if (!Array.isArray(obj) || !obj.length) {
            return '';
        }
    
        const directives = [];
        
        for (let item of obj) {
            directives.push(`@${item.name}${this.compieDirectiveArguments(item.args)}`)
        }
    
        return directives;
    }
    
    static compieDirectiveArguments(object) {
        if (typeof object !== 'object') {
            return '';
        }
    
        const args = [];
    
        for (let item in object) {
            args.push(`${item}: ${object[item]}`)
        }
    
        return args.length ? `(${args.join(', ')})` : '';
    }
    
    /**
     *
     * @param object
     * @returns {string}
     */
    static compieArgs(object) {
        if (typeof object !== 'object') {
            return '';
        }
        
        const args = [];
        
        for (let item in object) {
            if (typeof object[item] === 'string') {
                args.push(`${item}: ${object[item]}`);
            } else if (typeof object[item] === 'object' && object[item].type) {
                if (object[item].description) {
                    args.push(`"""${object[item].description}"""`);
                }
                if (object[item].hasOwnProperty('defaultValue')) {
                    args.push(
                        `${item}: ${object[item].type}=${object[item].defaultValue}`);
                } else {
                    args.push(`${item}: ${object[item].type}`);
                }
            }
        }
        
        return args.length ? `(${args.join(', ')})` : '';
    }
    
    /**
     *
     * @param name String
     * @param prefix String
     * @return {{}}
     */
    static rules(name, prefix) {
        
        let obj = this.getType(name);
        
        if (!obj) {
            return {};
        }
        
        if (this.hasListOf(name)) {
            prefix = `${prefix}.*`;
        }
        
        let rules = {};
        
        for (let i in obj.fields()) {
            let item = obj.fields()[i];
            
            if (typeof item === 'object' && item.rules) {
                rules[`${prefix}.${i}`] = item.rules;
            }
            
            rules = merge(this.rules(item.type, `${prefix}.${i}`), rules);
        }
        
        return rules;
    }
    
    /**
     * @param typeName
     * @returns {boolean}
     */
    static hasListOf(typeName) {
        return /^\[.*\]$/g.test(typeName);
    }
    
    /**
     * @param typeName
     * @returns {*}
     */
    static getType(typeName, type = 'input') {
        
        if (this.hasListOf(typeName)) {
            typeName = typeName.replace(/^\[|\]$/g, '');
        }
        
        if (!this.listType[type + typeName]) {
            return false;
        }
        
        return this.listType[type + typeName];
    }
}