'use strict';

import { buildQuery } from '../src/query';
import { buildMutation } from '../src/mutation';
import RegisterType from './register-types';
import { srcDir } from './utils';

/**
 * Created by Phan Trung Nguyên.
 * User: nguyenpl117
 * Date: 6/22/2019
 * Time: 8:52 PM
 */
var fs = require('fs')
    , p = require('path')
;
const path = require('path');
const loadPath = require('./load-path');

let appDir = srcDir();

function autoloadGraphQL(path) {
    
    const obj = {
        'query': [],
        'mutation': [],
        'input': [],
        'union': [],
        'object': [],
        'enum': [],
        'filter': [],
        'scalar': [],
        'sort': [],
    };
    
    let build = {
        'query': buildQuery,
        'mutation': buildMutation,
        'input': RegisterType.add.bind(RegisterType),
        'union': RegisterType.add.bind(RegisterType),
        'object': RegisterType.add.bind(RegisterType),
        'enum': RegisterType.add.bind(RegisterType),
        'filter': RegisterType.add.bind(RegisterType),
        'scalar': RegisterType.add.bind(RegisterType),
        'sort': RegisterType.add.bind(RegisterType),
    }
    
    loadPath(appDir + '/' + path).forEach(file => {
        const item = require(file).default;
        obj[item.iniName].push(build[item.iniName](item));
    });
    
    return obj;
}

module.exports = {
    autoloadGraphQL
}