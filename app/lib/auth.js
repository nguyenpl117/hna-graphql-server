'use strict';

/**
 * Created by Phan Trung Nguyên.
 * User: nguyenpl117
 * Date: 3/27/2019
 * Time: 9:08 PM
 */
class Auth {
    static init() {
        this.dataUser = null;
        this.scopes = null;
        this.roles = [];
    }
    
    static check() {
        return !!this.dataUser;
    }
    
    /**
     *
     * @param user
     * @return {null}
     */
    static setUser(user) {
        // Nếu không có user
        if (!user) {
            return null;
        }
    
        this.dataUser = user;
        
        if (user.roles && Array.isArray(user.roles)) {
            this.roles = user.roles.map(item => (item.name));
        }
    }
    
    static id() {
        return this.dataUser ? this.dataUser.id : null;
    }
    
    static user() {
        return this.dataUser;
    }

    static setScope(scopes) {
        this.scopes = scopes;
    }

    static getScopes() {
        return this.scopes || [];
    }

    static getRoles() {
        return this.roles || [];
    }
}

export default Auth;