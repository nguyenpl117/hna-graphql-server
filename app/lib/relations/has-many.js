import BaseRelation from './base';
import _ from 'lodash';
import Arr from '../../lib/arr';

class HasMany extends BaseRelation {
    constructor(parentInstance, RelatedModel, options) {
        super(parentInstance, RelatedModel, options);
    }

    /**
     *
     * @param {Array} modelInstances
     * @return {Array}
     */
    mapValues(modelInstances) {
        return _.transform(Arr.array_wrap(modelInstances), (result, modelInstance) => {
            if (modelInstance[this.primaryKey]) {
                result.push(modelInstance[this.primaryKey]);
            }
            return result;
        }, []);
    }

    /**
     *
     * @param {Array} relatedInstances
     */
    group(relatedInstances) {
        return _.transform(relatedInstances, (result, relatedInstance) => {
            const foreignKeyValue = relatedInstance[this.foreignKey];

            (result[foreignKeyValue] || (result[foreignKeyValue] = [])).push(relatedInstance);

            return result;
        }, {});
    }

    getItems(relatedInstances, modelInstance) {
        return !relatedInstances[modelInstance[this.primaryKey]] ? [] : relatedInstances[modelInstance[this.primaryKey]];
    }
}

export default HasMany;