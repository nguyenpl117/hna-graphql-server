import BaseRelation from './base';
import _ from 'lodash';
import Arr from '../../lib/arr';
import {merge} from '../utils';

class BelongsToMany extends BaseRelation {
    constructor(parentInstance, RelatedModel, options) {
        super(parentInstance, RelatedModel, options);
        this.otherKey = options.otherKey;
    }

    _eagerLoadFn(foreignKey, values, options) {
        this.options.through.model.belongsTo(this.relatedModel,
            {foreignKey: this.otherKey});
        return this.options.through.model.findAll(merge(
            {
                where: {
                    [foreignKey]: {'in': _.uniq(values)}
                },
                include: [
                    {
                        model: this.relatedModel,
                        ...merge(this.options.scope, options)
                    }]
            }, this.options.through.scope));
    }

    /**
     *
     * @param {Array} modelInstances
     * @return {Array}
     */
    mapValues(modelInstances) {
        return _.transform(Arr.array_wrap(modelInstances),
            (result, modelInstance) => {
                if (modelInstance[this.primaryKey]) {
                    result.push(modelInstance[this.primaryKey]);
                }
                return result;
            }, []);
    }

    /**
     *
     * @param {Array} relatedInstances
     */
    group(relatedInstances) {
        return _.transform(relatedInstances, (result, relatedInstance) => {
            const foreignKeyValue = relatedInstance[this.foreignKey];
            const value = relatedInstance[this.relatedModel.name];

            if (value) {
                if (Array.isArray(this.options.appendField)) {
                    this.options.appendField.forEach(item => {
                        value.setDataValue(item, relatedInstance.getDataValue(item));
                    });
                }
                
                (result[foreignKeyValue] ||
                    (result[foreignKeyValue] = [])).push(value);
            }

            return result;
        }, {});
    }

    getItems(relatedInstances, modelInstance) {
        return !relatedInstances[modelInstance[this.primaryKey]]
            ? []
            : relatedInstances[modelInstance[this.primaryKey]];
    }

    /**
     *
     * @param data
     */
    async sync(syncData = [], instance) {
        syncData = syncData.map(item => {
            return _.toString(item);
        });

        const result = await this.options.through.model.findAll({
            where: {
                [this.foreignKey]: instance[this.primaryKey]
            }
        });

        let pushData = syncData.slice(0);

        for (let item of result) {
            if (syncData.indexOf(_.toString(item[this.otherKey])) === -1) {
                await this.options.through.model.destroy({where: item.toJSON()});
            } else {
                pushData = pushData.filter(x => !_.eq(x, _.toString(item[this.otherKey])));
            }
        }

        for (let item of pushData) {
            await this.options.through.model.create({
                [this.otherKey]: item,
                [this.foreignKey]: instance[this.primaryKey]
            });
        }
    }
}

export default BelongsToMany;