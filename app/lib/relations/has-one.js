import BaseRelation from './base';
import _ from 'lodash';
import Arr from '../../lib/arr';
import { merge } from '../utils';

const Sequelize = require('sequelize');

class HasOne extends BaseRelation {
    constructor(parentInstance, RelatedModel, options) {
        super(parentInstance, RelatedModel, options);
    }
    
    _types(type) {
        const types = {};
        this.options.type.type.forEach(item => {
            types[item[0]] = item[1];
            types[item[1]] = item[1];
        });
        
        return types[type];
    }
    
    _eagerLoadFn(foreignKey, values, options) {
        let where = {
            [foreignKey]: { 'in': _.uniq(values) }
        };
        
        if (this.options.type) {
            const types = this.options.type;
            where = {
                [Sequelize.Op.or]: [
                    ...types.type.filter(type => {
                        return values.filter(data => {
                            return data[types.primaryKey[1]] === type[0];
                        }).length !== 0;
                    }).map(type => {
                        return {
                            [types.foreignKey[0]]: {
                                [Sequelize.Op.in]: values.filter(data => {
                                    return data[types.primaryKey[1]] === type[0];
                                }).map(data => {
                                    return data[types.primaryKey[0]];
                                })
                            },
                            [types.foreignKey[1]]: type[1]
                        };
                    })
                ]
            };
        }
        
        let opt = merge(
            {
                where: where
            },
            this.options.scope,
            options,
            {
                attributes: [this.foreignKey]
            });
    
        return this.relatedModel.findAll(opt);
    }
    
    /**
     *
     * @param {Array} modelInstances
     * @return {Array}
     */
    mapValues(modelInstances) {
        if (this.options.type) {
            return modelInstances;
        }
        return _.transform(Arr.array_wrap(modelInstances),
            (result, modelInstance) => {
                if (modelInstance[this._primaryKey()]) {
                    result.push(modelInstance[this._primaryKey()]);
                }
                return result;
            }, []);
    }
    
    /**
     *
     * @param {Array} relatedInstances
     */
    group(relatedInstances) {
        return _.transform(relatedInstances, (result, relatedInstance) => {
            const foreignKeyValue = relatedInstance[this._foreignKey()];
            
            (result[foreignKeyValue] || (result[foreignKeyValue] = [])).push(
                relatedInstance);
            
            return result;
        }, {});
    }
    
    getItems(relatedInstances, modelInstance) {
        return !relatedInstances[modelInstance[this._primaryKey()]]
            ? null
            : _.first(relatedInstances[modelInstance[this._primaryKey()]]);
    }
}

export default HasOne;