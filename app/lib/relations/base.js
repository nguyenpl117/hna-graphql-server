import _ from 'lodash';
import { merge } from '../utils';
const md5 = require("crypto-js/md5");

class BaseRelation {
    constructor(parentInstance, RelatedModel, options) {
        this.parentInstance = parentInstance;
        this.relatedModel = RelatedModel;
        this.primaryKey = options.primaryKey;
        this.foreignKey = options.foreignKey;
        this.options = options;
    }
    
    _primaryKey() {
        let primaryKey = this.primaryKey;
        // if (this.options.type && this.options.type.primaryKey) {
        //     primaryKey = md5(this.options.type.primaryKey);
        // }
        return primaryKey;
    }
    
    _foreignKey() {
        let foreignKey = this.foreignKey;
        // if (this.options.type && this.options.type.foreignKey) {
        //     foreignKey = md5(this.options.type.foreignKey);
        // }
        return foreignKey;
    }

    _eagerLoadFn(foreignKey, values, options) {
        return this.relatedModel.findAll(merge(
            {
                where: {
                    [foreignKey]: { 'in': _.uniq(values) }
                }
            },
            this.options.scope,
            options,
            {
                attributes: [this.foreignKey]
            }));
    }

    async eagerLoad(rows, options) {
        const mappedRows = this.mapValues(rows);
    
        if (!mappedRows || !mappedRows.length) {
            return this.group([])
        }
        
        let res = await this._eagerLoadFn(this._foreignKey(), mappedRows, options);
    
        return this.group(res);
    }
}

export default BaseRelation;