import PaginationType from '../src/type/definition/Pagination';

const Types = {
    ListOf: (name) => (`[${name}]`),
    NotNull: (name) => (`${name}!`),
    Pagination: (name) => {
        if (!PaginationType.has(name)) {
            PaginationType.push(name);
        }
        return `${name}Pagination @cost(complexity: 1, multipliers: ["limit"])`;
    },
    Type: (name) => (name),
    ArgsPagination: (name) => {
        return {
            filter: 'Filter' + name,
            sortBy: Types.ListOf('Sort' + name),
            limit: 'Int = 20',
            page: {
                type: Types.Int,
                defaultValue: 1,
                description: ''
            }
        };
    },
    ArgsOne: (name) => {
        return {
            filter: 'Filter' + name,
            sortBy: Types.ListOf('Sort' + name),
        };
    },
    ID: 'ID_CRYPTO',
    Int: 'Int',
    String: 'String',
    Boolean: 'Boolean',
    Timestamp: 'Timestamp',
    Delete: 'Delete',
    MessageSuccess: 'MessageSuccess',
    Post: 'Post',
    Status: 'STATUS',
    Float: 'Float',
    Any: 'Any',
    JSON: 'JSON'
};

module.exports = {Types};