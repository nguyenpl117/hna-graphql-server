var fs = require('fs')
    , p = require('path')
;
const path = require('path');
const basename = path.basename(__filename);

// how to know when you are done?
function recursiveReaddirSync(path) {
    var list = []
        , files = fs.readdirSync(path)
        , stats
    ;

    files.forEach(function (file) {
        stats = fs.lstatSync(p.join(path, file));
        if(stats.isDirectory()) {
            list = list.concat(recursiveReaddirSync(p.join(path, file)));
        } else {
            list.push(p.join(path, file));
        }
    });

    return list;
}

function loadPath(path) {
    if (!fs.existsSync(path)) {
        return [];
    }
    return recursiveReaddirSync(path).filter(file => {
        return (file.indexOf('.') !== 0) && (file !== p.join(__dirname, basename)) && (file.slice(-3) === '.js');
    });
}

module.exports = loadPath
module.loadPath = loadPath;