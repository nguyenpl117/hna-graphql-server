
export class ArrayCache {
    static init() {
        if (!this.dataCache) {
            this.dataCache = {};
        }
        if (!this.timeout) {
            this.timeout = (new Date()).getTime() + 1000;
        }
        this.timenow = (new Date()).getTime();
        if (this.timenow > this.timeout) {
            this.dataCache = {};
            this.timeout = (new Date()).getTime() + 1000;
        }
    }

    static clear() {
        this.dataCache = {};
    }

    static remember(key, callback) {
        if (!this.dataCache) {
            this.dataCache = {};
        }

        if (!this.dataCache.hasOwnProperty(key) || !this.dataCache[key]) {
            this.dataCache[key] = callback();
        }

        return this.dataCache[key];
    }

    static remove(key) {
        this.dataCache[key] = null;
    }
}