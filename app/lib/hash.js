const bcrypt = require('bcrypt');

export class Hash {
    static make(data) {
        return bcrypt.hashSync(data, 10).replace(/^\$2b/g,'$2y').replace(/^\$2a/g,'$2x');
    }
    
    static check(data, hash) {
        return bcrypt.compareSync(data, hash.replace(/^\$2y/g,'$2b').replace(/^\$2x/g,'$2a'));
    }
}