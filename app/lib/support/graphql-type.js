import RegisterType from '../register-types';

export class GraphqlType {
    static iniName = 'object';
    
    constructor() {
        this.description = '';
        this.name = '';
        this.cacheControl = {};
    }
    
    fields() {
        return {};
    }
    
    types() {
        return `"""${this.description}""" type ${this.name} {${RegisterType.getPropertiesType(this.fields())}}`;
    }
}