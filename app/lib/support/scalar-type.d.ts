import { GraphQLScalarLiteralParser, GraphQLScalarSerializer, GraphQLScalarValueParser } from 'graphql';
import Maybe from 'graphql/tsutils/Maybe';

export class ScalarType {
    name: string;
    
    description?: Maybe<string>;
    
    serialize(value: GraphQLScalarSerializer<any>): any
    
    parseValue(value: GraphQLScalarValueParser<any>): any
    
    parseLiteral(ast: GraphQLScalarLiteralParser<any>): any
}

