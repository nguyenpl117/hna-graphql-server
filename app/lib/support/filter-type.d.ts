export class FilterType {
    name: string;
    
    description: string;
    
    values(): object
    
    static renderFilterType(name, args): string
}

