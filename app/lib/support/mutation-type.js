'use strict';

import { Field } from '../field';

/**
 * Created by Phan Trung Nguyên.
 * User: nguyenpl117
 * Date: 6/22/2019
 * Time: 9:07 PM
 */
export class MutationType extends Field {
    static iniName = 'mutation';
}