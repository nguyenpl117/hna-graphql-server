import RegisterType from '../register-types';

export class InputType {
    static iniName = 'input';
    
    constructor() {
        this.description = '';
        this.name = '';
        this.input = true;
    }
    
    fields() {
        return {};
    }
    
    types() {
        return `"""${this.description}""" input ${this.name} {${RegisterType.getPropertiesType(this.args())}}`;
    }
}