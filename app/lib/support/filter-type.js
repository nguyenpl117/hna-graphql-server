export class FilterType {
    static iniName = 'filter';
    
    constructor() {
        this.description = '';
        this.name = '';
    }
    
    values() {
        return {
        };
    }
    
    static renderFilterType(name, args, desc) {
        return `
        """${desc}"""
input Filter${name} {
    groups: [FilterGroup${name}]
    operator: Operator
    value: [ValueType]
    field: FilterField${name}
    items: [Filter${name}]
}
input FilterGroup${name} {
    operator: Operator
    items: [Filter${name}]
}
enum FilterField${name}{
    ${args}
}
    `
    }
}

