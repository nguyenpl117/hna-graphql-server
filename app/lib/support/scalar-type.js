export class ScalarType {
    static iniName = 'scalar';
    
    constructor() {
        this.description = '';
        this.name = '';
    }
    
    serialize(value) {
        return value;
    }
    
    parseValue(value) {
        return value;
    }
    
    parseLiteral(ast) {
        return ast;
    }
}

