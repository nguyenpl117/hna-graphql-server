export class SortType {
    name: string;

    description: string;

    fields(): object

    types(): string
}

