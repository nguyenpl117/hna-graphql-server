'use strict';

import { Field } from '../field';

/**
 * Created by Phan Trung Nguyên.
 * User: nguyenpl117
 * Date: 6/22/2019
 * Time: 8:55 PM
 */
export class QueryType extends Field {
    static iniName = 'query';
}