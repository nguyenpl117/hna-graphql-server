export default {
    array_wrap(value, split = '') {
        if (!value) {
            return [];
        }

        return !Array.isArray(value) ? split ? value.split(split) : [value] : value;
    }
}