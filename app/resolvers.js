import {GraphQLInputInt} from 'graphql-input-number';

const {getFields} = require('./lib/resolve-info');
const TimestampType = require('./src/type/definition/Timestamp');
const uidType = require('./src/type/definition/uid');
const statusType = require('./src/type/definition/Status');
const jsonType = require('./src/type/definition/Json');
const anyType = require('./src/type/definition/Any');
const ValueType = require('./src/type/definition/ValueType');
const jwt = require('jsonwebtoken');
const md5 = require("crypto-js/md5");
import {resolvers} from './src/type/index';
import {resolversQuery} from './src/query/index';
import {resolversMutation} from './src/mutation/index';
import Arr from './lib/arr';
import { Filters } from './lib/filters';
import {resolversSubcription, schemaSubcription} from './src/subscription';
/*import {pubsub} from './lib/pubsub';
import {NOTIFICATION_ADDED} from './src/subscription/notifications';
import {COMMENT_ADDED} from './src/subscription/comments';*/

const LimitAmount = GraphQLInputInt({
    name: 'LimitAmount',
    description: `Là số lượng kết quả được trả trên 1 page. Tối thiểu là 1 và tối đa là 100`,
    min: 1,
    max: 100,
});
const resolversType = {};
for (let i in resolvers) {
    resolversType[i] = {};
    for (let j in resolvers[i]) {
        if (typeof resolvers[i][j] === 'string') {
            resolversType[i][j] = resolvers[i][j];
            continue;
        }

        resolversType[i][j] = (parent, args, context, info) => {
            if (args) {
                const order = Arr.array_wrap(args.sortBy).map(item => {
                    for (let i in item) {
                        return [i, item[i]];
                    }
                });
                args.order = order.length ? order : [['id','DESC']];
                
                const { where, include } = Filters.compieFilter(args.filter);
                args.where = where;
                args.include = include;
            }

            return resolvers[i][j](parent, args, context, info);
        };
    }
}

module.exports = {
    Timestamp: TimestampType,
    ID_CRYPTO: uidType,
    STATUS: statusType,
    JSON: jsonType,
    Any: anyType,
    ValueType: ValueType,
    LimitAmount: LimitAmount,

    SortValue: {
        ASC: 'ASC',
        DESC: 'DESC',
        ascend: 'ASC',
        descend: 'DESC',
        ascending: 'ASC',
        descending: 'DESC',
    },
    Operator: {
        contains: 'like',
        AND: 'and',
        OR: 'or'
    },
    // FilterFieldPost: {
    //     slug: 'slugable.slug'
    // },
    ...resolversType,
    Query: {
        test: () => {
            return {
                message: 'success'
            };
        },
        ...resolversQuery
    },
    Mutation: {
        test: () => {
            return {
                message: 'success'
            };
        },
        ...resolversMutation
    },
    Subscription: {
        ...resolversSubcription,
        test: {
            // Additional event labels can be passed to asyncIterator creation
            subscribe: () => pubsub.asyncIterator(['test']),
        }
    },
};