require('dotenv').config();
const rateLimit = require("express-rate-limit");
const url = require('url');

export const apiLimiter = rateLimit({
    windowMs: 10 * 60 * 1000, // 1 minutes
    max: 100,
    onLimitReached: function(req, res, optionsUsed) {
        console.log(optionsUsed.message);
    },
    handler: function(req, res) {
        res.json({
            errors: [
                {
                    message: 'Too many requests, please try again later.'
                }
            ]
        });
    },
    skip: function(req) {
        if (!req.headers.origin) {
            return false;
        }

        const host = url.parse(req.headers.origin);
    
        if (['localhost', '127.0.0.1'].indexOf(host.hostname) === -1) {
            return false;
        }
        
        return true;
    },
    keyGenerator: function (req) {
        return req.clientIp;
    }
});

export const createAccountLimiter = rateLimit({
    windowMs: 60 * 60 * 1000, // 1 hour window
    max: 5, // start blocking after 5 requests
    // message: "Too many accounts created from this IP, please try again after an hour"
    message: "Quá nhiều tài khoản được tạo ra từ IP này, Vui lòng thử lại sau 1 giờ."
});