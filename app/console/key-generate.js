'use strict';


const CommandCli = require('../lib/command-cli');

/**
 * Created by Phan Trung Nguyên.
 * User: nguyenpl117
 * Date: 6/16/2019
 * Time: 4:17 PM
 */
const fs = require('fs');

class KeyGenerate extends CommandCli {

    signature() {
        return 'key:generate';
    }

    options() {
        return [
            ['-s, --show', 'Hiển thị khóa thay vì sửa đổi tập tin.'],
            ['--force', 'Buộc tạo ra khóa mới.'],
        ];
    }

    description() {
        return 'Đặt khóa ứng dụng';
    }

    async handle(options) {
        console.info('Tạo khóa ứng dụng.');

        let key = await new Promise(resolve => {
            require('crypto').randomBytes(32, function(err, buffer) {
                resolve('base64:' + buffer.toString('base64'));
            });
        })

        if (options.show) {
            return console.info(key);
        }

        let file_env = fs.readFileSync('./.env');

        fs.writeFile("./.env", file_env.toString('UTF-8').replace(/(APP_KEY=(.*))/ig, `APP_KEY=${key}`), 'utf8', function(err) {
            if(err) {
                return console.log(err);
            }

            console.info("Tạo khóa ứng dụng thành công.");
        });
    }
}

module.exports = KeyGenerate;