'use strict';


const CommandCli = require('../lib/command-cli');

/**
 * Created by Phan Trung Nguyên.
 * User: nguyenpl117
 * Date: 6/16/2019
 * Time: 4:17 PM
 */
const fs = require('fs');

var RSA = require('hybrid-crypto-js').RSA;
var rsa = new RSA();

class Keys extends CommandCli {

    signature() {
        return 'keys';
    }

    options() {
        return [
            ['--force', 'Buộc tạo ra khóa mã hóa mới.'],
            ['--length [length]', 'Độ dài của private key', 4096],
        ];
    }

    description() {
        return 'Tạo các khóa mã hóa để xác thực API';
    }

    async handle(options) {
        /*if (!await this.confirmToProceed()) {
            return;
        }*/

        console.info(
            'Chúng tôi đang tạo tập tin khóa mã hóa, bạn vui lòng chờ một chút...');

        let keypair = await new Promise(resolve => {
            // Generate 4096 bit RSA key pair
            rsa.generateKeypair(function(key) {
                resolve(key);
            }, options.length ? options.length : 4096);
        });

        let [publicKey, privateKey] = [
            './oauth-public.key',
            './oauth-private.key'
        ];

        if ((fs.existsSync(publicKey) || fs.existsSync(privateKey)) && !options.force) {
            return console.warn('Đã tồn tại private key. Sử dụng tùy chọn --force để ghi đè lên chúng.');
        }

        fs.writeFileSync(publicKey, keypair.publicKey);
        fs.writeFileSync(privateKey, keypair.privateKey);

        console.info('Khóa mã hóa được tạo thành công.');
    }
}

module.exports = Keys;