const Serialize = require('php-serialize');

const Redis = require('ioredis');
const redis = new Redis({
    port: process.env.REDIS_PORT,          // Redis port
    host: process.env.REDIS_HOST,   // Redis host
    family: 4,           // 4 (IPv4) or 6 (IPv6)
    password: process.env.REDIS_PASSWORD,
    db: process.env.REDIS_DB
});

class SendQueuedMailable {
    constructor(props) {
        this.data = props;
    }
}

export const Queue = {
    redisPush (name, object) {
        const command = Serialize.serialize({mailable: object}, {'Illuminate\\Mail\\SendQueuedMailable': SendQueuedMailable});
        
        let data = {
            job: 'Illuminate\\Queue\\CallQueuedHandler@call',
            data: {
                commandName: object.name,
                command
            },
            id: Date.now(),
            attempts: 1
        };
    
        redis.rpush('queues:default', JSON.stringify(data), (err, replay) => {
            // Queue pushed
        });
    }
};