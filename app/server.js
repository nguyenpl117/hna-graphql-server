import { GraphQLError } from 'graphql';
import http from 'http';
require('dotenv').config();
const express = require('express');
const { ApolloServer, gql, ApolloError } = require('apollo-server-express');
const typeDefs = require("./schema");
const resolvers = require("./resolvers");
const db = require("./models");
const jwt = require('jsonwebtoken');
import depthLimit from 'graphql-depth-limit';
import costAnalysis from 'graphql-cost-analysis'
import Auth from './lib/auth';
import {apiLimiter} from './middleware/rete-limit';
import GraphQLExceptions from './exceptions/graphQL-exceptions';
import CostDirective from './lib/directive/cost-directive';
import { json } from 'body-parser';
import {ArrayCache} from './lib/array-cache';
import CacheIpView from './lib/cache-ip-view';
import {pubsub} from './lib/pubsub';
import {Cache} from './cache/cacheManage';
const requestIp = require('request-ip');
const url = require('url');
const responseTime = require('response-time')
const path = require('path');
import moment from 'moment';
moment.tz.setDefault("Asia/Ho_Chi_Minh");

const createError = require('http-errors');

const costAnalyzer = costAnalysis({
    maximumCost: 2000,
    defaultCost: 1,
    onComplete: (cost) => {
        // console.log('query cost: ', cost);
    }
})

const cacheManage = Cache.driver(process.env.CACHE_DRIVER || 'array');

const server = new ApolloServer({
    engine: process.env.ENGINE === 'true',
    tracing: process.env.TRACING === 'true',
    typeDefs: typeDefsPath(),
    resolvers,
    schemaDirectives: {
        cost: CostDirective
    },
    cacheControl: false,
    subscriptions: {
        path: '/graphql',
        onConnect: async (connectionParams, websocket, context) => {
            Auth.init();
            if (connectionParams.authorization) {
                Auth.setUser(await getUser(connectionParams.authorization));
                return {
                    db,
                    pubsub,
                    cache: cacheManage,
                    req:null,
                    user: Auth.user()
                };
            }
        }
    },
    // debug: true,
    // cache: true,
    validationRules: [
        depthLimit(5),
        costAnalyzer
    ],
    formatError: GraphQLExceptions.formatError,
    context: async ({req, connection }) => {
        db.sequelize.query("SET CHARACTER SET utf8mb4");
        if (!req) {
            return {
                req,
                db,
                pubsub,
                cache: cacheManage,
                user: Auth.user()
            };
        }
        Auth.init();
        Auth.setUser(await getUser(req.headers.authorization));
    
        return {
            db,
            pubsub,
            cache: cacheManage,
            req,
            user: Auth.user()
        };
    },
    playground: true,
    introspection: true
});

async function getUser(authorization) {
    try {
        const decoded = jwt.decode(authorization.replace(/^(Bearer\s)/g, ''), {complete: true});
    
        if (decoded.payload.exp * 1000 < (new Date().getTime())) {
            return null;
        }
    
        return db.user.findOne({where: {id: decoded.payload.id}, logging: false}).then(data => (data && data.toJSON()));
    } catch (e) {
    }

    return null;
}

const app = express();
app.use('/graphql', responseTime((req, res, time) => {
    if (time> 200) console.log(time);
}));

app.use('/graphql', requestIp.mw());
app.use('/graphql',json({}));
// app.use('/graphql', apiLimiter);

let counter = 0;
setInterval(() => {
    // console.log(counter);
    counter = 0;
}, 1000);
app.use('/graphql', (req, res, next) => {
    counter++;
    ArrayCache.init();
    CacheIpView.init({
        timeout: 1000 * 60
    });

    if (!req.headers.origin) {
        return next();
    }

    const host = url.parse(req.headers.origin);
    let maxBatch = 10;
    if (['localhost', '127.0.0.1'].indexOf(host.hostname) !== -1) {
        maxBatch = 20;
    }

    if (Array.isArray(req.body) && req.body.length > maxBatch) {
        return next(createError('request batch too many', { limit: maxBatch }));
    }
    return next();
});

server.applyMiddleware({ app, path: '/graphql', bodyParserConfig: {limit: "100kb"} });
const httpServer = http.createServer(app);
server.installSubscriptionHandlers(httpServer);

app.get('/graphiql', (req, res) => {
    res.sendFile(path.join(__dirname+'/graphiql.html'));
})
app.use('/graphql', (err, req, res, next) => {
    console.error(err);
    // if (err) {
    //     res.status(err.status).json({errors: [err]});
    // }
});

app.use(express.static("app/public"));

const host = process.env.HOST || '127.0.0.1';
const port = process.env.PORT || '8000';
// Listen the server
httpServer.listen(port, host, () => {
    console.log(`🚀 Server ready at http://localhost:${port}/graphiql`)
    console.log(`🚀 Subscriptions ready at ws://localhost:${port}${server.subscriptionsPath}`)
})

// app.listen(port, host, function() {
//     console.log(`Server listening on http://${host}:${port}/graphql`);
// });