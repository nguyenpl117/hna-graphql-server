
class GraphQLExceptions {
    static formatError(error) {
        try {
            if (error.originalError.name === 'ValidationError') {
                error.validation = error.originalError.getValidatorMessages();
            }
            if (error.originalError.name === 'ApiValidationError') {
                error.validation = error.originalError.getValidatorMessages();
            }
    
            if (process.env.NODE_ENV === 'production') {
                delete error.extensions;
            }
        } catch (e) {
        
        }

        return error;
    }
}

export default GraphQLExceptions;