
export class InvalidOperationException extends Error{
    constructor(message) {
        super(message);

    }

}

export class AuthorizationError extends Error{
    constructor(message = 'Unauthorized') {
        super(message);

    }
}

export class AuthenticationException extends Error{
    constructor(message = 'Unauthenticated') {
        super(message);

    }
}