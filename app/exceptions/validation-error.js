
class ValidationError extends Error{
    
    constructor(message) {
        super(message);
        this.validator;
        this.name = 'ValidationError';
    }
    
    setValidator(validator)
    {
        this.validator = validator;
        
        return this;
    }
    
    getValidatorMessages()
    {
        return this.validator ? this.validator.errors.all() : [];
    }
}

export default ValidationError;