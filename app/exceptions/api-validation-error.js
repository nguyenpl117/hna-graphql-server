
class ApiValidationError extends Error{
    
    constructor(message) {
        super(message);
        this.validator;
        this.name = 'ApiValidationError';
    }
    
    setValidator(validator)
    {
        this.validator = validator;
        
        return this;
    }
    
    getValidatorMessages()
    {
        return this.validator;
    }
}

export default ApiValidationError;