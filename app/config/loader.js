import {merge} from '../lib/utils';

const list = [
    '../packages/user/index.js'
];

if (process.env.MODULE_NEWS === 'true') {
    list.push('../packages/news/index.js');
}

if (process.env.MODULE_SHOPS === 'true') {
    list.push('../packages/shops/index.js');
}
if (process.env.MODULE_STATUS === 'true') {
    list.push('../packages/status/index.js');
}

let loader = {};
for (let item of list) {
    loader = merge(loader, require(item));
}

module.exports = loader