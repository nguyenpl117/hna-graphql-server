import {merge, withAsync} from '../../lib/utils';
import ValidationError from '../../exceptions/validation-error';
import Validator from '../../lib/validate/validator';
import {
    AuthenticationException,
    AuthorizationError
} from '../../exceptions/errors';
import { getFields } from '../../lib/resolve-info';
import { Resource } from '../../lib/resource';
import RegisterType from '../../lib/register-types';
import Arr from '../../lib/arr';

const fs = require('fs');
const path = require('path');
const basename = path.basename(__filename);
const {Op} = require('sequelize');



// Các loại mutation
export const TypeMutation = {
    DELETE: 'delete',
    UPDATE: 'update',
    CREATE: 'create'
}

/**
 * Tạo mới dữ liệu
 * @param parent
 * @param args
 * @param context
 * @param info
 * @param repo
 * @param resolve
 * @return {Promise<void|PromiseLike<T|never>|Promise<T|never>|*>}
 */
async function create(parent, args, context, info, {repo, resolve}) {
    // Các attribute có thể lấy của model
    const attributes = getFields(info, repo.model);

    // Gọi tới create của repo
    const obj = await repo.create(resolve ? resolve(args) : args);
    
    // Lấy dữ liệu sau khi xử lý xong.
    return repo.findOne({where: {id: obj.id}, ...attributes});
}

/**
 * Cập nhật dữ liệu
 * @param parent
 * @param args
 * @param context
 * @param info
 * @param repo
 * @param resolve
 * @return {Promise<void|PromiseLike<T|never>|Promise<T|never>|*>}
 */
async function update(parent, args, context, info, {repo, resolve}) {
    // Các attribute có thể lấy của model
    const attributes = getFields(info, repo.model);
    
    // Gọi tới update của repo
    await repo.update(resolve ? resolve(args) : args, args.id);
    
    // Lấy dữ liệu sau khi xử lý xong.
    return repo.findOne({where: {id: args.id}, ...attributes});
}

/**
 * Xóa dữ liệu
 * @param parent
 * @param args
 * @param context
 * @param info
 * @param repo
 * @return {PromiseLike<({status, message, data}|{status, message}) | never> | Promise<({status, message, data}|{status, message}) | never>}
 */
function resolve_delete(parent, args, context, info, {repo}) {
    console.log('test delete');
    // Gọi tới hàm xóa nhiều ID của repo
    return repo.destroy(args.id).then(data => {
        return Resource.delete(data);
    });
}

/**
 * Lấy rules ra từ các args
 * @param object
 */
function getRules(object) {
    let args = {};

    for (let item in object) {
        // Nếu phần tử có attribute rules
        if (typeof object[item] === 'object' && object[item].rules) {
            args[item] = object[item].rules;
        }
    
        // Xử lý phần tử có kiểu dữ liệu là 1 input type
        args = merge(RegisterType.rules(object[item].type, item), args);
    }

    return args;
}

/**
 * Tạo args cho schema
 * @param object
 * @return {string}
 */
function compieArgs(object) {
    const args = [];

    for (let item in object) {
        if (typeof object[item] === 'string') {
            args.push(`${item}: ${object[item]}`);
        } else if (typeof object[item] === 'object' && object[item].type) {
            
            // nếu có comment
            if (object[item].description) {
                args.push(`"""${object[item].description}"""`);
            }
            
            // Nếu có giá trị mặc định
            if (object[item].hasOwnProperty('defaultValue')) {
                args.push(`${item}: ${object[item].type}=${object[item].defaultValue}`);
            } else {
                args.push(`${item}: ${object[item].type}`);
            }
        }
    }

    return args.length ? `(${args.join(', ')})` : '';
}

function buildMutation(mutationResolveData) {
    const schemaQueries = [];
    let resolversQuery = {};
    
    const mutationResolve = new mutationResolveData();
    
    // Nếu mutation này có comment
    if (mutationResolve.description) {
        schemaQueries.push(`"""${mutationResolve.description}"""`);
    }
    // Thêm mutation vào schema
    schemaQueries.push(`${mutationResolve.name}${compieArgs(mutationResolve.args())}: ${mutationResolve.type()}`);
    
    // Thêm resolve vào resolvers
    resolversQuery = {
        ...resolversQuery,
        [mutationResolve.name]: async (parent, args, context, info) => {
            // Nếu mutation yêu cầu xác thực người dùng. Mà không có người dùng thì sẽ báo lỗi chưa xác thực.
            if (mutationResolve.authentication && !context.user) {
                throw new AuthenticationException('Unauthenticated');
            }
    
            if (Array.isArray(mutationResolve.middleware)) {
                await Promise.all(mutationResolve.middleware.map(middleware => {
                    return middleware(parent, args, context, info);
                }));
            }
            
            // Lấy rules ra từ các args
            let rules = getRules(mutationResolve.args());
            
            // Nếu viết riêng rules thì merge với rules đã lấy được từ args
            if (typeof mutationResolve.rules === 'function') {
                rules = merge(await withAsync(mutationResolve.rules(args)), rules);
            }
            
            // Custom message thông báo lỗi validate
            let validation = new Validator(args, rules, mutationResolve.validationErrorMessages(args));
            
            // Check validate.
            await new Promise((resolve, reject) => {
                validation.checkAsync(() => {
                    resolve(true);
                }, () => {
                    reject({});
                });
            }).catch(err => {
                throw (new ValidationError('validation')).setValidator(validation);
            })
            
            // Check quyền của người dùng. false sẽ báo lỗi
            if (!await mutationResolve.authorize(args)) {
                throw new AuthorizationError('Unauthorized');
            }
            
            try {
                const body = Arr.array_wrap(context.req.body);
                for (let item of body) {
                    if (info.operation.name.value === item.operationName) {
                        info.variableValues = item.variables;
                    }
                }
            } catch (e) {
            
            }
            
            // xử lý resolve
            const mutation = mutationResolve.resolve(parent, args, context, info);
            
            // Nếu resolve là 1 promise thì xử lý luôn promise
            if (mutation instanceof Promise) {
                return mutation;
            }
            
            // Nếu resolve là 1 object. Với các type TypeMutation thì xử lý theo các hàm viết sẵn.
            if (mutation.type === TypeMutation.DELETE) {
                return resolve_delete(parent, args, context, info, mutation);
            } else if (mutation.type === TypeMutation.UPDATE) {
                return update(parent, args, context, info, mutation);
            } else if (mutation.type === TypeMutation.CREATE) {
                return create(parent, args, context, info, mutation);
            }
        }
    };
    
    return {
        schemaMutation: schemaQueries,
        resolversMutation: resolversQuery
    };
}

module.exports = {
    buildMutation
};