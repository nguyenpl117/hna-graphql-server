import Arr from '../../lib/arr';
import {
    AuthenticationException,
    AuthorizationError
} from '../../exceptions/errors';
import { Filters } from '../../lib/filters';
import {getFieldSelection} from '../../lib/resolve-info';
const { withFilter } = require('apollo-server');
import {pubsub} from '../../lib/pubsub';
const fs = require('fs');
const path = require('path');
const basename = path.basename(__filename);
const schemaQueries = [];
let resolversQuery = {};

function compieArgs(object) {
    const args = [];

    for (let item in object) {
        args.push(`${item}: ${object[item]}`);
    }

    return args.length ? `(${args.join(', ')})` : '';
}

function buildSubscription(subscriptions) {
    subscriptions.forEach(file => {
        const query = new (require(file));
        schemaQueries.push(`${query.name}${compieArgs(query.args())}: ${query.type()}`);
        resolversQuery = {
            ...resolversQuery,
            [query.name]: {
                resolve: (parent, args, context, info) => {
                    // console.log(parent);
                    const time = Date.now();
                    const key = [JSON.stringify(args), JSON.stringify(getFieldSelection(info, 5))];
                    
                    if (query.authentication && !context.user) {
                        throw new AuthenticationException('Unauthenticated');
                    }
                    
                    const order = Arr.array_wrap(args.sortBy).map(item => {
                        for (let i in item) {
                            return [i, item[i]];
                        }
                    });
                    args.order = order.length ? order : [['id','DESC']];
                    const { where, include } = Filters.compieFilter(args.filter);
                    args.where = where;
                    args.include = include;
                    
                    if (!query.authorize(args)) {
                        throw new AuthorizationError('Unauthorized');
                    }
                    
                    return query.resolve(parent, args, context, info);
                },
                subscribe: withFilter(
                    (_, args) => {
                        return pubsub.asyncIterator(query.subscribe(args))
                    },
                    (payload, variables) => {
                        return query.filter(payload, variables);
                    }
                )
            }
        };
    });
    
    return {
        schemaSubcription: schemaQueries,
        resolversSubcription: resolversQuery
    };
}

module.exports = {
    buildSubscription
};