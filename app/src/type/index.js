import { merge } from '../../lib/utils';
import RegisterType from '../../lib/register-types';

const fs = require('fs');
const path = require('path');
const basename = path.basename(__filename);
const FilterType = require('./filter-type-input/index');
const SortType = require('./sort-type/index');
const UnionType = require('./union/index');
const EnumType = require('./enum/index');
const InputType = require('./input/index');
const types = [];
const resolvers = {};

const {graphqlType} = require('../../config/loader');

graphqlType
.forEach(file => {
    RegisterType.add(require(file).default);
    // push_object(require(`./${file}`))
});

module.exports = merge({
    types: RegisterType.types(),
    resolvers: RegisterType.resolvers()
}, FilterType, UnionType, EnumType, SortType);