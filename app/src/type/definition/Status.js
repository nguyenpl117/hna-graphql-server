import { GraphQLScalarType } from 'graphql';
const moment = require('moment');

module.exports = new GraphQLScalarType({
    name: 'STATUS',
    description: 'Date format W3C',
    serialize(value) {
        return value;
    },
    parseValue(value) {
        return value;
    },
    parseLiteral(ast) {
        switch (ast.kind) {
            // Implement your own behavior here by returning what suits your needs
            // depending on ast.kind
            case 'StringValue':
                return ast.value;
            case 'IntValue':
                return ast.value;
            default:
                return null;
        }
    }
});