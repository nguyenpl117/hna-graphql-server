import { GraphQLScalarType } from 'graphql';

module.exports = new GraphQLScalarType({
    name: 'ValueType',
    description: `The ID scalar type represents a unique identifier,
     often used to refetch an object or as key for a cache.
      The ID type appears in a JSON response as a String; however,
      it is not intended to be human-readable. When expected as an input type,
      any string (such as "4") or integer (such as 4) input value will be accepted as an ID.`,
    serialize(value) {
        let result;
        // Implement your own behavior here by setting the 'result' variable
        return value ? value.toString() : null;
    },
    parseValue(value) {
        let result;
        // Implement your own behavior here by setting the 'result' variable
        return value ? value.toString() : null;
    },
    parseLiteral(ast) {
        switch (ast.kind) {
            // Implement your own behavior here by returning what suits your needs
            // depending on ast.kind
            case 'StringValue':
                return ast.value;
            case 'IntValue':
                return ast.value;
            default:
                return ast.value;
        }
    }
});