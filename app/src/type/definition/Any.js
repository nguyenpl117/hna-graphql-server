import { GraphQLScalarType } from 'graphql';

module.exports = new GraphQLScalarType({
    name: 'Any',
    description: 'Any data',
    serialize(value) {
        if (typeof value === 'string') {
            return JSON.parse(value);
        }
        return value;
    },
    parseValue(value) {
        return value;
    },
    parseLiteral(ast) {
        switch (ast.kind) {
            // Implement your own behavior here by returning what suits your needs
            // depending on ast.kind
        }
    }
});