class PaginationType {
    static render(name) {
        return `
type ${name}Pagination {
    """Số mục bắt đầu"""
    from: Int
   """Số mục kết thúc"""
    to: Int
    """Số lượng trên trang"""
    perPage: Int!
    """Trang hiện tại"""
    currentPage: Int!
    """Tổng số"""
    total: Int!
    data: [${name}]
}
    `;
    }
    
    static has(name) {
        if (!this.data) {
            this.data = [];
        }
        return this.data.indexOf(name) !== -1;
    }

    static push(name) {
        if (!this.data) {
            this.data = [];
        }
        this.data.push(name);
    }

    static types() {
        if (!this.data) {
            this.data = [];
        }
        return this.data.map(x => (this.render(x)));
    }
}

export default PaginationType;