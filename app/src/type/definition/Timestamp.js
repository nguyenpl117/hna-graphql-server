import { GraphQLScalarType } from 'graphql';
const moment = require('moment');
module.exports = new GraphQLScalarType({
    name: 'Timestamp',
    description: 'Date format W3C',
    serialize(value) {
        let result;
        // Implement your own behavior here by setting the 'result' variable
        return moment(value).format();
    },
    parseValue(value) {
        let result;
        // Implement your own behavior here by setting the 'result' variable
        return result;
    },
    parseLiteral(ast) {
        // console.log(ast.value);
        return ast.value;
        switch (ast.kind) {
            // Implement your own behavior here by returning what suits your needs
            // depending on ast.kind
        }
    }
});