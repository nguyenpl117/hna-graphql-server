import { GraphQLScalarType } from 'graphql';

module.exports = new GraphQLScalarType({
    name: 'ID_CRYPTO',
    description: `Number Or String`,
    serialize(value) {
        let result;
        // Implement your own behavior here by setting the 'result' variable
        return (value !== null || value !== undefined) ? value.toString() : null;
    },
    parseValue(value) {
        let result;
        // Implement your own behavior here by setting the 'result' variable
        return (value !== null || value !== undefined) ? value.toString() : null;
    },
    parseLiteral(ast) {
        switch (ast.kind) {
            // Implement your own behavior here by returning what suits your needs
            // depending on ast.kind
            case 'StringValue':
                return ast.value;
            case 'IntValue':
                return ast.value;
            default:
                return null;
        }
    }
});