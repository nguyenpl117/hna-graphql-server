import RegisterType from '../../../lib/register-types';

const fs = require('fs');
const path = require('path');
const basename = path.basename(__filename);
const types = [];
const resolvers = {};

const {unionType} = require('../../../config/loader');

unionType
.forEach(file => {
    RegisterType.add(require(file).default);
});

module.exports = {
    types,
    resolvers
};