const fs = require('fs');
const path = require('path');
const basename = path.basename(__filename);
const types = [];
const resolvers = {};
const {sortType} = require('../../../config/loader');

sortType
.forEach(file => {
    push_object(require(file))
});

function push_object({resolve, type}) {
    types.push(type);
    
    for (let i in resolve) {
        resolvers[i] = resolve[i];
    }
}

module.exports = {
    types,
    resolvers
};