const fs = require('fs');
const path = require('path');
const basename = path.basename(__filename);
const types = [];
const resolvers = {};
const {filterType} = require('../../../config/loader');

filterType
.forEach(file => {
    push_object(require(file))
});

function push_object({resolve, name, args}) {
    types.push(render(name, args));
    
    for (let i in resolve) {
        resolvers[i] = resolve[i];
    }
}

function render(name, args) {
    return `
input Filter${name} {
    groups: [FilterGroup${name}]
    operator: Operator
    value: [ValueType]
    field: FilterField${name}
    items: [Filter${name}]
}
input FilterGroup${name} {
    operator: Operator
    items: [Filter${name}]
}
enum FilterField${name}{
    ${args}
}
    `
}

module.exports = {
    types,
    resolvers
};