import Arr from '../../lib/arr';
import {
    AuthenticationException,
    AuthorizationError
} from '../../exceptions/errors';
import { Filters } from '../../lib/filters';
import {getFieldSelection} from '../../lib/resolve-info';
import {schemaMutation} from '../mutation';
import {merge, withAsync} from '../../lib/utils';
import Validator from '../../lib/validate/validator';
import ValidationError from '../../exceptions/validation-error';
import RegisterType from '../../lib/register-types';
const fs = require('fs');
const path = require('path');
const basename = path.basename(__filename);


/**
 * Lấy rules ra từ các args
 * @param object
 */
function getRules(object) {
    let args = {};
    
    for (let item in object) {
        // Nếu phần tử có attribute rules
        if (typeof object[item] === 'object' && object[item].rules) {
            args[item] = object[item].rules;
        }
        
        // Xử lý phần tử có kiểu dữ liệu là 1 input type
        args = merge(RegisterType.rules(object[item].type, item), args);
    }
    
    return args;
}

function compieArgs(object) {
    const args = [];
    
    for (let item in object) {
        if (typeof object[item] === 'string') {
            args.push(`${item}: ${object[item]}`);
        } else if (typeof object[item] === 'object' && object[item].type) {
            
            // nếu có comment
            if (object[item].description) {
                args.push(`"""${object[item].description}"""`);
            }
            
            // Nếu có giá trị mặc định
            if (object[item].hasOwnProperty('defaultValue')) {
                args.push(`${item}: ${object[item].type}=${object[item].defaultValue}`);
            } else {
                args.push(`${item}: ${object[item].type}`);
            }
        }
    }
    
    return args.length ? `(${args.join(', ')})` : '';
}

function buildQuery(querydata) {
    const schemaQueries = [];
    let resolversQuery = {};
    
    let query = new querydata();

    if (query.description) {
        schemaQueries.push(`"""${query.description}"""`);
    }
    schemaQueries.push(`${query.name}${compieArgs(query.args())}: ${query.type()}`);
    resolversQuery = {
        ...resolversQuery,
        [query.name]: async (parent, args, context, info) => {
            const time = Date.now();
            const key = [JSON.stringify(args), JSON.stringify(getFieldSelection(info, 5))];
            if (query.authentication && !context.user) {
                throw new AuthenticationException('Unauthenticated');
            }
    
            if (Array.isArray(query.middleware)) {
                await Promise.all(query.middleware.map(middleware => {
                    return middleware(parent, args, context, info);
                }));
            }
    
            let sortType = await RegisterType.getType(query.args().sortBy, 'sort');
    
            let fields = {};
            if (sortType) {
                fields = sortType.fields();
            }
            
            const order = await Promise.all(Arr.array_wrap(args.sortBy).map(async item => {
                for (let i in item) {
                    if (typeof fields[i].map === 'function') {
                        return [await fields[i].map(), item[i]];
                    }
                    return [i, item[i]];
                }
            }));
            
            args.order = order.length ? order : [];
            const { where, include } = await Filters.compieFilter(args.filter);
            args.where = where;
            args.include = include;
            
            // Lấy rules ra từ các args
            let rules = getRules(query.args());
            
            // Nếu viết riêng rules thì merge với rules đã lấy được từ args
            if (typeof query.rules === 'function') {
                rules = merge(await withAsync(query.rules(args)), rules);
            }
            
            // Custom message thông báo lỗi validate
            let validation = new Validator(args, rules, query.validationErrorMessages(args));
            
            // Check validate.
            await new Promise((resolve, reject) => {
                validation.checkAsync(() => {
                    resolve(true);
                }, () => {
                    reject({});
                });
            }).catch(err => {
                throw (new ValidationError('validation')).setValidator(validation);
            })
            
            if (!query.authorize(args)) {
                throw new AuthorizationError('Unauthorized');
            }
            
            try {
                const body = Arr.array_wrap(context.req.body);
                for (let item of body) {
                    if (info.operation.name.value === item.operationName) {
                        info.variableValues = item.variables;
                    }
                }
            } catch (e) {
            
            }
            
            const docs = await query.resolve(parent, args, context, info);
            
            const times = Date.now() - time;
            if (times > 100) console.log('time query', times, query.name);
            return docs;
        }
    };
    
    return {
        schemaQueries,
        resolversQuery
    };
}

module.exports = {buildQuery};