import Arr from '../../lib/arr';

const db = require('../../models/index');
import {Model} from 'sequelize';
import sequelizeErrors from 'sequelize/lib/errors';
import _ from 'lodash';

class BaseRepository {
    constructor() {
    }
    
    /**
     * Tạo mới transaction
     * @param options
     * @param callback
     * @return {Promise<*>}
     */
    static async transaction(options, callback) {
        if (typeof options === 'function') {
            callback = options;
            options = undefined;
        }
        
        let transaction = {};
        
        if (db.namespace.get('transaction')) {
            transaction = {
                transaction: db.namespace.get('transaction')
            };
        }
        
        if (!options) {
            options = {
                ...transaction
            };
        } else {
            options = {
                ...transaction,
                ...options
            };
        }
        return this.model.sequelize.transaction(options, callback);
    }
    
    /**
     * @return {{}|*}
     */
    static get models() {
        return db.sequelize.models;
    }
    
    /**
     * @return {{eq, ne, gte, gt, lte, lt, not, is, in, notIn, like, notLike, iLike, notILike, regexp, notRegexp, iRegexp, notIRegexp, between, notBetween, overlap, contains, contained, adjacent, strictLeft, strictRight, noExtendRight, noExtendLeft, and, or, any, all, values, col, placeholder, join, raw}|*}
     * @constructor
     */
    static get Op() {
        return db.sequelize.Op;
    }
    
    /**
     *
     * @return Model
     */
    static get model() {
        if (!this.modelName || !db[this.modelName()]) {
            throw new sequelizeErrors.BaseError('Can not find model');
        }
        
        return db[this.modelName()];
    }
    
    /**
     *
     * @param options
     * @return {Promise<{}>}
     */
    static paginate(options) {
        return this.model.paginate(options);
    }
    
    /**
     *
     * @param options
     * @return {void|PromiseLike<T | never>|Promise<T | never>|*}
     */
    static findOne(options) {
        return this.model.findOne(options).
            then(data => (data && data.toJSON()));
    }
    
    /**
     *
     * @param options
     * @return {void|PromiseLike<T | never>|Promise<T | never>|*}
     */
    static findAll(options) {
        return this.model.findAll(options).map(data => (data && data.toJSON()));
    }
    
    /**
     *
     * @param attribute
     * @param value
     * @param columns
     * @return {void|PromiseLike<T | never>|Promise<T | never>|*}
     */
    static findBy(attribute, value, columns) {
        return this.model.findOne(
            {where: {[attribute]: value}, attributes: Arr.array_wrap(columns)}).
            then(data => (data && data.toJSON()));
    }
    
    /**
     *
     * @param attribute
     * @param value
     * @param columns
     * @return {void|PromiseLike<T | never>|Promise<T | never>|*}
     */
    static findAllBy(attribute, value, columns) {
        return this.model.findAll(
            {where: {[attribute]: value}, attributes: columns}).
            map(data => (data && data.toJSON()));
    }
    
    /**
     *
     * @param data
     * @return {Promise<data>}
     */
    static create(data) {
        return this.model.create(data);
    }
    
    /**
     *
     * @param data
     * @param id
     * @return {Promise<this>}
     */
    static async update(data, id) {
        data.id = id;
        return _.get(await this.model.update(data,
            {where: {id: id}, individualHooks: true}), '1.0');
    }
    
    static findOrFail(id) {
        return this.model.findOne({where: {id}}).
            then(data => (data && data.toJSON()));
    }
    
    static async destroy(ids, attribute = 'id') {
        const result = [];
    
        let data = await this.model.findAll({
            where: {
                [attribute]: {
                    [db.sequelize.Op.in]: Arr.array_wrap(ids)
                }
            }
        });
    
        if (data && Array.isArray(data)) {
            for (let item of data) {
                const deleted = await this.delete(item[attribute].toString(), attribute);
                if (deleted) {
                    result.push(item[attribute]);
                }
            }
        }
    
        return result.length === 0 ? false : result;
    }
    
    static async delete(id, attribute = 'id') {
        const deleted = await this.model.destroy({
            where: {
                [attribute]: id
            }
        });
        
        return deleted;
    }
    
    /**
     *
     * @return {string}
     */
    static getTable() {
        return this.model.getTableName();
    }
}

export default BaseRepository;