import {Types} from '../../../../lib/types';
import ValidationError from '../../../../exceptions/validation-error';
import {Field} from '../../../../lib/field';

const request = require('request');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');

class UserAuth extends Field {
    name = 'auth';

    description = 'Đăng nhập tài khoản.';

    type() {
        return Types.Type('User');
    }

    args() {
        return {
            phone: {
                type: Types.String,
                description: 'Số điện thoại.',
                rules: [
                    'required'
                ]
            },
            password: {
                type: Types.String,
                description: 'Mật khẩu đăng nhập',
                rules: [
                    'required',
                    'string'
                ]
            }
        };
    }
    
    async resolve(parent, args, {db, cache, req}, info) {
        const user = await db.user.findOne({
            where: {
                phone: args.phone
            },
            attributes: { exclude: [] }
        });
    
        if (!user || !bcrypt.compareSync(args.password, user.password)) {
            throw new ValidationError('Sai tài khoản hoặc mật khẩu.');
        }
        
        user.setDataValue('password', null);
        
        user.setDataValue('exp',
            Math.floor((new Date().getTime()) / 1000) + (60 * 60 * 24 * 365));
        
        user.token = jwt.sign(user.toJSON(), 'shhhhh');
        
        return user;
    }
}

module.exports = UserAuth;