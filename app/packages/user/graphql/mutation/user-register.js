import {Field} from '../../../../lib/field';
import {Types} from '../../../../lib/types';
import {requestApi} from '../../../../lib/utils';
import UserRepository from '../../repository/elo/user-repository';
import {getFields} from '../../../../lib/resolve-info';
import Rule from '../../../../lib/validate/rule';

const request = require('request');

class UserRegister extends Field {
    constructor(props) {
        super(props);
        this.name = 'register';
        this.description = 'Đăng ký';
    }
    
    type() {
        return Types.Type('User');
    }
    
    args() {
        return {
            id: {
                type: Types.ID,
                description: 'ID. uuid.v4()'
            },
            name: {
                type: Types.String,
                description: 'Họ và Tên'
            },
            gender: {
                description: 'giới tính: 1-Nam, 2-Nữ',
                type: Types.Int,
                defaultValue: "1",
                rules: [
                    'in:1,2'
                ]
            },
            phone: {
                type: Types.String,
                description: 'số điện thoại',
                rules: [
                    'required',
                    'between:9,10',
                    Rule.unique('user', 'phone')
                ]
            },
            password: {
                type: Types.String,
                description: 'Mật khẩu đăng nhập',
                rules: [
                    'required',
                    'string',
                    'between:6,32'
                ]
            },
            email: {
                type: Types.String,
                description: 'Email đăng ký'
            }
        };
    }
    
    validationErrorMessages(args) {
        return {
            'between.phone': 'Số điện thoại không hợp lệ.'
        };
    }
    
    async resolve(parent, args, {db, cache, req}, info) {
        const created = await UserRepository.create(args);
    
        const attributes = getFields(info, db.user);
    
        return db.user.findOne({
            where: {
                id: created.id
            },
            attributes
        });
    }
}

module.exports = UserRegister;