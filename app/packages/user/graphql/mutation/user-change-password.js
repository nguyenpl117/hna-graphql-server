import {Field} from '../../../../lib/field';
import {Types} from '../../../../lib/types';
import {requestApi} from '../../../../lib/utils';
import UserRepository from '../../repository/elo/user-repository';
import {getFields} from '../../../../lib/resolve-info';
import Rule from '../../../../lib/validate/rule';
import Auth from '../../../../lib/auth';

class UserChangePassword extends Field {
    constructor(props) {
        super(props);
        this.name = 'userChangePassword';
        this.description = 'Đổi mật khẩu người dùng';
        this.authentication = true;
    }
    
    type() {
        return Types.Type('User');
    }
    
    args() {
        return {
            password: {
                type: Types.String,
                description: 'Mật khẩu mới',
                rules: [
                    'required'
                ]
            },
            passwordConfirmation: {
                type: Types.String,
                description: 'Nhập lại mật khẩu mới',
                rules: [
                    'required'
                ]
            }
        };
    }
    
    rules(args) {
        args.password_confirmation = args.passwordConfirmation;
        return {
        }
    }
    
    async resolve(parent, args, {db, cache, req}, info) {
        
        const updated = await UserRepository.changePassword(args.password, Auth.id());
    
        const attributes = getFields(info, db.user);
    
        return db.user.findOne({
            where: {
                id: Auth.id()
            },
            attributes
        });
    }
}

module.exports = UserChangePassword;