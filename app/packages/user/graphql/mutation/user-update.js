import {Field} from '../../../../lib/field';
import {Types} from '../../../../lib/types';
import {requestApi} from '../../../../lib/utils';
import UserRepository from '../../repository/elo/user-repository';
import {getFields} from '../../../../lib/resolve-info';
import Rule from '../../../../lib/validate/rule';
import Auth from '../../../../lib/auth';

class UserUpdate extends Field {
    constructor(props) {
        super(props);
        this.name = 'updateUser';
        this.description = 'Cập nhật người dùng';
        this.authentication = true;
    }
    
    type() {
        return Types.Type('User');
    }
    
    args() {
        return {
            name: {
                type: Types.String,
                description: 'Họ và tên',
                rules: [
                    'filled'
                ]
            },
            email: {
                type: Types.String,
                description: 'email tài khoản',
                rules: [
                    'email'
                ]
            },
            phone: {
                description: 'số điện thoại',
                type: Types.String,
                rules: [
                    'filled'
                ]
            },
            gender: {
                description: 'giới tính',
                type: Types.Int,
                rules: [
                    'in:1,2,3'
                ]
            },
            dob: {
                description: 'Ngày sinh thần',
                type: Types.Timestamp
            },
            avatar: {
                description: 'đường dẫn avata',
                type: Types.String
            },
            cover: {
                type: Types.String
            }
        };
    }
    
    rules(args) {
        return {
            phone: [
                Rule.unique('user', 'phone').ignore(args.id)
            ]
        }
    }
    
    async resolve(parent, args, {db, cache, req}, info) {
        
        const updated = await UserRepository.update(args, Auth.id());
    
        const attributes = getFields(info, db.user);
    
        return db.user.findOne({
            where: {
                id: Auth.id()
            },
            attributes
        });
    }
}

module.exports = UserUpdate;