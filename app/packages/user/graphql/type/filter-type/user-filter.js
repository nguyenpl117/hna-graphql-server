module.exports = {
    name: 'User',
    args: `
    id
    name
    phone
    email
    avatar
    cover
    password
    dob
    gender
    createdAt
    updatedAt
    `,
    resolve: {}
};