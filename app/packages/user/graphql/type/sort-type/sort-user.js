module.exports = {
    type: `
input SortUser {
    id: SortValue
    name: SortValue
    phone: SortValue
    email: SortValue
    avatar: SortValue
    cover: SortValue
    password: SortValue
    dob: SortValue
    gender: SortValue
    createdAt: SortValue
    updatedAt: SortValue
}
  `,
    resolve: {
        SortUser: {
        }
    }
};