import {Types} from '../../../../../lib/types';
import {GraphqlType} from '../../../../../lib/support/graphql-type';

export default class Delete extends GraphqlType {
    constructor() {
        super();
        this.name = 'Delete';
        this.description = '';
    }
    
    fields() {
        return {
            
            status: {
                type: Types.Type('STATUS'),
                description: 'Trạng thái'
            },
            message: {
                type: Types.String,
                description: 'Thông báo'
            },
            data: {
                type: Types.ListOf('ID'),
                description: 'danh sách các ID đã xóa'
            }
        };
    }
}
        