import {GraphqlType} from '../../../../../lib/support/graphql-type';
import {Types} from '../../../../../lib/types';

export default class User extends GraphqlType {
    constructor() {
        super();
        this.name = 'User';
        this.description = '';
    }
    
    args() {
        return {
            id: {
                type: Types.ID,
                description: ''
            },
            name: {
                type: Types.String,
                description: 'Họ và Tên'
            },
            email: {
                type: Types.String,
                description: 'email '
            },
            avatar: {
                type: Types.String,
                description: 'Ảnh đại diện'
            },
            cover: {
                type: Types.String,
                description: 'Ảnh tường'
            },
            phone: {
                type: Types.String,
                description: 'số điện thoại '
            },
            gender: {
                type: Types.Int,
                description: 'giới tính 1 nam 2 nữ '
            },
            dob: {
                type: Types.Timestamp,
                description: 'ngày sinh '
            },
            token: {
                type: Types.String,
                description: 'Token đăng nhập'
            },
            createdAt: {
                type: Types.Timestamp,
                description: 'thời gian tạo '
            },
            updatedAt: {
                type: Types.Timestamp,
                description: ''
            }
        };
    }
}