import {Types} from '../../../../../lib/types';
import {GraphqlType} from '../../../../../lib/support/graphql-type';

export default class SocialAccount extends GraphqlType {
    constructor() {
        super();
        this.name = 'SocialAccount';
        this.description = '';
    }
    
    args() {
        return {
            
            id: {
                type: Types.ID,
                description: ''
            },
            provider: {
                type: Types.String,
                description: ' facebook , google '
            },
            providerId: {
                type: Types.ID,
                description: ' id của facebook , hoặc google '
            },
            url: {
                type: Types.String,
                description: ' link đên provide '
            },
            createdAt: {
                type: Types.Timestamp,
                description: ''
            },
            updatedAt: {
                type: Types.Timestamp,
                description: ''
            }
        };
    }
}
        