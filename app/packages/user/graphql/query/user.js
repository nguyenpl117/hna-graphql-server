import {Field} from '../../../../lib/field';
import {Types} from '../../../../lib/types';
import {
    getFields,
    getPaginateFields
} from '../../../../lib/resolve-info';
import UserRepository from '../../../user/repository/elo/user-repository';

class Users extends Field {
    constructor(props) {
        super(props);
        this.name = 'user';
        this.description = 'Thông tin thành viên';
        this.authentication=true;
    }

    type() {
        return Types.Type('User');
    }

    args() {
        return Types.ArgsOne('User');
    }

    resolve(parent, args, {db, user, cache}, info) {
        const {order, where, page, limit} = args;
        const attributes = getFields(info, db.user);

        return cache.remember(['users'], [args, attributes], async () => {
            return UserRepository.findOne({
                where,
                order,
                ...attributes
            });
        })

    }
}

module.exports = Users;