import {Field} from '../../../../lib/field';
import {Types} from '../../../../lib/types';
import {
    getPaginateFields
} from '../../../../lib/resolve-info';
import UserRepository from '../../../user/repository/elo/user-repository';
import {pubsub} from '../../../../lib/pubsub';
class Users extends Field {
    constructor(props) {
        super(props);
        this.name = 'users';
        this.description = 'Danh sách thành viên';
        this.authentication=true;
    }

    type() {
        return Types.Pagination('User');
    }

    args() {
        return Types.ArgsPagination('User');
    }

    resolve(parent, args, {db, user, cache}, info) {
        const {order, where, page, limit} = args;

        const attributes = getPaginateFields(info, db.user);

        return cache.remember(['users'], [args, attributes], async () => {
            return UserRepository.paginate({
                where,
                order,
                page: page, // Default 1
                paginate: limit, // Default 25
                ...attributes
            });
        })
    }
}

module.exports = Users;