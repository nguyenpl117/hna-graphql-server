import {Field} from '../../../../lib/field';
import {Types} from '../../../../lib/types';
import {
    getFields,
} from '../../../../lib/resolve-info';
import UserRepository from '../../../user/repository/elo/user-repository';
import Auth from '../../../../lib/auth';

class Users extends Field {
    constructor(props) {
        super(props);
        this.name = 'profile';
        this.description = 'Thông tin cá nhân';
        this.authentication=true;
    }

    type() {
        return Types.Type('User');
    }

    args() {
        return {};
    }

    resolve(parent, args, {db, user, cache}, info) {
        if (!user) {
            return null;
        }
        
        const {order, where, page, limit} = args;
        const attributes = getFields(info, db.user);

        return cache.remember(['users'], [args, attributes], async () => {
            return UserRepository.findOne({
                where: {id: Auth.id()},
                order,
                ...attributes
            });
        })

    }
}

module.exports = Users;