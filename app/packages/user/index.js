'use strict';

/**
 * Created by Phan Trung Nguyên.
 * User: nguyenpl117
 * Date: 5/29/2019
 * Time: 8:42 PM
 */
var fs = require('fs')
    , p = require('path')
;
const path = require('path');
const basename = path.basename(__filename);

// how to know when you are done?
function recursiveReaddirSync(path) {
    var list = []
        , files = fs.readdirSync(path)
        , stats
    ;
    
    files.forEach(function (file) {
        stats = fs.lstatSync(p.join(path, file));
        if(stats.isDirectory()) {
            list = list.concat(recursiveReaddirSync(p.join(path, file)));
        } else {
            list.push(p.join(path, file));
        }
    });
    
    return list;
}

function loadPath(path) {
    if (!fs.existsSync(path)) {
        return [];
    }
    return recursiveReaddirSync(path).filter(file => {
        return (file.indexOf('.') !== 0) && (file !== p.join(__dirname, basename)) && (file.slice(-3) === '.js');
    });
}

const mutation = loadPath(__dirname + '/graphql/mutation');
const query = loadPath(__dirname + '/graphql/query');
const subscriptions = loadPath(__dirname + '/graphql/subscriptions');

const enumType = loadPath(__dirname + '/graphql/type/enum-type');
const filterType = loadPath(__dirname + '/graphql/type/filter-type');
const graphqlType = loadPath(__dirname + '/graphql/type/graphql-type');
const inputType = loadPath(__dirname + '/graphql/type/input-type');
const sortType = loadPath(__dirname + '/graphql/type/sort-type');
const unionType = loadPath(__dirname + '/graphql/type/union-type');

const models = loadPath(__dirname + '/models');

module.exports = {
    mutation,
    query,
    subscriptions,
    enumType,
    filterType,
    graphqlType,
    inputType,
    sortType,
    unionType,
    models
};