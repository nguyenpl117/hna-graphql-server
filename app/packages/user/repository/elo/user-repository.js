import BaseRepository from '../../../../repository/elo/base-repository';
const bcrypt = require('bcrypt');

const uuid = require('uuid');

const saltRounds = 10;

const salt = bcrypt.genSaltSync(saltRounds);

class UserRepository extends BaseRepository {
    static modelName() {
        return 'user';
    }
    
    /**
     * Tạo mới user
     * @param data
     * @return {Promise<data>}
     */
    static async create(data) {
        return this.model.sequelize.transaction(async t => {
            if (!data.id) {
                data.id = uuid.v4();
            }
    
            data.password = bcrypt.hashSync(data.password, salt);
    
            const created = await super.create(data);
    
            return created;
        });
    }
    
    /**
     * Cập nhật user
     * @param data
     * @param id
     * @return {Promise<this>}
     */
    static update(data, id) {
        return super.update(data, id);
    }
    
    /**
     * Đổi mật khẩu
     * @param password
     * @param id
     * @return {Promise<this>}
     */
    static changePassword(password, id) {
        return this.model.update({password: bcrypt.hashSync(password, salt) }, {
            where: {
                id: id
            }
        })
    }
}

export default UserRepository;