'use strict';
module.exports = (sequelize, DataTypes) => {
    const User = sequelize.define('user', {
        name: DataTypes.STRING,
        phone: DataTypes.STRING,
        email: DataTypes.STRING,
        avatar: DataTypes.STRING,
        cover: DataTypes.STRING,
        password: DataTypes.STRING,
        dob: DataTypes.DATE,
        gender: DataTypes.TINYINT,
        createdAt: {
            type: DataTypes.DATE,
            field: 'created_at'
        },
        updatedAt: {
            type: DataTypes.DATE,
            field: 'updated_at'
        }
    }, {
        defaultScope: {
            attributes: { exclude: ['password'] }
        }
    });
    User.associate = function(models) {
        // associations can be defined here
    };
    return User;
};