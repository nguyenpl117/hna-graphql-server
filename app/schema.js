import PaginationType from './src/type/definition/Pagination';

const {types} = require('./src/type/index');
import {schemaQueries} from './src/query/index';
import {schemaMutation} from './src/mutation/index';
import {schemaSubcription} from './src/subscription';

module.exports = `
  directive @cost(useMultipliers: Boolean, complexity: Int, multipliers: [String]) on FIELD_DEFINITION
  
  scalar ValueType
  scalar Timestamp
  scalar ID_CRYPTO
  scalar STATUS
  scalar JSON
  scalar Any
  scalar LimitAmount
  
  enum ContentFormatEnum{
    TEXT
    HTML
  }
  
  enum SortValue {
      ASC
      DESC
      ascend
      descend
      ascending
      descending
  }
  enum Operator{
  """// ="""
    eq
  """// !="""
    ne
    """// >="""
    gte
    """// >"""
    gt
    """// <="""
    lte
    """// <"""
    lt
    """// NOT"""
    not
    """// ="""
    is
    """// IN"""
    in
    """// notIn"""
    notIn
    """// LIKE '%hat'"""
    like
    """// NOT LIKE '%hat'"""
    notLike
    """// REGEXP/~ '^[h|a|t]'"""
    regexp
    """// NOT REGEXP/!~ '^[h|a|t]'"""
    notRegexp
    between
    notBetween
    """// NOT LIKE '%hat'"""
    contains
    
    and
    or
    
    AND
    OR
  }
  ${types}
  ${PaginationType.types()}
  
  type Test{
    message: String
  }
  
  type Subscription {
    test: Test
    ${schemaSubcription}
  }
  type Query {
    test: Test
    ${schemaQueries}
  }
  type Mutation {
    test: Test
    ${schemaMutation}
  }
`;