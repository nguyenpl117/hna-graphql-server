'use strict';
module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable('users', {
            id: {
                allowNull: false,
                primaryKey: true,
                autoIncrement: true,
                type: Sequelize.INTEGER.UNSIGNED
            },
            name: {
                type: Sequelize.STRING
            },
            phone: {
                type: Sequelize.STRING(13),
                unique: true
            },
            email: {
                type: Sequelize.STRING
            },
            avatar: {
                type: Sequelize.STRING
            },
            cover: {
                type: Sequelize.STRING
            },
            password: {
                type: Sequelize.STRING
            },
            dob: {
                type: Sequelize.DATE
            },
            gender: {
                type: Sequelize.TINYINT
            },
            created_at: {
                allowNull: false,
                type: Sequelize.DATE
            },
            updated_at: {
                allowNull: false,
                type: Sequelize.DATE
            }
        });
    },
    down: (queryInterface, Sequelize) => {
        return queryInterface.dropTable('users');
    }
};