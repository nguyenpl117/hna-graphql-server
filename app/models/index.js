'use strict';

import { getConfig } from '../lib/get-configs';
import { Paginate } from '../lib/paginate';
import {clone, isObject, isString, extend} from 'lodash';
import Model from '../lib/model';
import loadPath from '../lib/load-path';
import { srcDir } from '../lib/utils';

const fs = require('fs');
const path = require('path');
const Sequelize = require('sequelize');
const basename = path.basename(__filename);
const appDir = srcDir();
const cls = require('continuation-local-storage'),
    namespace = cls.createNamespace('my-very-own-namespace');

extend(Sequelize.Model, Model);

const env = process.env.NODE_ENV === 'production' ? 'production' : 'development'

const config = require('../config/config')[env];

const db = {};
Sequelize.useCLS(namespace);
let sequelize = new Sequelize(config.database, config.username, config.password, {...config, timezone: '+07:00'});

console.log('start database', config.host, config.database);

let modelPaths = getConfig.app.modelPath;

if (!Array.isArray(modelPaths)) {
    modelPaths = [modelPaths];
}

for (let modelPath of modelPaths) {
    loadPath(appDir + "/" + modelPath).forEach(file => {
        const model = sequelize['import'](file);
        Paginate.paginate(model);
        // model.tableName = config.prefixTable + model.tableName;
        db[model.name] = model;
    });
}

Object.keys(db).forEach(modelName => {
  if (db[modelName].associate) {
    db[modelName].associate(db);
  }
});

function toJSON(data) {
    if (!isObject(data)) {
        return data;
    }
    for (let i in data) {
        if (data[i] instanceof Sequelize.Model) {
            data[i] = clone(data[i].get({plain: true}));
        }
        toJSON(data[i]);
    }
}

Sequelize.Model.prototype.toJSON = function() {
    const data = clone(this.get({plain: true}));
    toJSON(data);
    return data;
}

db.sequelize = sequelize;
db.Sequelize = Sequelize;
db.namespace = namespace;

module.exports = db;
