export default class Mailable {
    constructor(props) {
        this.data = {
            name: 'Illuminate\\Mail\\SendQueuedMailable',
            template: 'rejectAuthenticationKYC.html',
            from: [
                {
                    name: 'Hệ thống (khoinghiep.com)',
                    address: 'no-reply@khoinghiep.com'
                }
            ],
            to: [
                {
                    name: '',
                    address: ''
                }
            ],
            subject: 'Xác thực người dùng.'
        }
        
        this.data.data = props;
        if (typeof props === 'object') {
            for (let i in props) {
                this.data[i] = props[i];
            }
        }
    }
    
    name(name) {
        this.data.name = name;
        return this;
    }
    
    template(filename) {
        this.data.template = filename;
        return this;
    }
    
    from(address, name) {
        this.data.from = [{
            address, name
        }];
        return this;
    }
    
    subject(subject) {
        this.data.subject = subject;
        return this;
    }
}