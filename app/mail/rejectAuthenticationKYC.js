import Mailable from './mailable';

export class RejectAuthenticationKYC extends Mailable{
    constructor(props) {
        super(props);
    
        this.name('Illuminate\\Mail\\SendQueuedMailable');
    
        this.template('rejectAuthenticationKYC.html');
        this.from('no-reply@khoinghiep.com', 'Hệ thống (khoinghiep.com)');
        this.subject('Xác thực người dùng.');
    }
}