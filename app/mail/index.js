import {Queue} from '../queue';

export default class Mail {
    
    /**
     * Gửi tới user ( hoặ email )
     * @param user
     * @return {Mail}
     */
    static to(user) {
        if (typeof user === 'string') {
            this.address = user;
        } else if (typeof user === 'object') {
            this.address = user.email;
        }
        return this;
    }
    
    /**
     * Gửi vào queue
     * @param mailable
     * @return {*|void}
     */
    static queue(mailable) {
        const data = mailable.data;
        data.to = [{
            address: this.address
        }];
        
        return Queue.redisPush(data.name, data);
    }
}