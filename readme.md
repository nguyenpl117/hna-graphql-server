### Cài đặt
```
# graphql server:

# Copy file .env.example to .env
# config db mysql và redis trong file .env

$ yarn install
$ yarn start    #start server 
```
## Build production
```
$ yarn build
$ yarn serve    # Start server
```

## Migrate
```
Sequelize CLI [Node: 6.11.2, CLI: 3.0.0, ORM: 4.8.0]

Commands:
  db:migrate                        Run pending migrations
  db:migrate:schema:timestamps:add  Update migration table to have timestamps
  db:migrate:status                 List the status of all migrations
  db:migrate:undo                   Reverts a migration
  db:migrate:undo:all               Revert all migrations ran
  db:seed                           Run specified seeder
  db:seed:undo                      Deletes data from the database
  db:seed:all                       Run every seeder
  db:seed:undo:all                  Deletes data from the database
  db:create                         Create database specified by configuration
  db:drop                           Drop database specified by configuration
  init                              Initializes project
  init:config                       Initializes configuration
  init:migrations                   Initializes migrations
  init:models                       Initializes models
  init:seeders                      Initializes seeders
  migration:generate                Generates a new migration file       [aliases: migration:create]
  model:generate                    Generates a model and its migration  [aliases: model:create]
  seed:generate                     Generates a new seed file            [aliases: seed:create]

Options:
  --version  Show version number                                         [boolean]
  --help     Show help                                                   [boolean]
```

## Validate

Tạo một mutation createPost:
```js
import {Field} from '../../lib/field';
import {Types} from '../../lib/types';

class CreatePost extends Field {
    constructor(props) {
        super(props);
        this.name = 'createPost';
    }

    type() {
        return Types.Int;
    }

    args() {
        return {
            name: {
                type: Types.String,
                rules: [
                    'required' // 
                ]
            },
            email: {
                type: Types.String,
                rules: [
                    'email'
                ]
            }
        };
    }

    rules(args) {
        return {
            name: [
                'filled',
                // Rule.unique('user', 'userName').where({}).ignore(1)
            ]
        };
    }

    resolve(parent, args, {db, cache}, info) {
        const {order, where, page, limit} = args;

        return 0;
    }
}

```

Ví dụ mutation:
```gql
mutation create{
  createPost(name: null, email: "asdf")
}
```

Kết quả:
```json
{
    "errors": [
        {
            "message": "validate",
            "validation": {
                "name": [
                  "The name field must have a value.",
                  "name bắt buộc nhập."
                ],
                "email": [
                  "email không phải là email."
                ]
            }
        }
    ]
}
```

### Available Rules

Validation rules do not have an implicit 'required'. If a field is _undefined_ or an empty string, it will pass validation. If you want a validation to fail for undefined or '', use the _required_ rule.

#### accepted

The field under validation must be yes, on, 1 or true. This is useful for validating "Terms of Service" acceptance.

#### after:date

The field under validation must be after the given date.

#### after_or_equal:date

The field unter validation must be after or equal to the given field

#### alpha

The field under validation must be entirely alphabetic characters.

#### alpha_dash

The field under validation may have alpha-numeric characters, as well as dashes and underscores.

#### alpha_num

The field under validation must be entirely alpha-numeric characters.

#### array

The field under validation must be an array.

#### before:date

The field under validation must be before the given date.


#### before_or_equal:date

The field under validation must be before or equal to the given date.

#### between:min,max

The field under validation must have a size between the given min and max. Strings, numerics, and files are evaluated in the same fashion as the size rule.

#### boolean

The field under validation must be a boolean value of the form `true`, `false`, `0`, `1`, `'true'`, `'false'`, `'0'`, `'1'`,

#### confirmed

The field under validation must have a matching field of foo_confirmation. For example, if the field under validation is password, a matching password_confirmation field must be present in the input.

#### date

The field under validation must be a valid date format which is acceptable by Javascript's `Date` object.

#### digits:value

The field under validation must be numeric and must have an exact length of value.

#### different:attribute

The given field must be different than the field under validation.

#### email

The field under validation must be formatted as an e-mail address.

#### hex
The field under validation should be a hexadecimal format. Useful in combination with other rules, like `hex|size:6` for hex color code validation.

#### in:foo,bar,...

The field under validation must be included in the given list of values. The field can be an array or string.

#### integer

The field under validation must have an integer value.

#### max:value

Validate that an attribute is no greater than a given size

_Note: Maximum checks are inclusive._

#### min:value

Validate that an attribute is at least a given size.

_Note: Minimum checks are inclusive._

#### not_in:foo,bar,...

The field under validation must not be included in the given list of values.

#### numeric

Validate that an attribute is numeric. The string representation of a number will pass.

#### required

Checks if the length of the String representation of the value is >

#### required_if:anotherfield,value

The field under validation must be present and not empty if the anotherfield field is equal to any value.

#### required_unless:anotherfield,value

The field under validation must be present and not empty unless the anotherfield field is equal to any value.

#### required_with:foo,bar,...

The field under validation must be present and not empty only if any of the other specified fields are present.

#### required_with_all:foo,bar,...

The field under validation must be present and not empty only if all of the other specified fields are present.

#### required_without:foo,bar,...

The field under validation must be present and not empty only when any of the other specified fields are not present.

#### required_without_all:foo,bar,...

The field under validation must be present and not empty only when all of the other specified fields are not present.

#### same:attribute

The given field must match the field under validation.

#### size:value

The field under validation must have a size matching the given value. For string data, value corresponds to the number of characters. For numeric data, value corresponds to a given integer value.

#### string

The field under validation must be a string.

#### url

Validate that an attribute has a valid URL format

#### regex:pattern

The field under validation must match the given regular expression.

**Note**: When using the ``regex`` pattern, it may be necessary to specify rules in an array instead of using pipe delimiters, especially if the regular expression contains a pipe character.
For each backward slash that you used in your regex pattern, you must escape each one with another backward slash.

#### unique:model,column,except,idColumn

The field under validation must be unique in a given database table. If the column option is not specified, the field name will be used.

##### Specifying A Custom Column Name:
```text
'email' => 'unique:user,email_address'
```

##### Forcing A Unique Rule To Ignore A Given ID:
```text
'email' => [
    'required',
    Rule::unique('user')->ignore($user->id),
],
```

If your table uses a primary key column name other than id, you may specify the name of the column when calling the ignore method:
```text
Rule::unique('user')->ignore($user->id, 'user_id')
```

#### exists:table,column

The field under validation must exist on a given database table.

##### Basic Usage Of Exists Rule
```text
'state' => 'exists:states'
```


##### If you would like to customize the query executed by the validation rule

```text
Rule::exists('staff')->where(function ($query) {
    $query->where('account_id', 1);
}),
```

#### filled

The field under validation must not be empty when it is present.

## Documentation

- [Migrations Documentation](http://docs.sequelizejs.com/manual/tutorial/migrations.html)
- [CLI Options](docs/README.md)
- [Frequently Asked Questions](docs/FAQ.md)
- [Graphql Server Documentation](https://www.apollographql.com/docs/apollo-server/)
- [Sequelize Documentation](http://docs.sequelizejs.com/manual/getting-started.html)
- [Validate documentation](https://github.com/skaterdav85/validatorjs)