"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _objectSpread2 = _interopRequireDefault(require("@babel/runtime/helpers/objectSpread"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _arr = _interopRequireDefault(require("../../lib/arr"));

var _sequelize = require("sequelize");

var _errors = _interopRequireDefault(require("sequelize/lib/errors"));

var _lodash = _interopRequireDefault(require("lodash"));

var db = require('../../models/index');

var BaseRepository =
/*#__PURE__*/
function () {
  function BaseRepository() {
    (0, _classCallCheck2.default)(this, BaseRepository);
  }
  /**
   * Tạo mới transaction
   * @param options
   * @param callback
   * @return {Promise<*>}
   */


  (0, _createClass2.default)(BaseRepository, null, [{
    key: "transaction",
    value: function () {
      var _transaction = (0, _asyncToGenerator2.default)(
      /*#__PURE__*/
      _regenerator.default.mark(function _callee(options, callback) {
        var transaction;
        return _regenerator.default.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                if (typeof options === 'function') {
                  callback = options;
                  options = undefined;
                }

                transaction = {};

                if (db.namespace.get('transaction')) {
                  transaction = {
                    transaction: db.namespace.get('transaction')
                  };
                }

                if (!options) {
                  options = (0, _objectSpread2.default)({}, transaction);
                } else {
                  options = (0, _objectSpread2.default)({}, transaction, options);
                }

                return _context.abrupt("return", this.model.sequelize.transaction(options, callback));

              case 5:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      function transaction(_x, _x2) {
        return _transaction.apply(this, arguments);
      }

      return transaction;
    }()
    /**
     * @return {{}|*}
     */

  }, {
    key: "paginate",

    /**
     *
     * @param options
     * @return {Promise<{}>}
     */
    value: function paginate(options) {
      return this.model.paginate(options);
    }
    /**
     *
     * @param options
     * @return {void|PromiseLike<T | never>|Promise<T | never>|*}
     */

  }, {
    key: "findOne",
    value: function findOne(options) {
      return this.model.findOne(options).then(function (data) {
        return data && data.toJSON();
      });
    }
    /**
     *
     * @param options
     * @return {void|PromiseLike<T | never>|Promise<T | never>|*}
     */

  }, {
    key: "findAll",
    value: function findAll(options) {
      return this.model.findAll(options).map(function (data) {
        return data && data.toJSON();
      });
    }
    /**
     *
     * @param attribute
     * @param value
     * @param columns
     * @return {void|PromiseLike<T | never>|Promise<T | never>|*}
     */

  }, {
    key: "findBy",
    value: function findBy(attribute, value, columns) {
      return this.model.findOne({
        where: (0, _defineProperty2.default)({}, attribute, value),
        attributes: _arr.default.array_wrap(columns)
      }).then(function (data) {
        return data && data.toJSON();
      });
    }
    /**
     *
     * @param attribute
     * @param value
     * @param columns
     * @return {void|PromiseLike<T | never>|Promise<T | never>|*}
     */

  }, {
    key: "findAllBy",
    value: function findAllBy(attribute, value, columns) {
      return this.model.findAll({
        where: (0, _defineProperty2.default)({}, attribute, value),
        attributes: columns
      }).map(function (data) {
        return data && data.toJSON();
      });
    }
    /**
     *
     * @param data
     * @return {Promise<data>}
     */

  }, {
    key: "create",
    value: function create(data) {
      return this.model.create(data);
    }
    /**
     *
     * @param data
     * @param id
     * @return {Promise<this>}
     */

  }, {
    key: "update",
    value: function () {
      var _update = (0, _asyncToGenerator2.default)(
      /*#__PURE__*/
      _regenerator.default.mark(function _callee2(data, id) {
        return _regenerator.default.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                data.id = id;
                _context2.t0 = _lodash.default;
                _context2.next = 4;
                return this.model.update(data, {
                  where: {
                    id: id
                  },
                  individualHooks: true
                });

              case 4:
                _context2.t1 = _context2.sent;
                return _context2.abrupt("return", _context2.t0.get.call(_context2.t0, _context2.t1, '1.0'));

              case 6:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, this);
      }));

      function update(_x3, _x4) {
        return _update.apply(this, arguments);
      }

      return update;
    }()
  }, {
    key: "findOrFail",
    value: function findOrFail(id) {
      return this.model.findOne({
        where: {
          id: id
        }
      }).then(function (data) {
        return data && data.toJSON();
      });
    }
  }, {
    key: "destroy",
    value: function () {
      var _destroy = (0, _asyncToGenerator2.default)(
      /*#__PURE__*/
      _regenerator.default.mark(function _callee3(ids) {
        var attribute,
            result,
            data,
            _iteratorNormalCompletion,
            _didIteratorError,
            _iteratorError,
            _iterator,
            _step,
            item,
            deleted,
            _args3 = arguments;

        return _regenerator.default.wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                attribute = _args3.length > 1 && _args3[1] !== undefined ? _args3[1] : 'id';
                result = [];
                _context3.next = 4;
                return this.model.findAll({
                  where: (0, _defineProperty2.default)({}, attribute, (0, _defineProperty2.default)({}, db.sequelize.Op.in, _arr.default.array_wrap(ids)))
                });

              case 4:
                data = _context3.sent;

                if (!(data && Array.isArray(data))) {
                  _context3.next = 34;
                  break;
                }

                _iteratorNormalCompletion = true;
                _didIteratorError = false;
                _iteratorError = undefined;
                _context3.prev = 9;
                _iterator = data[Symbol.iterator]();

              case 11:
                if (_iteratorNormalCompletion = (_step = _iterator.next()).done) {
                  _context3.next = 20;
                  break;
                }

                item = _step.value;
                _context3.next = 15;
                return this.delete(item[attribute].toString(), attribute);

              case 15:
                deleted = _context3.sent;

                if (deleted) {
                  result.push(item[attribute]);
                }

              case 17:
                _iteratorNormalCompletion = true;
                _context3.next = 11;
                break;

              case 20:
                _context3.next = 26;
                break;

              case 22:
                _context3.prev = 22;
                _context3.t0 = _context3["catch"](9);
                _didIteratorError = true;
                _iteratorError = _context3.t0;

              case 26:
                _context3.prev = 26;
                _context3.prev = 27;

                if (!_iteratorNormalCompletion && _iterator.return != null) {
                  _iterator.return();
                }

              case 29:
                _context3.prev = 29;

                if (!_didIteratorError) {
                  _context3.next = 32;
                  break;
                }

                throw _iteratorError;

              case 32:
                return _context3.finish(29);

              case 33:
                return _context3.finish(26);

              case 34:
                return _context3.abrupt("return", result.length === 0 ? false : result);

              case 35:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3, this, [[9, 22, 26, 34], [27,, 29, 33]]);
      }));

      function destroy(_x5) {
        return _destroy.apply(this, arguments);
      }

      return destroy;
    }()
  }, {
    key: "delete",
    value: function () {
      var _delete2 = (0, _asyncToGenerator2.default)(
      /*#__PURE__*/
      _regenerator.default.mark(function _callee4(id) {
        var attribute,
            deleted,
            _args4 = arguments;
        return _regenerator.default.wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                attribute = _args4.length > 1 && _args4[1] !== undefined ? _args4[1] : 'id';
                _context4.next = 3;
                return this.model.destroy({
                  where: (0, _defineProperty2.default)({}, attribute, id)
                });

              case 3:
                deleted = _context4.sent;
                return _context4.abrupt("return", deleted);

              case 5:
              case "end":
                return _context4.stop();
            }
          }
        }, _callee4, this);
      }));

      function _delete(_x6) {
        return _delete2.apply(this, arguments);
      }

      return _delete;
    }()
    /**
     *
     * @return {string}
     */

  }, {
    key: "getTable",
    value: function getTable() {
      return this.model.getTableName();
    }
  }, {
    key: "models",
    get: function get() {
      return db.sequelize.models;
    }
    /**
     * @return {{eq, ne, gte, gt, lte, lt, not, is, in, notIn, like, notLike, iLike, notILike, regexp, notRegexp, iRegexp, notIRegexp, between, notBetween, overlap, contains, contained, adjacent, strictLeft, strictRight, noExtendRight, noExtendLeft, and, or, any, all, values, col, placeholder, join, raw}|*}
     * @constructor
     */

  }, {
    key: "Op",
    get: function get() {
      return db.sequelize.Op;
    }
    /**
     *
     * @return Model
     */

  }, {
    key: "model",
    get: function get() {
      if (!this.modelName || !db[this.modelName()]) {
        throw new _errors.default.BaseError('Can not find model');
      }

      return db[this.modelName()];
    }
  }]);
  return BaseRepository;
}();

var _default = BaseRepository;
exports.default = _default;