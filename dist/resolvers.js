"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

var _objectSpread2 = _interopRequireDefault(require("@babel/runtime/helpers/objectSpread"));

var _graphqlInputNumber = require("graphql-input-number");

var _index = require("./src/type/index");

var _index2 = require("./src/query/index");

var _index3 = require("./src/mutation/index");

var _arr = _interopRequireDefault(require("./lib/arr"));

var _filters = require("./lib/filters");

var _subscription = require("./src/subscription");

var _require = require('./lib/resolve-info'),
    getFields = _require.getFields;

var TimestampType = require('./src/type/definition/Timestamp');

var uidType = require('./src/type/definition/uid');

var statusType = require('./src/type/definition/Status');

var jsonType = require('./src/type/definition/Json');

var anyType = require('./src/type/definition/Any');

var ValueType = require('./src/type/definition/ValueType');

var jwt = require('jsonwebtoken');

var md5 = require("crypto-js/md5");

/*import {pubsub} from './lib/pubsub';
import {NOTIFICATION_ADDED} from './src/subscription/notifications';
import {COMMENT_ADDED} from './src/subscription/comments';*/
var LimitAmount = (0, _graphqlInputNumber.GraphQLInputInt)({
  name: 'LimitAmount',
  description: "L\xE0 s\u1ED1 l\u01B0\u1EE3ng k\u1EBFt qu\u1EA3 \u0111\u01B0\u1EE3c tr\u1EA3 tr\xEAn 1 page. T\u1ED1i thi\u1EC3u l\xE0 1 v\xE0 t\u1ED1i \u0111a l\xE0 100",
  min: 1,
  max: 100
});
var resolversType = {};

var _loop = function _loop(i) {
  resolversType[i] = {};

  var _loop2 = function _loop2(j) {
    if (typeof _index.resolvers[i][j] === 'string') {
      resolversType[i][j] = _index.resolvers[i][j];
      return "continue";
    }

    resolversType[i][j] = function (parent, args, context, info) {
      if (args) {
        var order = _arr.default.array_wrap(args.sortBy).map(function (item) {
          for (var _i in item) {
            return [_i, item[_i]];
          }
        });

        args.order = order.length ? order : [['id', 'DESC']];

        var _Filters$compieFilter = _filters.Filters.compieFilter(args.filter),
            where = _Filters$compieFilter.where,
            include = _Filters$compieFilter.include;

        args.where = where;
        args.include = include;
      }

      return _index.resolvers[i][j](parent, args, context, info);
    };
  };

  for (var j in _index.resolvers[i]) {
    var _ret = _loop2(j);

    if (_ret === "continue") continue;
  }
};

for (var i in _index.resolvers) {
  _loop(i);
}

module.exports = (0, _objectSpread2.default)({
  Timestamp: TimestampType,
  ID_CRYPTO: uidType,
  STATUS: statusType,
  JSON: jsonType,
  Any: anyType,
  ValueType: ValueType,
  LimitAmount: LimitAmount,
  SortValue: {
    ASC: 'ASC',
    DESC: 'DESC',
    ascend: 'ASC',
    descend: 'DESC',
    ascending: 'ASC',
    descending: 'DESC'
  },
  Operator: {
    contains: 'like',
    AND: 'and',
    OR: 'or'
  }
}, resolversType, {
  Query: (0, _objectSpread2.default)({
    test: function test() {
      return {
        message: 'success'
      };
    }
  }, _index2.resolversQuery),
  Mutation: (0, _objectSpread2.default)({
    test: function test() {
      return {
        message: 'success'
      };
    }
  }, _index3.resolversMutation),
  Subscription: (0, _objectSpread2.default)({}, _subscription.resolversSubcription, {
    test: {
      // Additional event labels can be passed to asyncIterator creation
      subscribe: function subscribe() {
        return pubsub.asyncIterator(['test']);
      }
    }
  })
});