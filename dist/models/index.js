'use strict';

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

var _objectSpread2 = _interopRequireDefault(require("@babel/runtime/helpers/objectSpread"));

var _getConfigs = require("../lib/get-configs");

var _paginate = require("../lib/paginate");

var _lodash = require("lodash");

var _model = _interopRequireDefault(require("../lib/model"));

var _loadPath = _interopRequireDefault(require("../lib/load-path"));

var _utils = require("../lib/utils");

var fs = require('fs');

var path = require('path');

var Sequelize = require('sequelize');

var basename = path.basename(__filename);
var appDir = (0, _utils.srcDir)();

var cls = require('continuation-local-storage'),
    namespace = cls.createNamespace('my-very-own-namespace');

(0, _lodash.extend)(Sequelize.Model, _model.default);
var env = process.env.NODE_ENV === 'production' ? 'production' : 'development';

var config = require('../config/config')[env];

var db = {};
Sequelize.useCLS(namespace);
var sequelize = new Sequelize(config.database, config.username, config.password, (0, _objectSpread2.default)({}, config, {
  timezone: '+07:00'
}));
console.log('start database', config.host, config.database);
var modelPaths = _getConfigs.getConfig.app.modelPath;

if (!Array.isArray(modelPaths)) {
  modelPaths = [modelPaths];
}

var _iteratorNormalCompletion = true;
var _didIteratorError = false;
var _iteratorError = undefined;

try {
  for (var _iterator = modelPaths[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
    var modelPath = _step.value;
    (0, _loadPath.default)(appDir + "/" + modelPath).forEach(function (file) {
      var model = sequelize['import'](file);

      _paginate.Paginate.paginate(model); // model.tableName = config.prefixTable + model.tableName;


      db[model.name] = model;
    });
  }
} catch (err) {
  _didIteratorError = true;
  _iteratorError = err;
} finally {
  try {
    if (!_iteratorNormalCompletion && _iterator.return != null) {
      _iterator.return();
    }
  } finally {
    if (_didIteratorError) {
      throw _iteratorError;
    }
  }
}

Object.keys(db).forEach(function (modelName) {
  if (db[modelName].associate) {
    db[modelName].associate(db);
  }
});

function toJSON(data) {
  if (!(0, _lodash.isObject)(data)) {
    return data;
  }

  for (var i in data) {
    if (data[i] instanceof Sequelize.Model) {
      data[i] = (0, _lodash.clone)(data[i].get({
        plain: true
      }));
    }

    toJSON(data[i]);
  }
}

Sequelize.Model.prototype.toJSON = function () {
  var data = (0, _lodash.clone)(this.get({
    plain: true
  }));
  toJSON(data);
  return data;
};

db.sequelize = sequelize;
db.Sequelize = Sequelize;
db.namespace = namespace;
module.exports = db;