"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

var _Pagination = _interopRequireDefault(require("./src/type/definition/Pagination"));

var _index = require("./src/query/index");

var _index2 = require("./src/mutation/index");

var _subscription = require("./src/subscription");

var _require = require('./src/type/index'),
    types = _require.types;

module.exports = "\n  directive @cost(useMultipliers: Boolean, complexity: Int, multipliers: [String]) on FIELD_DEFINITION\n  \n  scalar ValueType\n  scalar Timestamp\n  scalar ID_CRYPTO\n  scalar STATUS\n  scalar JSON\n  scalar Any\n  scalar LimitAmount\n  \n  enum ContentFormatEnum{\n    TEXT\n    HTML\n  }\n  \n  enum SortValue {\n      ASC\n      DESC\n      ascend\n      descend\n      ascending\n      descending\n  }\n  enum Operator{\n  \"\"\"// =\"\"\"\n    eq\n  \"\"\"// !=\"\"\"\n    ne\n    \"\"\"// >=\"\"\"\n    gte\n    \"\"\"// >\"\"\"\n    gt\n    \"\"\"// <=\"\"\"\n    lte\n    \"\"\"// <\"\"\"\n    lt\n    \"\"\"// NOT\"\"\"\n    not\n    \"\"\"// =\"\"\"\n    is\n    \"\"\"// IN\"\"\"\n    in\n    \"\"\"// notIn\"\"\"\n    notIn\n    \"\"\"// LIKE '%hat'\"\"\"\n    like\n    \"\"\"// NOT LIKE '%hat'\"\"\"\n    notLike\n    \"\"\"// REGEXP/~ '^[h|a|t]'\"\"\"\n    regexp\n    \"\"\"// NOT REGEXP/!~ '^[h|a|t]'\"\"\"\n    notRegexp\n    between\n    notBetween\n    \"\"\"// NOT LIKE '%hat'\"\"\"\n    contains\n    \n    and\n    or\n    \n    AND\n    OR\n  }\n  ".concat(types, "\n  ").concat(_Pagination.default.types(), "\n  \n  type Test{\n    message: String\n  }\n  \n  type Subscription {\n    test: Test\n    ").concat(_subscription.schemaSubcription, "\n  }\n  type Query {\n    test: Test\n    ").concat(_index.schemaQueries, "\n  }\n  type Mutation {\n    test: Test\n    ").concat(_index2.schemaMutation, "\n  }\n");