'use strict';

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _possibleConstructorReturn2 = _interopRequireDefault(require("@babel/runtime/helpers/possibleConstructorReturn"));

var _getPrototypeOf2 = _interopRequireDefault(require("@babel/runtime/helpers/getPrototypeOf"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime/helpers/inherits"));

var CommandCli = require('../lib/command-cli');
/**
 * Created by Phan Trung Nguyên.
 * User: nguyenpl117
 * Date: 6/16/2019
 * Time: 4:17 PM
 */


var fs = require('fs');

var RSA = require('hybrid-crypto-js').RSA;

var rsa = new RSA();

var Keys =
/*#__PURE__*/
function (_CommandCli) {
  (0, _inherits2.default)(Keys, _CommandCli);

  function Keys() {
    (0, _classCallCheck2.default)(this, Keys);
    return (0, _possibleConstructorReturn2.default)(this, (0, _getPrototypeOf2.default)(Keys).apply(this, arguments));
  }

  (0, _createClass2.default)(Keys, [{
    key: "signature",
    value: function signature() {
      return 'keys';
    }
  }, {
    key: "options",
    value: function options() {
      return [['--force', 'Buộc tạo ra khóa mã hóa mới.'], ['--length [length]', 'Độ dài của private key', 4096]];
    }
  }, {
    key: "description",
    value: function description() {
      return 'Tạo các khóa mã hóa để xác thực API';
    }
  }, {
    key: "handle",
    value: function () {
      var _handle = (0, _asyncToGenerator2.default)(
      /*#__PURE__*/
      _regenerator.default.mark(function _callee(options) {
        var keypair, publicKey, privateKey;
        return _regenerator.default.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                /*if (!await this.confirmToProceed()) {
                    return;
                }*/
                console.info('Chúng tôi đang tạo tập tin khóa mã hóa, bạn vui lòng chờ một chút...');
                _context.next = 3;
                return new Promise(function (resolve) {
                  // Generate 4096 bit RSA key pair
                  rsa.generateKeypair(function (key) {
                    resolve(key);
                  }, options.length ? options.length : 4096);
                });

              case 3:
                keypair = _context.sent;
                publicKey = './oauth-public.key', privateKey = './oauth-private.key';

                if (!((fs.existsSync(publicKey) || fs.existsSync(privateKey)) && !options.force)) {
                  _context.next = 7;
                  break;
                }

                return _context.abrupt("return", console.warn('Đã tồn tại private key. Sử dụng tùy chọn --force để ghi đè lên chúng.'));

              case 7:
                fs.writeFileSync(publicKey, keypair.publicKey);
                fs.writeFileSync(privateKey, keypair.privateKey);
                console.info('Khóa mã hóa được tạo thành công.');

              case 10:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      }));

      function handle(_x) {
        return _handle.apply(this, arguments);
      }

      return handle;
    }()
  }]);
  return Keys;
}(CommandCli);

module.exports = Keys;