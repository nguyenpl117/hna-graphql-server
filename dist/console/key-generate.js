'use strict';

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _possibleConstructorReturn2 = _interopRequireDefault(require("@babel/runtime/helpers/possibleConstructorReturn"));

var _getPrototypeOf2 = _interopRequireDefault(require("@babel/runtime/helpers/getPrototypeOf"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime/helpers/inherits"));

var CommandCli = require('../lib/command-cli');
/**
 * Created by Phan Trung Nguyên.
 * User: nguyenpl117
 * Date: 6/16/2019
 * Time: 4:17 PM
 */


var fs = require('fs');

var KeyGenerate =
/*#__PURE__*/
function (_CommandCli) {
  (0, _inherits2.default)(KeyGenerate, _CommandCli);

  function KeyGenerate() {
    (0, _classCallCheck2.default)(this, KeyGenerate);
    return (0, _possibleConstructorReturn2.default)(this, (0, _getPrototypeOf2.default)(KeyGenerate).apply(this, arguments));
  }

  (0, _createClass2.default)(KeyGenerate, [{
    key: "signature",
    value: function signature() {
      return 'key:generate';
    }
  }, {
    key: "options",
    value: function options() {
      return [['-s, --show', 'Hiển thị khóa thay vì sửa đổi tập tin.'], ['--force', 'Buộc tạo ra khóa mới.']];
    }
  }, {
    key: "description",
    value: function description() {
      return 'Đặt khóa ứng dụng';
    }
  }, {
    key: "handle",
    value: function () {
      var _handle = (0, _asyncToGenerator2.default)(
      /*#__PURE__*/
      _regenerator.default.mark(function _callee(options) {
        var key, file_env;
        return _regenerator.default.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                console.info('Tạo khóa ứng dụng.');
                _context.next = 3;
                return new Promise(function (resolve) {
                  require('crypto').randomBytes(32, function (err, buffer) {
                    resolve('base64:' + buffer.toString('base64'));
                  });
                });

              case 3:
                key = _context.sent;

                if (!options.show) {
                  _context.next = 6;
                  break;
                }

                return _context.abrupt("return", console.info(key));

              case 6:
                file_env = fs.readFileSync('./.env');
                fs.writeFile("./.env", file_env.toString('UTF-8').replace(/(APP_KEY=(.*))/ig, "APP_KEY=".concat(key)), 'utf8', function (err) {
                  if (err) {
                    return console.log(err);
                  }

                  console.info("Tạo khóa ứng dụng thành công.");
                });

              case 8:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      }));

      function handle(_x) {
        return _handle.apply(this, arguments);
      }

      return handle;
    }()
  }]);
  return KeyGenerate;
}(CommandCli);

module.exports = KeyGenerate;