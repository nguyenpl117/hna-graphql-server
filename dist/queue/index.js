"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Queue = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var Serialize = require('php-serialize');

var Redis = require('ioredis');

var redis = new Redis({
  port: process.env.REDIS_PORT,
  // Redis port
  host: process.env.REDIS_HOST,
  // Redis host
  family: 4,
  // 4 (IPv4) or 6 (IPv6)
  password: process.env.REDIS_PASSWORD,
  db: process.env.REDIS_DB
});

var SendQueuedMailable = function SendQueuedMailable(props) {
  (0, _classCallCheck2.default)(this, SendQueuedMailable);
  this.data = props;
};

var Queue = {
  redisPush: function redisPush(name, object) {
    var command = Serialize.serialize({
      mailable: object
    }, {
      'Illuminate\\Mail\\SendQueuedMailable': SendQueuedMailable
    });
    var data = {
      job: 'Illuminate\\Queue\\CallQueuedHandler@call',
      data: {
        commandName: object.name,
        command: command
      },
      id: Date.now(),
      attempts: 1
    };
    redis.rpush('queues:default', JSON.stringify(data), function (err, replay) {// Queue pushed
    });
  }
};
exports.Queue = Queue;