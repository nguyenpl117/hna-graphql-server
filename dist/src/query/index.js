"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _objectSpread3 = _interopRequireDefault(require("@babel/runtime/helpers/objectSpread"));

var _typeof2 = _interopRequireDefault(require("@babel/runtime/helpers/typeof"));

var _arr = _interopRequireDefault(require("../../lib/arr"));

var _errors = require("../../exceptions/errors");

var _filters = require("../../lib/filters");

var _resolveInfo = require("../../lib/resolve-info");

var _mutation = require("../mutation");

var _utils = require("../../lib/utils");

var _validator = _interopRequireDefault(require("../../lib/validate/validator"));

var _validationError = _interopRequireDefault(require("../../exceptions/validation-error"));

var _registerTypes = _interopRequireDefault(require("../../lib/register-types"));

var fs = require('fs');

var path = require('path');

var basename = path.basename(__filename);
/**
 * Lấy rules ra từ các args
 * @param object
 */

function getRules(object) {
  var args = {};

  for (var item in object) {
    // Nếu phần tử có attribute rules
    if ((0, _typeof2.default)(object[item]) === 'object' && object[item].rules) {
      args[item] = object[item].rules;
    } // Xử lý phần tử có kiểu dữ liệu là 1 input type


    args = (0, _utils.merge)(_registerTypes.default.rules(object[item].type, item), args);
  }

  return args;
}

function compieArgs(object) {
  var args = [];

  for (var item in object) {
    if (typeof object[item] === 'string') {
      args.push("".concat(item, ": ").concat(object[item]));
    } else if ((0, _typeof2.default)(object[item]) === 'object' && object[item].type) {
      // nếu có comment
      if (object[item].description) {
        args.push("\"\"\"".concat(object[item].description, "\"\"\""));
      } // Nếu có giá trị mặc định


      if (object[item].hasOwnProperty('defaultValue')) {
        args.push("".concat(item, ": ").concat(object[item].type, "=").concat(object[item].defaultValue));
      } else {
        args.push("".concat(item, ": ").concat(object[item].type));
      }
    }
  }

  return args.length ? "(".concat(args.join(', '), ")") : '';
}

function buildQuery(querydata) {
  var schemaQueries = [];
  var resolversQuery = {};
  var query = new querydata();

  if (query.description) {
    schemaQueries.push("\"\"\"".concat(query.description, "\"\"\""));
  }

  schemaQueries.push("".concat(query.name).concat(compieArgs(query.args()), ": ").concat(query.type()));
  resolversQuery = (0, _objectSpread3.default)({}, resolversQuery, (0, _defineProperty2.default)({}, query.name, function () {
    var _ref = (0, _asyncToGenerator2.default)(
    /*#__PURE__*/
    _regenerator.default.mark(function _callee2(parent, args, context, info) {
      var time, key, sortType, fields, order, _ref3, where, include, rules, validation, body, _iteratorNormalCompletion, _didIteratorError, _iteratorError, _iterator, _step, item, docs, times;

      return _regenerator.default.wrap(function _callee2$(_context2) {
        while (1) {
          switch (_context2.prev = _context2.next) {
            case 0:
              time = Date.now();
              key = [JSON.stringify(args), JSON.stringify((0, _resolveInfo.getFieldSelection)(info, 5))];

              if (!(query.authentication && !context.user)) {
                _context2.next = 4;
                break;
              }

              throw new _errors.AuthenticationException('Unauthenticated');

            case 4:
              if (!Array.isArray(query.middleware)) {
                _context2.next = 7;
                break;
              }

              _context2.next = 7;
              return Promise.all(query.middleware.map(function (middleware) {
                return middleware(parent, args, context, info);
              }));

            case 7:
              _context2.next = 9;
              return _registerTypes.default.getType(query.args().sortBy, 'sort');

            case 9:
              sortType = _context2.sent;
              fields = {};

              if (sortType) {
                fields = sortType.fields();
              }

              _context2.next = 14;
              return Promise.all(_arr.default.array_wrap(args.sortBy).map(
              /*#__PURE__*/
              function () {
                var _ref2 = (0, _asyncToGenerator2.default)(
                /*#__PURE__*/
                _regenerator.default.mark(function _callee(item) {
                  var i;
                  return _regenerator.default.wrap(function _callee$(_context) {
                    while (1) {
                      switch (_context.prev = _context.next) {
                        case 0:
                          _context.t0 = _regenerator.default.keys(item);

                        case 1:
                          if ((_context.t1 = _context.t0()).done) {
                            _context.next = 12;
                            break;
                          }

                          i = _context.t1.value;

                          if (!(typeof fields[i].map === 'function')) {
                            _context.next = 9;
                            break;
                          }

                          _context.next = 6;
                          return fields[i].map();

                        case 6:
                          _context.t2 = _context.sent;
                          _context.t3 = item[i];
                          return _context.abrupt("return", [_context.t2, _context.t3]);

                        case 9:
                          return _context.abrupt("return", [i, item[i]]);

                        case 12:
                        case "end":
                          return _context.stop();
                      }
                    }
                  }, _callee);
                }));

                return function (_x5) {
                  return _ref2.apply(this, arguments);
                };
              }()));

            case 14:
              order = _context2.sent;
              args.order = order.length ? order : [];
              _context2.next = 18;
              return _filters.Filters.compieFilter(args.filter);

            case 18:
              _ref3 = _context2.sent;
              where = _ref3.where;
              include = _ref3.include;
              args.where = where;
              args.include = include; // Lấy rules ra từ các args

              rules = getRules(query.args()); // Nếu viết riêng rules thì merge với rules đã lấy được từ args

              if (!(typeof query.rules === 'function')) {
                _context2.next = 31;
                break;
              }

              _context2.t0 = _utils.merge;
              _context2.next = 28;
              return (0, _utils.withAsync)(query.rules(args));

            case 28:
              _context2.t1 = _context2.sent;
              _context2.t2 = rules;
              rules = (0, _context2.t0)(_context2.t1, _context2.t2);

            case 31:
              // Custom message thông báo lỗi validate
              validation = new _validator.default(args, rules, query.validationErrorMessages(args)); // Check validate.

              _context2.next = 34;
              return new Promise(function (resolve, reject) {
                validation.checkAsync(function () {
                  resolve(true);
                }, function () {
                  reject({});
                });
              }).catch(function (err) {
                throw new _validationError.default('validation').setValidator(validation);
              });

            case 34:
              if (query.authorize(args)) {
                _context2.next = 36;
                break;
              }

              throw new _errors.AuthorizationError('Unauthorized');

            case 36:
              _context2.prev = 36;
              body = _arr.default.array_wrap(context.req.body);
              _iteratorNormalCompletion = true;
              _didIteratorError = false;
              _iteratorError = undefined;
              _context2.prev = 41;

              for (_iterator = body[Symbol.iterator](); !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
                item = _step.value;

                if (info.operation.name.value === item.operationName) {
                  info.variableValues = item.variables;
                }
              }

              _context2.next = 49;
              break;

            case 45:
              _context2.prev = 45;
              _context2.t3 = _context2["catch"](41);
              _didIteratorError = true;
              _iteratorError = _context2.t3;

            case 49:
              _context2.prev = 49;
              _context2.prev = 50;

              if (!_iteratorNormalCompletion && _iterator.return != null) {
                _iterator.return();
              }

            case 52:
              _context2.prev = 52;

              if (!_didIteratorError) {
                _context2.next = 55;
                break;
              }

              throw _iteratorError;

            case 55:
              return _context2.finish(52);

            case 56:
              return _context2.finish(49);

            case 57:
              _context2.next = 61;
              break;

            case 59:
              _context2.prev = 59;
              _context2.t4 = _context2["catch"](36);

            case 61:
              _context2.next = 63;
              return query.resolve(parent, args, context, info);

            case 63:
              docs = _context2.sent;
              times = Date.now() - time;
              if (times > 100) console.log('time query', times, query.name);
              return _context2.abrupt("return", docs);

            case 67:
            case "end":
              return _context2.stop();
          }
        }
      }, _callee2, null, [[36, 59], [41, 45, 49, 57], [50,, 52, 56]]);
    }));

    return function (_x, _x2, _x3, _x4) {
      return _ref.apply(this, arguments);
    };
  }()));
  return {
    schemaQueries: schemaQueries,
    resolversQuery: resolversQuery
  };
}

module.exports = {
  buildQuery: buildQuery
};