"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _objectSpread3 = _interopRequireDefault(require("@babel/runtime/helpers/objectSpread"));

var _arr = _interopRequireDefault(require("../../lib/arr"));

var _errors = require("../../exceptions/errors");

var _filters = require("../../lib/filters");

var _resolveInfo = require("../../lib/resolve-info");

var _pubsub = require("../../lib/pubsub");

var _require = require('apollo-server'),
    withFilter = _require.withFilter;

var fs = require('fs');

var path = require('path');

var basename = path.basename(__filename);
var schemaQueries = [];
var resolversQuery = {};

function compieArgs(object) {
  var args = [];

  for (var item in object) {
    args.push("".concat(item, ": ").concat(object[item]));
  }

  return args.length ? "(".concat(args.join(', '), ")") : '';
}

function buildSubscription(subscriptions) {
  subscriptions.forEach(function (file) {
    var query = new (require(file))();
    schemaQueries.push("".concat(query.name).concat(compieArgs(query.args()), ": ").concat(query.type()));
    resolversQuery = (0, _objectSpread3.default)({}, resolversQuery, (0, _defineProperty2.default)({}, query.name, {
      resolve: function resolve(parent, args, context, info) {
        // console.log(parent);
        var time = Date.now();
        var key = [JSON.stringify(args), JSON.stringify((0, _resolveInfo.getFieldSelection)(info, 5))];

        if (query.authentication && !context.user) {
          throw new _errors.AuthenticationException('Unauthenticated');
        }

        var order = _arr.default.array_wrap(args.sortBy).map(function (item) {
          for (var i in item) {
            return [i, item[i]];
          }
        });

        args.order = order.length ? order : [['id', 'DESC']];

        var _Filters$compieFilter = _filters.Filters.compieFilter(args.filter),
            where = _Filters$compieFilter.where,
            include = _Filters$compieFilter.include;

        args.where = where;
        args.include = include;

        if (!query.authorize(args)) {
          throw new _errors.AuthorizationError('Unauthorized');
        }

        return query.resolve(parent, args, context, info);
      },
      subscribe: withFilter(function (_, args) {
        return _pubsub.pubsub.asyncIterator(query.subscribe(args));
      }, function (payload, variables) {
        return query.filter(payload, variables);
      })
    }));
  });
  return {
    schemaSubcription: schemaQueries,
    resolversSubcription: resolversQuery
  };
}

module.exports = {
  buildSubscription: buildSubscription
};