"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.TypeMutation = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _objectSpread3 = _interopRequireDefault(require("@babel/runtime/helpers/objectSpread"));

var _typeof2 = _interopRequireDefault(require("@babel/runtime/helpers/typeof"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _utils = require("../../lib/utils");

var _validationError = _interopRequireDefault(require("../../exceptions/validation-error"));

var _validator = _interopRequireDefault(require("../../lib/validate/validator"));

var _errors = require("../../exceptions/errors");

var _resolveInfo = require("../../lib/resolve-info");

var _resource = require("../../lib/resource");

var _registerTypes = _interopRequireDefault(require("../../lib/register-types"));

var _arr = _interopRequireDefault(require("../../lib/arr"));

var fs = require('fs');

var path = require('path');

var basename = path.basename(__filename);

var _require = require('sequelize'),
    Op = _require.Op; // Các loại mutation


var TypeMutation = {
  DELETE: 'delete',
  UPDATE: 'update',
  CREATE: 'create'
  /**
   * Tạo mới dữ liệu
   * @param parent
   * @param args
   * @param context
   * @param info
   * @param repo
   * @param resolve
   * @return {Promise<void|PromiseLike<T|never>|Promise<T|never>|*>}
   */

};
exports.TypeMutation = TypeMutation;

function create(_x, _x2, _x3, _x4, _x5) {
  return _create.apply(this, arguments);
}
/**
 * Cập nhật dữ liệu
 * @param parent
 * @param args
 * @param context
 * @param info
 * @param repo
 * @param resolve
 * @return {Promise<void|PromiseLike<T|never>|Promise<T|never>|*>}
 */


function _create() {
  _create = (0, _asyncToGenerator2.default)(
  /*#__PURE__*/
  _regenerator.default.mark(function _callee2(parent, args, context, info, _ref) {
    var repo, resolve, attributes, obj;
    return _regenerator.default.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            repo = _ref.repo, resolve = _ref.resolve;
            // Các attribute có thể lấy của model
            attributes = (0, _resolveInfo.getFields)(info, repo.model); // Gọi tới create của repo

            _context2.next = 4;
            return repo.create(resolve ? resolve(args) : args);

          case 4:
            obj = _context2.sent;
            return _context2.abrupt("return", repo.findOne((0, _objectSpread3.default)({
              where: {
                id: obj.id
              }
            }, attributes)));

          case 6:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2);
  }));
  return _create.apply(this, arguments);
}

function update(_x6, _x7, _x8, _x9, _x10) {
  return _update.apply(this, arguments);
}
/**
 * Xóa dữ liệu
 * @param parent
 * @param args
 * @param context
 * @param info
 * @param repo
 * @return {PromiseLike<({status, message, data}|{status, message}) | never> | Promise<({status, message, data}|{status, message}) | never>}
 */


function _update() {
  _update = (0, _asyncToGenerator2.default)(
  /*#__PURE__*/
  _regenerator.default.mark(function _callee3(parent, args, context, info, _ref2) {
    var repo, resolve, attributes;
    return _regenerator.default.wrap(function _callee3$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            repo = _ref2.repo, resolve = _ref2.resolve;
            // Các attribute có thể lấy của model
            attributes = (0, _resolveInfo.getFields)(info, repo.model); // Gọi tới update của repo

            _context3.next = 4;
            return repo.update(resolve ? resolve(args) : args, args.id);

          case 4:
            return _context3.abrupt("return", repo.findOne((0, _objectSpread3.default)({
              where: {
                id: args.id
              }
            }, attributes)));

          case 5:
          case "end":
            return _context3.stop();
        }
      }
    }, _callee3);
  }));
  return _update.apply(this, arguments);
}

function resolve_delete(parent, args, context, info, _ref3) {
  var repo = _ref3.repo;
  console.log('test delete'); // Gọi tới hàm xóa nhiều ID của repo

  return repo.destroy(args.id).then(function (data) {
    return _resource.Resource.delete(data);
  });
}
/**
 * Lấy rules ra từ các args
 * @param object
 */


function getRules(object) {
  var args = {};

  for (var item in object) {
    // Nếu phần tử có attribute rules
    if ((0, _typeof2.default)(object[item]) === 'object' && object[item].rules) {
      args[item] = object[item].rules;
    } // Xử lý phần tử có kiểu dữ liệu là 1 input type


    args = (0, _utils.merge)(_registerTypes.default.rules(object[item].type, item), args);
  }

  return args;
}
/**
 * Tạo args cho schema
 * @param object
 * @return {string}
 */


function compieArgs(object) {
  var args = [];

  for (var item in object) {
    if (typeof object[item] === 'string') {
      args.push("".concat(item, ": ").concat(object[item]));
    } else if ((0, _typeof2.default)(object[item]) === 'object' && object[item].type) {
      // nếu có comment
      if (object[item].description) {
        args.push("\"\"\"".concat(object[item].description, "\"\"\""));
      } // Nếu có giá trị mặc định


      if (object[item].hasOwnProperty('defaultValue')) {
        args.push("".concat(item, ": ").concat(object[item].type, "=").concat(object[item].defaultValue));
      } else {
        args.push("".concat(item, ": ").concat(object[item].type));
      }
    }
  }

  return args.length ? "(".concat(args.join(', '), ")") : '';
}

function buildMutation(mutationResolveData) {
  var schemaQueries = [];
  var resolversQuery = {};
  var mutationResolve = new mutationResolveData(); // Nếu mutation này có comment

  if (mutationResolve.description) {
    schemaQueries.push("\"\"\"".concat(mutationResolve.description, "\"\"\""));
  } // Thêm mutation vào schema


  schemaQueries.push("".concat(mutationResolve.name).concat(compieArgs(mutationResolve.args()), ": ").concat(mutationResolve.type())); // Thêm resolve vào resolvers

  resolversQuery = (0, _objectSpread3.default)({}, resolversQuery, (0, _defineProperty2.default)({}, mutationResolve.name, function () {
    var _ref4 = (0, _asyncToGenerator2.default)(
    /*#__PURE__*/
    _regenerator.default.mark(function _callee(parent, args, context, info) {
      var rules, validation, body, _iteratorNormalCompletion, _didIteratorError, _iteratorError, _iterator, _step, item, mutation;

      return _regenerator.default.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              if (!(mutationResolve.authentication && !context.user)) {
                _context.next = 2;
                break;
              }

              throw new _errors.AuthenticationException('Unauthenticated');

            case 2:
              if (!Array.isArray(mutationResolve.middleware)) {
                _context.next = 5;
                break;
              }

              _context.next = 5;
              return Promise.all(mutationResolve.middleware.map(function (middleware) {
                return middleware(parent, args, context, info);
              }));

            case 5:
              // Lấy rules ra từ các args
              rules = getRules(mutationResolve.args()); // Nếu viết riêng rules thì merge với rules đã lấy được từ args

              if (!(typeof mutationResolve.rules === 'function')) {
                _context.next = 13;
                break;
              }

              _context.t0 = _utils.merge;
              _context.next = 10;
              return (0, _utils.withAsync)(mutationResolve.rules(args));

            case 10:
              _context.t1 = _context.sent;
              _context.t2 = rules;
              rules = (0, _context.t0)(_context.t1, _context.t2);

            case 13:
              // Custom message thông báo lỗi validate
              validation = new _validator.default(args, rules, mutationResolve.validationErrorMessages(args)); // Check validate.

              _context.next = 16;
              return new Promise(function (resolve, reject) {
                validation.checkAsync(function () {
                  resolve(true);
                }, function () {
                  reject({});
                });
              }).catch(function (err) {
                throw new _validationError.default('validation').setValidator(validation);
              });

            case 16:
              _context.next = 18;
              return mutationResolve.authorize(args);

            case 18:
              if (_context.sent) {
                _context.next = 20;
                break;
              }

              throw new _errors.AuthorizationError('Unauthorized');

            case 20:
              _context.prev = 20;
              body = _arr.default.array_wrap(context.req.body);
              _iteratorNormalCompletion = true;
              _didIteratorError = false;
              _iteratorError = undefined;
              _context.prev = 25;

              for (_iterator = body[Symbol.iterator](); !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
                item = _step.value;

                if (info.operation.name.value === item.operationName) {
                  info.variableValues = item.variables;
                }
              }

              _context.next = 33;
              break;

            case 29:
              _context.prev = 29;
              _context.t3 = _context["catch"](25);
              _didIteratorError = true;
              _iteratorError = _context.t3;

            case 33:
              _context.prev = 33;
              _context.prev = 34;

              if (!_iteratorNormalCompletion && _iterator.return != null) {
                _iterator.return();
              }

            case 36:
              _context.prev = 36;

              if (!_didIteratorError) {
                _context.next = 39;
                break;
              }

              throw _iteratorError;

            case 39:
              return _context.finish(36);

            case 40:
              return _context.finish(33);

            case 41:
              _context.next = 45;
              break;

            case 43:
              _context.prev = 43;
              _context.t4 = _context["catch"](20);

            case 45:
              // xử lý resolve
              mutation = mutationResolve.resolve(parent, args, context, info); // Nếu resolve là 1 promise thì xử lý luôn promise

              if (!(mutation instanceof Promise)) {
                _context.next = 48;
                break;
              }

              return _context.abrupt("return", mutation);

            case 48:
              if (!(mutation.type === TypeMutation.DELETE)) {
                _context.next = 52;
                break;
              }

              return _context.abrupt("return", resolve_delete(parent, args, context, info, mutation));

            case 52:
              if (!(mutation.type === TypeMutation.UPDATE)) {
                _context.next = 56;
                break;
              }

              return _context.abrupt("return", update(parent, args, context, info, mutation));

            case 56:
              if (!(mutation.type === TypeMutation.CREATE)) {
                _context.next = 58;
                break;
              }

              return _context.abrupt("return", create(parent, args, context, info, mutation));

            case 58:
            case "end":
              return _context.stop();
          }
        }
      }, _callee, null, [[20, 43], [25, 29, 33, 41], [34,, 36, 40]]);
    }));

    return function (_x11, _x12, _x13, _x14) {
      return _ref4.apply(this, arguments);
    };
  }()));
  return {
    schemaMutation: schemaQueries,
    resolversMutation: resolversQuery
  };
}

module.exports = {
  buildMutation: buildMutation
};