"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

var _utils = require("../../lib/utils");

var _registerTypes = _interopRequireDefault(require("../../lib/register-types"));

var fs = require('fs');

var path = require('path');

var basename = path.basename(__filename);

var FilterType = require('./filter-type-input/index');

var SortType = require('./sort-type/index');

var UnionType = require('./union/index');

var EnumType = require('./enum/index');

var InputType = require('./input/index');

var types = [];
var resolvers = {};

var _require = require('../../config/loader'),
    graphqlType = _require.graphqlType;

graphqlType.forEach(function (file) {
  _registerTypes.default.add(require(file).default); // push_object(require(`./${file}`))

});
module.exports = (0, _utils.merge)({
  types: _registerTypes.default.types(),
  resolvers: _registerTypes.default.resolvers()
}, FilterType, UnionType, EnumType, SortType);