"use strict";

var fs = require('fs');

var path = require('path');

var basename = path.basename(__filename);
var types = [];
var resolvers = {};

var _require = require('../../../config/loader'),
    enumType = _require.enumType;

enumType.forEach(function (file) {
  push_object(require(file));
});

function push_object(_ref) {
  var resolve = _ref.resolve,
      type = _ref.type;
  types.push(type);

  for (var i in resolve) {
    resolvers[i] = resolve[i];
  }
}

module.exports = {
  types: types,
  resolvers: resolvers
};