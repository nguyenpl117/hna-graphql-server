"use strict";

var fs = require('fs');

var path = require('path');

var basename = path.basename(__filename);
var types = [];
var resolvers = {};

var _require = require('../../../config/loader'),
    filterType = _require.filterType;

filterType.forEach(function (file) {
  push_object(require(file));
});

function push_object(_ref) {
  var resolve = _ref.resolve,
      name = _ref.name,
      args = _ref.args;
  types.push(render(name, args));

  for (var i in resolve) {
    resolvers[i] = resolve[i];
  }
}

function render(name, args) {
  return "\ninput Filter".concat(name, " {\n    groups: [FilterGroup").concat(name, "]\n    operator: Operator\n    value: [ValueType]\n    field: FilterField").concat(name, "\n    items: [Filter").concat(name, "]\n}\ninput FilterGroup").concat(name, " {\n    operator: Operator\n    items: [Filter").concat(name, "]\n}\nenum FilterField").concat(name, "{\n    ").concat(args, "\n}\n    ");
}

module.exports = {
  types: types,
  resolvers: resolvers
};