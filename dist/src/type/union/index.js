"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

var _registerTypes = _interopRequireDefault(require("../../../lib/register-types"));

var fs = require('fs');

var path = require('path');

var basename = path.basename(__filename);
var types = [];
var resolvers = {};

var _require = require('../../../config/loader'),
    unionType = _require.unionType;

unionType.forEach(function (file) {
  _registerTypes.default.add(require(file).default);
});
module.exports = {
  types: types,
  resolvers: resolvers
};