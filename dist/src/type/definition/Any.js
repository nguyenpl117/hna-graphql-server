"use strict";

var _graphql = require("graphql");

module.exports = new _graphql.GraphQLScalarType({
  name: 'Any',
  description: 'Any data',
  serialize: function serialize(value) {
    if (typeof value === 'string') {
      return JSON.parse(value);
    }

    return value;
  },
  parseValue: function parseValue(value) {
    return value;
  },
  parseLiteral: function parseLiteral(ast) {
    switch (ast.kind) {}
  }
});