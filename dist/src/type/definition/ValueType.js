"use strict";

var _graphql = require("graphql");

module.exports = new _graphql.GraphQLScalarType({
  name: 'ValueType',
  description: "The ID scalar type represents a unique identifier,\n     often used to refetch an object or as key for a cache.\n      The ID type appears in a JSON response as a String; however,\n      it is not intended to be human-readable. When expected as an input type,\n      any string (such as \"4\") or integer (such as 4) input value will be accepted as an ID.",
  serialize: function serialize(value) {
    var result; // Implement your own behavior here by setting the 'result' variable

    return value ? value.toString() : null;
  },
  parseValue: function parseValue(value) {
    var result; // Implement your own behavior here by setting the 'result' variable

    return value ? value.toString() : null;
  },
  parseLiteral: function parseLiteral(ast) {
    switch (ast.kind) {
      // Implement your own behavior here by returning what suits your needs
      // depending on ast.kind
      case 'StringValue':
        return ast.value;

      case 'IntValue':
        return ast.value;

      default:
        return ast.value;
    }
  }
});