"use strict";

var _graphql = require("graphql");

var moment = require('moment');

module.exports = new _graphql.GraphQLScalarType({
  name: 'Timestamp',
  description: 'Date format W3C',
  serialize: function serialize(value) {
    var result; // Implement your own behavior here by setting the 'result' variable

    return moment(value).format();
  },
  parseValue: function parseValue(value) {
    var result; // Implement your own behavior here by setting the 'result' variable

    return result;
  },
  parseLiteral: function parseLiteral(ast) {
    // console.log(ast.value);
    return ast.value;

    switch (ast.kind) {}
  }
});