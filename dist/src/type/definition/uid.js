"use strict";

var _graphql = require("graphql");

module.exports = new _graphql.GraphQLScalarType({
  name: 'ID_CRYPTO',
  description: "Number Or String",
  serialize: function serialize(value) {
    var result; // Implement your own behavior here by setting the 'result' variable

    return value !== null || value !== undefined ? value.toString() : null;
  },
  parseValue: function parseValue(value) {
    var result; // Implement your own behavior here by setting the 'result' variable

    return value !== null || value !== undefined ? value.toString() : null;
  },
  parseLiteral: function parseLiteral(ast) {
    switch (ast.kind) {
      // Implement your own behavior here by returning what suits your needs
      // depending on ast.kind
      case 'StringValue':
        return ast.value;

      case 'IntValue':
        return ast.value;

      default:
        return null;
    }
  }
});