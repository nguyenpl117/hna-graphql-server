"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var PaginationType =
/*#__PURE__*/
function () {
  function PaginationType() {
    (0, _classCallCheck2.default)(this, PaginationType);
  }

  (0, _createClass2.default)(PaginationType, null, [{
    key: "render",
    value: function render(name) {
      return "\ntype ".concat(name, "Pagination {\n    \"\"\"S\xF4\u0301 mu\u0323c b\u0103\u0301t \u0111\xE2\u0300u\"\"\"\n    from: Int\n   \"\"\"S\xF4\u0301 mu\u0323c k\xEA\u0301t thu\u0301c\"\"\"\n    to: Int\n    \"\"\"S\xF4\u0301 l\u01B0\u01A1\u0323ng tr\xEAn trang\"\"\"\n    perPage: Int!\n    \"\"\"Trang hi\xEA\u0323n ta\u0323i\"\"\"\n    currentPage: Int!\n    \"\"\"T\xF4\u0309ng s\xF4\u0301\"\"\"\n    total: Int!\n    data: [").concat(name, "]\n}\n    ");
    }
  }, {
    key: "has",
    value: function has(name) {
      if (!this.data) {
        this.data = [];
      }

      return this.data.indexOf(name) !== -1;
    }
  }, {
    key: "push",
    value: function push(name) {
      if (!this.data) {
        this.data = [];
      }

      this.data.push(name);
    }
  }, {
    key: "types",
    value: function types() {
      var _this = this;

      if (!this.data) {
        this.data = [];
      }

      return this.data.map(function (x) {
        return _this.render(x);
      });
    }
  }]);
  return PaginationType;
}();

var _default = PaginationType;
exports.default = _default;