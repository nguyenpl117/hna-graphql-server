"use strict";

var _graphql = require("graphql");

var moment = require('moment');

module.exports = new _graphql.GraphQLScalarType({
  name: 'STATUS',
  description: 'Date format W3C',
  serialize: function serialize(value) {
    return value;
  },
  parseValue: function parseValue(value) {
    return value;
  },
  parseLiteral: function parseLiteral(ast) {
    switch (ast.kind) {
      // Implement your own behavior here by returning what suits your needs
      // depending on ast.kind
      case 'StringValue':
        return ast.value;

      case 'IntValue':
        return ast.value;

      default:
        return null;
    }
  }
});