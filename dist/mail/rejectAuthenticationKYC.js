"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.RejectAuthenticationKYC = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _possibleConstructorReturn2 = _interopRequireDefault(require("@babel/runtime/helpers/possibleConstructorReturn"));

var _getPrototypeOf2 = _interopRequireDefault(require("@babel/runtime/helpers/getPrototypeOf"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime/helpers/inherits"));

var _mailable = _interopRequireDefault(require("./mailable"));

var RejectAuthenticationKYC =
/*#__PURE__*/
function (_Mailable) {
  (0, _inherits2.default)(RejectAuthenticationKYC, _Mailable);

  function RejectAuthenticationKYC(props) {
    var _this;

    (0, _classCallCheck2.default)(this, RejectAuthenticationKYC);
    _this = (0, _possibleConstructorReturn2.default)(this, (0, _getPrototypeOf2.default)(RejectAuthenticationKYC).call(this, props));

    _this.name('Illuminate\\Mail\\SendQueuedMailable');

    _this.template('rejectAuthenticationKYC.html');

    _this.from('no-reply@khoinghiep.com', 'Hệ thống (khoinghiep.com)');

    _this.subject('Xác thực người dùng.');

    return _this;
  }

  return RejectAuthenticationKYC;
}(_mailable.default);

exports.RejectAuthenticationKYC = RejectAuthenticationKYC;