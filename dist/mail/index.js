"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _typeof2 = _interopRequireDefault(require("@babel/runtime/helpers/typeof"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _queue = require("../queue");

var Mail =
/*#__PURE__*/
function () {
  function Mail() {
    (0, _classCallCheck2.default)(this, Mail);
  }

  (0, _createClass2.default)(Mail, null, [{
    key: "to",

    /**
     * Gửi tới user ( hoặ email )
     * @param user
     * @return {Mail}
     */
    value: function to(user) {
      if (typeof user === 'string') {
        this.address = user;
      } else if ((0, _typeof2.default)(user) === 'object') {
        this.address = user.email;
      }

      return this;
    }
    /**
     * Gửi vào queue
     * @param mailable
     * @return {*|void}
     */

  }, {
    key: "queue",
    value: function queue(mailable) {
      var data = mailable.data;
      data.to = [{
        address: this.address
      }];
      return _queue.Queue.redisPush(data.name, data);
    }
  }]);
  return Mail;
}();

exports.default = Mail;