"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _typeof2 = _interopRequireDefault(require("@babel/runtime/helpers/typeof"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var Mailable =
/*#__PURE__*/
function () {
  function Mailable(props) {
    (0, _classCallCheck2.default)(this, Mailable);
    this.data = {
      name: 'Illuminate\\Mail\\SendQueuedMailable',
      template: 'rejectAuthenticationKYC.html',
      from: [{
        name: 'Hệ thống (khoinghiep.com)',
        address: 'no-reply@khoinghiep.com'
      }],
      to: [{
        name: '',
        address: ''
      }],
      subject: 'Xác thực người dùng.'
    };
    this.data.data = props;

    if ((0, _typeof2.default)(props) === 'object') {
      for (var i in props) {
        this.data[i] = props[i];
      }
    }
  }

  (0, _createClass2.default)(Mailable, [{
    key: "name",
    value: function name(_name) {
      this.data.name = _name;
      return this;
    }
  }, {
    key: "template",
    value: function template(filename) {
      this.data.template = filename;
      return this;
    }
  }, {
    key: "from",
    value: function from(address, name) {
      this.data.from = [{
        address: address,
        name: name
      }];
      return this;
    }
  }, {
    key: "subject",
    value: function subject(_subject) {
      this.data.subject = _subject;
      return this;
    }
  }]);
  return Mailable;
}();

exports.default = Mailable;