"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.complexityLimitExceededErrorMessage = complexityLimitExceededErrorMessage;
exports.createComplexityLimitRule = createComplexityLimitRule;
exports.ComplexityVisitor = exports.CostCalculator = void 0;

var _objectWithoutProperties2 = _interopRequireDefault(require("@babel/runtime/helpers/objectWithoutProperties"));

var _slicedToArray2 = _interopRequireDefault(require("@babel/runtime/helpers/slicedToArray"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _graphql = require("graphql");

var IntrospectionTypes = _interopRequireWildcard(require("graphql/type/introspection"));

var _warning = _interopRequireDefault(require("warning"));

var CostCalculator =
/*#__PURE__*/
function () {
  function CostCalculator() {
    (0, _classCallCheck2.default)(this, CostCalculator);
    this.immediateCost = 0;
    this.fragmentCosts = [];
    this.cost = null;
  }

  (0, _createClass2.default)(CostCalculator, [{
    key: "addImmediate",
    value: function addImmediate(cost) {
      this.immediateCost += cost;
    }
  }, {
    key: "addFragment",
    value: function addFragment(costFactor, name) {
      this.fragmentCosts.push([costFactor, name]);
    }
  }, {
    key: "calculateCost",
    value: function calculateCost(fragmentCalculators) {
      if (this.cost !== null) {
        return this.cost;
      }

      var cost = this.immediateCost;
      this.fragmentCosts.forEach(function (_ref) {
        var _ref2 = (0, _slicedToArray2.default)(_ref, 2),
            costFactor = _ref2[0],
            name = _ref2[1];

        var fragmentCalculator = fragmentCalculators[name];

        if (!fragmentCalculator) {
          // Illegal query with undefined fragment.
          return;
        }

        cost += costFactor * fragmentCalculator.calculateCost(fragmentCalculators);
      });
      this.cost = cost;
      return cost;
    }
  }]);
  return CostCalculator;
}();

exports.CostCalculator = CostCalculator;

var ComplexityVisitor =
/*#__PURE__*/
function () {
  function ComplexityVisitor(context, _ref3) {
    var _ref3$scalarCost = _ref3.scalarCost,
        scalarCost = _ref3$scalarCost === void 0 ? 1 : _ref3$scalarCost,
        _ref3$objectCost = _ref3.objectCost,
        objectCost = _ref3$objectCost === void 0 ? 0 : _ref3$objectCost,
        _ref3$listFactor = _ref3.listFactor,
        listFactor = _ref3$listFactor === void 0 ? 10 : _ref3$listFactor,
        _ref3$introspectionLi = _ref3.introspectionListFactor,
        introspectionListFactor = _ref3$introspectionLi === void 0 ? 2 : _ref3$introspectionLi;
    (0, _classCallCheck2.default)(this, ComplexityVisitor);
    this.context = context;
    this.scalarCost = scalarCost;
    this.objectCost = objectCost;
    this.listFactor = listFactor;
    this.introspectionListFactor = introspectionListFactor;
    this.currentFragment = null;
    this.costFactor = 1;
    this.rootCalculator = new CostCalculator();
    this.fragmentCalculators = Object.create(null);
    this.Field = {
      enter: this.enterField,
      leave: this.leaveField
    };
    this.FragmentSpread = this.enterFragmentSpread;
    this.FragmentDefinition = {
      enter: this.enterFragmentDefinition,
      leave: this.leaveFragmentDefinition
    };
  }

  (0, _createClass2.default)(ComplexityVisitor, [{
    key: "enterField",
    value: function enterField() {
      this.costFactor *= this.getFieldCostFactor();
      this.getCalculator().addImmediate(this.costFactor * this.getFieldCost());
    }
  }, {
    key: "leaveField",
    value: function leaveField() {
      this.costFactor /= this.getFieldCostFactor();
    }
  }, {
    key: "getFieldCostFactor",
    value: function getFieldCostFactor() {
      var fieldDef = this.context.getFieldDef();

      if (fieldDef && fieldDef.getCostFactor) {
        return fieldDef.getCostFactor();
      }

      var directiveCostFactor = this.getDirectiveValue('costFactor');

      if (directiveCostFactor != null) {
        return directiveCostFactor;
      }

      return this.getTypeCostFactor(this.context.getType());
    }
  }, {
    key: "getTypeCostFactor",
    value: function getTypeCostFactor(type) {
      if (type instanceof _graphql.GraphQLNonNull) {
        return this.getTypeCostFactor(type.ofType);
      } else if (type instanceof _graphql.GraphQLList) {
        var typeListFactor = this.isIntrospectionList(type) ? this.introspectionListFactor : this.listFactor;
        return typeListFactor * this.getTypeCostFactor(type.ofType);
      }

      return 1;
    }
  }, {
    key: "isIntrospectionList",
    value: function isIntrospectionList(_ref4) {
      var ofType = _ref4.ofType;
      var type = ofType;

      if (type instanceof _graphql.GraphQLNonNull) {
        type = type.ofType;
      }

      return IntrospectionTypes[type.name] === type;
    }
  }, {
    key: "getFieldCost",
    value: function getFieldCost() {
      var fieldDef = this.context.getFieldDef();

      if (fieldDef && fieldDef.getCost) {
        return fieldDef.getCost();
      } // console.log(fieldDef);


      var directiveCost = this.getDirectiveValue('cost');

      if (directiveCost != null) {
        return directiveCost;
      }

      return this.getTypeCost(this.context.getType());
    }
  }, {
    key: "getTypeCost",
    value: function getTypeCost(type) {
      if (type instanceof _graphql.GraphQLNonNull || type instanceof _graphql.GraphQLList) {
        return this.getTypeCost(type.ofType);
      }

      return type instanceof _graphql.GraphQLObjectType ? this.objectCost : this.scalarCost;
    }
  }, {
    key: "getDirectiveValue",
    value: function getDirectiveValue(directiveName) {
      var fieldDef = this.context.getFieldDef();

      if (!fieldDef || !fieldDef.astNode || !fieldDef.astNode.directives) {
        return null;
      }

      var directive = fieldDef.astNode.directives.find(function (_ref5) {
        var name = _ref5.name;
        return name.value === directiveName;
      });

      if (!directive) {
        return null;
      }

      var valueArgument = directive.arguments.find(function (argument) {
        return argument.name.value === 'complexity';
      });
      var multipliersArg = directive.arguments.find(function (argument) {
        return argument.name.value === 'multipliers';
      });
      var multipliers = multipliersArg && multipliersArg.value && multipliersArg.value.kind === 'ListValue' ? this.getMultipliersFromListNode(multipliersArg.value.values, fieldArgs) : []; // console.log('multipliersArgument', multipliers, valueArgument);

      if (!valueArgument) {
        var fieldName = fieldDef.name;
        var parentTypeName = this.context.getParentType().name;
        throw new Error("No `value` argument defined in `@".concat(directiveName, "` directive ") + "on `".concat(fieldName, "` field on `").concat(parentTypeName, "`."));
      }

      return parseFloat(valueArgument.value.value);
    }
  }, {
    key: "getMultipliersFromListNode",
    value: function getMultipliersFromListNode(listNode, fieldArgs) {
      var multipliers = [];
      listNode.forEach(function (node) {
        if (node.kind === 'StringValue') {
          multipliers.push(node.value);
        }
      });
      return this.getMultipliersFromString(multipliers, fieldArgs);
    }
  }, {
    key: "getMultipliersFromString",
    value: function getMultipliersFromString(multipliers, fieldArgs) {
      // get arguments values, convert to integer and delete 0 values from list
      return multipliers.map(function (multiplier) {
        var value = selectn(multiplier, fieldArgs); // if the argument is an array, the multiplier will be the length of it

        if (Array.isArray(value)) {
          return value.length;
        }

        return Number(value) || 0;
      }).filter(function (multiplier) {
        return multiplier !== 0;
      });
    }
  }, {
    key: "getCalculator",
    value: function getCalculator() {
      return this.currentFragment === null ? this.rootCalculator : this.fragmentCalculators[this.currentFragment];
    }
  }, {
    key: "enterFragmentSpread",
    value: function enterFragmentSpread(node) {
      this.getCalculator().addFragment(this.costFactor, node.name.value);
    }
  }, {
    key: "enterFragmentDefinition",
    value: function enterFragmentDefinition(node) {
      var fragmentName = node.name.value;
      this.fragmentCalculators[fragmentName] = new CostCalculator();
      this.currentFragment = fragmentName;
    }
  }, {
    key: "leaveFragmentDefinition",
    value: function leaveFragmentDefinition() {
      this.currentFragment = null;
    }
  }, {
    key: "getCost",
    value: function getCost() {
      return this.rootCalculator.calculateCost(this.fragmentCalculators);
    }
  }]);
  return ComplexityVisitor;
}();

exports.ComplexityVisitor = ComplexityVisitor;

function complexityLimitExceededErrorMessage() {
  // By default, don't respond with the cost to avoid leaking information about
  // the cost scheme to a potentially malicious client.
  return 'query exceeds complexity limit';
}

function createComplexityLimitRule(maxCost) {
  var _ref6 = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {},
      onCost = _ref6.onCost,
      createError = _ref6.createError,
      formatErrorMessage = _ref6.formatErrorMessage,
      options = (0, _objectWithoutProperties2.default)(_ref6, ["onCost", "createError", "formatErrorMessage"]);

  (0, _warning.default)(!(createError && formatErrorMessage), 'formatErrorMessage is ignored when createError is specified.');
  formatErrorMessage = // eslint-disable-line no-param-reassign
  formatErrorMessage || complexityLimitExceededErrorMessage;
  return function ComplexityLimit(context) {
    var visitor = new ComplexityVisitor(context, options);
    return {
      enter: function enter(node) {
        var visit = (0, _graphql.getVisitFn)(visitor, node.kind, false);

        if (visit) {
          visit.apply(visitor, arguments); // eslint-disable-line prefer-rest-params
        }
      },
      leave: function leave(node) {
        var visit = (0, _graphql.getVisitFn)(visitor, node.kind, true);

        if (visit) {
          visit.apply(visitor, arguments); // eslint-disable-line prefer-rest-params
        }

        if (node.kind === 'Document') {
          var cost = visitor.getCost();

          if (onCost) {
            onCost(cost, context);
          }

          if (cost > maxCost) {
            context.reportError(createError ? createError(cost, node) : new _graphql.GraphQLError(formatErrorMessage(cost), [node]));
          }
        }
      }
    };
  };
}