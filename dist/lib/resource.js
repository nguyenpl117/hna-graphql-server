"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Resource = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var Resource =
/*#__PURE__*/
function () {
  function Resource() {
    (0, _classCallCheck2.default)(this, Resource);
  }

  (0, _createClass2.default)(Resource, null, [{
    key: "delete",
    value: function _delete(data) {
      if (data) {
        return {
          'status': true,
          'message': 'Xóa thành công',
          'data': data
        };
      } else {
        return {
          'status': false,
          'message': 'Xóa không thành công'
        };
      }
    }
  }]);
  return Resource;
}();

exports.Resource = Resource;