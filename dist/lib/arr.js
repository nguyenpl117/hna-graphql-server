"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var _default = {
  array_wrap: function array_wrap(value) {
    var split = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : '';

    if (!value) {
      return [];
    }

    return !Array.isArray(value) ? split ? value.split(split) : [value] : value;
  }
};
exports.default = _default;