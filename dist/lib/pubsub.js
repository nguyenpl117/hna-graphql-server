"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.pubsub = void 0;

var _graphqlRedisSubscriptions = require("graphql-redis-subscriptions");

var Redis = require('ioredis');

var options = {
  port: process.env.REDIS_PORT,
  // Redis port
  host: process.env.REDIS_HOST,
  // Redis host
  family: 4,
  // 4 (IPv4) or 6 (IPv6)
  password: process.env.REDIS_PASSWORD,
  db: process.env.REDIS_DB
};
var pubsub = new _graphqlRedisSubscriptions.RedisPubSub({
  publisher: new Redis(options),
  subscriber: new Redis(options)
});
exports.pubsub = pubsub;