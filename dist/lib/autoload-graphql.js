'use strict';

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

var _query = require("../src/query");

var _mutation = require("../src/mutation");

var _registerTypes = _interopRequireDefault(require("./register-types"));

var _utils = require("./utils");

/**
 * Created by Phan Trung Nguyên.
 * User: nguyenpl117
 * Date: 6/22/2019
 * Time: 8:52 PM
 */
var fs = require('fs'),
    p = require('path');

var path = require('path');

var loadPath = require('./load-path');

var appDir = (0, _utils.srcDir)();

function autoloadGraphQL(path) {
  var obj = {
    'query': [],
    'mutation': [],
    'input': [],
    'union': [],
    'object': [],
    'enum': [],
    'filter': [],
    'scalar': [],
    'sort': []
  };
  var build = {
    'query': _query.buildQuery,
    'mutation': _mutation.buildMutation,
    'input': _registerTypes.default.add.bind(_registerTypes.default),
    'union': _registerTypes.default.add.bind(_registerTypes.default),
    'object': _registerTypes.default.add.bind(_registerTypes.default),
    'enum': _registerTypes.default.add.bind(_registerTypes.default),
    'filter': _registerTypes.default.add.bind(_registerTypes.default),
    'scalar': _registerTypes.default.add.bind(_registerTypes.default),
    'sort': _registerTypes.default.add.bind(_registerTypes.default)
  };
  loadPath(appDir + '/' + path).forEach(function (file) {
    var item = require(file).default;

    obj[item.iniName].push(build[item.iniName](item));
  });
  return obj;
}

module.exports = {
  autoloadGraphQL: autoloadGraphQL
};