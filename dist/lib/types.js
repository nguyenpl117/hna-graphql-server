"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

var _Pagination = _interopRequireDefault(require("../src/type/definition/Pagination"));

var Types = {
  ListOf: function ListOf(name) {
    return "[".concat(name, "]");
  },
  NotNull: function NotNull(name) {
    return "".concat(name, "!");
  },
  Pagination: function Pagination(name) {
    if (!_Pagination.default.has(name)) {
      _Pagination.default.push(name);
    }

    return "".concat(name, "Pagination @cost(complexity: 1, multipliers: [\"limit\"])");
  },
  Type: function Type(name) {
    return name;
  },
  ArgsPagination: function ArgsPagination(name) {
    return {
      filter: 'Filter' + name,
      sortBy: Types.ListOf('Sort' + name),
      limit: 'Int = 20',
      page: {
        type: Types.Int,
        defaultValue: 1,
        description: ''
      }
    };
  },
  ArgsOne: function ArgsOne(name) {
    return {
      filter: 'Filter' + name,
      sortBy: Types.ListOf('Sort' + name)
    };
  },
  ID: 'ID_CRYPTO',
  Int: 'Int',
  String: 'String',
  Boolean: 'Boolean',
  Timestamp: 'Timestamp',
  Delete: 'Delete',
  MessageSuccess: 'MessageSuccess',
  Post: 'Post',
  Status: 'STATUS',
  Float: 'Float',
  Any: 'Any',
  JSON: 'JSON'
};
module.exports = {
  Types: Types
};