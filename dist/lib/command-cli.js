"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var Confirm = require('prompt-confirm');

var CommandCli =
/*#__PURE__*/
function () {
  function CommandCli() {
    (0, _classCallCheck2.default)(this, CommandCli);
    this.dataOptions = {};
  }

  (0, _createClass2.default)(CommandCli, [{
    key: "signature",
    value: function signature() {
      return '';
    }
  }, {
    key: "description",
    value: function description() {
      return '';
    }
  }, {
    key: "options",
    value: function options() {
      return [];
    }
  }, {
    key: "setOptions",
    value: function setOptions(options) {
      this.dataOptions = options;
    }
  }, {
    key: "option",
    value: function option(key) {
      return this.dataOptions[key];
    }
  }, {
    key: "handle",
    value: function handle() {}
  }, {
    key: "confirmToProceed",
    value: function () {
      var _confirmToProceed = (0, _asyncToGenerator2.default)(
      /*#__PURE__*/
      _regenerator.default.mark(function _callee() {
        return _regenerator.default.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                return _context.abrupt("return", new Promise(function (resolve) {
                  new Confirm('Bạn có muốn muốn chạy câu lệnh này?').ask(function (answer) {
                    resolve(answer);
                  });
                }));

              case 1:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      }));

      function confirmToProceed() {
        return _confirmToProceed.apply(this, arguments);
      }

      return confirmToProceed;
    }()
  }]);
  return CommandCli;
}();

module.exports = CommandCli;