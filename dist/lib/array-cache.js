"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ArrayCache = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var ArrayCache =
/*#__PURE__*/
function () {
  function ArrayCache() {
    (0, _classCallCheck2.default)(this, ArrayCache);
  }

  (0, _createClass2.default)(ArrayCache, null, [{
    key: "init",
    value: function init() {
      if (!this.dataCache) {
        this.dataCache = {};
      }

      if (!this.timeout) {
        this.timeout = new Date().getTime() + 1000;
      }

      this.timenow = new Date().getTime();

      if (this.timenow > this.timeout) {
        this.dataCache = {};
        this.timeout = new Date().getTime() + 1000;
      }
    }
  }, {
    key: "clear",
    value: function clear() {
      this.dataCache = {};
    }
  }, {
    key: "remember",
    value: function remember(key, callback) {
      if (!this.dataCache) {
        this.dataCache = {};
      }

      if (!this.dataCache.hasOwnProperty(key) || !this.dataCache[key]) {
        this.dataCache[key] = callback();
      }

      return this.dataCache[key];
    }
  }, {
    key: "remove",
    value: function remove(key) {
      this.dataCache[key] = null;
    }
  }]);
  return ArrayCache;
}();

exports.ArrayCache = ArrayCache;