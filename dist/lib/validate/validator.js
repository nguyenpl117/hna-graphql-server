"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

var _objectSpread4 = _interopRequireDefault(require("@babel/runtime/helpers/objectSpread"));

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _typeof2 = _interopRequireDefault(require("@babel/runtime/helpers/typeof"));

var _rule = _interopRequireDefault(require("./rule"));

var _utils = require("../utils");

var Validator = require('validatorjs');

var db = require("../../models");

var fs = require('fs');

var path = require('path');

var loadPath = require('../load-path');

var appDir = (0, _utils.srcDir)();
Validator.useLang('vi');
var validate = loadPath(appDir + '/rules').map(function (item) {
  return require(item)(Validator, db);
});

Validator.prototype._replaceWildCards = function (path, nums) {
  if (!nums) {
    return path;
  }

  var path2 = path;
  nums.forEach(function (value) {
    if (!Array.isArray(path2) && typeof path2 !== 'string') {
      return path2;
    }

    if (Array.isArray(path2)) {
      path2 = path2[0];
    }

    var pos = path2.indexOf('*');

    if (pos === -1) {
      return path2;
    }

    path2 = path2.substr(0, pos) + value + path2.substr(pos + 1);
  });

  if (Array.isArray(path)) {
    path[0] = path2;
    path2 = path;
  }

  return path2;
};

Validator.register('required', function (val, requirement, attribute) {
  // requirement parameter defaults to null
  var str;

  if (val === undefined || val === null) {
    return false;
  }

  if ((0, _typeof2.default)(val) === 'object' && Object.keys(val).length) {
    return true;
  }

  str = String(val).replace(/\s/g, "");
  return str.length > 0 ? true : false;
}, ":attribute bắt buộc nhập.");
/*
Validator.register('telephone', function(value, requirement, attribute) { // requirement parameter defaults to null
    // requirement = this.getParameters();
    // this.ruleValue()
    // console.log('requirement', requirement, attribute, this.ruleValue);
    return value.match(/^\d{3}-\d{3}-\d{4}$/);
}, 'The :attribute phone number is not in the format XXX-XXX-XXXX.');
*/

/**
 *
 */

Validator.registerImplicit('filled', function (val, requirement, attribute) {
  // requirement parameter defaults to null
  if (val === undefined) {
    return true;
  }

  var str;

  if (val === null) {
    return false;
  }

  str = String(val).replace(/\s/g, "");
  return str.length > 0 ? true : false;
}, 'Trường :attribute không được bỏ trống.');
Validator.registerImplicit('without_spaces', function (val, requirement, attribute) {
  // requirement parameter defaults to null
  return /^\S+$/g.test(String(val || ''));
}, 'Trường :attribute không hợp lệ.');
/**
 * unique:model,column,except,idColumn
 */

Validator.registerAsync('unique', function (value, req, attribute, passes) {
  // requirement parameter defaults to null
  var ruleValue = this.ruleValue;

  if ((0, _typeof2.default)(this.ruleValue) !== 'object') {
    req = this.getParameters();
    ruleValue = _rule.default.unique(req[0], req[1]).ignore(req[2], req[3]).unique;
  }

  var model = db[ruleValue.model];
  var idColumn = ruleValue.ignore.idColumn || 'id';
  var except = {};

  if (ruleValue.ignore.id) {
    except = (0, _defineProperty2.default)({}, idColumn, (0, _defineProperty2.default)({}, db.sequelize.Op.ne, ruleValue.ignore.id));
  }

  model.count({
    where: (0, _objectSpread4.default)((0, _defineProperty2.default)({}, ruleValue.column, value), ruleValue.where, except)
  }).then(function (total) {
    passes(!total);
  });
}, 'Trường :attribute đã có trong cơ sở dữ liệu.');
/**
 * exists:model,column
 */

Validator.registerAsync('exists', function (value, req, attribute, passes) {
  // requirement parameter defaults to null
  var ruleValue = this.ruleValue;

  if ((0, _typeof2.default)(this.ruleValue) !== 'object') {
    req = this.getParameters();
    ruleValue = _rule.default.exists(req[0], req[1]).exists;
  }

  var model = db[ruleValue.model];
  model.count({
    where: (0, _objectSpread4.default)((0, _defineProperty2.default)({}, ruleValue.column, value), ruleValue.where)
  }).then(function (total) {
    passes(!!total);
  });
}, 'Giá trị đã chọn trong trường :attribute không hợp lệ.');
Validator.registerAsync('language_unique', function (value, req, attribute, passes) {
  // requirement parameter defaults to null
  passes();
}, 'The selected :attribute is invalid.');
module.exports = Validator;