"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _objectSpread5 = _interopRequireDefault(require("@babel/runtime/helpers/objectSpread"));

var _typeof2 = _interopRequireDefault(require("@babel/runtime/helpers/typeof"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _require = require('sequelize'),
    Op = _require.Op;

var Unique =
/*#__PURE__*/
function () {
  function Unique(model, column) {
    (0, _classCallCheck2.default)(this, Unique);
    this.unique = {
      model: model,
      column: column,
      ignore: {
        id: null,
        idColumn: 'id'
      }
    };
  }
  /**
   *
   * @param id
   * @param idColumn
   * @returns {Unique}
   */


  (0, _createClass2.default)(Unique, [{
    key: "ignore",
    value: function ignore(id) {
      var idColumn = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'id';
      this.unique.ignore = {
        id: id,
        idColumn: idColumn
      };
      return this;
    }
    /**
     *
     * @param options
     * @returns {Unique}
     */

  }, {
    key: "where",
    value: function where() {
      if ((0, _typeof2.default)(arguments.length <= 0 ? undefined : arguments[0]) === 'object') {
        this.unique.where = (0, _objectSpread5.default)({}, this.unique.where, arguments.length <= 0 ? undefined : arguments[0]);
      } else if (typeof (arguments.length <= 0 ? undefined : arguments[0]) === 'string') {
        var operator = Op.eq;

        if (!(arguments.length <= 1 ? undefined : arguments[1])) {
          operator = Op.is;
        }

        this.unique.where = (0, _objectSpread5.default)({}, this.unique.where, (0, _defineProperty2.default)({}, arguments.length <= 0 ? undefined : arguments[0], (0, _defineProperty2.default)({}, operator, arguments.length <= 1 ? undefined : arguments[1])));
      }

      return this;
    }
  }, {
    key: "whereNot",
    value: function whereNot(column, value) {
      if (typeof value !== 'string') {
        return this;
      }

      this.unique.where = (0, _objectSpread5.default)({}, this.unique.where, (0, _defineProperty2.default)({}, column, (0, _defineProperty2.default)({}, Op.ne, value)));
      return this;
    }
    /**
     *
     * @param column String
     * @param value Array
     * @returns {Unique}
     */

  }, {
    key: "whereIn",
    value: function whereIn(column, value) {
      if (!Array.isArray(value)) {
        return this;
      }

      this.unique.where = (0, _objectSpread5.default)({}, this.unique.where, (0, _defineProperty2.default)({}, column, (0, _defineProperty2.default)({}, Op.in, value)));
      return this;
    }
  }]);
  return Unique;
}();

var _default = Unique;
exports.default = _default;