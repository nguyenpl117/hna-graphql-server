'use strict';

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _objectSpread3 = _interopRequireDefault(require("@babel/runtime/helpers/objectSpread"));

var _typeof2 = _interopRequireDefault(require("@babel/runtime/helpers/typeof"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _require = require('sequelize'),
    Op = _require.Op;
/**
 * Created by Phan Trung Nguyên.
 * User: nguyenpl117
 * Date: 3/29/2019
 * Time: 12:33 AM
 */


var Exists =
/*#__PURE__*/
function () {
  function Exists(model, column) {
    (0, _classCallCheck2.default)(this, Exists);
    this.exists = {
      model: model,
      column: column
    };
  }
  /**
   *
   * @param args
   * @returns {Exists}
   */


  (0, _createClass2.default)(Exists, [{
    key: "where",
    value: function where() {
      if ((0, _typeof2.default)(arguments.length <= 0 ? undefined : arguments[0]) === 'object') {
        this.exists.where = (0, _objectSpread3.default)({}, this.exists.where, arguments.length <= 0 ? undefined : arguments[0]);
      } else if (typeof (arguments.length <= 0 ? undefined : arguments[0]) === 'string') {
        var operator = Op.eq;

        if (!(arguments.length <= 1 ? undefined : arguments[1])) {
          operator = Op.is;
        }

        this.exists.where = (0, _objectSpread3.default)({}, this.exists.where, (0, _defineProperty2.default)({}, arguments.length <= 0 ? undefined : arguments[0], (0, _defineProperty2.default)({}, operator, arguments.length <= 1 ? undefined : arguments[1])));
      }

      return this;
    }
  }]);
  return Exists;
}();

var _default = Exists;
exports.default = _default;