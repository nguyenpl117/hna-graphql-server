'use strict';

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _unique = _interopRequireDefault(require("./rules/unique"));

var _exists = _interopRequireDefault(require("./rules/exists"));

/**
 * Created by Phan Trung Nguyên.
 * User: nguyenpl117
 * Date: 3/29/2019
 * Time: 12:15 AM
 */
var Rule =
/*#__PURE__*/
function () {
  function Rule() {
    (0, _classCallCheck2.default)(this, Rule);
  }

  (0, _createClass2.default)(Rule, null, [{
    key: "unique",

    /**
     *
     * @param model
     * @param column
     * @returns {Unique}
     */
    value: function unique(model, column) {
      return new _unique.default(model, column);
    }
    /**
     *
     * @param model
     * @param column
     * @returns {Exists}
     */

  }, {
    key: "exists",
    value: function exists(model, column) {
      return new _exists.default(model, column);
    }
    /**
     *
     * @param args Array
     * @returns {string}
     */

  }, {
    key: "notIn",
    value: function notIn(args) {
      return "not_in:".concat(args.join(','));
    }
    /**
     *
     * @param args Array
     * @returns {string}
     */

  }, {
    key: "in",
    value: function _in(args) {
      return "in:".concat(args.join(','));
    }
  }]);
  return Rule;
}();

var _default = Rule;
exports.default = _default;