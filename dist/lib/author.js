"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _arr = _interopRequireDefault(require("./arr"));

var _auth = _interopRequireDefault(require("./auth"));

var _values = _interopRequireDefault(require("lodash/values"));

var _errors = require("../exceptions/errors");

var Author =
/*#__PURE__*/
function () {
  function Author() {
    (0, _classCallCheck2.default)(this, Author);
  }

  (0, _createClass2.default)(Author, null, [{
    key: "hasRole",

    /**
     * Checks if the user has a role by its name.
     * @param {string | Array}          roles Role name or array of role names.
     * @param {boolean} requireAll      All roles in the array are required.
     * @return {boolean}
     */
    value: function hasRole(roles) {
      var requireAll = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;
      roles = _arr.default.array_wrap(roles, ',');
      var _iteratorNormalCompletion = true;
      var _didIteratorError = false;
      var _iteratorError = undefined;

      try {
        for (var _iterator = roles[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
          var role = _step.value;
          var check = _auth.default.getRoles().indexOf(role) !== -1;

          if (check && !requireAll) {
            return true;
          } else if (!check && requireAll) {
            return false;
          }
        }
      } catch (err) {
        _didIteratorError = true;
        _iteratorError = err;
      } finally {
        try {
          if (!_iteratorNormalCompletion && _iterator.return != null) {
            _iterator.return();
          }
        } finally {
          if (_didIteratorError) {
            throw _iteratorError;
          }
        }
      }

      return requireAll;
    }
    /**
     *  Check if user has a permission by its name.
     * @param {string|Array} permission         Permission string or array of permissions.
     * @param {boolean} requireAll          All permissions in the array are required.
     * @return {boolean}
     */

  }, {
    key: "can",
    value: function can(permission) {
      var requireAll = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;
      permission = _arr.default.array_wrap(permission, ',');
      var _iteratorNormalCompletion2 = true;
      var _didIteratorError2 = false;
      var _iteratorError2 = undefined;

      try {
        for (var _iterator2 = permission[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
          var perName = _step2.value;
          var check = _auth.default.getScopes().indexOf(perName) !== -1;

          if (check && !requireAll) {
            return true;
          } else if (!check && requireAll) {
            return false;
          }
        }
      } catch (err) {
        _didIteratorError2 = true;
        _iteratorError2 = err;
      } finally {
        try {
          if (!_iteratorNormalCompletion2 && _iterator2.return != null) {
            _iterator2.return();
          }
        } finally {
          if (_didIteratorError2) {
            throw _iteratorError2;
          }
        }
      }

      return requireAll;
    }
    /**
     * Checks role(s) and permission(s).
     * @param {string|Array} roles          Array of roles or comma separated string
     * @param {string|Array} permission     Array of permissions or comma separated string.
     * @param {object} options              validateAll (true|false) or returnType (boolean|array|both)
     * @return {array|bool}
     */

  }, {
    key: "ability",
    value: function ability(roles, permission, _ref) {
      var validateAll = _ref.validateAll,
          returnType = _ref.returnType;
      roles = _arr.default.array_wrap(roles, ',');
      permission = _arr.default.array_wrap(permission, ',');
      validateAll = validateAll || false;
      returnType = returnType || 'boolean';

      if (typeof validateAll !== 'boolean') {
        throw new _errors.InvalidOperationException();
      }

      if (['boolean', 'array', 'both'].indexOf(returnType) !== -1) {
        throw new InvalidArgumentException();
      } // Loop through roles and permissions and check each.


      var checkedRoles = {};
      var checkedPermissions = {};
      var _iteratorNormalCompletion3 = true;
      var _didIteratorError3 = false;
      var _iteratorError3 = undefined;

      try {
        for (var _iterator3 = roles[Symbol.iterator](), _step3; !(_iteratorNormalCompletion3 = (_step3 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
          var role = _step3.value;
          checkedRoles[role] = this.hasRole(role);
        }
      } catch (err) {
        _didIteratorError3 = true;
        _iteratorError3 = err;
      } finally {
        try {
          if (!_iteratorNormalCompletion3 && _iterator3.return != null) {
            _iterator3.return();
          }
        } finally {
          if (_didIteratorError3) {
            throw _iteratorError3;
          }
        }
      }

      var _iteratorNormalCompletion4 = true;
      var _didIteratorError4 = false;
      var _iteratorError4 = undefined;

      try {
        for (var _iterator4 = permission[Symbol.iterator](), _step4; !(_iteratorNormalCompletion4 = (_step4 = _iterator4.next()).done); _iteratorNormalCompletion4 = true) {
          var perName = _step4.value;
          checkedPermissions[perName] = this.can(perName);
        }
      } catch (err) {
        _didIteratorError4 = true;
        _iteratorError4 = err;
      } finally {
        try {
          if (!_iteratorNormalCompletion4 && _iterator4.return != null) {
            _iterator4.return();
          }
        } finally {
          if (_didIteratorError4) {
            throw _iteratorError4;
          }
        }
      }

      if (validateAll && !((0, _values.default)(checkedRoles).indexOf(false) === -1 || (0, _values.default)(checkedPermissions).indexOf(false) === -1) || !validateAll && ((0, _values.default)(checkedRoles).indexOf(true) === -1 || (0, _values.default)(checkedPermissions).indexOf(true) === -1)) {
        validateAll = true;
      } else {
        validateAll = false;
      }

      if (returnType === 'boolean') {
        return validateAll;
      } else if (returnType === 'array') {
        return {
          roles: checkedRoles,
          permissions: checkedPermissions
        };
      } else {
        return [validateAll, {
          roles: checkedRoles,
          permissions: checkedPermissions
        }];
      }
    }
  }]);
  return Author;
}();

var _default = Author;
exports.default = _default;