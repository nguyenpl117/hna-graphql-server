'use strict';

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Filters = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _lodash = _interopRequireDefault(require("lodash"));

var _arr = _interopRequireDefault(require("./arr"));

var _require = require('sequelize'),
    Op = _require.Op;

var sequelize = require('sequelize');
/**
 * Created by Phan Trung Nguyên.
 * User: nguyenpl117
 * Date: 3/31/2019
 * Time: 9:04 AM
 */


var Filters =
/*#__PURE__*/
function () {
  function Filters() {
    (0, _classCallCheck2.default)(this, Filters);
  }

  (0, _createClass2.default)(Filters, null, [{
    key: "compieFilter",
    value: function () {
      var _compieFilter = (0, _asyncToGenerator2.default)(
      /*#__PURE__*/
      _regenerator.default.mark(function _callee(filters) {
        var result;
        return _regenerator.default.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                this.include = [];
                _context.next = 3;
                return this.renderFilter(filters);

              case 3:
                _context.t0 = _context.sent;
                _context.t1 = _lodash.default.clone(this.include);
                result = {
                  where: _context.t0,
                  include: _context.t1
                };
                return _context.abrupt("return", result);

              case 7:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      function compieFilter(_x) {
        return _compieFilter.apply(this, arguments);
      }

      return compieFilter;
    }()
  }, {
    key: "renderFilter",
    value: function () {
      var _renderFilter = (0, _asyncToGenerator2.default)(
      /*#__PURE__*/
      _regenerator.default.mark(function _callee4(filters) {
        var _this = this;

        return _regenerator.default.wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                if (_lodash.default.isObject(filters)) {
                  _context4.next = 2;
                  break;
                }

                return _context4.abrupt("return", {});

              case 2:
                if (!(this.is_group(filters) && filters.groups.filter(function (group) {
                  return _this.is_item(group);
                }).length)) {
                  _context4.next = 12;
                  break;
                }

                _context4.t0 = _defineProperty2.default;
                _context4.t1 = {};
                _context4.t2 = Op[filters.operator];
                _context4.next = 8;
                return Promise.all(filters.groups.map(
                /*#__PURE__*/
                function () {
                  var _ref = (0, _asyncToGenerator2.default)(
                  /*#__PURE__*/
                  _regenerator.default.mark(function _callee2(filter) {
                    return _regenerator.default.wrap(function _callee2$(_context2) {
                      while (1) {
                        switch (_context2.prev = _context2.next) {
                          case 0:
                            return _context2.abrupt("return", _this.renderFilter(filter));

                          case 1:
                          case "end":
                            return _context2.stop();
                        }
                      }
                    }, _callee2);
                  }));

                  return function (_x3) {
                    return _ref.apply(this, arguments);
                  };
                }()));

              case 8:
                _context4.t3 = _context4.sent;
                return _context4.abrupt("return", (0, _context4.t0)(_context4.t1, _context4.t2, _context4.t3));

              case 12:
                if (!(this.is_item(filters) && filters.items.filter(function (item) {
                  return _this.is_item(item) || _this.is_group(item) || _this.is_where(item);
                }).length)) {
                  _context4.next = 22;
                  break;
                }

                _context4.t4 = _defineProperty2.default;
                _context4.t5 = {};
                _context4.t6 = Op[filters.operator];
                _context4.next = 18;
                return Promise.all(filters.items.map(
                /*#__PURE__*/
                function () {
                  var _ref3 = (0, _asyncToGenerator2.default)(
                  /*#__PURE__*/
                  _regenerator.default.mark(function _callee3(filter) {
                    return _regenerator.default.wrap(function _callee3$(_context3) {
                      while (1) {
                        switch (_context3.prev = _context3.next) {
                          case 0:
                            return _context3.abrupt("return", _this.renderFilter(filter));

                          case 1:
                          case "end":
                            return _context3.stop();
                        }
                      }
                    }, _callee3);
                  }));

                  return function (_x4) {
                    return _ref3.apply(this, arguments);
                  };
                }()));

              case 18:
                _context4.t7 = _context4.sent;
                return _context4.abrupt("return", (0, _context4.t4)(_context4.t5, _context4.t6, _context4.t7));

              case 22:
                if (!this.is_where(filters)) {
                  _context4.next = 26;
                  break;
                }

                _context4.next = 25;
                return this.filter(filters);

              case 25:
                return _context4.abrupt("return", _context4.sent);

              case 26:
                return _context4.abrupt("return", {});

              case 27:
              case "end":
                return _context4.stop();
            }
          }
        }, _callee4, this);
      }));

      function renderFilter(_x2) {
        return _renderFilter.apply(this, arguments);
      }

      return renderFilter;
    }()
  }, {
    key: "filter",
    value: function () {
      var _filter2 = (0, _asyncToGenerator2.default)(
      /*#__PURE__*/
      _regenerator.default.mark(function _callee5(_filter) {
        var array, value, _field, field;

        return _regenerator.default.wrap(function _callee5$(_context5) {
          while (1) {
            switch (_context5.prev = _context5.next) {
              case 0:
                array = ['between', 'notBetween', 'in', 'notIn'];
                value = array.indexOf(_filter.operator) !== -1 ? _filter.value : _arr.default.array_wrap(_filter.value).join('');

                if (_filter.value === null) {
                  value = _filter.value;
                }

                if (!(typeof _filter.field === 'function')) {
                  _context5.next = 18;
                  break;
                }

                _context5.next = 6;
                return _filter.field(value, _filter.operator);

              case 6:
                _field = _context5.sent;

                if (!(typeof _field === 'string')) {
                  _context5.next = 17;
                  break;
                }

                _context5.t0 = _defineProperty2.default;
                _context5.t1 = {};
                _context5.t2 = Op.and;
                _context5.t3 = sequelize;
                _context5.next = 14;
                return _filter.field(value, _filter.operator);

              case 14:
                _context5.t4 = _context5.sent;
                _context5.t5 = _context5.t3.literal.call(_context5.t3, _context5.t4);
                return _context5.abrupt("return", (0, _context5.t0)(_context5.t1, _context5.t2, _context5.t5));

              case 17:
                return _context5.abrupt("return", (0, _defineProperty2.default)({}, _field.field, (0, _defineProperty2.default)({}, Op[_filter.operator], _field.value)));

              case 18:
                field = _arr.default.array_wrap(_filter.field, '.');

                if (!(field.length === 2)) {
                  _context5.next = 23;
                  break;
                }

                if (!this.include) {
                  this.include = [];
                }

                this.include.push(_lodash.default.first(field));
                return _context5.abrupt("return", (0, _defineProperty2.default)({}, Op.col, sequelize.where(sequelize.col(_filter.field), Op[_filter.operator], value)));

              case 23:
                return _context5.abrupt("return", (0, _defineProperty2.default)({}, _filter.field, (0, _defineProperty2.default)({}, Op[_filter.operator], value)));

              case 24:
              case "end":
                return _context5.stop();
            }
          }
        }, _callee5, this);
      }));

      function filter(_x5) {
        return _filter2.apply(this, arguments);
      }

      return filter;
    }()
  }, {
    key: "getInclude",
    value: function getInclude() {
      return this.include;
    }
  }, {
    key: "is_item",
    value: function is_item(item) {
      return item.items && item.operator;
    }
  }, {
    key: "is_group",
    value: function is_group(group) {
      return group.groups && group.operator;
    }
  }, {
    key: "is_where",
    value: function is_where(where) {
      if (!where) return false;
      return where.field && 'value' in where && where.operator;
    }
  }]);
  return Filters;
}();

exports.Filters = Filters;