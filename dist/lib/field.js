"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var Field =
/*#__PURE__*/
function () {
  function Field() {
    (0, _classCallCheck2.default)(this, Field);
    this.authentication = false;
    this.cache = true;
  }
  /**
   * Kiểm tra quyền hạn của người dùng
   *
   * @param args
   * @return {Promise<boolean>}
   */


  (0, _createClass2.default)(Field, [{
    key: "authorize",
    value: function () {
      var _authorize = (0, _asyncToGenerator2.default)(
      /*#__PURE__*/
      _regenerator.default.mark(function _callee(args) {
        return _regenerator.default.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                return _context.abrupt("return", true);

              case 1:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      }));

      function authorize(_x) {
        return _authorize.apply(this, arguments);
      }

      return authorize;
    }()
    /**
     * Kết quả trả ra dạng
     *
     * @return {*}
     */

  }, {
    key: "type",
    value: function type() {
      return Types.ListOf(Types.Post);
    }
    /**
     * Giá trị truyền lên
     *
     * @return {{}}
     */

  }, {
    key: "args",
    value: function args() {
      return {
        filter: Types.String
      };
    }
    /**
     * Validate dữ liệu trước khi xử lý
     *
     * @param args
     */

  }, {
    key: "rules",
    value: function rules(args) {}
    /**
     * Custom lại tin nhắn validate trước khi response
     *
     * @param args
     * @return {{}}
     */

  }, {
    key: "validationErrorMessages",
    value: function validationErrorMessages(args) {
      return {};
    }
    /**
     * Thao tác xử lý dữ liệu
     *
     * @param parent
     * @param args
     * @param context
     * @param info
     */

  }, {
    key: "resolve",
    value: function resolve(parent, args, context, info) {}
    /**
     * Filter dữ liệu ( socket )
     *
     * @param payload
     * @param args
     */

  }, {
    key: "filter",
    value: function filter(payload, args) {}
  }]);
  return Field;
}();

module.exports = {
  Field: Field
};