'use strict';
/**
 * Created by Phan Trung Nguyên.
 * User: nguyenpl117
 * Date: 3/27/2019
 * Time: 9:08 PM
 */

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var Auth =
/*#__PURE__*/
function () {
  function Auth() {
    (0, _classCallCheck2.default)(this, Auth);
  }

  (0, _createClass2.default)(Auth, null, [{
    key: "init",
    value: function init() {
      this.dataUser = null;
      this.scopes = null;
      this.roles = [];
    }
  }, {
    key: "check",
    value: function check() {
      return !!this.dataUser;
    }
    /**
     *
     * @param user
     * @return {null}
     */

  }, {
    key: "setUser",
    value: function setUser(user) {
      // Nếu không có user
      if (!user) {
        return null;
      }

      this.dataUser = user;

      if (user.roles && Array.isArray(user.roles)) {
        this.roles = user.roles.map(function (item) {
          return item.name;
        });
      }
    }
  }, {
    key: "id",
    value: function id() {
      return this.dataUser ? this.dataUser.id : null;
    }
  }, {
    key: "user",
    value: function user() {
      return this.dataUser;
    }
  }, {
    key: "setScope",
    value: function setScope(scopes) {
      this.scopes = scopes;
    }
  }, {
    key: "getScopes",
    value: function getScopes() {
      return this.scopes || [];
    }
  }, {
    key: "getRoles",
    value: function getRoles() {
      return this.roles || [];
    }
  }]);
  return Auth;
}();

var _default = Auth;
exports.default = _default;