"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _typeof2 = _interopRequireDefault(require("@babel/runtime/helpers/typeof"));

var _toConsumableArray2 = _interopRequireDefault(require("@babel/runtime/helpers/toConsumableArray"));

var _hasOne = _interopRequireDefault(require("./relations/has-one"));

var _belongsToMany = _interopRequireDefault(require("./relations/belongs-to-many"));

var _hasMany = _interopRequireDefault(require("./relations/has-many"));

var _errors = _interopRequireDefault(require("sequelize/lib/errors"));

var _base = _interopRequireDefault(require("./relations/base"));

var _require = require('sequelize'),
    Utils = _require.Utils,
    Model = _require.Model,
    Promise = _require.Promise;

var _ = require('lodash');

Model.findAll = function (options) {
  var _this = this;

  if (options !== undefined && !_.isPlainObject(options)) {
    throw new _errors.default.QueryError('The argument passed to findAll must be an options object, use findByPk if you wish to pass a single primary key value');
  }

  if (options !== undefined && options.attributes) {
    if (!Array.isArray(options.attributes) && !_.isPlainObject(options.attributes)) {
      throw new _errors.default.QueryError('The attributes option must be an array of column names or an object');
    }
  }

  this.warnOnInvalidOptions(options, Object.keys(this.rawAttributes));
  var tableNames = {};
  var originalOptions;
  tableNames[this.getTableName(options)] = true;
  options = Utils.cloneDeep(options);

  _.defaults(options, {
    hooks: true,
    rejectOnEmpty: this.options.rejectOnEmpty
  }); // set rejectOnEmpty option from model config


  options.rejectOnEmpty = options.rejectOnEmpty || this.options.rejectOnEmpty;
  return Promise.try(function () {
    _this._injectScope(options);

    if (options.hooks) {
      return _this.runHooks('beforeFind', options);
    }
  }).then(function () {
    _this._conformOptions(options, _this);

    _this._expandIncludeAll(options);

    if (options.hooks) {
      return _this.runHooks('beforeFindAfterExpandIncludeAll', options);
    }
  }).then(function () {
    if (options.include) {
      options.hasJoin = true;

      _this._validateIncludedElements(options, tableNames); // If we're not raw, we have to make sure we include the primary key for deduplication


      if (options.attributes && !options.raw && _this.primaryKeyAttribute && options.attributes.indexOf(_this.primaryKeyAttribute) === -1) {
        options.originalAttributes = options.attributes;

        if (!options.group || !options.hasSingleAssociation || options.hasMultiAssociation) {
          options.attributes = [_this.primaryKeyAttribute].concat(options.attributes);
        }
      }
    }

    if (!options.attributes) {
      options.attributes = Object.keys(_this.tableAttributes);
    }

    if (options.with && Array.isArray(options.with)) {
      options.with.forEach(function (data) {
        if (typeof _this[data.name] === 'function') {
          var relation = _this[data.name]();

          options.attributes.push(relation.primaryKey);
        }
      });
    }

    if (_this.options && _this.options.appendSelect && Array.isArray(_this.options.appendSelect)) {
      options.attributes = [].concat((0, _toConsumableArray2.default)(options.attributes), (0, _toConsumableArray2.default)(_this.options.appendSelect));
    }

    options.attributes = _.uniq(options.attributes); // whereCollection is used for non-primary key updates

    _this.options.whereCollection = options.where || null;
    Utils.mapFinderOptions(options, _this);
    options = _this._paranoidClause(_this, options);

    if (options.hooks) {
      return _this.runHooks('beforeFindAfterOptions', options);
    }
  }).then(function () {
    originalOptions = Utils.cloneDeep(options);
    options.tableNames = Object.keys(tableNames);
    return _this.QueryInterface.select(_this, _this.getTableName(options), options);
  }).tap(function (results) {
    if (options.hooks) {
      return _this.runHooks('afterFind', results, options);
    }
  }).then(function (results) {
    //rejectOnEmpty mode
    if (_.isEmpty(results) && options.rejectOnEmpty) {
      if (typeof options.rejectOnEmpty === 'function') {
        throw new options.rejectOnEmpty();
      } else if ((0, _typeof2.default)(options.rejectOnEmpty) === 'object') {
        throw options.rejectOnEmpty;
      } else {
        throw new _errors.default.EmptyResultError();
      }
    }

    return Model._findSeparate(results, originalOptions);
  }).then(function (results) {
    return _this._eagerLoad(results, originalOptions);
  });
};

Model.selectQuery = function (options) {
  var _this2 = this;

  if (options !== undefined && !_.isPlainObject(options)) {
    throw new _errors.default.QueryError('The argument passed to findAll must be an options object, use findByPk if you wish to pass a single primary key value');
  }

  if (options !== undefined && options.attributes) {
    if (!Array.isArray(options.attributes) && !_.isPlainObject(options.attributes)) {
      throw new _errors.default.QueryError('The attributes option must be an array of column names or an object');
    }
  }

  this.warnOnInvalidOptions(options, Object.keys(this.rawAttributes));
  var tableNames = {};
  var originalOptions;
  tableNames[this.getTableName(options)] = true;
  options = Utils.cloneDeep(options);

  _.defaults(options, {
    hooks: true,
    rejectOnEmpty: this.options.rejectOnEmpty
  }); // set rejectOnEmpty option from model config


  options.rejectOnEmpty = options.rejectOnEmpty || this.options.rejectOnEmpty;
  return Promise.try(function () {
    _this2._injectScope(options);
  }).then(function () {
    _this2._conformOptions(options, _this2);

    _this2._expandIncludeAll(options);
  }).then(function () {
    if (options.include) {
      options.hasJoin = true;

      _this2._validateIncludedElements(options, tableNames); // If we're not raw, we have to make sure we include the primary key for deduplication


      if (options.attributes && !options.raw && _this2.primaryKeyAttribute && options.attributes.indexOf(_this2.primaryKeyAttribute) === -1) {
        options.originalAttributes = options.attributes;

        if (!options.group || !options.hasSingleAssociation || options.hasMultiAssociation) {
          options.attributes = [_this2.primaryKeyAttribute].concat(options.attributes);
        }
      }
    }

    if (!options.attributes) {
      options.attributes = Object.keys(_this2.tableAttributes);
    }

    if (options.with && Array.isArray(options.with)) {
      options.with.forEach(function (data) {
        if (typeof _this2[data.name] === 'function') {
          var relation = _this2[data.name]();

          options.attributes.push(relation.primaryKey);
        }
      });
    }

    if (_this2.options && _this2.options.appendSelect && Array.isArray(_this2.options.appendSelect)) {
      options.attributes = [].concat((0, _toConsumableArray2.default)(options.attributes), (0, _toConsumableArray2.default)(_this2.options.appendSelect));
    }

    options.attributes = _.uniq(options.attributes); // whereCollection is used for non-primary key updates

    _this2.options.whereCollection = options.where || null;
    Utils.mapFinderOptions(options, _this2);
    options = _this2._paranoidClause(_this2, options);
  }).then(function () {
    originalOptions = Utils.cloneDeep(options);
    options.tableNames = Object.keys(tableNames);
    options = Utils.cloneDeep(options);
    options.type = 'SELECT';
    options.model = _this2;
    return _this2.sequelize.dialect.QueryGenerator.selectQuery(_this2.getTableName(options), options, _this2);
  });
};
/**
 *
 * @param results
 * @param options
 * @return {*}
 * @private
 */


Model._eagerLoad = function (results, options) {
  var _this3 = this;

  if (!options.with || options.raw || !results) return results;
  var original = results;
  if (options.plain) results = [results];
  if (!results.length) return original;
  return Promise.map(options.with,
  /*#__PURE__*/
  function () {
    var _ref = (0, _asyncToGenerator2.default)(
    /*#__PURE__*/
    _regenerator.default.mark(function _callee(data) {
      var relation, values, _iteratorNormalCompletion, _didIteratorError, _iteratorError, _iterator, _step, item, value;

      return _regenerator.default.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              if (!(typeof _this3[data.name] !== 'function')) {
                _context.next = 2;
                break;
              }

              return _context.abrupt("return");

            case 2:
              relation = _this3[data.name]();

              if (relation instanceof _base.default) {
                _context.next = 5;
                break;
              }

              return _context.abrupt("return");

            case 5:
              _context.next = 7;
              return relation.eagerLoad(results, data);

            case 7:
              values = _context.sent;
              _iteratorNormalCompletion = true;
              _didIteratorError = false;
              _iteratorError = undefined;
              _context.prev = 11;

              for (_iterator = results[Symbol.iterator](); !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
                item = _step.value;
                value = relation.getItems(values, item); // console.log(value);

                if (value) {
                  item.set(data.name, value, {
                    raw: true
                  });
                }
              }

              _context.next = 19;
              break;

            case 15:
              _context.prev = 15;
              _context.t0 = _context["catch"](11);
              _didIteratorError = true;
              _iteratorError = _context.t0;

            case 19:
              _context.prev = 19;
              _context.prev = 20;

              if (!_iteratorNormalCompletion && _iterator.return != null) {
                _iterator.return();
              }

            case 22:
              _context.prev = 22;

              if (!_didIteratorError) {
                _context.next = 25;
                break;
              }

              throw _iteratorError;

            case 25:
              return _context.finish(22);

            case 26:
              return _context.finish(19);

            case 27:
            case "end":
              return _context.stop();
          }
        }
      }, _callee, null, [[11, 15, 19, 27], [20,, 22, 26]]);
    }));

    return function (_x) {
      return _ref.apply(this, arguments);
    };
  }()).return(original);
};
/**
 *
 * @param target
 * @param primaryKey
 * @param foreignKey
 * @param {*} scope
 * @return {HasOne}
 */


Model.customHasOne = function (target, _ref2) {
  var primaryKey = _ref2.primaryKey,
      foreignKey = _ref2.foreignKey,
      scope = _ref2.scope,
      type = _ref2.type;
  return new _hasOne.default(this, target, {
    primaryKey: primaryKey,
    foreignKey: foreignKey,
    scope: scope,
    type: type
  });
};
/**
 *
 * @param target
 * @param primaryKey
 * @param foreignKey
 * @param {model: Model, scope: {*}} through
 * @param otherKey
 * @param scope
 * @return {BelongsToMany}
 */


Model.customBelongsToMany = function (target, _ref3) {
  var primaryKey = _ref3.primaryKey,
      foreignKey = _ref3.foreignKey,
      through = _ref3.through,
      otherKey = _ref3.otherKey,
      scope = _ref3.scope,
      appendField = _ref3.appendField;

  if (typeof through === 'function') {
    through = {
      model: through
    };
  }

  return new _belongsToMany.default(this, target, {
    primaryKey: primaryKey,
    foreignKey: foreignKey,
    through: through,
    otherKey: otherKey,
    scope: scope,
    appendField: appendField
  });
};

Model.customHasMany = function (target, _ref4) {
  var primaryKey = _ref4.primaryKey,
      foreignKey = _ref4.foreignKey,
      scope = _ref4.scope;
  return new _hasMany.default(this, target, {
    primaryKey: primaryKey,
    foreignKey: foreignKey,
    scope: scope
  });
};

var _default = Model;
exports.default = _default;