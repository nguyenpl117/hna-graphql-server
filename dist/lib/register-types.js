"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _typeof2 = _interopRequireDefault(require("@babel/runtime/helpers/typeof"));

var _objectSpread2 = _interopRequireDefault(require("@babel/runtime/helpers/objectSpread"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _utils = require("./utils");

var _enumType = require("./support/enum-type");

var _unionType = require("./support/union-type");

var _graphqlType = require("./support/graphql-type");

var _inputType = require("./support/input-type");

var _filterType = require("./support/filter-type");

var _scalarType = require("./support/scalar-type");

var _graphql = require("graphql");

var _sortType = require("./support/sort-type");

var RegisterType =
/*#__PURE__*/
function () {
  function RegisterType() {
    (0, _classCallCheck2.default)(this, RegisterType);
  }

  (0, _createClass2.default)(RegisterType, null, [{
    key: "add",

    /**
     *
     * @param objectType
     * @returns {RegisterType}
     */
    value: function add(objectType) {
      /**
       * @type ObjectType
       */
      var obj = new objectType();
      var name = objectType.iniName + obj.name;

      if (!this.listType[name]) {
        this.listType[name] = obj;
      }

      return this;
    }
    /**
     *
     * @returns {Array}
     */

  }, {
    key: "types",
    value: function types() {
      var types = [];

      for (var i in this.listType) {
        /**
         * @type ObjectType
         */
        var item = this.listType[i];

        if (item instanceof _unionType.UnionType) {
          types.push("\"\"\"".concat(item.description, "\"\"\" union ").concat(item.name, " = ").concat(item.types().join('|')));
        } else if (item instanceof _enumType.EnumType) {
          types.push("\"\"\"".concat(item.description, "\"\"\" enum ").concat(item.name, " {").concat(this.getValueEnum(item.values()), "}"));
        } else if (item instanceof _sortType.SortType) {
          types.push("\"\"\"".concat(item.description, "\"\"\" input ").concat(item.name, " {").concat(this.getValueSort(item.fields()), "}"));
        } else if (item instanceof _filterType.FilterType) {
          types.push("".concat(_filterType.FilterType.renderFilterType(item.name, this.getValueEnum(item.values()), item.description)));
        } else if (item instanceof _inputType.InputType) {
          types.push("\"\"\"".concat(item.description, "\"\"\" input ").concat(item.name, " {").concat(this.getPropertiesType(item.fields()), "}"));
        } else if (item instanceof _scalarType.ScalarType) {
          types.push("scalar ".concat(item.name));
        } else if (item instanceof _graphqlType.GraphqlType) {
          types.push("\"\"\"".concat(item.description, "\"\"\" type ").concat(item.name, " ").concat(this.getCacheControl(item.cacheControl), " {").concat(this.getPropertiesType((0, _objectSpread2.default)({}, item.fields(), {
            noCache: {
              type: 'Boolean @cacheControl(maxAge: 0)',
              description: 'Không lấy cache.'
            }
          })), "}"));
        } else {
          types.push("\"\"\"".concat(item.description, "\"\"\" type ").concat(item.name, " {").concat(this.getPropertiesType(item.fields()), "}"));
        }
      }

      return types;
    }
  }, {
    key: "getCacheControl",
    value: function getCacheControl(cacheControl) {
      if (!cacheControl) {
        return '';
      }

      var maxAge = cacheControl.maxAge,
          scope = cacheControl.scope;
      var m = maxAge ? 'maxAge:' + maxAge : '';
      var s = scope ? 'scope:' + scope : '';

      if ([m, s].join('').length < 1) {
        return '';
      }

      return "@cacheControl(".concat([m, s].join(','), ")");
    }
  }, {
    key: "getValueSort",
    value: function getValueSort(fields) {
      var properties = [];

      for (var key in fields) {
        if (fields[key].description) {
          properties.push("\"\"\"".concat(fields[key].description, "\"\"\""));
        }

        properties.push("".concat(key, ": SortValue"));
      }

      return properties;
    }
  }, {
    key: "getValueEnum",
    value: function getValueEnum(values) {
      var properties = [];

      for (var key in values) {
        if (values[key].description) {
          properties.push("\"\"\"".concat(values[key].description, "\"\"\""));
        }

        properties.push("".concat(key));
      }

      return properties;
    }
    /**
     * @returns {*}
     */

  }, {
    key: "resolvers",
    value: function resolvers() {
      var _this = this;

      var resolvers = {};

      var _loop = function _loop(i) {
        /**
         * @type GraphqlType|EnumType|FilterType|InputType|ScalarType
         */
        var item = _this.listType[i];

        if (item instanceof _unionType.UnionType) {
          resolvers[item.name] = {
            __resolveType: function __resolveType(obj, context, info) {
              return item.resolveType(obj, context, info);
            }
          };
        } else if (item instanceof _enumType.EnumType) {
          resolvers[item.name] = (0, _objectSpread2.default)({}, _this.getResolveEnum(item.values()));
        } else if (item instanceof _filterType.FilterType) {
          resolvers['FilterField' + item.name] = (0, _objectSpread2.default)({}, _this.getResolveEnum(item.values()));
        } else if (item instanceof _inputType.InputType) {
          resolvers[item.name] = (0, _objectSpread2.default)({}, _this.getResolve(item.fields()));
        } else if (item instanceof _scalarType.ScalarType) {
          resolvers[item.name] = new _graphql.GraphQLScalarType(item);
        } else if (item instanceof _sortType.SortType) {
          resolvers[item.name] = (0, _objectSpread2.default)({}, _this.getResolve(item.fields()));
        } else {
          resolvers[item.name] = (0, _objectSpread2.default)({}, _this.getResolve(item.fields()));
        }
      };

      for (var i in this.listType) {
        _loop(i);
      }

      return resolvers;
    }
  }, {
    key: "getResolveEnum",
    value: function getResolveEnum(values) {
      var r = {};

      for (var i in values) {
        r[i] = i;

        if (values[i].value) {
          r[i] = values[i].value;
        }
      }

      return r;
    }
    /**
     *
     * @param obj ObjectType
     */

  }, {
    key: "getResolve",
    value: function getResolve(obj) {
      if (!obj || (0, _typeof2.default)(obj) !== 'object') {
        return {};
      }

      var res = {};

      var _loop2 = function _loop2(key) {
        var item = obj[key];

        if (item.hasOwnProperty('value')) {
          res[key] = item.value;
        }

        if (item.hasOwnProperty('resolve')) {
          res[key] = item.resolve;
        }

        if (item.hasOwnProperty('privacy')) {
          res[key] = function (root, args, cnt, info) {
            if (item.privacy(root, args, cnt, info)) {
              if (item.hasOwnProperty('resolve')) {
                return item.resolve(root, args, cnt, info);
              }

              return root[key];
            }

            return null;
          };
        }
      };

      for (var key in obj) {
        _loop2(key);
      }

      return res;
    }
    /**
     *
     * @param obj ObjectType
     */

  }, {
    key: "getPropertiesType",
    value: function getPropertiesType(obj) {
      var properties = [];

      for (var key in obj) {
        if (obj[key].description) {
          properties.push("\"\"\"".concat(obj[key].description, "\"\"\""));
        }

        if (obj[key].type) {
          properties.push("".concat(key).concat(this.compieArgs(obj[key].args), ":").concat(obj[key].type).concat(this.compiedDirectives(obj[key].directives)));
        } else {
          properties.push("".concat(key, ":").concat(obj[key]));
        }
      }

      return properties;
    }
  }, {
    key: "compiedDirectives",
    value: function compiedDirectives(obj) {
      if (!Array.isArray(obj) || !obj.length) {
        return '';
      }

      var directives = [];
      var _iteratorNormalCompletion = true;
      var _didIteratorError = false;
      var _iteratorError = undefined;

      try {
        for (var _iterator = obj[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
          var item = _step.value;
          directives.push("@".concat(item.name).concat(this.compieDirectiveArguments(item.args)));
        }
      } catch (err) {
        _didIteratorError = true;
        _iteratorError = err;
      } finally {
        try {
          if (!_iteratorNormalCompletion && _iterator.return != null) {
            _iterator.return();
          }
        } finally {
          if (_didIteratorError) {
            throw _iteratorError;
          }
        }
      }

      return directives;
    }
  }, {
    key: "compieDirectiveArguments",
    value: function compieDirectiveArguments(object) {
      if ((0, _typeof2.default)(object) !== 'object') {
        return '';
      }

      var args = [];

      for (var item in object) {
        args.push("".concat(item, ": ").concat(object[item]));
      }

      return args.length ? "(".concat(args.join(', '), ")") : '';
    }
    /**
     *
     * @param object
     * @returns {string}
     */

  }, {
    key: "compieArgs",
    value: function compieArgs(object) {
      if ((0, _typeof2.default)(object) !== 'object') {
        return '';
      }

      var args = [];

      for (var item in object) {
        if (typeof object[item] === 'string') {
          args.push("".concat(item, ": ").concat(object[item]));
        } else if ((0, _typeof2.default)(object[item]) === 'object' && object[item].type) {
          if (object[item].description) {
            args.push("\"\"\"".concat(object[item].description, "\"\"\""));
          }

          if (object[item].hasOwnProperty('defaultValue')) {
            args.push("".concat(item, ": ").concat(object[item].type, "=").concat(object[item].defaultValue));
          } else {
            args.push("".concat(item, ": ").concat(object[item].type));
          }
        }
      }

      return args.length ? "(".concat(args.join(', '), ")") : '';
    }
    /**
     *
     * @param name String
     * @param prefix String
     * @return {{}}
     */

  }, {
    key: "rules",
    value: function rules(name, prefix) {
      var obj = this.getType(name);

      if (!obj) {
        return {};
      }

      if (this.hasListOf(name)) {
        prefix = "".concat(prefix, ".*");
      }

      var rules = {};

      for (var i in obj.fields()) {
        var item = obj.fields()[i];

        if ((0, _typeof2.default)(item) === 'object' && item.rules) {
          rules["".concat(prefix, ".").concat(i)] = item.rules;
        }

        rules = (0, _utils.merge)(this.rules(item.type, "".concat(prefix, ".").concat(i)), rules);
      }

      return rules;
    }
    /**
     * @param typeName
     * @returns {boolean}
     */

  }, {
    key: "hasListOf",
    value: function hasListOf(typeName) {
      return /^\[.*\]$/g.test(typeName);
    }
    /**
     * @param typeName
     * @returns {*}
     */

  }, {
    key: "getType",
    value: function getType(typeName) {
      var type = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'input';

      if (this.hasListOf(typeName)) {
        typeName = typeName.replace(/^\[|\]$/g, '');
      }

      if (!this.listType[type + typeName]) {
        return false;
      }

      return this.listType[type + typeName];
    }
  }]);
  return RegisterType;
}();

exports.default = RegisterType;
(0, _defineProperty2.default)(RegisterType, "listType", {});