"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ScalarType = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var ScalarType =
/*#__PURE__*/
function () {
  function ScalarType() {
    (0, _classCallCheck2.default)(this, ScalarType);
    this.description = '';
    this.name = '';
  }

  (0, _createClass2.default)(ScalarType, [{
    key: "serialize",
    value: function serialize(value) {
      return value;
    }
  }, {
    key: "parseValue",
    value: function parseValue(value) {
      return value;
    }
  }, {
    key: "parseLiteral",
    value: function parseLiteral(ast) {
      return ast;
    }
  }]);
  return ScalarType;
}();

exports.ScalarType = ScalarType;
(0, _defineProperty2.default)(ScalarType, "iniName", 'scalar');