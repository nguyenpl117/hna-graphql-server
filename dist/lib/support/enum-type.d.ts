export class EnumType {
    name: string;
    
    description: string;
    
    values(): object
    
    types(): string
}

