'use strict';

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.QueryType = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _possibleConstructorReturn2 = _interopRequireDefault(require("@babel/runtime/helpers/possibleConstructorReturn"));

var _getPrototypeOf2 = _interopRequireDefault(require("@babel/runtime/helpers/getPrototypeOf"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime/helpers/inherits"));

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _field = require("../field");

/**
 * Created by Phan Trung Nguyên.
 * User: nguyenpl117
 * Date: 6/22/2019
 * Time: 8:55 PM
 */
var QueryType =
/*#__PURE__*/
function (_Field) {
  (0, _inherits2.default)(QueryType, _Field);

  function QueryType() {
    (0, _classCallCheck2.default)(this, QueryType);
    return (0, _possibleConstructorReturn2.default)(this, (0, _getPrototypeOf2.default)(QueryType).apply(this, arguments));
  }

  return QueryType;
}(_field.Field);

exports.QueryType = QueryType;
(0, _defineProperty2.default)(QueryType, "iniName", 'query');