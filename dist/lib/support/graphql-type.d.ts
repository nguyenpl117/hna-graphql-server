export class GraphqlType {
    name: string;
    
    description: string;
    
    cacheControl: Object;
    
    fields(): object
    
    types(): string
}