"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.EnumType = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var EnumType =
/*#__PURE__*/
function () {
  function EnumType() {
    (0, _classCallCheck2.default)(this, EnumType);
    this.description = '';
    this.name = '';
  }

  (0, _createClass2.default)(EnumType, [{
    key: "values",
    value: function values() {
      return {};
    }
  }]);
  return EnumType;
}();

exports.EnumType = EnumType;
(0, _defineProperty2.default)(EnumType, "iniName", 'enum');