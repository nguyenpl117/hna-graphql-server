export class InputType {
    name: string;
    
    description: string;
    
    fields(): object;
    
    types(): string;
}