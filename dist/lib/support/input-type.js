"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.InputType = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _registerTypes = _interopRequireDefault(require("../register-types"));

var InputType =
/*#__PURE__*/
function () {
  function InputType() {
    (0, _classCallCheck2.default)(this, InputType);
    this.description = '';
    this.name = '';
    this.input = true;
  }

  (0, _createClass2.default)(InputType, [{
    key: "fields",
    value: function fields() {
      return {};
    }
  }, {
    key: "types",
    value: function types() {
      return "\"\"\"".concat(this.description, "\"\"\" input ").concat(this.name, " {").concat(_registerTypes.default.getPropertiesType(this.args()), "}");
    }
  }]);
  return InputType;
}();

exports.InputType = InputType;
(0, _defineProperty2.default)(InputType, "iniName", 'input');