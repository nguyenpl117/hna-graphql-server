"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.SortType = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var SortType =
/*#__PURE__*/
function () {
  function SortType() {
    (0, _classCallCheck2.default)(this, SortType);
    this.description = '';
    this.name = '';
  }

  (0, _createClass2.default)(SortType, [{
    key: "fields",
    value: function fields() {
      return {};
    }
  }]);
  return SortType;
}();

exports.SortType = SortType;
(0, _defineProperty2.default)(SortType, "iniName", 'sort');