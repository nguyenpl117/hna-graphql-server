"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.FilterType = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var FilterType =
/*#__PURE__*/
function () {
  function FilterType() {
    (0, _classCallCheck2.default)(this, FilterType);
    this.description = '';
    this.name = '';
  }

  (0, _createClass2.default)(FilterType, [{
    key: "values",
    value: function values() {
      return {};
    }
  }], [{
    key: "renderFilterType",
    value: function renderFilterType(name, args, desc) {
      return "\n        \"\"\"".concat(desc, "\"\"\"\ninput Filter").concat(name, " {\n    groups: [FilterGroup").concat(name, "]\n    operator: Operator\n    value: [ValueType]\n    field: FilterField").concat(name, "\n    items: [Filter").concat(name, "]\n}\ninput FilterGroup").concat(name, " {\n    operator: Operator\n    items: [Filter").concat(name, "]\n}\nenum FilterField").concat(name, "{\n    ").concat(args, "\n}\n    ");
    }
  }]);
  return FilterType;
}();

exports.FilterType = FilterType;
(0, _defineProperty2.default)(FilterType, "iniName", 'filter');