"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.GraphqlType = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _registerTypes = _interopRequireDefault(require("../register-types"));

var GraphqlType =
/*#__PURE__*/
function () {
  function GraphqlType() {
    (0, _classCallCheck2.default)(this, GraphqlType);
    this.description = '';
    this.name = '';
    this.cacheControl = {};
  }

  (0, _createClass2.default)(GraphqlType, [{
    key: "fields",
    value: function fields() {
      return {};
    }
  }, {
    key: "types",
    value: function types() {
      return "\"\"\"".concat(this.description, "\"\"\" type ").concat(this.name, " {").concat(_registerTypes.default.getPropertiesType(this.fields()), "}");
    }
  }]);
  return GraphqlType;
}();

exports.GraphqlType = GraphqlType;
(0, _defineProperty2.default)(GraphqlType, "iniName", 'object');