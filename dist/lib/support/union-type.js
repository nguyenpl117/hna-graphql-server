"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.UnionType = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var UnionType = function UnionType() {
  (0, _classCallCheck2.default)(this, UnionType);
  this.description = '';
  this.name = '';
};

exports.UnionType = UnionType;
(0, _defineProperty2.default)(UnionType, "iniName", 'union');