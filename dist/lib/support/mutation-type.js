'use strict';

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.MutationType = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _possibleConstructorReturn2 = _interopRequireDefault(require("@babel/runtime/helpers/possibleConstructorReturn"));

var _getPrototypeOf2 = _interopRequireDefault(require("@babel/runtime/helpers/getPrototypeOf"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime/helpers/inherits"));

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _field = require("../field");

/**
 * Created by Phan Trung Nguyên.
 * User: nguyenpl117
 * Date: 6/22/2019
 * Time: 9:07 PM
 */
var MutationType =
/*#__PURE__*/
function (_Field) {
  (0, _inherits2.default)(MutationType, _Field);

  function MutationType() {
    (0, _classCallCheck2.default)(this, MutationType);
    return (0, _possibleConstructorReturn2.default)(this, (0, _getPrototypeOf2.default)(MutationType).apply(this, arguments));
  }

  return MutationType;
}(_field.Field);

exports.MutationType = MutationType;
(0, _defineProperty2.default)(MutationType, "iniName", 'mutation');