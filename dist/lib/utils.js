"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.srcDir = srcDir;
exports.decrypt = decrypt;
exports.encrypt = encrypt;
exports.strip_tags = strip_tags;
exports.substr = substr;
exports.nl2br = nl2br;
exports.htmlField = htmlField;
exports.merge = merge;
exports.menu_recursive = menu_recursive;
exports.withdata = withdata;
exports.generateRandomString = generateRandomString;
exports.requestApi = requestApi;
exports.round = round;
exports.hash_affiliate_id_encode = hash_affiliate_id_encode;
exports.hash_affiliate_id_decode = hash_affiliate_id_decode;
exports.withAsync = withAsync;
exports.tinhgio = tinhgio;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _objectSpread2 = _interopRequireDefault(require("@babel/runtime/helpers/objectSpread"));

var _typeof2 = _interopRequireDefault(require("@babel/runtime/helpers/typeof"));

var _lodash = _interopRequireDefault(require("lodash"));

var serialize = _interopRequireWildcard(require("./serialize"));

var _apiValidationError = _interopRequireDefault(require("../exceptions/api-validation-error"));

require('dotenv').config();

var request = require('request');

var marked = require('marked');

var LCrypt = require('lcrypt');

var lcrypt = new LCrypt(process.env.APP_KEY);
var uri = process.env.NODE_ENV === 'production' ? process.env.LARAVEL_API_SERVER : process.env.LARAVEL_API;

var Hashids = require('hashids');

function srcDir() {
  return process.cwd() + '/' + (process.env.SRC_DIR || (process.env.NODE_ENV === 'production' ? 'dist' : 'app'));
}

function decrypt(payload) {
  var $serialize = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;

  if ($serialize) {
    return serialize.unserialize(lcrypt.decode(payload));
  }

  return lcrypt.decode(payload);
}

function encrypt(value) {
  var $serialize = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;

  if ($serialize) {
    return lcrypt.encode(serialize.serialize(value));
  }

  return lcrypt.encode(value);
}

function _phpCastString(value) {
  var type = (0, _typeof2.default)(value);

  switch (type) {
    case 'boolean':
      return value ? '1' : '';

    case 'string':
      return value;

    case 'number':
      if (isNaN(value)) {
        return 'NAN';
      }

      if (!isFinite(value)) {
        return (value < 0 ? '-' : '') + 'INF';
      }

      return value + '';

    case 'undefined':
      return '';

    case 'object':
      if (Array.isArray(value)) {
        return 'Array';
      }

      if (value !== null) {
        return 'Object';
      }

      return '';

    case 'function': // fall through

    default:
      throw new Error('Unsupported value type');
  }
}

function ini_get(varname) {
  // eslint-disable-line camelcase
  var $global = typeof window !== 'undefined' ? window : global;
  $global.$locutus = $global.$locutus || {};
  var $locutus = $global.$locutus;
  $locutus.php = $locutus.php || {};
  $locutus.php.ini = $locutus.php.ini || {};

  if ($locutus.php.ini[varname] && $locutus.php.ini[varname].local_value !== undefined) {
    if ($locutus.php.ini[varname].local_value === null) {
      return '';
    }

    return $locutus.php.ini[varname].local_value;
  }

  return '';
}

function strip_tags(input, allowed) {
  // making sure the allowed arg is a string containing only tags in lowercase (<a><b><c>)
  allowed = (((allowed || '') + '').toLowerCase().match(/<[a-z][a-z0-9]*>/g) || []).join('');
  var tags = /<\/?([a-z0-9]*)\b[^>]*>?/gi;
  var commentsAndPhpTags = /<!--[\s\S]*?-->|<\?(?:php)?[\s\S]*?\?>/gi;

  var after = _phpCastString(input); // removes tha '<' char at the end of the string to replicate PHP's behaviour


  after = after.substring(after.length - 1) === '<' ? after.substring(0, after.length - 1) : after; // recursively remove tags to ensure that the returned string doesn't contain forbidden tags after previous passes (e.g. '<<bait/>switch/>')

  while (true) {
    var before = after;
    after = before.replace(commentsAndPhpTags, '').replace(tags, function ($0, $1) {
      return allowed.indexOf('<' + $1.toLowerCase() + '>') > -1 ? $0 : '';
    }); // return once no more tags are removed

    if (before === after) {
      return after;
    }
  }
}

function substr(str, start, len) {
  str += '';
  var end = str.length;
  var iniVal = (typeof require !== 'undefined' ? ini_get('unicode.emantics') : undefined) || 'off';

  if (iniVal === 'off') {
    // assumes there are no non-BMP characters;
    // if there may be such characters, then it is best to turn it on (critical in true XHTML/XML)
    if (start < 0) {
      start += end;
    }

    if (typeof len !== 'undefined') {
      if (len < 0) {
        end = len + end;
      } else {
        end = len + start;
      }
    } // PHP returns false if start does not fall within the string.
    // PHP returns false if the calculated end comes before the calculated start.
    // PHP returns an empty string if start and end are the same.
    // Otherwise, PHP returns the portion of the string from start to end.


    if (start >= str.length || start < 0 || start > end) {
      return false;
    }

    return str.slice(start, end);
  } // Full-blown Unicode including non-Basic-Multilingual-Plane characters


  var i = 0;
  var allBMP = true;
  var es = 0;
  var el = 0;
  var se = 0;
  var ret = '';

  for (i = 0; i < str.length; i++) {
    if (/[\uD800-\uDBFF]/.test(str.charAt(i)) && /[\uDC00-\uDFFF]/.test(str.charAt(i + 1))) {
      allBMP = false;
      break;
    }
  }

  if (!allBMP) {
    if (start < 0) {
      for (i = end - 1, es = start += end; i >= es; i--) {
        if (/[\uDC00-\uDFFF]/.test(str.charAt(i)) && /[\uD800-\uDBFF]/.test(str.charAt(i - 1))) {
          start--;
          es--;
        }
      }
    } else {
      var surrogatePairs = /[\uD800-\uDBFF][\uDC00-\uDFFF]/g;

      while (surrogatePairs.exec(str) !== null) {
        var li = surrogatePairs.lastIndex;

        if (li - 2 < start) {
          start++;
        } else {
          break;
        }
      }
    }

    if (start >= end || start < 0) {
      return false;
    }

    if (len < 0) {
      for (i = end - 1, el = end += len; i >= el; i--) {
        if (/[\uDC00-\uDFFF]/.test(str.charAt(i)) && /[\uD800-\uDBFF]/.test(str.charAt(i - 1))) {
          end--;
          el--;
        }
      }

      if (start > end) {
        return false;
      }

      return str.slice(start, end);
    } else {
      se = start + len;

      for (i = start; i < se; i++) {
        ret += str.charAt(i);

        if (/[\uD800-\uDBFF]/.test(str.charAt(i)) && /[\uDC00-\uDFFF]/.test(str.charAt(i + 1))) {
          // Go one further, since one of the "characters" is part of a surrogate pair
          se++;
        }
      }

      return ret;
    }
  }
}

function nl2br(str, isXhtml) {
  // Some latest browsers when str is null return and unexpected null value
  if (typeof str === 'undefined' || str === null) {
    return '';
  } // Adjust comment to avoid issue on locutus.io display


  var breakTag = isXhtml || typeof isXhtml === 'undefined' ? '<br ' + '/>' : '<br>';
  return (str + '').replace(/(\r\n|\n\r|\r|\n)/g, breakTag + '$1');
}

function htmlField(html, args) {
  html = html || '';

  if (!html) {
    return '';
  }

  var text = strip_tags(marked(html, {
    sanitize: false,
    breaks: false,
    langPrefix: 'hljs '
  })).trim();
  var safeText = text;

  if (args['maxLength'] && args['maxLength'] > 0) {
    safeText = substr(text, 0, args['maxLength']);
  }

  switch (args['format']) {
    case 'HTML':
      if (safeText !== text) {
        // Text was truncated, so just show what's safe:
        return nl2br(safeText);
      } else {
        return html;
      }

    case 'TEXT':
    default:
      return safeText;
  }
}

function customizer(objValue, srcValue) {
  if (_lodash.default.isArray(objValue)) {
    return _lodash.default.uniq(objValue.concat(srcValue));
  }

  if (_lodash.default.isObject(objValue)) {
    return (0, _objectSpread2.default)({}, objValue, srcValue);
  }
}

function merge() {
  for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
    args[_key] = arguments[_key];
  }

  return _lodash.default.mergeWith.apply(_lodash.default, args.concat([customizer]));
}

function menu_recursive(data) {
  var parentKey = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'parentId';
  var idKey = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 'id';
  var childrenKey = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : 'children';
  var n = {};
  var _iteratorNormalCompletion = true;
  var _didIteratorError = false;
  var _iteratorError = undefined;

  try {
    for (var _iterator = data[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
      var item = _step.value;
      if (!n["".concat(item[parentKey] || 0, "-key")]) n["".concat(item[parentKey] || 0, "-key")] = [];
      n["".concat(item[parentKey] || 0, "-key")].push(item);
    }
  } catch (err) {
    _didIteratorError = true;
    _iteratorError = err;
  } finally {
    try {
      if (!_iteratorNormalCompletion && _iterator.return != null) {
        _iterator.return();
      }
    } finally {
      if (_didIteratorError) {
        throw _iteratorError;
      }
    }
  }

  var _iteratorNormalCompletion2 = true;
  var _didIteratorError2 = false;
  var _iteratorError2 = undefined;

  try {
    for (var _iterator2 = data[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
      var _item = _step2.value;

      if (n[_item[idKey] + '-key']) {
        _item[childrenKey] = n[_item[idKey] + '-key'];
      }
    }
  } catch (err) {
    _didIteratorError2 = true;
    _iteratorError2 = err;
  } finally {
    try {
      if (!_iteratorNormalCompletion2 && _iterator2.return != null) {
        _iterator2.return();
      }
    } finally {
      if (_didIteratorError2) {
        throw _iteratorError2;
      }
    }
  }

  return n['0-key'];
}

function withdata(value, callback) {
  return !callback ? value : callback(value);
}

function generateRandomString(length) {
  var characters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
  var charsLength = characters.length - 1;
  var string = "";

  for (var i = 0; i < length; i++) {
    var randNum = Math.floor(Math.random() * charsLength);
    string += characters[randNum];
  }

  return string;
}

function requestApi(_x, _x2, _x3) {
  return _requestApi.apply(this, arguments);
}

function _requestApi() {
  _requestApi = (0, _asyncToGenerator2.default)(
  /*#__PURE__*/
  _regenerator.default.mark(function _callee(req, info, key) {
    var query, body, data;
    return _regenerator.default.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            query = info.operation.loc.source.body.replace(/((\s[a-z]+)([\s]+)?\:([\s]+)?([a-z]+))/ig, '$5');
            body = {
              operationName: info.operation.name.value,
              query: query,
              variables: info.variableValues
            };
            _context.next = 4;
            return new Promise(function (resolve, reject) {
              request(uri, {
                method: 'POST',
                headers: {
                  authorization: req.headers.authorization
                },
                json: body
              }, function (error, response, body) {
                if (!error) {
                  if (body.errors) {
                    reject(body);
                  } else if (body && body.data && body.data[key]) {
                    resolve(body.data[key]);
                  } else {
                    resolve(body);
                  }
                } else {
                  reject(error);
                }
              });
            }).catch(function (err) {
              if (err.errors) {
                throw new _apiValidationError.default(err.errors[0].message).setValidator(err.errors[0].validation);
              }

              return err;
            });

          case 4:
            data = _context.sent;
            return _context.abrupt("return", data);

          case 6:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));
  return _requestApi.apply(this, arguments);
}

function round(value, precision) {
  return Number.parseFloat(Number.parseFloat(value).toFixed(precision));
}

function hash_affiliate_id_encode(id) {
  var hashids = new Hashids('', 6, 'QWERTYUIOPSDFGHJKLZXCVBNM'.toLowerCase());
  return hashids.encodeHex(id);
}

function hash_affiliate_id_decode(code) {
  var hashids = new Hashids('', 6, 'QWERTYUIOPSDFGHJKLZXCVBNM'.toLowerCase());
  return hashids.decodeHex(code);
}
/**
 * Convert các object, func trả về là Promise
 * @param value
 * @return {Promise<*>}
 */


function withAsync(_x4) {
  return _withAsync.apply(this, arguments);
}
/**
 * Tính thời gian làm việc
 *
 * Thời gian làm việc được tính bằng s. 60s = 1p
 * @param time {Number}
 *
 * default: h
 * @param hourChar {String}
 *
 * default: p
 * @param minuteChar {String}
 *
 * @return {string}
 */


function _withAsync() {
  _withAsync = (0, _asyncToGenerator2.default)(
  /*#__PURE__*/
  _regenerator.default.mark(function _callee2(value) {
    return _regenerator.default.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            if (!(value instanceof Promise)) {
              _context2.next = 4;
              break;
            }

            _context2.next = 3;
            return value;

          case 3:
            return _context2.abrupt("return", _context2.sent);

          case 4:
            return _context2.abrupt("return", value);

          case 5:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2);
  }));
  return _withAsync.apply(this, arguments);
}

function tinhgio(time) {
  var hourChar = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'h';
  var minuteChar = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 'p';
  // Giờ âm dương
  var s = time < 0 ? '-' : '',
      t = Math.abs(time),
      // Số ngày.
  d = 0,
      // Số giờ.
  h = 0,
      // Số phút.
  m = Math.floor(time / 60); // Nếu quá 60 phút.

  if (m > 60) {
    // Số giờ = số phút / 60 (Làm tròn bỏ đi số dư).
    h = Math.floor(m / 60); // Số phút = số dư giờ * 60 (Làm tròn số).

    m = Math.round((m / 60 - h) * 60);
  }

  if (h) {
    // Format nếu chỉ có số h > 0.
    return "".concat(s).concat(h).concat(hourChar).concat(m).concat(minuteChar);
  } // Nếu chỉ có phút.


  return "".concat(s).concat(m).concat(minuteChar);
}