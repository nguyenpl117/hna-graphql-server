'use strict';

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.NotificationHelper = exports.TOTAL_ACCOUNT_POINT_MINUS = exports.TOTAL_ACCOUNT_POINT_PLUS = exports.CREATE_INVITE_MEMBER = exports.DONATE_POINT = exports.NOT_CONFIRM_KYC = exports.POST_NOT_APPROVAL = exports.POST_IS_APPROVAL = exports.CONFIRM_KYC = exports.CONFIRM_FRIEND = exports.STATUS_TRADE_UNVERIFIED = exports.STATUS_TRADE_VERIFIED = exports.STATUS_TRADE_JOIN = exports.CREATE_KYC = exports.CREATE_FRIEND = exports.CREATE_COMMENT = exports.CREATE_CLIPS = exports.CREATE_DISLIKE = exports.CREATE_LIKE = exports.CREATE_STATUS = exports.DELETE_POST = exports.CREATE_POST = exports.CREATE_FOLLOW = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _auth = _interopRequireDefault(require("./auth"));

var _notificationRepository = _interopRequireDefault(require("../packages/user/repository/elo/notification-repository"));

/**
 * Created by Phan Trung Nguyên.
 * User: nguyenpl117
 * Date: 3/30/2019
 * Time: 6:38 AM
 */
var CREATE_FOLLOW = 'new_follow';
exports.CREATE_FOLLOW = CREATE_FOLLOW;
var CREATE_POST = 'new_post';
exports.CREATE_POST = CREATE_POST;
var DELETE_POST = 'delete_post';
exports.DELETE_POST = DELETE_POST;
var CREATE_STATUS = 'new_status';
exports.CREATE_STATUS = CREATE_STATUS;
var CREATE_LIKE = 'new_likes';
exports.CREATE_LIKE = CREATE_LIKE;
var CREATE_DISLIKE = 'new_dislikes';
exports.CREATE_DISLIKE = CREATE_DISLIKE;
var CREATE_CLIPS = 'new_clips';
exports.CREATE_CLIPS = CREATE_CLIPS;
var CREATE_COMMENT = 'new_comment';
exports.CREATE_COMMENT = CREATE_COMMENT;
var CREATE_FRIEND = 'new_friend';
exports.CREATE_FRIEND = CREATE_FRIEND;
var CREATE_KYC = 'new_kyc';
exports.CREATE_KYC = CREATE_KYC;
var STATUS_TRADE_JOIN = 'status_trade_join';
exports.STATUS_TRADE_JOIN = STATUS_TRADE_JOIN;
var STATUS_TRADE_VERIFIED = 'status_trade_verified';
exports.STATUS_TRADE_VERIFIED = STATUS_TRADE_VERIFIED;
var STATUS_TRADE_UNVERIFIED = 'status_trade_unverified';
exports.STATUS_TRADE_UNVERIFIED = STATUS_TRADE_UNVERIFIED;
var CONFIRM_FRIEND = 'confirm_friend';
exports.CONFIRM_FRIEND = CONFIRM_FRIEND;
var CONFIRM_KYC = 'confirm_kyc';
exports.CONFIRM_KYC = CONFIRM_KYC;
var POST_IS_APPROVAL = 'is_approval';
exports.POST_IS_APPROVAL = POST_IS_APPROVAL;
var POST_NOT_APPROVAL = 'not_approval';
exports.POST_NOT_APPROVAL = POST_NOT_APPROVAL;
var NOT_CONFIRM_KYC = 'not_confirm_kyc';
exports.NOT_CONFIRM_KYC = NOT_CONFIRM_KYC;
var DONATE_POINT = 'donate_point';
exports.DONATE_POINT = DONATE_POINT;
var CREATE_INVITE_MEMBER = 'new_invite_member';
exports.CREATE_INVITE_MEMBER = CREATE_INVITE_MEMBER;
var TOTAL_ACCOUNT_POINT_PLUS = 'plus_total_account';
exports.TOTAL_ACCOUNT_POINT_PLUS = TOTAL_ACCOUNT_POINT_PLUS;
var TOTAL_ACCOUNT_POINT_MINUS = 'minus_total_account';
exports.TOTAL_ACCOUNT_POINT_MINUS = TOTAL_ACCOUNT_POINT_MINUS;

var NotificationHelper =
/*#__PURE__*/
function () {
  function NotificationHelper() {
    (0, _classCallCheck2.default)(this, NotificationHelper);
  }

  (0, _createClass2.default)(NotificationHelper, null, [{
    key: "getUrl",
    value: function getUrl(root) {
      var _urls;

      var urls = (_urls = {}, (0, _defineProperty2.default)(_urls, CREATE_POST, '/notification/p/' + root.notificationableId), (0, _defineProperty2.default)(_urls, DELETE_POST, '/notification/p/' + root.notificationableId), (0, _defineProperty2.default)(_urls, CREATE_FOLLOW, '/notification/u/' + root.senderId), (0, _defineProperty2.default)(_urls, CREATE_FRIEND, '/notification/u/' + root.senderId), (0, _defineProperty2.default)(_urls, CREATE_KYC, '/notification/kyc/' + root.senderId), (0, _defineProperty2.default)(_urls, CONFIRM_KYC, '/notification/kyc/' + root.userId), (0, _defineProperty2.default)(_urls, NOT_CONFIRM_KYC, '/notification/kyc/' + root.userId), (0, _defineProperty2.default)(_urls, STATUS_TRADE_JOIN, '/notification/trade/' + root.senderId), (0, _defineProperty2.default)(_urls, STATUS_TRADE_VERIFIED, '/notification/trade_verified/' + root.senderId), (0, _defineProperty2.default)(_urls, STATUS_TRADE_UNVERIFIED, '/notification/trade_verified/' + root.senderId), (0, _defineProperty2.default)(_urls, CONFIRM_FRIEND, '/notification/u/' + root.senderId), (0, _defineProperty2.default)(_urls, CREATE_STATUS, '/notification/s/' + root.notificationableId), (0, _defineProperty2.default)(_urls, POST_IS_APPROVAL, '/notification/post_approval/' + root.userId), (0, _defineProperty2.default)(_urls, POST_NOT_APPROVAL, '/notification/post_approval/' + root.userId), (0, _defineProperty2.default)(_urls, CREATE_INVITE_MEMBER, '/notification/i/' + root.notificationableId), (0, _defineProperty2.default)(_urls, TOTAL_ACCOUNT_POINT_PLUS, '/notification/plus_total_account/' + root.userId), (0, _defineProperty2.default)(_urls, TOTAL_ACCOUNT_POINT_MINUS, '/notification/minus_total_account/' + root.userId), _urls);
      return urls[root.notificationableType];
    }
  }, {
    key: "deletePost",
    value: function deletePost(post, message) {
      if (!_auth.default.check()) {
        return;
      }

      var sender = _auth.default.user();

      return _notificationRepository.default.send({
        userId: post.authorId,
        senderId: sender.id,
        notificationableType: CREATE_POST,
        notificationableId: post.id,
        title: "B\xE0i vi\u1EBFt <b>".concat(post.title, "</b> \u0111\xE3 b\u1ECB x\xF3a. L\xFD do \"").concat(message, "\"")
      });
    }
  }, {
    key: "publishNewPost",
    value: function publishNewPost(post) {
      if (!_auth.default.check()) {
        return;
      }

      var sender = _auth.default.user();

      return _notificationRepository.default.sendFollowing({
        senderId: sender.id,
        notificationableType: CREATE_POST,
        notificationableId: post.id,
        title: "<b>".concat(sender.fullname, "</b> \u0111\xE3 \u0111\u0103ng b\xE0i vi\u1EBFt m\u1EDBi <b>").concat(post.title, "</b>")
      });
    }
  }, {
    key: "follow",
    value: function follow(_follow) {
      if (!_auth.default.check()) {
        return;
      }

      var sender = _auth.default.user();

      return _notificationRepository.default.send({
        senderId: sender.id,
        userId: _follow.following,
        notificationableType: CREATE_FOLLOW,
        notificationableId: _follow.following,
        title: "<b>".concat(sender.fullname, "</b> \u0111\xE3 theo d\xF5i b\u1EA1n")
      });
    }
  }, {
    key: "addFriend",
    value: function addFriend($friend) {
      if (!_auth.default.check()) {
        return;
      }

      var $sender = _auth.default.user();

      return _notificationRepository.default.send({
        'senderId': $sender.id,
        'userId': $friend.friendId,
        'notificationableType': CREATE_FRIEND,
        'notificationableId': $friend.id,
        'title': "<b>".concat($sender.fullname, "</b> \u0111\xE3 g\u1EEDi l\u1EDDi m\u1EDDi k\u1EBFt b\u1EA1n t\u1EDBi b\u1EA1n")
      }); // DB.table(User.getTable()).
      //     where({ 'id': $friend.friendId }).
      //     increment('add_friend', 1);
    }
  }, {
    key: "confirmFriend",
    value: function confirmFriend($friend) {
      if (!_auth.default.check()) {
        return;
      }

      var $sender = _auth.default.user();

      return _notificationRepository.default.send({
        'senderId': $sender.id,
        'userId': $friend.friendId,
        'notificationableType': CONFIRM_FRIEND,
        'notificationableId': $friend.id,
        'title': "<b>".concat($sender.fullname, "</b> \u0111\xE3 \u0111\u1ED3ng \xFD k\u1EBFt b\u1EA1n")
      }); // DB::table(User::getTable()).where(['id': $friend.friendId]).increment('add_friend', 1);
    }
  }, {
    key: "publishNewStatus",
    value: function publishNewStatus($status) {
      if (!_auth.default.check()) {
        return;
      }

      var $sender = _auth.default.user();

      return _notificationRepository.default.sendFollowing({
        'senderId': $sender.id,
        'notificationableType': CREATE_STATUS,
        'notificationableId': $status.id,
        'title': "<b>".concat($sender.fullname, "</b> \u0111\xE3 c\u1EADp nh\u1EADt tr\u1EA1ng th\xE1i m\u1EDBi.")
      });
    }
  }, {
    key: "donatePoint",
    value: function donatePoint($data) {
      if (!_auth.default.check()) {
        return;
      }

      var $sender = _auth.default.user();

      return _notificationRepository.default.send({
        'senderId': $sender.id,
        'userId': $data.reciveId,
        'notificationableType': CONFIRM_FRIEND,
        'notificationableId': $data.notificationableId,
        'title': $data['comment']
      }); // DB.table(User::getTable())->where(['id' => $data['reciveId']])->increment('not_view_notifi', 1);
    }
  }, {
    key: "createUserKYC",
    value: function createUserKYC(userKyc, listUserId) {
      if (!_auth.default.check()) {
        return;
      }

      var sender = _auth.default.user();

      return _notificationRepository.default.sendMultiUser({
        'senderId': sender.id,
        'notificationableType': CREATE_KYC,
        'notificationableId': userKyc.id,
        'title': "<b>".concat(sender.fullname, "</b> g\u1EEDi th\xF4ng tin KYC")
      }, listUserId);
    }
  }, {
    key: "userJoinTrade",
    value: function userJoinTrade(listUserId) {
      if (!_auth.default.check()) {
        return;
      }

      var sender = _auth.default.user();

      return _notificationRepository.default.sendMultiUser({
        'senderId': sender.id,
        'notificationableType': STATUS_TRADE_JOIN,
        'notificationableId': sender.id,
        'title': "<b>".concat(sender.fullname, "</b> g\u1EEDi y\xEAu c\u1EA7u tham gia t\xE0i kho\u1EA3n th\u01B0\u01A1ng m\u1EA1i")
      }, listUserId);
    }
  }, {
    key: "confirmUserKYC",
    value: function confirmUserKYC(userKyc, status) {
      if (!_auth.default.check()) {
        return;
      }

      var sender = _auth.default.user();

      var title = "";
      var notiType = "";

      if (status === true) {
        title = "Hệ thống đã xác thực thông tin";
        notiType = CONFIRM_KYC;
      } else {
        title = "Thông tin KYC của bạn không được xác thưc";
        notiType = NOT_CONFIRM_KYC;
      }

      return _notificationRepository.default.send({
        'senderId': sender.id,
        'userId': userKyc.userId,
        'notificationableType': notiType,
        'notificationableId': userKyc.id,
        'title': title
      }); // DB::table(User::getTable()).where(['id': $friend.friendId]).increment('add_friend', 1);
    }
  }, {
    key: "verifiedUserStatusTrade",
    value: function verifiedUserStatusTrade(userId, status) {
      if (!_auth.default.check()) {
        return;
      }

      var sender = _auth.default.user();

      var title = "";
      var notiType = "";

      if (status === true) {
        title = "Tài khoản của bạn đã là tài khoản thương mại";
        notiType = STATUS_TRADE_VERIFIED;
      } else {
        title = "Bạn không đủ điều kiện tham gia tài khoản thương mại";
        notiType = STATUS_TRADE_UNVERIFIED;
      }

      return _notificationRepository.default.send({
        'senderId': sender.id,
        'userId': userId,
        'notificationableType': notiType,
        'notificationableId': userId,
        'title': title
      }); // DB::table(User::getTable()).where(['id': $friend.friendId]).increment('add_friend', 1);
    }
  }, {
    key: "notifiApprovalPost",
    value: function notifiApprovalPost(post, data) {
      if (!_auth.default.check()) {
        return;
      }

      var sender = _auth.default.user();

      var title = "";
      var notiType = "";

      if (data.approval === true) {
        title = "B\xE0i vi\u1EBFt: <b>".concat(post.title, "</b> \u0111\xE3 \u0111\u01B0\u1EE3c ph\xEA duy\u1EC7t");
        notiType = POST_IS_APPROVAL;
      } else {
        title = "B\xE0i vi\u1EBFt: <b>".concat(post.title, "</b> kh\xF4ng \u0111\u01B0\u1EE3c ph\xEA duy\u1EC7t");
        notiType = POST_NOT_APPROVAL;
      }

      return _notificationRepository.default.send({
        'senderId': sender.id,
        'userId': post.authorId,
        'notificationableType': notiType,
        'notificationableId': post.id,
        'title': title
      }); // DB::table(User::getTable()).where(['id': $friend.friendId]).increment('add_friend', 1);
    }
  }, {
    key: "chargeTotalAccount",
    value: function chargeTotalAccount(listUserId, money) {
      if (!_auth.default.check()) {
        return;
      }

      var sender = _auth.default.user();

      var title, notificationableType;
      title = "<b>".concat(sender.userName, "</b> th\xEAm ").concat(money, " v\xE0o t\xE0i kho\u1EA3n t\u1ED5ng");
      notificationableType = TOTAL_ACCOUNT_POINT_PLUS;
      return _notificationRepository.default.sendMultiUser({
        'senderId': sender.id,
        'notificationableType': notificationableType,
        'notificationableId': sender.id,
        'title': title
      }, listUserId);
    }
  }]);
  return NotificationHelper;
}();

exports.NotificationHelper = NotificationHelper;