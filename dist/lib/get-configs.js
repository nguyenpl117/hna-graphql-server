"use strict";

var _utils = require("./utils");

var fs = require('fs'),
    p = require('path');

var path = require('path');

var loadPath = require('./load-path');

var appDir = (0, _utils.srcDir)();

function getConfig() {
  var configs = {};
  loadPath(appDir + '/configs').forEach(function (item) {
    configs[item.replace(/\\/g, '/').split('/').reverse()[0].replace('\.js', '')] = require(item);
  });
  return configs;
}

exports.getConfig = getConfig();