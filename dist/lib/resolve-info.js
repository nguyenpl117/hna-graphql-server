"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

var _objectSpread2 = _interopRequireDefault(require("@babel/runtime/helpers/objectSpread"));

var _toConsumableArray2 = _interopRequireDefault(require("@babel/runtime/helpers/toConsumableArray"));

var _typeof2 = _interopRequireDefault(require("@babel/runtime/helpers/typeof"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _base = _interopRequireDefault(require("./relations/base"));

var _require = require('graphql'),
    FragmentSpreadNode = _require.FragmentSpreadNode,
    InlineFragmentNode = _require.InlineFragmentNode;

var _ = require('lodash');

var ResolveInfo =
/*#__PURE__*/
function () {
  function ResolveInfo(info) {
    (0, _classCallCheck2.default)(this, ResolveInfo);
    var fieldName = info.fieldName,
        fieldNodes = info.fieldNodes,
        returnType = info.returnType,
        parentType = info.parentType,
        path = info.path,
        schema = info.schema,
        fragments = info.fragments,
        rootValue = info.rootValue,
        operation = info.operation,
        variableValues = info.variableValues;
    this.fieldName = fieldName;
    this.fieldNodes = fieldNodes;
    this.returnType = returnType;
    this.parentType = parentType;
    this.path = path;
    this.schema = schema;
    this.fragments = fragments;
    this.rootValue = rootValue;
    this.operation = operation;
    this.variableValues = variableValues;
  }

  (0, _createClass2.default)(ResolveInfo, [{
    key: "getFieldSelection",
    value: function getFieldSelection() {
      var depth = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 0;
      var fields = {};
      /** @var FieldNode fieldNode */

      var _iteratorNormalCompletion = true;
      var _didIteratorError = false;
      var _iteratorError = undefined;

      try {
        for (var _iterator = this.fieldNodes[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
          var fieldNode = _step.value;

          if (!fieldNode.selectionSet) {
            continue;
          }

          fields = _.mergeWith(fields, this.foldSelectionSet(fieldNode.selectionSet, depth), customizer);
        }
      } catch (err) {
        _didIteratorError = true;
        _iteratorError = err;
      } finally {
        try {
          if (!_iteratorNormalCompletion && _iterator.return != null) {
            _iterator.return();
          }
        } finally {
          if (_didIteratorError) {
            throw _iteratorError;
          }
        }
      }

      return fields;
    }
  }, {
    key: "foldSelectionSet",
    value: function foldSelectionSet(selectionSet, descend) {
      var fields = {};
      var _iteratorNormalCompletion2 = true;
      var _didIteratorError2 = false;
      var _iteratorError2 = undefined;

      try {
        for (var _iterator2 = selectionSet.selections[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
          var selectionNode = _step2.value;

          if (selectionNode.kind === 'Field') {
            fields[selectionNode.name.value] = descend > 0 && !empty(selectionNode.selectionSet) ? this.foldSelectionSet(selectionNode.selectionSet, descend - 1) : true;
          } else if (selectionNode.kind === 'FragmentSpread') {
            var spreadName = selectionNode.name.value;

            if (isset(this.fragments[spreadName])) {
              /** @var FragmentDefinitionNode $fragment */
              var fragment = this.fragments[spreadName];
              fields = merge(this.foldSelectionSet(fragment.selectionSet, descend), fields);
            }
          } else if (selectionNode.kind === 'InlineFragment') {
            fields = merge(this.foldSelectionSet(selectionNode.selectionSet, descend), fields);
          }
        }
      } catch (err) {
        _didIteratorError2 = true;
        _iteratorError2 = err;
      } finally {
        try {
          if (!_iteratorNormalCompletion2 && _iterator2.return != null) {
            _iterator2.return();
          }
        } finally {
          if (_didIteratorError2) {
            throw _iteratorError2;
          }
        }
      }

      return fields;
    }
  }]);
  return ResolveInfo;
}();

function merge() {
  for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
    args[_key] = arguments[_key];
  }

  return _.mergeWith.apply(_, args.concat([customizer]));
}

function customizer(objValue, srcValue) {
  if (_.isObject(objValue)) {
    return _.merge(objValue, srcValue);
  }

  if (_.isArray(objValue)) {
    return _.uniq(objValue.concat(srcValue));
  }
}

function empty(mixedVar) {
  var undef;
  var key;
  var i;
  var len;
  var emptyValues = [undef, null, false, 0, '', '0'];

  for (i = 0, len = emptyValues.length; i < len; i++) {
    if (mixedVar === emptyValues[i]) {
      return true;
    }
  }

  if ((0, _typeof2.default)(mixedVar) === 'object') {
    for (key in mixedVar) {
      if (mixedVar.hasOwnProperty(key)) {
        return false;
      }
    }

    return true;
  }

  return false;
}

module.exports = {
  getFieldSelection: function getFieldSelection(info) {
    var depth = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;
    var rinfo = new ResolveInfo(info);
    return rinfo.getFieldSelection(depth);
  },
  getFields: function getFields(info, model) {
    var rinfo = new ResolveInfo(info);
    var fields = rinfo.getFieldSelection(4);
    return getField(fields, model);
  },
  getPaginateFields: function getPaginateFields(info, model) {
    var rinfo = new ResolveInfo(info);
    var fields = rinfo.getFieldSelection(5);
    return getField(fields.data, model);
  }
};

function getField(fields, model) {
  var result = [];
  var includes = [];
  var withs = [];

  if (model.options.appendwith) {
    withs = [].concat((0, _toConsumableArray2.default)(withs), (0, _toConsumableArray2.default)(model.options.appendwith));
  }

  for (var field in fields) {
    if (!_.isObject(fields[field])) {
      if (model.rawAttributes[field]) {
        result.push(field);
      }

      if (typeof model[field] === 'function') {
        var relation = model[field]();

        if (!(relation instanceof _base.default)) {
          continue;
        }

        withs.push({
          name: field
        });
      }
    } else {
      var association = model.associations[field];

      if (association) {
        includes.push((0, _objectSpread2.default)({
          association: association.associationAccessor
        }, getField(fields[field], association.target)));
      }

      if (typeof model[field] === 'function') {
        var _relation = model[field]();

        if (!(_relation instanceof _base.default)) {
          continue;
        } // console.log(getField(fields[field], relation.relatedModel));


        withs.push((0, _objectSpread2.default)({
          name: field
        }, getField(fields[field], _relation.relatedModel)));
      }
    }
  }

  return {
    attributes: result,
    include: includes,
    with: withs
  };
}