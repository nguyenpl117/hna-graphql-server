"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _objectWithoutProperties2 = _interopRequireDefault(require("@babel/runtime/helpers/objectWithoutProperties"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var SequelizePaginate =
/*#__PURE__*/
function () {
  function SequelizePaginate() {
    (0, _classCallCheck2.default)(this, SequelizePaginate);
  }

  (0, _createClass2.default)(SequelizePaginate, [{
    key: "paginate",

    /** @typedef {import('sequelize').Model} Model */

    /**
     * Method to append paginate method to Model.
     *
     * @param {Model} Model - Sequelize Model.
     * @returns {*} -
     * @example
     * const sequelizePaginate = require('sequelize-paginate')
     *
     * sequelizePaginate.paginate(MyModel)
     */
    value: function paginate(Model) {
      /**
       * @typedef {Object} Paginate Sequelize query options
       * @property {number} [paginate=25] Results per page
       * @property {number} [page=1] Number of page
       */

      /**
       * @typedef {import('sequelize').FindOptions & Paginate} paginateOptions
       */

      /**
       * The paginate result
       * @typedef {Object} PaginateResult
       * @property {Array} docs Docs
       * @property {number} pages Number of page
       * @property {number} total Total of docs
       */

      /**
       * Pagination.
       *
       * @param {paginateOptions} [params] - Options to filter query.
       * @returns {Promise<PaginateResult>} Total pages and docs.
       * @example
       * const { docs, pages, total } = await MyModel.paginate({ page: 1, paginate: 25 })
       * @memberof Model
       */
      var pagination =
      /*#__PURE__*/
      function () {
        var _ref = (0, _asyncToGenerator2.default)(
        /*#__PURE__*/
        _regenerator.default.mark(function _callee() {
          var _ref2,
              _ref2$page,
              page,
              _ref2$paginate,
              paginate,
              params,
              options,
              countOptions,
              total,
              pages,
              docs,
              from,
              to,
              _args = arguments;

          return _regenerator.default.wrap(function _callee$(_context) {
            while (1) {
              switch (_context.prev = _context.next) {
                case 0:
                  _ref2 = _args.length > 0 && _args[0] !== undefined ? _args[0] : {}, _ref2$page = _ref2.page, page = _ref2$page === void 0 ? 1 : _ref2$page, _ref2$paginate = _ref2.paginate, paginate = _ref2$paginate === void 0 ? 25 : _ref2$paginate, params = (0, _objectWithoutProperties2.default)(_ref2, ["page", "paginate"]);
                  options = Object.assign({}, params);
                  countOptions = Object.keys(options).reduce(function (acc, key) {
                    if (!['order', 'attributes'].includes(key)) {
                      // eslint-disable-next-line security/detect-object-injection
                      acc[key] = options[key];
                    }

                    return acc;
                  }, {});
                  _context.next = 5;
                  return Model.count(countOptions);

                case 5:
                  total = _context.sent;

                  if (options.group !== undefined) {
                    // @ts-ignore
                    total = total.length;
                  }

                  if (total) {
                    _context.next = 9;
                    break;
                  }

                  return _context.abrupt("return", {
                    data: null,
                    perPage: paginate,
                    currentPage: page,
                    from: null,
                    to: null,
                    // pages,
                    total: 0
                  });

                case 9:
                  pages = Math.ceil(total / paginate);
                  options.limit = paginate;
                  options.offset = paginate * (page - 1);
                  /* eslint-disable no-console */

                  if (params.limit) {
                    console.warn("(sequelize-pagination) Warning: limit option is ignored.");
                  }

                  if (params.offset) {
                    console.warn("(sequelize-pagination) Warning: offset option is ignored.");
                  }
                  /* eslint-enable no-console */


                  if (params.order) options.order = params.order;
                  _context.next = 17;
                  return Model.findAll(options).map(function (data) {
                    return data.toJSON();
                  });

                case 17:
                  docs = _context.sent;
                  from = options.offset + 1;
                  to = options.offset + docs.length;

                  if (!docs.length) {
                    from = null;
                    to = null;
                  }

                  return _context.abrupt("return", {
                    data: docs,
                    perPage: paginate,
                    currentPage: page,
                    from: from > to ? null : from,
                    to: from > to ? null : to,
                    pages: pages,
                    total: total
                  });

                case 22:
                case "end":
                  return _context.stop();
              }
            }
          }, _callee);
        }));

        return function pagination() {
          return _ref.apply(this, arguments);
        };
      }();

      var instanceOrModel = Model.Instance || Model; // @ts-ignore

      instanceOrModel.paginate = pagination;
    }
  }]);
  return SequelizePaginate;
}();

module.exports = {
  Paginate: new SequelizePaginate()
};