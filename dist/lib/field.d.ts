export class Field {
    /**
     * Tên query hoặc mutation
     *
     * @return {string}
     */
    name: string;

    /**
     * Mô tả
     *
     * @return {string}
     */
    description: string;

    /**
     * Có bắt xác thực hay không
     *
     * @default false
     * @return {boolean}
     */
    authentication: boolean;

    /**
     * Có lưu cache hay không
     *
     * @default false
     * @return {boolean}
     */
    cache: boolean;
    
    /**
     * Middleware
     */
    middleware: [Function];

    /**
     * Kiểm tra quyền hạn của người dùng
     *
     * @param args
     * @return {Promise<boolean>}
     */
    authorize(args: object): boolean

    /**
     * Kết quả trả ra dạng
     *
     * @return {*}
     */
    type(): string

    /**
     * Giá trị truyền lên
     *
     * @return {{}}
     */
    args(): object

    /**
     * Validate dữ liệu trước khi xử lý
     *
     * @param args
     */
    rules(args: object): object

    /**
     * Custom lại tin nhắn validate trước khi response
     *
     * @param args
     * @return {{}}
     */
    validationErrorMessages(args: object): object

    /**
     * Thao tác xử lý dữ liệu
     *
     * @param parent
     * @param args
     * @param context
     * @param info
     */
    resolve(parent: object, args: object, context: object, info: object): object

    /**
     * Filter dữ liệu ( socket )
     *
     * @param payload
     * @param args
     */
    filter(payload, args): object
}