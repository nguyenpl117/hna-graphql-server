"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.SlugMixin = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _lodash = _interopRequireDefault(require("lodash"));

var db = require('../../models');

var SlugMixin =
/*#__PURE__*/
function () {
  function SlugMixin() {
    (0, _classCallCheck2.default)(this, SlugMixin);
  }

  (0, _createClass2.default)(SlugMixin, null, [{
    key: "SlugModel",
    value: function SlugModel(Model, options) {
      var settings = _lodash.default.extend({
        slug: {
          source: 'title'
        }
      }, options);

      Model.afterCreate(
      /*#__PURE__*/
      function () {
        var _ref = (0, _asyncToGenerator2.default)(
        /*#__PURE__*/
        _regenerator.default.mark(function _callee(model, options) {
          var models, path;
          return _regenerator.default.wrap(function _callee$(_context) {
            while (1) {
              switch (_context.prev = _context.next) {
                case 0:
                  models = model.sequelize.models;
                  path = null;

                  if (!(model instanceof models.category && model.parentId > 0)) {
                    _context.next = 6;
                    break;
                  }

                  _context.next = 5;
                  return models.slug.findOne({
                    where: {
                      slugType: models.category.options.namespace,
                      slugId: model.parentId
                    },
                    attributes: ['slug', 'path']
                  });

                case 5:
                  path = _context.sent;

                case 6:
                  if (path) {
                    path = '/' + "".concat(path.path, "/").concat(path.slug).replace(/^\/|\/$/gm, '');
                  }

                  models.slug.create({
                    name: model[settings.slug.source],
                    slugType: this.options.namespace,
                    slugId: model.id,
                    path: path
                  });

                case 8:
                case "end":
                  return _context.stop();
              }
            }
          }, _callee, this);
        }));

        return function (_x, _x2) {
          return _ref.apply(this, arguments);
        };
      }());
    }
  }]);
  return SlugMixin;
}();

exports.SlugMixin = SlugMixin;