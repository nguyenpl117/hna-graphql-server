"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.IS_APPROVAL = exports.NOT_APPROVAL = exports.USER_STATUS_TRADE_WAITING = exports.USER_STATUS_TRADE_TRADE = exports.USER_STATUS_TRADE_NORMAL = exports.USER_LEVEL_NORMAL = exports.USER_LEVEL_LEADER = exports.USER_LEVEL_VIP = exports.USER_STATUS_KYC_UPDATE_NOT_COMFIRM = exports.USER_STATUS_KYC_COMFIRM_FALSE = exports.USER_STATUS_KYC_COMFIRM = exports.USER_STATUS_KYC_NOT_COMFIRM = exports.USER_STATUS_KYC_NOT_INFO = exports.FRIEND_STATUS_INVITED = exports.FRIEND_STATUS_FRIEND = exports.FRIEND_STATUS_ADD = exports.NamspaceModel = exports.ModelNamspace = void 0;
var ModelNamspace = {
  user: "App\\User",
  post: 'App\\Models\\Post',
  category: 'App\\Models\\Category',
  comment: 'App\\Models\\Comment'
};
exports.ModelNamspace = ModelNamspace;
var NamspaceModel = {
  "App\\User": 'user',
  'App\\Models\\Post': 'post',
  'App\\Models\\Category': 'category',
  'App\\Models\\Comment': 'comment',
  'App\\Models\\Status': 'status',
  'App\\Models\\Setting': 'setting'
}; // mời kết bạn

exports.NamspaceModel = NamspaceModel;
var FRIEND_STATUS_ADD = 1; // bạn bè

exports.FRIEND_STATUS_ADD = FRIEND_STATUS_ADD;
var FRIEND_STATUS_FRIEND = 2; // được mời kết bạn

exports.FRIEND_STATUS_FRIEND = FRIEND_STATUS_FRIEND;
var FRIEND_STATUS_INVITED = 3; // chưa gửi thông tin KYC

exports.FRIEND_STATUS_INVITED = FRIEND_STATUS_INVITED;
var USER_STATUS_KYC_NOT_INFO = 1; // gửi thông tin KYC chưa được xác nhận

exports.USER_STATUS_KYC_NOT_INFO = USER_STATUS_KYC_NOT_INFO;
var USER_STATUS_KYC_NOT_COMFIRM = 2; // đã được xác nhận

exports.USER_STATUS_KYC_NOT_COMFIRM = USER_STATUS_KYC_NOT_COMFIRM;
var USER_STATUS_KYC_COMFIRM = 3; // xác nhận không qua

exports.USER_STATUS_KYC_COMFIRM = USER_STATUS_KYC_COMFIRM;
var USER_STATUS_KYC_COMFIRM_FALSE = 4; // xác nhận rồi update lại chưa được duyệt lại

exports.USER_STATUS_KYC_COMFIRM_FALSE = USER_STATUS_KYC_COMFIRM_FALSE;
var USER_STATUS_KYC_UPDATE_NOT_COMFIRM = 5; // tài khoản vip

exports.USER_STATUS_KYC_UPDATE_NOT_COMFIRM = USER_STATUS_KYC_UPDATE_NOT_COMFIRM;
var USER_LEVEL_VIP = "1"; // tài khoản leader

exports.USER_LEVEL_VIP = USER_LEVEL_VIP;
var USER_LEVEL_LEADER = "2"; // tài khoản thương

exports.USER_LEVEL_LEADER = USER_LEVEL_LEADER;
var USER_LEVEL_NORMAL = "3"; // chưa tham gia trạng thái thương mại

exports.USER_LEVEL_NORMAL = USER_LEVEL_NORMAL;
var USER_STATUS_TRADE_NORMAL = 1; // trạng thái thương mại

exports.USER_STATUS_TRADE_NORMAL = USER_STATUS_TRADE_NORMAL;
var USER_STATUS_TRADE_TRADE = 2; /// chờ xác nhận tham gia

exports.USER_STATUS_TRADE_TRADE = USER_STATUS_TRADE_TRADE;
var USER_STATUS_TRADE_WAITING = 3; /////// chưa duyệt bài

exports.USER_STATUS_TRADE_WAITING = USER_STATUS_TRADE_WAITING;
var NOT_APPROVAL = 1; ////đã duyệt bài

exports.NOT_APPROVAL = NOT_APPROVAL;
var IS_APPROVAL = 2; // export const TRANSACTION_TYPE_POINT_PLUS = '1';// cộng điểm
// export const TRANSACTION_TYPE_POINT_MINUS = '2';//trừ điểm
// export const TRANSACTION_TYPE_POINT_FEES_POST = '3';//nhận tiền bài viết
// export const TRANSACTION_TYPE_POINT_PAID_FEES_POST = '4';//trả tiền để xem bài viết
// export const TRANSACTION_TYPE_DONATE = '5';//donate tien
// export const TRANSACTION_TYPE_RECEIVE_DONATE = '6';//Nhận donate

exports.IS_APPROVAL = IS_APPROVAL;