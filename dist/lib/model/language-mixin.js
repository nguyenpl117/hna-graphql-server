"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.LanguageMixin = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var LanguageMixin =
/*#__PURE__*/
function () {
  function LanguageMixin() {
    (0, _classCallCheck2.default)(this, LanguageMixin);
  }

  (0, _createClass2.default)(LanguageMixin, null, [{
    key: "languageModel",
    value: function languageModel(Model, options) {
      Model.beforeCreate(
      /*#__PURE__*/
      function () {
        var _ref = (0, _asyncToGenerator2.default)(
        /*#__PURE__*/
        _regenerator.default.mark(function _callee(model, options) {
          var lang;
          return _regenerator.default.wrap(function _callee$(_context) {
            while (1) {
              switch (_context.prev = _context.next) {
                case 0:
                  if (!model.language) {
                    _context.next = 2;
                    break;
                  }

                  return _context.abrupt("return");

                case 2:
                  _context.next = 4;
                  return this.sequelize.models.language.findOne({
                    where: {
                      default: 2
                    }
                  });

                case 4:
                  lang = _context.sent;
                  model.setDataValue('language', lang.id);

                case 6:
                case "end":
                  return _context.stop();
              }
            }
          }, _callee, this);
        }));

        return function (_x, _x2) {
          return _ref.apply(this, arguments);
        };
      }());
      Model.afterCreate(function (model, options) {
        if (!model.languageMaster) {
          Model.update({
            languageMaster: model.id
          }, {
            where: {
              id: model.id
            }
          });
        }
      });
      Model.beforeUpdate(
      /*#__PURE__*/
      function () {
        var _ref2 = (0, _asyncToGenerator2.default)(
        /*#__PURE__*/
        _regenerator.default.mark(function _callee2(model, options) {
          var seq;
          return _regenerator.default.wrap(function _callee2$(_context2) {
            while (1) {
              switch (_context2.prev = _context2.next) {
                case 0:
                  _context2.next = 2;
                  return this.count({
                    where: {
                      language: model.language,
                      languageMaster: model.languageMaster,
                      id: (0, _defineProperty2.default)({}, this.sequelize.Op.not, model.id)
                    }
                  });

                case 2:
                  seq = _context2.sent;

                  if (seq > 0) {
                    model.setDataValue('languageMaster', model.id);
                  }

                case 4:
                case "end":
                  return _context2.stop();
              }
            }
          }, _callee2, this);
        }));

        return function (_x3, _x4) {
          return _ref2.apply(this, arguments);
        };
      }());
    }
  }]);
  return LanguageMixin;
}();

exports.LanguageMixin = LanguageMixin;