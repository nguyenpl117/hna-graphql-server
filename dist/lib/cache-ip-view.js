"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _lodash = _interopRequireDefault(require("lodash"));

var CacheIpView =
/*#__PURE__*/
function () {
  function CacheIpView() {
    (0, _classCallCheck2.default)(this, CacheIpView);
  }

  (0, _createClass2.default)(CacheIpView, null, [{
    key: "init",
    value: function init() {
      var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

      if (!this.dataCache) {
        this.dataCache = {};
      }

      this.config = _lodash.default.extend({
        timeout: 1000 * 60 * 5
      }, options);
    }
  }, {
    key: "checkIp",
    value: function checkIp(ip) {
      var now = new Date().getTime();

      if (this.dataCache.hasOwnProperty(ip) && now <= this.dataCache[ip]) {
        return false;
      }

      this.dataCache[ip] = new Date().getTime() + this.config.timeout;
      return true;
    }
  }]);
  return CacheIpView;
}();

exports.default = CacheIpView;