"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Hash = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var bcrypt = require('bcrypt');

var Hash =
/*#__PURE__*/
function () {
  function Hash() {
    (0, _classCallCheck2.default)(this, Hash);
  }

  (0, _createClass2.default)(Hash, null, [{
    key: "make",
    value: function make(data) {
      return bcrypt.hashSync(data, 10).replace(/^\$2b/g, '$2y').replace(/^\$2a/g, '$2x');
    }
  }, {
    key: "check",
    value: function check(data, hash) {
      return bcrypt.compareSync(data, hash.replace(/^\$2y/g, '$2b').replace(/^\$2x/g, '$2a'));
    }
  }]);
  return Hash;
}();

exports.Hash = Hash;