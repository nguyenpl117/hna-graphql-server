"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _lodash = _interopRequireDefault(require("lodash"));

var _utils = require("../utils");

var md5 = require("crypto-js/md5");

var BaseRelation =
/*#__PURE__*/
function () {
  function BaseRelation(parentInstance, RelatedModel, options) {
    (0, _classCallCheck2.default)(this, BaseRelation);
    this.parentInstance = parentInstance;
    this.relatedModel = RelatedModel;
    this.primaryKey = options.primaryKey;
    this.foreignKey = options.foreignKey;
    this.options = options;
  }

  (0, _createClass2.default)(BaseRelation, [{
    key: "_primaryKey",
    value: function _primaryKey() {
      var primaryKey = this.primaryKey; // if (this.options.type && this.options.type.primaryKey) {
      //     primaryKey = md5(this.options.type.primaryKey);
      // }

      return primaryKey;
    }
  }, {
    key: "_foreignKey",
    value: function _foreignKey() {
      var foreignKey = this.foreignKey; // if (this.options.type && this.options.type.foreignKey) {
      //     foreignKey = md5(this.options.type.foreignKey);
      // }

      return foreignKey;
    }
  }, {
    key: "_eagerLoadFn",
    value: function _eagerLoadFn(foreignKey, values, options) {
      return this.relatedModel.findAll((0, _utils.merge)({
        where: (0, _defineProperty2.default)({}, foreignKey, {
          'in': _lodash.default.uniq(values)
        })
      }, this.options.scope, options, {
        attributes: [this.foreignKey]
      }));
    }
  }, {
    key: "eagerLoad",
    value: function () {
      var _eagerLoad = (0, _asyncToGenerator2.default)(
      /*#__PURE__*/
      _regenerator.default.mark(function _callee(rows, options) {
        var mappedRows, res;
        return _regenerator.default.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                mappedRows = this.mapValues(rows);

                if (!(!mappedRows || !mappedRows.length)) {
                  _context.next = 3;
                  break;
                }

                return _context.abrupt("return", this.group([]));

              case 3:
                _context.next = 5;
                return this._eagerLoadFn(this._foreignKey(), mappedRows, options);

              case 5:
                res = _context.sent;
                return _context.abrupt("return", this.group(res));

              case 7:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      function eagerLoad(_x, _x2) {
        return _eagerLoad.apply(this, arguments);
      }

      return eagerLoad;
    }()
  }]);
  return BaseRelation;
}();

var _default = BaseRelation;
exports.default = _default;