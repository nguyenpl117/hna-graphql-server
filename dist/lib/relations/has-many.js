"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _possibleConstructorReturn2 = _interopRequireDefault(require("@babel/runtime/helpers/possibleConstructorReturn"));

var _getPrototypeOf2 = _interopRequireDefault(require("@babel/runtime/helpers/getPrototypeOf"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime/helpers/inherits"));

var _base = _interopRequireDefault(require("./base"));

var _lodash = _interopRequireDefault(require("lodash"));

var _arr = _interopRequireDefault(require("../../lib/arr"));

var HasMany =
/*#__PURE__*/
function (_BaseRelation) {
  (0, _inherits2.default)(HasMany, _BaseRelation);

  function HasMany(parentInstance, RelatedModel, options) {
    (0, _classCallCheck2.default)(this, HasMany);
    return (0, _possibleConstructorReturn2.default)(this, (0, _getPrototypeOf2.default)(HasMany).call(this, parentInstance, RelatedModel, options));
  }
  /**
   *
   * @param {Array} modelInstances
   * @return {Array}
   */


  (0, _createClass2.default)(HasMany, [{
    key: "mapValues",
    value: function mapValues(modelInstances) {
      var _this = this;

      return _lodash.default.transform(_arr.default.array_wrap(modelInstances), function (result, modelInstance) {
        if (modelInstance[_this.primaryKey]) {
          result.push(modelInstance[_this.primaryKey]);
        }

        return result;
      }, []);
    }
    /**
     *
     * @param {Array} relatedInstances
     */

  }, {
    key: "group",
    value: function group(relatedInstances) {
      var _this2 = this;

      return _lodash.default.transform(relatedInstances, function (result, relatedInstance) {
        var foreignKeyValue = relatedInstance[_this2.foreignKey];
        (result[foreignKeyValue] || (result[foreignKeyValue] = [])).push(relatedInstance);
        return result;
      }, {});
    }
  }, {
    key: "getItems",
    value: function getItems(relatedInstances, modelInstance) {
      return !relatedInstances[modelInstance[this.primaryKey]] ? [] : relatedInstances[modelInstance[this.primaryKey]];
    }
  }]);
  return HasMany;
}(_base.default);

var _default = HasMany;
exports.default = _default;