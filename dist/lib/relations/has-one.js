"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _toConsumableArray2 = _interopRequireDefault(require("@babel/runtime/helpers/toConsumableArray"));

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _possibleConstructorReturn2 = _interopRequireDefault(require("@babel/runtime/helpers/possibleConstructorReturn"));

var _getPrototypeOf2 = _interopRequireDefault(require("@babel/runtime/helpers/getPrototypeOf"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime/helpers/inherits"));

var _base = _interopRequireDefault(require("./base"));

var _lodash = _interopRequireDefault(require("lodash"));

var _arr = _interopRequireDefault(require("../../lib/arr"));

var _utils = require("../utils");

var Sequelize = require('sequelize');

var HasOne =
/*#__PURE__*/
function (_BaseRelation) {
  (0, _inherits2.default)(HasOne, _BaseRelation);

  function HasOne(parentInstance, RelatedModel, options) {
    (0, _classCallCheck2.default)(this, HasOne);
    return (0, _possibleConstructorReturn2.default)(this, (0, _getPrototypeOf2.default)(HasOne).call(this, parentInstance, RelatedModel, options));
  }

  (0, _createClass2.default)(HasOne, [{
    key: "_types",
    value: function _types(type) {
      var types = {};
      this.options.type.type.forEach(function (item) {
        types[item[0]] = item[1];
        types[item[1]] = item[1];
      });
      return types[type];
    }
  }, {
    key: "_eagerLoadFn",
    value: function _eagerLoadFn(foreignKey, values, options) {
      var where = (0, _defineProperty2.default)({}, foreignKey, {
        'in': _lodash.default.uniq(values)
      });

      if (this.options.type) {
        var types = this.options.type;
        where = (0, _defineProperty2.default)({}, Sequelize.Op.or, (0, _toConsumableArray2.default)(types.type.filter(function (type) {
          return values.filter(function (data) {
            return data[types.primaryKey[1]] === type[0];
          }).length !== 0;
        }).map(function (type) {
          var _ref;

          return _ref = {}, (0, _defineProperty2.default)(_ref, types.foreignKey[0], (0, _defineProperty2.default)({}, Sequelize.Op.in, values.filter(function (data) {
            return data[types.primaryKey[1]] === type[0];
          }).map(function (data) {
            return data[types.primaryKey[0]];
          }))), (0, _defineProperty2.default)(_ref, types.foreignKey[1], type[1]), _ref;
        })));
      }

      var opt = (0, _utils.merge)({
        where: where
      }, this.options.scope, options, {
        attributes: [this.foreignKey]
      });
      return this.relatedModel.findAll(opt);
    }
    /**
     *
     * @param {Array} modelInstances
     * @return {Array}
     */

  }, {
    key: "mapValues",
    value: function mapValues(modelInstances) {
      var _this = this;

      if (this.options.type) {
        return modelInstances;
      }

      return _lodash.default.transform(_arr.default.array_wrap(modelInstances), function (result, modelInstance) {
        if (modelInstance[_this._primaryKey()]) {
          result.push(modelInstance[_this._primaryKey()]);
        }

        return result;
      }, []);
    }
    /**
     *
     * @param {Array} relatedInstances
     */

  }, {
    key: "group",
    value: function group(relatedInstances) {
      var _this2 = this;

      return _lodash.default.transform(relatedInstances, function (result, relatedInstance) {
        var foreignKeyValue = relatedInstance[_this2._foreignKey()];

        (result[foreignKeyValue] || (result[foreignKeyValue] = [])).push(relatedInstance);
        return result;
      }, {});
    }
  }, {
    key: "getItems",
    value: function getItems(relatedInstances, modelInstance) {
      return !relatedInstances[modelInstance[this._primaryKey()]] ? null : _lodash.default.first(relatedInstances[modelInstance[this._primaryKey()]]);
    }
  }]);
  return HasOne;
}(_base.default);

var _default = HasOne;
exports.default = _default;