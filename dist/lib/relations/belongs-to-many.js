"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _objectSpread2 = _interopRequireDefault(require("@babel/runtime/helpers/objectSpread"));

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _possibleConstructorReturn2 = _interopRequireDefault(require("@babel/runtime/helpers/possibleConstructorReturn"));

var _getPrototypeOf2 = _interopRequireDefault(require("@babel/runtime/helpers/getPrototypeOf"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime/helpers/inherits"));

var _base = _interopRequireDefault(require("./base"));

var _lodash = _interopRequireDefault(require("lodash"));

var _arr = _interopRequireDefault(require("../../lib/arr"));

var _utils = require("../utils");

var BelongsToMany =
/*#__PURE__*/
function (_BaseRelation) {
  (0, _inherits2.default)(BelongsToMany, _BaseRelation);

  function BelongsToMany(parentInstance, RelatedModel, options) {
    var _this;

    (0, _classCallCheck2.default)(this, BelongsToMany);
    _this = (0, _possibleConstructorReturn2.default)(this, (0, _getPrototypeOf2.default)(BelongsToMany).call(this, parentInstance, RelatedModel, options));
    _this.otherKey = options.otherKey;
    return _this;
  }

  (0, _createClass2.default)(BelongsToMany, [{
    key: "_eagerLoadFn",
    value: function _eagerLoadFn(foreignKey, values, options) {
      this.options.through.model.belongsTo(this.relatedModel, {
        foreignKey: this.otherKey
      });
      return this.options.through.model.findAll((0, _utils.merge)({
        where: (0, _defineProperty2.default)({}, foreignKey, {
          'in': _lodash.default.uniq(values)
        }),
        include: [(0, _objectSpread2.default)({
          model: this.relatedModel
        }, (0, _utils.merge)(this.options.scope, options))]
      }, this.options.through.scope));
    }
    /**
     *
     * @param {Array} modelInstances
     * @return {Array}
     */

  }, {
    key: "mapValues",
    value: function mapValues(modelInstances) {
      var _this2 = this;

      return _lodash.default.transform(_arr.default.array_wrap(modelInstances), function (result, modelInstance) {
        if (modelInstance[_this2.primaryKey]) {
          result.push(modelInstance[_this2.primaryKey]);
        }

        return result;
      }, []);
    }
    /**
     *
     * @param {Array} relatedInstances
     */

  }, {
    key: "group",
    value: function group(relatedInstances) {
      var _this3 = this;

      return _lodash.default.transform(relatedInstances, function (result, relatedInstance) {
        var foreignKeyValue = relatedInstance[_this3.foreignKey];
        var value = relatedInstance[_this3.relatedModel.name];

        if (value) {
          if (Array.isArray(_this3.options.appendField)) {
            _this3.options.appendField.forEach(function (item) {
              value.setDataValue(item, relatedInstance.getDataValue(item));
            });
          }

          (result[foreignKeyValue] || (result[foreignKeyValue] = [])).push(value);
        }

        return result;
      }, {});
    }
  }, {
    key: "getItems",
    value: function getItems(relatedInstances, modelInstance) {
      return !relatedInstances[modelInstance[this.primaryKey]] ? [] : relatedInstances[modelInstance[this.primaryKey]];
    }
    /**
     *
     * @param data
     */

  }, {
    key: "sync",
    value: function () {
      var _sync = (0, _asyncToGenerator2.default)(
      /*#__PURE__*/
      _regenerator.default.mark(function _callee() {
        var _this4 = this;

        var syncData,
            instance,
            result,
            pushData,
            _iteratorNormalCompletion,
            _didIteratorError,
            _iteratorError,
            _loop,
            _iterator,
            _step,
            _iteratorNormalCompletion2,
            _didIteratorError2,
            _iteratorError2,
            _iterator2,
            _step2,
            _this$options$through,
            item,
            _args2 = arguments;

        return _regenerator.default.wrap(function _callee$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                syncData = _args2.length > 0 && _args2[0] !== undefined ? _args2[0] : [];
                instance = _args2.length > 1 ? _args2[1] : undefined;
                syncData = syncData.map(function (item) {
                  return _lodash.default.toString(item);
                });
                _context2.next = 5;
                return this.options.through.model.findAll({
                  where: (0, _defineProperty2.default)({}, this.foreignKey, instance[this.primaryKey])
                });

              case 5:
                result = _context2.sent;
                pushData = syncData.slice(0);
                _iteratorNormalCompletion = true;
                _didIteratorError = false;
                _iteratorError = undefined;
                _context2.prev = 10;
                _loop =
                /*#__PURE__*/
                _regenerator.default.mark(function _loop() {
                  var item;
                  return _regenerator.default.wrap(function _loop$(_context) {
                    while (1) {
                      switch (_context.prev = _context.next) {
                        case 0:
                          item = _step.value;

                          if (!(syncData.indexOf(_lodash.default.toString(item[_this4.otherKey])) === -1)) {
                            _context.next = 6;
                            break;
                          }

                          _context.next = 4;
                          return _this4.options.through.model.destroy({
                            where: item.toJSON()
                          });

                        case 4:
                          _context.next = 7;
                          break;

                        case 6:
                          pushData = pushData.filter(function (x) {
                            return !_lodash.default.eq(x, _lodash.default.toString(item[_this4.otherKey]));
                          });

                        case 7:
                        case "end":
                          return _context.stop();
                      }
                    }
                  }, _loop);
                });
                _iterator = result[Symbol.iterator]();

              case 13:
                if (_iteratorNormalCompletion = (_step = _iterator.next()).done) {
                  _context2.next = 18;
                  break;
                }

                return _context2.delegateYield(_loop(), "t0", 15);

              case 15:
                _iteratorNormalCompletion = true;
                _context2.next = 13;
                break;

              case 18:
                _context2.next = 24;
                break;

              case 20:
                _context2.prev = 20;
                _context2.t1 = _context2["catch"](10);
                _didIteratorError = true;
                _iteratorError = _context2.t1;

              case 24:
                _context2.prev = 24;
                _context2.prev = 25;

                if (!_iteratorNormalCompletion && _iterator.return != null) {
                  _iterator.return();
                }

              case 27:
                _context2.prev = 27;

                if (!_didIteratorError) {
                  _context2.next = 30;
                  break;
                }

                throw _iteratorError;

              case 30:
                return _context2.finish(27);

              case 31:
                return _context2.finish(24);

              case 32:
                _iteratorNormalCompletion2 = true;
                _didIteratorError2 = false;
                _iteratorError2 = undefined;
                _context2.prev = 35;
                _iterator2 = pushData[Symbol.iterator]();

              case 37:
                if (_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done) {
                  _context2.next = 44;
                  break;
                }

                item = _step2.value;
                _context2.next = 41;
                return this.options.through.model.create((_this$options$through = {}, (0, _defineProperty2.default)(_this$options$through, this.otherKey, item), (0, _defineProperty2.default)(_this$options$through, this.foreignKey, instance[this.primaryKey]), _this$options$through));

              case 41:
                _iteratorNormalCompletion2 = true;
                _context2.next = 37;
                break;

              case 44:
                _context2.next = 50;
                break;

              case 46:
                _context2.prev = 46;
                _context2.t2 = _context2["catch"](35);
                _didIteratorError2 = true;
                _iteratorError2 = _context2.t2;

              case 50:
                _context2.prev = 50;
                _context2.prev = 51;

                if (!_iteratorNormalCompletion2 && _iterator2.return != null) {
                  _iterator2.return();
                }

              case 53:
                _context2.prev = 53;

                if (!_didIteratorError2) {
                  _context2.next = 56;
                  break;
                }

                throw _iteratorError2;

              case 56:
                return _context2.finish(53);

              case 57:
                return _context2.finish(50);

              case 58:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee, this, [[10, 20, 24, 32], [25,, 27, 31], [35, 46, 50, 58], [51,, 53, 57]]);
      }));

      function sync() {
        return _sync.apply(this, arguments);
      }

      return sync;
    }()
  }]);
  return BelongsToMany;
}(_base.default);

var _default = BelongsToMany;
exports.default = _default;