"use strict";

var _utils = require("../lib/utils");

var list = ['../packages/user/index.js'];

if (process.env.MODULE_NEWS === 'true') {
  list.push('../packages/news/index.js');
}

if (process.env.MODULE_SHOPS === 'true') {
  list.push('../packages/shops/index.js');
}

if (process.env.MODULE_STATUS === 'true') {
  list.push('../packages/status/index.js');
}

var loader = {};

for (var _i = 0, _list = list; _i < _list.length; _i++) {
  var item = _list[_i];
  loader = (0, _utils.merge)(loader, require(item));
}

module.exports = loader;