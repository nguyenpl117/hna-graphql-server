"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _possibleConstructorReturn2 = _interopRequireDefault(require("@babel/runtime/helpers/possibleConstructorReturn"));

var _getPrototypeOf2 = _interopRequireDefault(require("@babel/runtime/helpers/getPrototypeOf"));

var _get2 = _interopRequireDefault(require("@babel/runtime/helpers/get"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime/helpers/inherits"));

var _baseRepository = _interopRequireDefault(require("../../../../repository/elo/base-repository"));

var bcrypt = require('bcrypt');

var uuid = require('uuid');

var saltRounds = 10;
var salt = bcrypt.genSaltSync(saltRounds);

var UserRepository =
/*#__PURE__*/
function (_BaseRepository) {
  (0, _inherits2.default)(UserRepository, _BaseRepository);

  function UserRepository() {
    (0, _classCallCheck2.default)(this, UserRepository);
    return (0, _possibleConstructorReturn2.default)(this, (0, _getPrototypeOf2.default)(UserRepository).apply(this, arguments));
  }

  (0, _createClass2.default)(UserRepository, null, [{
    key: "modelName",
    value: function modelName() {
      return 'user';
    }
    /**
     * Tạo mới user
     * @param data
     * @return {Promise<data>}
     */

  }, {
    key: "create",
    value: function () {
      var _create = (0, _asyncToGenerator2.default)(
      /*#__PURE__*/
      _regenerator.default.mark(function _callee2(data) {
        var _this = this;

        return _regenerator.default.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                return _context2.abrupt("return", this.model.sequelize.transaction(
                /*#__PURE__*/
                function () {
                  var _ref = (0, _asyncToGenerator2.default)(
                  /*#__PURE__*/
                  _regenerator.default.mark(function _callee(t) {
                    var created;
                    return _regenerator.default.wrap(function _callee$(_context) {
                      while (1) {
                        switch (_context.prev = _context.next) {
                          case 0:
                            if (!data.id) {
                              data.id = uuid.v4();
                            }

                            data.password = bcrypt.hashSync(data.password, salt);
                            _context.next = 4;
                            return (0, _get2.default)((0, _getPrototypeOf2.default)(UserRepository), "create", _this).call(_this, data);

                          case 4:
                            created = _context.sent;
                            return _context.abrupt("return", created);

                          case 6:
                          case "end":
                            return _context.stop();
                        }
                      }
                    }, _callee);
                  }));

                  return function (_x2) {
                    return _ref.apply(this, arguments);
                  };
                }()));

              case 1:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, this);
      }));

      function create(_x) {
        return _create.apply(this, arguments);
      }

      return create;
    }()
    /**
     * Cập nhật user
     * @param data
     * @param id
     * @return {Promise<this>}
     */

  }, {
    key: "update",
    value: function update(data, id) {
      return (0, _get2.default)((0, _getPrototypeOf2.default)(UserRepository), "update", this).call(this, data, id);
    }
    /**
     * Đổi mật khẩu
     * @param password
     * @param id
     * @return {Promise<this>}
     */

  }, {
    key: "changePassword",
    value: function changePassword(password, id) {
      return this.model.update({
        password: bcrypt.hashSync(password, salt)
      }, {
        where: {
          id: id
        }
      });
    }
  }]);
  return UserRepository;
}(_baseRepository.default);

var _default = UserRepository;
exports.default = _default;