'use strict';
/**
 * Created by Phan Trung Nguyên.
 * User: nguyenpl117
 * Date: 5/29/2019
 * Time: 8:42 PM
 */

var fs = require('fs'),
    p = require('path');

var path = require('path');

var basename = path.basename(__filename); // how to know when you are done?

function recursiveReaddirSync(path) {
  var list = [],
      files = fs.readdirSync(path),
      stats;
  files.forEach(function (file) {
    stats = fs.lstatSync(p.join(path, file));

    if (stats.isDirectory()) {
      list = list.concat(recursiveReaddirSync(p.join(path, file)));
    } else {
      list.push(p.join(path, file));
    }
  });
  return list;
}

function loadPath(path) {
  if (!fs.existsSync(path)) {
    return [];
  }

  return recursiveReaddirSync(path).filter(function (file) {
    return file.indexOf('.') !== 0 && file !== p.join(__dirname, basename) && file.slice(-3) === '.js';
  });
}

var mutation = loadPath(__dirname + '/graphql/mutation');
var query = loadPath(__dirname + '/graphql/query');
var subscriptions = loadPath(__dirname + '/graphql/subscriptions');
var enumType = loadPath(__dirname + '/graphql/type/enum-type');
var filterType = loadPath(__dirname + '/graphql/type/filter-type');
var graphqlType = loadPath(__dirname + '/graphql/type/graphql-type');
var inputType = loadPath(__dirname + '/graphql/type/input-type');
var sortType = loadPath(__dirname + '/graphql/type/sort-type');
var unionType = loadPath(__dirname + '/graphql/type/union-type');
var models = loadPath(__dirname + '/models');
module.exports = {
  mutation: mutation,
  query: query,
  subscriptions: subscriptions,
  enumType: enumType,
  filterType: filterType,
  graphqlType: graphqlType,
  inputType: inputType,
  sortType: sortType,
  unionType: unionType,
  models: models
};