"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _possibleConstructorReturn2 = _interopRequireDefault(require("@babel/runtime/helpers/possibleConstructorReturn"));

var _getPrototypeOf2 = _interopRequireDefault(require("@babel/runtime/helpers/getPrototypeOf"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime/helpers/inherits"));

var _field = require("../../../../lib/field");

var _types = require("../../../../lib/types");

var _utils = require("../../../../lib/utils");

var _userRepository = _interopRequireDefault(require("../../repository/elo/user-repository"));

var _resolveInfo = require("../../../../lib/resolve-info");

var _rule = _interopRequireDefault(require("../../../../lib/validate/rule"));

var request = require('request');

var UserRegister =
/*#__PURE__*/
function (_Field) {
  (0, _inherits2.default)(UserRegister, _Field);

  function UserRegister(props) {
    var _this;

    (0, _classCallCheck2.default)(this, UserRegister);
    _this = (0, _possibleConstructorReturn2.default)(this, (0, _getPrototypeOf2.default)(UserRegister).call(this, props));
    _this.name = 'register';
    _this.description = 'Đăng ký';
    return _this;
  }

  (0, _createClass2.default)(UserRegister, [{
    key: "type",
    value: function type() {
      return _types.Types.Type('User');
    }
  }, {
    key: "args",
    value: function args() {
      return {
        id: {
          type: _types.Types.ID,
          description: 'ID. uuid.v4()'
        },
        name: {
          type: _types.Types.String,
          description: 'Họ và Tên'
        },
        gender: {
          description: 'giới tính: 1-Nam, 2-Nữ',
          type: _types.Types.Int,
          defaultValue: "1",
          rules: ['in:1,2']
        },
        phone: {
          type: _types.Types.String,
          description: 'số điện thoại',
          rules: ['required', 'between:9,10', _rule.default.unique('user', 'phone')]
        },
        password: {
          type: _types.Types.String,
          description: 'Mật khẩu đăng nhập',
          rules: ['required', 'string', 'between:6,32']
        },
        email: {
          type: _types.Types.String,
          description: 'Email đăng ký'
        }
      };
    }
  }, {
    key: "validationErrorMessages",
    value: function validationErrorMessages(args) {
      return {
        'between.phone': 'Số điện thoại không hợp lệ.'
      };
    }
  }, {
    key: "resolve",
    value: function () {
      var _resolve = (0, _asyncToGenerator2.default)(
      /*#__PURE__*/
      _regenerator.default.mark(function _callee(parent, args, _ref, info) {
        var db, cache, req, created, attributes;
        return _regenerator.default.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                db = _ref.db, cache = _ref.cache, req = _ref.req;
                _context.next = 3;
                return _userRepository.default.create(args);

              case 3:
                created = _context.sent;
                attributes = (0, _resolveInfo.getFields)(info, db.user);
                return _context.abrupt("return", db.user.findOne({
                  where: {
                    id: created.id
                  },
                  attributes: attributes
                }));

              case 6:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      }));

      function resolve(_x, _x2, _x3, _x4) {
        return _resolve.apply(this, arguments);
      }

      return resolve;
    }()
  }]);
  return UserRegister;
}(_field.Field);

module.exports = UserRegister;