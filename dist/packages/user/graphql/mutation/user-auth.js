"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _possibleConstructorReturn2 = _interopRequireDefault(require("@babel/runtime/helpers/possibleConstructorReturn"));

var _getPrototypeOf3 = _interopRequireDefault(require("@babel/runtime/helpers/getPrototypeOf"));

var _assertThisInitialized2 = _interopRequireDefault(require("@babel/runtime/helpers/assertThisInitialized"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime/helpers/inherits"));

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _types = require("../../../../lib/types");

var _validationError = _interopRequireDefault(require("../../../../exceptions/validation-error"));

var _field = require("../../../../lib/field");

var request = require('request');

var jwt = require('jsonwebtoken');

var bcrypt = require('bcrypt');

var UserAuth =
/*#__PURE__*/
function (_Field) {
  (0, _inherits2.default)(UserAuth, _Field);

  function UserAuth() {
    var _getPrototypeOf2;

    var _this;

    (0, _classCallCheck2.default)(this, UserAuth);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = (0, _possibleConstructorReturn2.default)(this, (_getPrototypeOf2 = (0, _getPrototypeOf3.default)(UserAuth)).call.apply(_getPrototypeOf2, [this].concat(args)));
    (0, _defineProperty2.default)((0, _assertThisInitialized2.default)(_this), "name", 'auth');
    (0, _defineProperty2.default)((0, _assertThisInitialized2.default)(_this), "description", 'Đăng nhập tài khoản.');
    return _this;
  }

  (0, _createClass2.default)(UserAuth, [{
    key: "type",
    value: function type() {
      return _types.Types.Type('User');
    }
  }, {
    key: "args",
    value: function args() {
      return {
        phone: {
          type: _types.Types.String,
          description: 'Số điện thoại.',
          rules: ['required']
        },
        password: {
          type: _types.Types.String,
          description: 'Mật khẩu đăng nhập',
          rules: ['required', 'string']
        }
      };
    }
  }, {
    key: "resolve",
    value: function () {
      var _resolve = (0, _asyncToGenerator2.default)(
      /*#__PURE__*/
      _regenerator.default.mark(function _callee(parent, args, _ref, info) {
        var db, cache, req, user;
        return _regenerator.default.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                db = _ref.db, cache = _ref.cache, req = _ref.req;
                _context.next = 3;
                return db.user.findOne({
                  where: {
                    phone: args.phone
                  },
                  attributes: {
                    exclude: []
                  }
                });

              case 3:
                user = _context.sent;

                if (!(!user || !bcrypt.compareSync(args.password, user.password))) {
                  _context.next = 6;
                  break;
                }

                throw new _validationError.default('Sai tài khoản hoặc mật khẩu.');

              case 6:
                user.setDataValue('password', null);
                user.setDataValue('exp', Math.floor(new Date().getTime() / 1000) + 60 * 60 * 24 * 365);
                user.token = jwt.sign(user.toJSON(), 'shhhhh');
                return _context.abrupt("return", user);

              case 10:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      }));

      function resolve(_x, _x2, _x3, _x4) {
        return _resolve.apply(this, arguments);
      }

      return resolve;
    }()
  }]);
  return UserAuth;
}(_field.Field);

module.exports = UserAuth;