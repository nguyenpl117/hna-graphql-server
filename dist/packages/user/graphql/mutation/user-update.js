"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _possibleConstructorReturn2 = _interopRequireDefault(require("@babel/runtime/helpers/possibleConstructorReturn"));

var _getPrototypeOf2 = _interopRequireDefault(require("@babel/runtime/helpers/getPrototypeOf"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime/helpers/inherits"));

var _field = require("../../../../lib/field");

var _types = require("../../../../lib/types");

var _utils = require("../../../../lib/utils");

var _userRepository = _interopRequireDefault(require("../../repository/elo/user-repository"));

var _resolveInfo = require("../../../../lib/resolve-info");

var _rule = _interopRequireDefault(require("../../../../lib/validate/rule"));

var _auth = _interopRequireDefault(require("../../../../lib/auth"));

var UserUpdate =
/*#__PURE__*/
function (_Field) {
  (0, _inherits2.default)(UserUpdate, _Field);

  function UserUpdate(props) {
    var _this;

    (0, _classCallCheck2.default)(this, UserUpdate);
    _this = (0, _possibleConstructorReturn2.default)(this, (0, _getPrototypeOf2.default)(UserUpdate).call(this, props));
    _this.name = 'updateUser';
    _this.description = 'Cập nhật người dùng';
    _this.authentication = true;
    return _this;
  }

  (0, _createClass2.default)(UserUpdate, [{
    key: "type",
    value: function type() {
      return _types.Types.Type('User');
    }
  }, {
    key: "args",
    value: function args() {
      return {
        name: {
          type: _types.Types.String,
          description: 'Họ và tên',
          rules: ['filled']
        },
        email: {
          type: _types.Types.String,
          description: 'email tài khoản',
          rules: ['email']
        },
        phone: {
          description: 'số điện thoại',
          type: _types.Types.String,
          rules: ['filled']
        },
        gender: {
          description: 'giới tính',
          type: _types.Types.Int,
          rules: ['in:1,2,3']
        },
        dob: {
          description: 'Ngày sinh thần',
          type: _types.Types.Timestamp
        },
        avatar: {
          description: 'đường dẫn avata',
          type: _types.Types.String
        },
        cover: {
          type: _types.Types.String
        }
      };
    }
  }, {
    key: "rules",
    value: function rules(args) {
      return {
        phone: [_rule.default.unique('user', 'phone').ignore(args.id)]
      };
    }
  }, {
    key: "resolve",
    value: function () {
      var _resolve = (0, _asyncToGenerator2.default)(
      /*#__PURE__*/
      _regenerator.default.mark(function _callee(parent, args, _ref, info) {
        var db, cache, req, updated, attributes;
        return _regenerator.default.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                db = _ref.db, cache = _ref.cache, req = _ref.req;
                _context.next = 3;
                return _userRepository.default.update(args, _auth.default.id());

              case 3:
                updated = _context.sent;
                attributes = (0, _resolveInfo.getFields)(info, db.user);
                return _context.abrupt("return", db.user.findOne({
                  where: {
                    id: _auth.default.id()
                  },
                  attributes: attributes
                }));

              case 6:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      }));

      function resolve(_x, _x2, _x3, _x4) {
        return _resolve.apply(this, arguments);
      }

      return resolve;
    }()
  }]);
  return UserUpdate;
}(_field.Field);

module.exports = UserUpdate;