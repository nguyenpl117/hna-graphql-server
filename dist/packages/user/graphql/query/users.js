"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _objectSpread2 = _interopRequireDefault(require("@babel/runtime/helpers/objectSpread"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _possibleConstructorReturn2 = _interopRequireDefault(require("@babel/runtime/helpers/possibleConstructorReturn"));

var _getPrototypeOf2 = _interopRequireDefault(require("@babel/runtime/helpers/getPrototypeOf"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime/helpers/inherits"));

var _field = require("../../../../lib/field");

var _types = require("../../../../lib/types");

var _resolveInfo = require("../../../../lib/resolve-info");

var _userRepository = _interopRequireDefault(require("../../../user/repository/elo/user-repository"));

var _pubsub = require("../../../../lib/pubsub");

var Users =
/*#__PURE__*/
function (_Field) {
  (0, _inherits2.default)(Users, _Field);

  function Users(props) {
    var _this;

    (0, _classCallCheck2.default)(this, Users);
    _this = (0, _possibleConstructorReturn2.default)(this, (0, _getPrototypeOf2.default)(Users).call(this, props));
    _this.name = 'users';
    _this.description = 'Danh sách thành viên';
    _this.authentication = true;
    return _this;
  }

  (0, _createClass2.default)(Users, [{
    key: "type",
    value: function type() {
      return _types.Types.Pagination('User');
    }
  }, {
    key: "args",
    value: function args() {
      return _types.Types.ArgsPagination('User');
    }
  }, {
    key: "resolve",
    value: function resolve(parent, args, _ref, info) {
      var db = _ref.db,
          user = _ref.user,
          cache = _ref.cache;
      var order = args.order,
          where = args.where,
          page = args.page,
          limit = args.limit;
      var attributes = (0, _resolveInfo.getPaginateFields)(info, db.user);
      return cache.remember(['users'], [args, attributes],
      /*#__PURE__*/
      (0, _asyncToGenerator2.default)(
      /*#__PURE__*/
      _regenerator.default.mark(function _callee() {
        return _regenerator.default.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                return _context.abrupt("return", _userRepository.default.paginate((0, _objectSpread2.default)({
                  where: where,
                  order: order,
                  page: page,
                  // Default 1
                  paginate: limit
                }, attributes)));

              case 1:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      })));
    }
  }]);
  return Users;
}(_field.Field);

module.exports = Users;