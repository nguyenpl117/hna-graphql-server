"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _possibleConstructorReturn2 = _interopRequireDefault(require("@babel/runtime/helpers/possibleConstructorReturn"));

var _getPrototypeOf2 = _interopRequireDefault(require("@babel/runtime/helpers/getPrototypeOf"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime/helpers/inherits"));

var _graphqlType = require("../../../../../lib/support/graphql-type");

var _types = require("../../../../../lib/types");

var User =
/*#__PURE__*/
function (_GraphqlType) {
  (0, _inherits2.default)(User, _GraphqlType);

  function User() {
    var _this;

    (0, _classCallCheck2.default)(this, User);
    _this = (0, _possibleConstructorReturn2.default)(this, (0, _getPrototypeOf2.default)(User).call(this));
    _this.name = 'User';
    _this.description = '';
    return _this;
  }

  (0, _createClass2.default)(User, [{
    key: "args",
    value: function args() {
      return {
        id: {
          type: _types.Types.ID,
          description: ''
        },
        name: {
          type: _types.Types.String,
          description: 'Họ và Tên'
        },
        email: {
          type: _types.Types.String,
          description: 'email '
        },
        avatar: {
          type: _types.Types.String,
          description: 'Ảnh đại diện'
        },
        cover: {
          type: _types.Types.String,
          description: 'Ảnh tường'
        },
        phone: {
          type: _types.Types.String,
          description: 'số điện thoại '
        },
        gender: {
          type: _types.Types.Int,
          description: 'giới tính 1 nam 2 nữ '
        },
        dob: {
          type: _types.Types.Timestamp,
          description: 'ngày sinh '
        },
        token: {
          type: _types.Types.String,
          description: 'Token đăng nhập'
        },
        createdAt: {
          type: _types.Types.Timestamp,
          description: 'thời gian tạo '
        },
        updatedAt: {
          type: _types.Types.Timestamp,
          description: ''
        }
      };
    }
  }]);
  return User;
}(_graphqlType.GraphqlType);

exports.default = User;