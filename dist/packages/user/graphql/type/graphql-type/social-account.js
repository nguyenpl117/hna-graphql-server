"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _possibleConstructorReturn2 = _interopRequireDefault(require("@babel/runtime/helpers/possibleConstructorReturn"));

var _getPrototypeOf2 = _interopRequireDefault(require("@babel/runtime/helpers/getPrototypeOf"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime/helpers/inherits"));

var _types = require("../../../../../lib/types");

var _graphqlType = require("../../../../../lib/support/graphql-type");

var SocialAccount =
/*#__PURE__*/
function (_GraphqlType) {
  (0, _inherits2.default)(SocialAccount, _GraphqlType);

  function SocialAccount() {
    var _this;

    (0, _classCallCheck2.default)(this, SocialAccount);
    _this = (0, _possibleConstructorReturn2.default)(this, (0, _getPrototypeOf2.default)(SocialAccount).call(this));
    _this.name = 'SocialAccount';
    _this.description = '';
    return _this;
  }

  (0, _createClass2.default)(SocialAccount, [{
    key: "args",
    value: function args() {
      return {
        id: {
          type: _types.Types.ID,
          description: ''
        },
        provider: {
          type: _types.Types.String,
          description: ' facebook , google '
        },
        providerId: {
          type: _types.Types.ID,
          description: ' id của facebook , hoặc google '
        },
        url: {
          type: _types.Types.String,
          description: ' link đên provide '
        },
        createdAt: {
          type: _types.Types.Timestamp,
          description: ''
        },
        updatedAt: {
          type: _types.Types.Timestamp,
          description: ''
        }
      };
    }
  }]);
  return SocialAccount;
}(_graphqlType.GraphqlType);

exports.default = SocialAccount;