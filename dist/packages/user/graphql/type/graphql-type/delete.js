"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _possibleConstructorReturn2 = _interopRequireDefault(require("@babel/runtime/helpers/possibleConstructorReturn"));

var _getPrototypeOf2 = _interopRequireDefault(require("@babel/runtime/helpers/getPrototypeOf"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime/helpers/inherits"));

var _types = require("../../../../../lib/types");

var _graphqlType = require("../../../../../lib/support/graphql-type");

var Delete =
/*#__PURE__*/
function (_GraphqlType) {
  (0, _inherits2.default)(Delete, _GraphqlType);

  function Delete() {
    var _this;

    (0, _classCallCheck2.default)(this, Delete);
    _this = (0, _possibleConstructorReturn2.default)(this, (0, _getPrototypeOf2.default)(Delete).call(this));
    _this.name = 'Delete';
    _this.description = '';
    return _this;
  }

  (0, _createClass2.default)(Delete, [{
    key: "fields",
    value: function fields() {
      return {
        status: {
          type: _types.Types.Type('STATUS'),
          description: 'Trạng thái'
        },
        message: {
          type: _types.Types.String,
          description: 'Thông báo'
        },
        data: {
          type: _types.Types.ListOf('ID'),
          description: 'danh sách các ID đã xóa'
        }
      };
    }
  }]);
  return Delete;
}(_graphqlType.GraphqlType);

exports.default = Delete;