"use strict";

module.exports = {
  type: "\ninput SortUser {\n    id: SortValue\n    name: SortValue\n    phone: SortValue\n    email: SortValue\n    avatar: SortValue\n    cover: SortValue\n    password: SortValue\n    dob: SortValue\n    gender: SortValue\n    createdAt: SortValue\n    updatedAt: SortValue\n}\n  ",
  resolve: {
    SortUser: {}
  }
};