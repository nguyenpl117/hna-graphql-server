"use strict";

module.exports = function (sequelize, DataTypes) {
  var UserSocial = sequelize.define('userSocial', {
    id: {
      type: DataTypes.BIGINT.UNSIGNED,
      primaryKey: true,
      autoIncrement: true
    },
    provider: {
      type: DataTypes.STRING(191),
      field: 'provider'
    },
    providerId: {
      type: DataTypes.STRING(191),
      field: 'provider_id'
    },
    userId: {
      type: DataTypes.STRING(32),
      field: 'user_id'
    },
    url: {
      type: DataTypes.TEXT,
      field: 'url'
    },
    createdAt: {
      type: DataTypes.DATE,
      field: 'created_at'
    },
    updatedAt: {
      type: DataTypes.DATE,
      field: 'updated_at'
    }
  }, {
    freezeTableName: true,
    tableName: 'user_social',
    deletedAt: false
  });
  return UserSocial;
};