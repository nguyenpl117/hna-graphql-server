"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.createAccountLimiter = exports.apiLimiter = void 0;

require('dotenv').config();

var rateLimit = require("express-rate-limit");

var url = require('url');

var apiLimiter = rateLimit({
  windowMs: 10 * 60 * 1000,
  // 1 minutes
  max: 100,
  onLimitReached: function onLimitReached(req, res, optionsUsed) {
    console.log(optionsUsed.message);
  },
  handler: function handler(req, res) {
    res.json({
      errors: [{
        message: 'Too many requests, please try again later.'
      }]
    });
  },
  skip: function skip(req) {
    if (!req.headers.origin) {
      return false;
    }

    var host = url.parse(req.headers.origin);

    if (['localhost', '127.0.0.1'].indexOf(host.hostname) === -1) {
      return false;
    }

    return true;
  },
  keyGenerator: function keyGenerator(req) {
    return req.clientIp;
  }
});
exports.apiLimiter = apiLimiter;
var createAccountLimiter = rateLimit({
  windowMs: 60 * 60 * 1000,
  // 1 hour window
  max: 5,
  // start blocking after 5 requests
  // message: "Too many accounts created from this IP, please try again after an hour"
  message: "Quá nhiều tài khoản được tạo ra từ IP này, Vui lòng thử lại sau 1 giờ."
});
exports.createAccountLimiter = createAccountLimiter;