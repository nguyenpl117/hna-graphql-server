"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _graphql = require("graphql");

var _http = _interopRequireDefault(require("http"));

var _graphqlDepthLimit = _interopRequireDefault(require("graphql-depth-limit"));

var _graphqlCostAnalysis = _interopRequireDefault(require("graphql-cost-analysis"));

var _auth = _interopRequireDefault(require("./lib/auth"));

var _reteLimit = require("./middleware/rete-limit");

var _graphQLExceptions = _interopRequireDefault(require("./exceptions/graphQL-exceptions"));

var _costDirective = _interopRequireDefault(require("./lib/directive/cost-directive"));

var _bodyParser = require("body-parser");

var _arrayCache = require("./lib/array-cache");

var _cacheIpView = _interopRequireDefault(require("./lib/cache-ip-view"));

var _pubsub = require("./lib/pubsub");

var _cacheManage = require("./cache/cacheManage");

var _moment = _interopRequireDefault(require("moment"));

require('dotenv').config();

var express = require('express');

var _require = require('apollo-server-express'),
    ApolloServer = _require.ApolloServer,
    gql = _require.gql,
    ApolloError = _require.ApolloError;

var typeDefs = require("./schema");

var resolvers = require("./resolvers");

var db = require("./models");

var jwt = require('jsonwebtoken');

var requestIp = require('request-ip');

var url = require('url');

var responseTime = require('response-time');

var path = require('path');

_moment.default.tz.setDefault("Asia/Ho_Chi_Minh");

var createError = require('http-errors');

var costAnalyzer = (0, _graphqlCostAnalysis.default)({
  maximumCost: 2000,
  defaultCost: 1,
  onComplete: function onComplete(cost) {// console.log('query cost: ', cost);
  }
});

var cacheManage = _cacheManage.Cache.driver(process.env.CACHE_DRIVER || 'array');

var server = new ApolloServer({
  engine: process.env.ENGINE === 'true',
  tracing: process.env.TRACING === 'true',
  typeDefs: typeDefsPath(),
  resolvers: resolvers,
  schemaDirectives: {
    cost: _costDirective.default
  },
  cacheControl: false,
  subscriptions: {
    path: '/graphql',
    onConnect: function () {
      var _onConnect = (0, _asyncToGenerator2.default)(
      /*#__PURE__*/
      _regenerator.default.mark(function _callee(connectionParams, websocket, context) {
        return _regenerator.default.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _auth.default.init();

                if (!connectionParams.authorization) {
                  _context.next = 8;
                  break;
                }

                _context.t0 = _auth.default;
                _context.next = 5;
                return getUser(connectionParams.authorization);

              case 5:
                _context.t1 = _context.sent;

                _context.t0.setUser.call(_context.t0, _context.t1);

                return _context.abrupt("return", {
                  db: db,
                  pubsub: _pubsub.pubsub,
                  cache: cacheManage,
                  req: null,
                  user: _auth.default.user()
                });

              case 8:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      }));

      function onConnect(_x, _x2, _x3) {
        return _onConnect.apply(this, arguments);
      }

      return onConnect;
    }()
  },
  // debug: true,
  // cache: true,
  validationRules: [(0, _graphqlDepthLimit.default)(5), costAnalyzer],
  formatError: _graphQLExceptions.default.formatError,
  context: function () {
    var _context2 = (0, _asyncToGenerator2.default)(
    /*#__PURE__*/
    _regenerator.default.mark(function _callee2(_ref) {
      var req, connection;
      return _regenerator.default.wrap(function _callee2$(_context3) {
        while (1) {
          switch (_context3.prev = _context3.next) {
            case 0:
              req = _ref.req, connection = _ref.connection;
              db.sequelize.query("SET CHARACTER SET utf8mb4");

              if (req) {
                _context3.next = 4;
                break;
              }

              return _context3.abrupt("return", {
                req: req,
                db: db,
                pubsub: _pubsub.pubsub,
                cache: cacheManage,
                user: _auth.default.user()
              });

            case 4:
              _auth.default.init();

              _context3.t0 = _auth.default;
              _context3.next = 8;
              return getUser(req.headers.authorization);

            case 8:
              _context3.t1 = _context3.sent;

              _context3.t0.setUser.call(_context3.t0, _context3.t1);

              return _context3.abrupt("return", {
                db: db,
                pubsub: _pubsub.pubsub,
                cache: cacheManage,
                req: req,
                user: _auth.default.user()
              });

            case 11:
            case "end":
              return _context3.stop();
          }
        }
      }, _callee2);
    }));

    function context(_x4) {
      return _context2.apply(this, arguments);
    }

    return context;
  }(),
  playground: true,
  introspection: true
});

function getUser(_x5) {
  return _getUser.apply(this, arguments);
}

function _getUser() {
  _getUser = (0, _asyncToGenerator2.default)(
  /*#__PURE__*/
  _regenerator.default.mark(function _callee3(authorization) {
    var decoded;
    return _regenerator.default.wrap(function _callee3$(_context4) {
      while (1) {
        switch (_context4.prev = _context4.next) {
          case 0:
            _context4.prev = 0;
            decoded = jwt.decode(authorization.replace(/^(Bearer\s)/g, ''), {
              complete: true
            });

            if (!(decoded.payload.exp * 1000 < new Date().getTime())) {
              _context4.next = 4;
              break;
            }

            return _context4.abrupt("return", null);

          case 4:
            return _context4.abrupt("return", db.user.findOne({
              where: {
                id: decoded.payload.id
              },
              logging: false
            }).then(function (data) {
              return data && data.toJSON();
            }));

          case 7:
            _context4.prev = 7;
            _context4.t0 = _context4["catch"](0);

          case 9:
            return _context4.abrupt("return", null);

          case 10:
          case "end":
            return _context4.stop();
        }
      }
    }, _callee3, null, [[0, 7]]);
  }));
  return _getUser.apply(this, arguments);
}

var app = express();
app.use('/graphql', responseTime(function (req, res, time) {
  if (time > 200) console.log(time);
}));
app.use('/graphql', requestIp.mw());
app.use('/graphql', (0, _bodyParser.json)({})); // app.use('/graphql', apiLimiter);

var counter = 0;
setInterval(function () {
  // console.log(counter);
  counter = 0;
}, 1000);
app.use('/graphql', function (req, res, next) {
  counter++;

  _arrayCache.ArrayCache.init();

  _cacheIpView.default.init({
    timeout: 1000 * 60
  });

  if (!req.headers.origin) {
    return next();
  }

  var host = url.parse(req.headers.origin);
  var maxBatch = 10;

  if (['localhost', '127.0.0.1'].indexOf(host.hostname) !== -1) {
    maxBatch = 20;
  }

  if (Array.isArray(req.body) && req.body.length > maxBatch) {
    return next(createError('request batch too many', {
      limit: maxBatch
    }));
  }

  return next();
});
server.applyMiddleware({
  app: app,
  path: '/graphql',
  bodyParserConfig: {
    limit: "100kb"
  }
});

var httpServer = _http.default.createServer(app);

server.installSubscriptionHandlers(httpServer);
app.get('/graphiql', function (req, res) {
  res.sendFile(path.join(__dirname + '/graphiql.html'));
});
app.use('/graphql', function (err, req, res, next) {
  console.error(err); // if (err) {
  //     res.status(err.status).json({errors: [err]});
  // }
});
app.use(express.static("app/public"));
var host = process.env.HOST || '127.0.0.1';
var port = process.env.PORT || '8000'; // Listen the server

httpServer.listen(port, host, function () {
  console.log("\uD83D\uDE80 Server ready at http://localhost:".concat(port, "/graphiql"));
  console.log("\uD83D\uDE80 Subscriptions ready at ws://localhost:".concat(port).concat(server.subscriptionsPath));
}); // app.listen(port, host, function() {
//     console.log(`Server listening on http://${host}:${port}/graphql`);
// });