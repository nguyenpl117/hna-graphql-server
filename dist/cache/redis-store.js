"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

var _toConsumableArray2 = _interopRequireDefault(require("@babel/runtime/helpers/toConsumableArray"));

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _slicedToArray2 = _interopRequireDefault(require("@babel/runtime/helpers/slicedToArray"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _arr = _interopRequireDefault(require("../lib/arr"));

var _lodash = _interopRequireDefault(require("lodash"));

var _this = void 0;

var md5 = require('crypto-js/md5');

var sha1 = require('crypto-js/sha1');

var Redis = require('ioredis');

var redis = new Redis({
  port: process.env.REDIS_PORT,
  // Redis port
  host: process.env.REDIS_HOST,
  // Redis host
  family: 4,
  // 4 (IPv4) or 6 (IPv6)
  password: process.env.REDIS_PASSWORD,
  db: process.env.REDIS_DB
});

var serialize = require('../lib/serialize');

var HIERARCHY_SEPARATOR = '|';
var TAG_SEPARATOR = '!';
var keyCache = {};

function getHierarchyKey(_x, _x2) {
  return _getHierarchyKey.apply(this, arguments);
}

function _getHierarchyKey() {
  _getHierarchyKey = (0, _asyncToGenerator2.default)(
  /*#__PURE__*/
  _regenerator.default.mark(function _callee7(key, object) {
    var keyString, index, _iteratorNormalCompletion6, _didIteratorError6, _iteratorError6, _iterator6, _step6, name;

    return _regenerator.default.wrap(function _callee7$(_context7) {
      while (1) {
        switch (_context7.prev = _context7.next) {
          case 0:
            if (isHierarchyKey(key)) {
              _context7.next = 2;
              break;
            }

            return _context7.abrupt("return", key);

          case 2:
            key = explodeKey(key);
            keyString = '', index = ''; // The comments below is for a $key = ["foo!tagHash", "bar!tagHash"]

            _iteratorNormalCompletion6 = true;
            _didIteratorError6 = false;
            _iteratorError6 = undefined;
            _context7.prev = 7;
            _iterator6 = key[Symbol.iterator]();

          case 9:
            if (_iteratorNormalCompletion6 = (_step6 = _iterator6.next()).done) {
              _context7.next = 28;
              break;
            }

            name = _step6.value;
            // 1) $keyString = "foo!tagHash"
            // 2) $keyString = "foo!tagHash![foo_index]!bar!tagHash"
            keyString += name;
            object.pathKey = sha1('path' + TAG_SEPARATOR + keyString).toString();

            if (!keyCache[object.pathKey]) {
              _context7.next = 17;
              break;
            }

            index = keyCache[object.pathKey];
            _context7.next = 24;
            break;

          case 17:
            _context7.next = 19;
            return redis.get(object.pathKey);

          case 19:
            _context7.t0 = _context7.sent;

            if (_context7.t0) {
              _context7.next = 22;
              break;
            }

            _context7.t0 = '';

          case 22:
            index = _context7.t0;
            keyCache[object.pathKey] = index;

          case 24:
            // 1) $keyString = "foo!tagHash![foo_index]!"
            // 2) $keyString = "foo!tagHash![foo_index]!bar!tagHash![bar_index]!"
            keyString += TAG_SEPARATOR + index + TAG_SEPARATOR;

          case 25:
            _iteratorNormalCompletion6 = true;
            _context7.next = 9;
            break;

          case 28:
            _context7.next = 34;
            break;

          case 30:
            _context7.prev = 30;
            _context7.t1 = _context7["catch"](7);
            _didIteratorError6 = true;
            _iteratorError6 = _context7.t1;

          case 34:
            _context7.prev = 34;
            _context7.prev = 35;

            if (!_iteratorNormalCompletion6 && _iterator6.return != null) {
              _iterator6.return();
            }

          case 37:
            _context7.prev = 37;

            if (!_didIteratorError6) {
              _context7.next = 40;
              break;
            }

            throw _iteratorError6;

          case 40:
            return _context7.finish(37);

          case 41:
            return _context7.finish(34);

          case 42:
            return _context7.abrupt("return", sha1(keyString).toString());

          case 43:
          case "end":
            return _context7.stop();
        }
      }
    }, _callee7, null, [[7, 30, 34, 42], [35,, 37, 41]]);
  }));
  return _getHierarchyKey.apply(this, arguments);
}

function isHierarchyKey(key) {
  return typeof key === 'string' && key[0] === HIERARCHY_SEPARATOR;
}

function explodeKey(string) {
  var _$split = "".concat(string).concat(TAG_SEPARATOR).split(TAG_SEPARATOR),
      _$split2 = (0, _slicedToArray2.default)(_$split, 2),
      key = _$split2[0],
      tag = _$split2[1];

  var parts = ['root'];

  if (key === HIERARCHY_SEPARATOR) {
    parts = ['root'];
  } else {
    parts = key.split(HIERARCHY_SEPARATOR);
    parts[0] = 'root';
  }

  return parts.map(function (level) {
    return level + TAG_SEPARATOR + tag;
  });
}

var manage = {
  getTagKey: function getTagKey(tag) {
    // return `tag${TAG_SEPARATOR}${HIERARCHY_SEPARATOR}${process.env.REDIS_PREFIX}_${HIERARCHY_SEPARATOR}${tag}`;
    return 'tag' + TAG_SEPARATOR + tag;
  },
  get: function get(key) {
    return redis.get(key).then(function (data) {
      return serialize.unserialize(data);
    });
  },
  set: function set(a, b, c, d) {
    return redis.set(a, b, c, d);
  },
  getList: function getList(name) {
    return redis.lrange(name, 0, -1);
  },
  clearOneObjectFromCache: function () {
    var _clearOneObjectFromCache = (0, _asyncToGenerator2.default)(
    /*#__PURE__*/
    _regenerator.default.mark(function _callee(key) {
      var obj, keyString, pathKey, deleted;
      return _regenerator.default.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              obj = {
                pathKey: ''
              };
              _context.next = 3;
              return getHierarchyKey(key, obj);

            case 3:
              keyString = _context.sent;
              _context.next = 6;
              return manage.preRemoveItem(keyString);

            case 6:
              pathKey = obj.pathKey;
              keyCache = [];
              _context.next = 10;
              return redis.del(keyString);

            case 10:
              deleted = _context.sent;
              return _context.abrupt("return", !!redis.del(keyString));

            case 12:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    }));

    function clearOneObjectFromCache(_x3) {
      return _clearOneObjectFromCache.apply(this, arguments);
    }

    return clearOneObjectFromCache;
  }(),
  preRemoveItem: function () {
    var _preRemoveItem = (0, _asyncToGenerator2.default)(
    /*#__PURE__*/
    _regenerator.default.mark(function _callee2(key) {
      var tags, _iteratorNormalCompletion, _didIteratorError, _iteratorError, _iterator, _step, tag;

      return _regenerator.default.wrap(function _callee2$(_context2) {
        while (1) {
          switch (_context2.prev = _context2.next) {
            case 0:
              _context2.next = 2;
              return manage.get(key).then(function (data) {
                if (!data) {
                  return [];
                }

                return data;
              });

            case 2:
              tags = _context2.sent;
              _iteratorNormalCompletion = true;
              _didIteratorError = false;
              _iteratorError = undefined;
              _context2.prev = 6;
              _iterator = tags[Symbol.iterator]();

            case 8:
              if (_iteratorNormalCompletion = (_step = _iterator.next()).done) {
                _context2.next = 15;
                break;
              }

              tag = _step.value;
              _context2.next = 12;
              return manage.removeListItem(manage.getTagKey(tag), key);

            case 12:
              _iteratorNormalCompletion = true;
              _context2.next = 8;
              break;

            case 15:
              _context2.next = 21;
              break;

            case 17:
              _context2.prev = 17;
              _context2.t0 = _context2["catch"](6);
              _didIteratorError = true;
              _iteratorError = _context2.t0;

            case 21:
              _context2.prev = 21;
              _context2.prev = 22;

              if (!_iteratorNormalCompletion && _iterator.return != null) {
                _iterator.return();
              }

            case 24:
              _context2.prev = 24;

              if (!_didIteratorError) {
                _context2.next = 27;
                break;
              }

              throw _iteratorError;

            case 27:
              return _context2.finish(24);

            case 28:
              return _context2.finish(21);

            case 29:
              return _context2.abrupt("return", _this);

            case 30:
            case "end":
              return _context2.stop();
          }
        }
      }, _callee2, null, [[6, 17, 21, 29], [22,, 24, 28]]);
    }));

    function preRemoveItem(_x4) {
      return _preRemoveItem.apply(this, arguments);
    }

    return preRemoveItem;
  }(),
  removeListItem: function removeListItem(name, key) {
    return redis.lrem(name, 0, key);
  },
  deleteItems: function () {
    var _deleteItems = (0, _asyncToGenerator2.default)(
    /*#__PURE__*/
    _regenerator.default.mark(function _callee3(keys) {
      var deleted, _iteratorNormalCompletion2, _didIteratorError2, _iteratorError2, _iterator2, _step2, key;

      return _regenerator.default.wrap(function _callee3$(_context3) {
        while (1) {
          switch (_context3.prev = _context3.next) {
            case 0:
              deleted = true;
              _iteratorNormalCompletion2 = true;
              _didIteratorError2 = false;
              _iteratorError2 = undefined;
              _context3.prev = 4;
              _iterator2 = keys[Symbol.iterator]();

            case 6:
              if (_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done) {
                _context3.next = 15;
                break;
              }

              key = _step2.value;
              _context3.next = 10;
              return manage.clearOneObjectFromCache(key);

            case 10:
              if (_context3.sent) {
                _context3.next = 12;
                break;
              }

              deleted = false;

            case 12:
              _iteratorNormalCompletion2 = true;
              _context3.next = 6;
              break;

            case 15:
              _context3.next = 21;
              break;

            case 17:
              _context3.prev = 17;
              _context3.t0 = _context3["catch"](4);
              _didIteratorError2 = true;
              _iteratorError2 = _context3.t0;

            case 21:
              _context3.prev = 21;
              _context3.prev = 22;

              if (!_iteratorNormalCompletion2 && _iterator2.return != null) {
                _iterator2.return();
              }

            case 24:
              _context3.prev = 24;

              if (!_didIteratorError2) {
                _context3.next = 27;
                break;
              }

              throw _iteratorError2;

            case 27:
              return _context3.finish(24);

            case 28:
              return _context3.finish(21);

            case 29:
              return _context3.abrupt("return", deleted);

            case 30:
            case "end":
              return _context3.stop();
          }
        }
      }, _callee3, null, [[4, 17, 21, 29], [22,, 24, 28]]);
    }));

    function deleteItems(_x5) {
      return _deleteItems.apply(this, arguments);
    }

    return deleteItems;
  }(),
  clearTags: function () {
    var _clearTags = (0, _asyncToGenerator2.default)(
    /*#__PURE__*/
    _regenerator.default.mark(function _callee4(tags) {
      var itemIds, _iteratorNormalCompletion3, _didIteratorError3, _iteratorError3, _iterator3, _step3, _tag, success, _iteratorNormalCompletion4, _didIteratorError4, _iteratorError4, _iterator4, _step4, tag;

      return _regenerator.default.wrap(function _callee4$(_context4) {
        while (1) {
          switch (_context4.prev = _context4.next) {
            case 0:
              tags = _arr.default.array_wrap(tags).map(function (tag) {
                return "".concat(HIERARCHY_SEPARATOR).concat(process.env.REDIS_PREFIX, "_").concat(HIERARCHY_SEPARATOR).concat(tag);
              });
              itemIds = [];
              _iteratorNormalCompletion3 = true;
              _didIteratorError3 = false;
              _iteratorError3 = undefined;
              _context4.prev = 5;
              _iterator3 = tags[Symbol.iterator]();

            case 7:
              if (_iteratorNormalCompletion3 = (_step3 = _iterator3.next()).done) {
                _context4.next = 20;
                break;
              }

              _tag = _step3.value;
              _context4.t0 = [];
              _context4.t1 = (0, _toConsumableArray2.default)(itemIds);
              _context4.t2 = _toConsumableArray2.default;
              _context4.next = 14;
              return manage.getList(manage.getTagKey(_tag));

            case 14:
              _context4.t3 = _context4.sent;
              _context4.t4 = (0, _context4.t2)(_context4.t3);
              itemIds = _context4.t0.concat.call(_context4.t0, _context4.t1, _context4.t4);

            case 17:
              _iteratorNormalCompletion3 = true;
              _context4.next = 7;
              break;

            case 20:
              _context4.next = 26;
              break;

            case 22:
              _context4.prev = 22;
              _context4.t5 = _context4["catch"](5);
              _didIteratorError3 = true;
              _iteratorError3 = _context4.t5;

            case 26:
              _context4.prev = 26;
              _context4.prev = 27;

              if (!_iteratorNormalCompletion3 && _iterator3.return != null) {
                _iterator3.return();
              }

            case 29:
              _context4.prev = 29;

              if (!_didIteratorError3) {
                _context4.next = 32;
                break;
              }

              throw _iteratorError3;

            case 32:
              return _context4.finish(29);

            case 33:
              return _context4.finish(26);

            case 34:
              _context4.next = 36;
              return manage.deleteItems(itemIds);

            case 36:
              success = _context4.sent;

              if (!success) {
                _context4.next = 64;
                break;
              }

              // Remove the tag list
              _iteratorNormalCompletion4 = true;
              _didIteratorError4 = false;
              _iteratorError4 = undefined;
              _context4.prev = 41;
              _iterator4 = tags[Symbol.iterator]();

            case 43:
              if (_iteratorNormalCompletion4 = (_step4 = _iterator4.next()).done) {
                _context4.next = 50;
                break;
              }

              tag = _step4.value;
              _context4.next = 47;
              return redis.del(manage.getTagKey(tag));

            case 47:
              _iteratorNormalCompletion4 = true;
              _context4.next = 43;
              break;

            case 50:
              _context4.next = 56;
              break;

            case 52:
              _context4.prev = 52;
              _context4.t6 = _context4["catch"](41);
              _didIteratorError4 = true;
              _iteratorError4 = _context4.t6;

            case 56:
              _context4.prev = 56;
              _context4.prev = 57;

              if (!_iteratorNormalCompletion4 && _iterator4.return != null) {
                _iterator4.return();
              }

            case 59:
              _context4.prev = 59;

              if (!_didIteratorError4) {
                _context4.next = 62;
                break;
              }

              throw _iteratorError4;

            case 62:
              return _context4.finish(59);

            case 63:
              return _context4.finish(56);

            case 64:
              return _context4.abrupt("return", success);

            case 65:
            case "end":
              return _context4.stop();
          }
        }
      }, _callee4, null, [[5, 22, 26, 34], [27,, 29, 33], [41, 52, 56, 64], [57,, 59, 63]]);
    }));

    function clearTags(_x6) {
      return _clearTags.apply(this, arguments);
    }

    return clearTags;
  }(),
  remember: function () {
    var _remember = (0, _asyncToGenerator2.default)(
    /*#__PURE__*/
    _regenerator.default.mark(function _callee5(tags, key, callback, ttl) {
      var prefix, obj, keyString, pathKey, data, listTags, _iteratorNormalCompletion5, _didIteratorError5, _iteratorError5, _iterator5, _step5, item, tagKey, keys, docs;

      return _regenerator.default.wrap(function _callee5$(_context5) {
        while (1) {
          switch (_context5.prev = _context5.next) {
            case 0:
              _context5.prev = 0;
              key = md5(JSON.stringify(key)).toString();
              prefix = "".concat(HIERARCHY_SEPARATOR).concat(process.env.REDIS_PREFIX, "_").concat(HIERARCHY_SEPARATOR);
              obj = {};
              _context5.next = 6;
              return getHierarchyKey("".concat(prefix).concat(key), obj);

            case 6:
              keyString = _context5.sent;
              pathKey = obj.pathKey;
              _context5.next = 10;
              return manage.get(keyString);

            case 10:
              data = _context5.sent;

              if (!(data && Array.isArray(data))) {
                _context5.next = 13;
                break;
              }

              return _context5.abrupt("return", data[1]);

            case 13:
              listTags = [];
              _iteratorNormalCompletion5 = true;
              _didIteratorError5 = false;
              _iteratorError5 = undefined;
              _context5.prev = 17;
              _iterator5 = _arr.default.array_wrap(tags)[Symbol.iterator]();

            case 19:
              if (_iteratorNormalCompletion5 = (_step5 = _iterator5.next()).done) {
                _context5.next = 35;
                break;
              }

              item = _step5.value;
              tagKey = manage.getTagKey("".concat(prefix).concat(item));
              _context5.next = 24;
              return manage.getList(tagKey);

            case 24:
              _context5.t0 = _context5.sent;

              if (_context5.t0) {
                _context5.next = 27;
                break;
              }

              _context5.t0 = [];

            case 27:
              keys = _context5.t0;
              keys.push("".concat(prefix).concat(key));

              if (!Array.isArray(keys) || keys.length === 0) {
                keys = ["".concat(prefix).concat(key)];
              }

              redis.lpush(tagKey, _lodash.default.uniq(keys));
              listTags.push(tagKey);

            case 32:
              _iteratorNormalCompletion5 = true;
              _context5.next = 19;
              break;

            case 35:
              _context5.next = 41;
              break;

            case 37:
              _context5.prev = 37;
              _context5.t1 = _context5["catch"](17);
              _didIteratorError5 = true;
              _iteratorError5 = _context5.t1;

            case 41:
              _context5.prev = 41;
              _context5.prev = 42;

              if (!_iteratorNormalCompletion5 && _iterator5.return != null) {
                _iterator5.return();
              }

            case 44:
              _context5.prev = 44;

              if (!_didIteratorError5) {
                _context5.next = 47;
                break;
              }

              throw _iteratorError5;

            case 47:
              return _context5.finish(44);

            case 48:
              return _context5.finish(41);

            case 49:
              _context5.next = 51;
              return callback();

            case 51:
              docs = _context5.sent;
              redis.set(keyString, serialize.serialize([true, docs, listTags]), 'EX', ttl ? ttl : process.env.REDIS_TTL);
              _context5.next = 58;
              break;

            case 55:
              _context5.prev = 55;
              _context5.t2 = _context5["catch"](0);
              console.log('error', _context5.t2);

            case 58:
              _context5.next = 60;
              return callback();

            case 60:
              return _context5.abrupt("return", _context5.sent);

            case 61:
            case "end":
              return _context5.stop();
          }
        }
      }, _callee5, null, [[0, 55], [17, 37, 41, 49], [42,, 44, 48]]);
    }));

    function remember(_x7, _x8, _x9, _x10) {
      return _remember.apply(this, arguments);
    }

    return remember;
  }(),
  remember2: function () {
    var _remember2 = (0, _asyncToGenerator2.default)(
    /*#__PURE__*/
    _regenerator.default.mark(function _callee6(tags, key, callback, ttl) {
      var prefix, obj, keyString, pathKey, data, listTags, docs;
      return _regenerator.default.wrap(function _callee6$(_context6) {
        while (1) {
          switch (_context6.prev = _context6.next) {
            case 0:
              _context6.prev = 0;
              key = md5(JSON.stringify(key)).toString();
              prefix = "".concat(HIERARCHY_SEPARATOR).concat(process.env.REDIS_PREFIX, "_").concat(HIERARCHY_SEPARATOR);
              obj = {};
              _context6.next = 6;
              return getHierarchyKey("".concat(prefix).concat(key), obj);

            case 6:
              keyString = _context6.sent;
              pathKey = obj.pathKey;
              _context6.next = 10;
              return manage.get(keyString);

            case 10:
              data = _context6.sent;

              if (!(data && Array.isArray(data))) {
                _context6.next = 13;
                break;
              }

              return _context6.abrupt("return", data[1]);

            case 13:
              listTags = [];
              _context6.next = 16;
              return callback();

            case 16:
              docs = _context6.sent;
              redis.set(keyString, serialize.serialize([true, docs, listTags]), 'EX', ttl ? ttl : process.env.REDIS_TTL);
              _context6.next = 23;
              break;

            case 20:
              _context6.prev = 20;
              _context6.t0 = _context6["catch"](0);
              console.log('error', _context6.t0);

            case 23:
              _context6.next = 25;
              return callback();

            case 25:
              return _context6.abrupt("return", _context6.sent);

            case 26:
            case "end":
              return _context6.stop();
          }
        }
      }, _callee6, null, [[0, 20]]);
    }));

    function remember2(_x11, _x12, _x13, _x14) {
      return _remember2.apply(this, arguments);
    }

    return remember2;
  }()
};
module.exports = manage;