"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Cache = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var stores = {
  redis: function redis() {
    return require('./redis-store');
  },
  rtl: function rtl() {
    return require('./rtl-store');
  },
  array: function array() {
    return require('./array-store');
  }
};

var Cache =
/*#__PURE__*/
function () {
  function Cache() {
    (0, _classCallCheck2.default)(this, Cache);
  }

  (0, _createClass2.default)(Cache, null, [{
    key: "store",
    value: function store() {
      var _store = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 'array';

      if (!this.stores) {
        this.stores = {};
      }

      if (!this.stores || !this.stores[_store]) {
        this.stores[_store] = stores[_store]();
      }

      return this.stores[_store];
    }
  }, {
    key: "driver",
    value: function driver() {
      var store = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 'array';
      return this.store(store);
    }
  }, {
    key: "get",
    value: function get(key) {
      return this.driver().get(key);
    }
  }, {
    key: "set",
    value: function set(key, data, ttl, d) {
      return this.driver().set(key, data, ttl, d);
    }
  }, {
    key: "clearTags",
    value: function clearTags(tags) {
      return this.driver().clearTags(tags);
    }
  }, {
    key: "remember",
    value: function remember(tags, key, callback, ttl) {
      return this.driver().remember(tags, key, callback, ttl);
    }
  }, {
    key: "remember2",
    value: function remember2(tags, key, callback, ttl) {
      return this.driver().remember2(tags, key, callback, ttl);
    }
  }]);
  return Cache;
}();

exports.Cache = Cache;