"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var manage = {
  get: function get(key) {
    return null;
  },
  set: function set(a, b, c, d) {
    return null;
  },
  clearTags: function clearTags(tags) {},
  remember: function () {
    var _remember = (0, _asyncToGenerator2.default)(
    /*#__PURE__*/
    _regenerator.default.mark(function _callee(tags, key, callback, ttl) {
      return _regenerator.default.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              _context.next = 2;
              return callback();

            case 2:
              return _context.abrupt("return", _context.sent);

            case 3:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    }));

    function remember(_x, _x2, _x3, _x4) {
      return _remember.apply(this, arguments);
    }

    return remember;
  }(),
  remember2: function () {
    var _remember2 = (0, _asyncToGenerator2.default)(
    /*#__PURE__*/
    _regenerator.default.mark(function _callee2(tags, key, callback, ttl) {
      return _regenerator.default.wrap(function _callee2$(_context2) {
        while (1) {
          switch (_context2.prev = _context2.next) {
            case 0:
              _context2.next = 2;
              return callback();

            case 2:
              return _context2.abrupt("return", _context2.sent);

            case 3:
            case "end":
              return _context2.stop();
          }
        }
      }, _callee2);
    }));

    function remember2(_x5, _x6, _x7, _x8) {
      return _remember2.apply(this, arguments);
    }

    return remember2;
  }()
};
module.exports = manage;