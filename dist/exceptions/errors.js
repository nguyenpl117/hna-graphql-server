"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AuthenticationException = exports.AuthorizationError = exports.InvalidOperationException = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _possibleConstructorReturn2 = _interopRequireDefault(require("@babel/runtime/helpers/possibleConstructorReturn"));

var _getPrototypeOf2 = _interopRequireDefault(require("@babel/runtime/helpers/getPrototypeOf"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime/helpers/inherits"));

var _wrapNativeSuper2 = _interopRequireDefault(require("@babel/runtime/helpers/wrapNativeSuper"));

var InvalidOperationException =
/*#__PURE__*/
function (_Error) {
  (0, _inherits2.default)(InvalidOperationException, _Error);

  function InvalidOperationException(message) {
    (0, _classCallCheck2.default)(this, InvalidOperationException);
    return (0, _possibleConstructorReturn2.default)(this, (0, _getPrototypeOf2.default)(InvalidOperationException).call(this, message));
  }

  return InvalidOperationException;
}((0, _wrapNativeSuper2.default)(Error));

exports.InvalidOperationException = InvalidOperationException;

var AuthorizationError =
/*#__PURE__*/
function (_Error2) {
  (0, _inherits2.default)(AuthorizationError, _Error2);

  function AuthorizationError() {
    var message = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 'Unauthorized';
    (0, _classCallCheck2.default)(this, AuthorizationError);
    return (0, _possibleConstructorReturn2.default)(this, (0, _getPrototypeOf2.default)(AuthorizationError).call(this, message));
  }

  return AuthorizationError;
}((0, _wrapNativeSuper2.default)(Error));

exports.AuthorizationError = AuthorizationError;

var AuthenticationException =
/*#__PURE__*/
function (_Error3) {
  (0, _inherits2.default)(AuthenticationException, _Error3);

  function AuthenticationException() {
    var message = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 'Unauthenticated';
    (0, _classCallCheck2.default)(this, AuthenticationException);
    return (0, _possibleConstructorReturn2.default)(this, (0, _getPrototypeOf2.default)(AuthenticationException).call(this, message));
  }

  return AuthenticationException;
}((0, _wrapNativeSuper2.default)(Error));

exports.AuthenticationException = AuthenticationException;