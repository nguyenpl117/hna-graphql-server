"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var GraphQLExceptions =
/*#__PURE__*/
function () {
  function GraphQLExceptions() {
    (0, _classCallCheck2.default)(this, GraphQLExceptions);
  }

  (0, _createClass2.default)(GraphQLExceptions, null, [{
    key: "formatError",
    value: function formatError(error) {
      try {
        if (error.originalError.name === 'ValidationError') {
          error.validation = error.originalError.getValidatorMessages();
        }

        if (error.originalError.name === 'ApiValidationError') {
          error.validation = error.originalError.getValidatorMessages();
        }

        if (process.env.NODE_ENV === 'production') {
          delete error.extensions;
        }
      } catch (e) {}

      return error;
    }
  }]);
  return GraphQLExceptions;
}();

var _default = GraphQLExceptions;
exports.default = _default;