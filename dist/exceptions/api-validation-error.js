"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _possibleConstructorReturn2 = _interopRequireDefault(require("@babel/runtime/helpers/possibleConstructorReturn"));

var _getPrototypeOf2 = _interopRequireDefault(require("@babel/runtime/helpers/getPrototypeOf"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime/helpers/inherits"));

var _wrapNativeSuper2 = _interopRequireDefault(require("@babel/runtime/helpers/wrapNativeSuper"));

var ApiValidationError =
/*#__PURE__*/
function (_Error) {
  (0, _inherits2.default)(ApiValidationError, _Error);

  function ApiValidationError(message) {
    var _this;

    (0, _classCallCheck2.default)(this, ApiValidationError);
    _this = (0, _possibleConstructorReturn2.default)(this, (0, _getPrototypeOf2.default)(ApiValidationError).call(this, message));
    _this.validator;
    _this.name = 'ApiValidationError';
    return _this;
  }

  (0, _createClass2.default)(ApiValidationError, [{
    key: "setValidator",
    value: function setValidator(validator) {
      this.validator = validator;
      return this;
    }
  }, {
    key: "getValidatorMessages",
    value: function getValidatorMessages() {
      return this.validator;
    }
  }]);
  return ApiValidationError;
}((0, _wrapNativeSuper2.default)(Error));

var _default = ApiValidationError;
exports.default = _default;