class Parent {
    constructor() {
    }

    get permission() {
        return false;
    }
}

class Nguyen extends Parent {
    permission = true;
}

const n = new Nguyen();

// n.permission = 1


console.log(n.permission);